# simple java image
FROM java:8-alpine

#define variable
ARG PROFILE
ENV PROFILE ${PROFILE}
ARG USER_NAME
ENV USER_NAME ${USER_NAME}
ARG JAR_NAME
ENV JAR_NAME ${JAR_NAME}
# copy distributable
RUN mkdir /gisett
RUN mkdir /gisett/gisett-main
RUN mkdir /gisett/gisett-docs
ADD gisett-backend/build/libs/${JAR_NAME} /gisett/gisett-main
#install git
RUN apk update && apk upgrade && apk add git
#install git-lfs
RUN apk --no-cache add openssl git curl \
    && curl -sLO https://github.com/github/git-lfs/releases/download/v2.0.1/git-lfs-linux-amd64-2.0.1.tar.gz \
    && tar zxvf git-lfs-linux-amd64-2.0.1.tar.gz \
    && mv git-lfs-2.0.1/git-lfs /usr/bin/ \
    && rm -rf git-lfs-2.0.1 \
    && rm -rf git-lfs-linux-amd64-2.0.1.tar.gz
RUN git lfs clone https://gitlab+deploy-token-1:eurycNQ_CEp6yoMzY5Km@src.codespring.ro/gisett/gisett-docs /gisett/gisett-docs

# go to bin of dist
WORKDIR /gisett/gisett-main

# expose webapp port
EXPOSE 18888

# execute app when starting image
CMD java -Dspring.profiles.active=${PROFILE} -Duser.name=${USER_NAME} -jar ${JAR_NAME}
