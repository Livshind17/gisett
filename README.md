﻿## Release notes

### v0.6.0

- POI selector on the map
- Informational POI popups when a marker is clicked on the map
- Property marker icons adjusted on the map
- Area grade information popup displayed when map is clicked
- Area grade information on property details page
- Property filtering
- City / Location selector in the main header changes map / poi / properties selection
- Property price selector issues fixed
- Property area marks displayed (the stars)
- Area indicator selector issues fixed
- Wizard 2nd screen SKIP functionality fixed

### v0.5.0

- Wizard screen now uses the max two selected indicator categories
- Changing the selected indicator groups will allow to update the heatmap accordingly
- Property detail view is available when user selects a property from the list
- Property filters added
- a predefined set of POI categories is considered to display POI s on the map
- POI clustering on the map
- Properties are displayed on the map
- Properties are clustered on the map
- When user clicks a property a minimal property information is displayed.
- Property information are coming from the server (not hardcoded informations)

### v0.4.0

- Heatmap component that is displaying indicator parameter information hardcoded for "Educational attainment" indicator. This component can be turned on or off.
- On the Area now we can see the list of indicator groups plus the ones the city has no data for (grayed out).
- Map display on the main view, currently using MapBox maps with a demo account.
- Labels in a translatable format
- A list of properties (the list is hardcoded - no backend response used)
- Price filter for properties, just a visible component, not affecting any functionality.

### v0.3.0

- Location selector component added in the header when the main view is displayed
- On the Area edit part of the application we can see the 4 draggable zones, and two demo buttons to try the draggable functionalities.
- Map display on the main view, positioned on the selected city.
- Indicator buttons are displayed properly on the wizard screen.
- Responsive design using flex.

### v0.2.0

- Wizard screen for selecting the city and the location according to the selected city
- Wizard screen for displaying the indicator categories
- Possibility to skip category selection and move to application main screen
- Display a map on the main screen and an empty drawer for further area and property operations
- importing process for cities, indicators, indicator categories
- REST API s for returning city, location and indicator information

## Guide to configure and use source formatter for backend in your IDE

- in directory gisett-tools/code-formatter you will find the two setting file used by Eclipse code formatter. Please configure your IDE to use them and ensure that before commit you have applied the rules in there.
- in case you are using IntelliJ IDEA, please use plugin called Eclipse Code Formatter, and in the Settings menu, apply the usage of it and set the two files to be used.
- if you are using IntelliJ, you need to setup to not collapse imports from same package. Please reffer the Settings > Editor > Code Style > Java menu and
  1.tick "Use single class import", 2. set 999 or some big number to "Class count to use import with '_'" 3. set 999 or some big number to "Names count to use static import with '_'"
- in case you are doing your commits from IntelliJ, on your commit window please ensure you have enabled: 1. Reformat code 2. Optimize imports 3. Perform code analysis 4. Check TODO
## Guide to setup InteliiJ to run/debug backend
Because we are using developer-properties in order to setup developer specific environment variables, and these parameters should be populated during build time by gradle in our config yml files, IDEA needs some additional configurations.  
When you configure you run settings, please ensure that you have the followings in the `Before launch` section:
- Run Gradle task: 'gisett-main:clean'
- Run Gradle task: 'gisett-main:processResources'
- Build
- Run Gradle task: 'gisett-main:updateResources'


## Integrating configuration metadata in **Intellij IDEA Enterprise Edition**

1. `File > Settings... > Build, Execution, Deployment > Compiler > Validation` 
Here check the **Validate on build** option. Make sure that all the validators are checked. 
1. `File > Project Structure... > Facets `

    If there is no facets click on the plus button to create a new *Spring facet*. 
    Select the current module. At the top of the 3rd panel click on the add button.
    Create an _additional properties file_, select the __application.properties__ 
    (application.yml) in the project then Apply.

1. Build the project.

1. The warnings and errors are listed in the build window. 
1. The IDE recommends to create the `additional-spring-configuration-metadata.json` in which can be defined 
specific constraints for each configuration variable. (eg. type, default value, predefined values).
1. In case of configuration changing: 
    - `$ gradle clean`  
    - `$ gradle build`  
    
    
Read more about setting up: 

1. https://jakubstas.com/configuration-meta-data-in-spring-boot/#.W6M4LugzaUl
1. https://jakubstas.com/advanced-config-with-configuration-meta-data-in-spring-boot/#.W6M4N-gzaUl

Read more about config file format:
1. https://docs.spring.io/spring-boot/docs/current/reference/html/configuration-metadata.html

## Guide to configure Prettier plugin in VSCode

Prettier is an opinionated code formatter. With Prettier you can format the code you write automatically to ensure a code style within your project. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary.

### Steps:

- call `npm install` to install the new devDependencies from the `package.json` file in web project (it is possible that you also have to install eslint and prettier globally by calling `npm install -g eslint prettier`)
- install the **ESLint** and **Prettier - Code formatter** extensions from VSCode Marketplace
- Prettier comes with a default configuration which probably won’t agree with the ESLint configuration that our project is using. Solution:
  1. Open the **User Settings** screen in VSCode (Cmd + , / Ctrl + ,)
  2. Set `prettier.eslintIntegration` to `true`
  3. set `formatOnSave` to `true` if you want Prettier to format your files whenever you save

**For more info:**

- https://medium.com/@pgivens/write-cleaner-code-using-prettier-and-eslint-in-vscode-d04f63805dcd
- https://www.futurehosting.com/blog/prettier-vs-eslint-whats-the-difference/
- https://prettier.io/docs/en/index.html

## Gitlab-ci and deployment

The gitlab-ci has 4 stages, so after every push a pipeline is started that:

- builds the code
- runs tests
- static code analysis (linters)
- deploys the application to the server (only included when the push was performed on the master or develop branches)

You can run the application by pulling the docker image from GitLab:

- run `docker login d.codespring.ro`
- run `docker pull d.codespring.ro/gisett/gisett-main:release-latest` | `docker pull d.codespring.ro/gisett/gisett-main:staging-latest`
- GO TO `gisett-main` folder that contains the `docker-compose.yml` file
- from _git bash_ run `tag=release-latest docker-compose up`| `tag=staging-latest docker-compose up`
- the app will be available on `localhost:18888/index.html` from your browser
- stop the running app with `docker-compose down --remove-orphans`

## Checkstyle and Findbugs

IntelliJ IDEA:

- Download and set `Checkstyle-IDEA and FindBugs-IDEA plugins`:
  - File->Settings->Plugins: download from repositories Checkstyle-IDEA and FindBugs-IDEA plugins -> Restart IDEA
  - File->Settings->Checkstyle : click to '+' then Use local checkstyle file and add `checkstyle.xml` from gisett-backend folder
- You can use them from IntelliJ IDEA or by running `gradle check -x test` command

## Internationalization at Frontend 
### react-i18next/ i18next - usage

   + Initialization in **index.js**

```ts
//need to import the respective translation files
   import common_ar from './translations/ar/common.json';
   import common_en from './translations/en/common.json';
```

```jsx
//initialize the i18next component
   i18next.init({
     fallbackLng: 'en',           //here we can define multiple fallback lng, which can depend on user language
     lng: 'ar',
     resources: {
        en: { common: common_en}, // 'common' is our custom namespace, and here can be defined more then one
        ar: { common: common_ar}  // namespace(ns), this lib can load multiple files. 
     },
     //here come the other configurations
   }); 
```

```jsx
//in ReactDom.render() we need to define the i18n Provider 
     <I18nextProvider i18n={i18next}>
        <App />
    </I18nextProvider>
```
   + For simple message: 
```jsx
    this.props.t('WIZARD.HELP_TEXT_1')
    
    //the "t" function is wrapped into the respective Component as seen below:
    export default translate('used_namespace')(MyComponent);
```
The aforementioned **namespace** provides the possibility to use the wanted translation file, which contains the needed *key-values*, independently from the used language.

   + For HTML message we use a special component:
```jsx
//at Component side
    <Trans i18nKey="WIZARD.FIND_PLACE" count={2}>
         Choose <b>{{number: 2}}</b> categories
    </Trans> 



//in translation file
{
  "WIZARD": {
      "FIND_PLACE": "Choose <1><0>{{number}}</0></1> category",
      "FIND_PLACE_plural": "Choose <1><0>{{number}}</0></1> categories"
  //...
  }
  //...
}
```
As seen in the translation file there are not mentioned the HTML tags but the respective *node numbers*,
 the placeholders are put in double curly brackets and there can be defined a plural version of 
 the message. The component decides which must be applied by giving a value for the **count** attribute.

For more information about the indenxing: https://react.i18next.com/misc/the-trans-component-explained     
Also there is a gitbook version: 
https://github.com/i18next/i18next-gitbook/blob/master/overview/getting-started.md
