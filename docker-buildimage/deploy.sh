# download & extract java
if [ ! -f "jdk-8u131-linux-x64.tar.gz" ]
then
  wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz
  tar -xf jdk-8u131-linux-x64.tar.gz
else
  echo "Java already downloaded"
fi

# download node.js & npm
if [ ! -f "node-v10.9.0-linux-x64.tar.gz" ]
then
  wget https://nodejs.org/dist/v10.9.0/node-v10.9.0-linux-x64.tar.gz
else
  echo "Node already downloaded"
fi

# download & extract gradle
if [ ! -f "gradle-4.10-bin.zip" ]
then
  wget https://services.gradle.org/distributions/gradle-4.10-bin.zip
  unzip gradle-4.10-bin.zip
else
  echo "Gradle already downloaded"
fi

# build Docker image (see Dockerfile for details)
docker build -t d.codespring.ro/gisett/gisett-main:build-image .

# push Docker image to edu registry
echo "Logging in to Docker registry"
winpty docker login d.codespring.ro
docker push d.codespring.ro/gisett/gisett-main:build-image
docker logout d.codespring.ro
