Integrating configuration metadata in **Intellij IDEA Enterprise Edition**


1. `File > Settings... > Build, Execution, Deployment > Compiler > Validation` 
Here check the **Validate on build** option. Make sure that all the validators are checked. 
1. `File > Project Structure... > Facets `

    If there is no facets click on the plus button to create a new *Spring facet*. 
    Select the current module. At the top of the 3rd panel click on the add button.
    Create an _additional properties file_, select the __application.properties__ 
    (application.yml) in the project then Apply.

1. Build the project.

1. The warnings and errors are listed in the build window. 
1. The IDE recommends to create the `additional-spring-configuration-metadata.json` in which can be defined 
specific constraints for each configuration variable. (eg. type, default value, predefined values).
1. In case of configuration changing: 
    - `$ gradle clean`  
    - `$ gradle build`  
    
    
Read more about setting up: 

1. https://jakubstas.com/configuration-meta-data-in-spring-boot/#.W6M4LugzaUl
1. https://jakubstas.com/advanced-config-with-configuration-meta-data-in-spring-boot/#.W6M4N-gzaUl

Read more about config file format:
1. https://docs.spring.io/spring-boot/docs/current/reference/html/configuration-metadata.html