package com.gisett;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import com.github.mongobee.Mongobee;

import com.gisett.mongobee.InitialChangelog;

/**
 * Created by juhasz.levente on 8/17/2018.
 */
@SpringBootApplication
public class GisApplication {

	@Value("${spring.data.mongodb.uri}")
	private String mongoURI;

	@Bean
	@Autowired
	public Mongobee mongobee(Environment environment) {
		Mongobee runner = new Mongobee(mongoURI);
		runner.setChangeLogsScanPackage(InitialChangelog.class.getPackage()
		                                                      .getName());
		runner.setSpringEnvironment(environment);
		runner.setEnabled(true);

		return runner;
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(GisApplication.class, args);
	}
}
