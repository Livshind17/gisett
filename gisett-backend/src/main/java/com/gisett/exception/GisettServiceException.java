package com.gisett.exception;

/**
 * Created by tuzes-boloni.kincso on 9/27/2018
 */
public class GisettServiceException extends RuntimeException {

	public GisettServiceException() {
		super();
	}

	public GisettServiceException(String message) {
		super(message);
	}

	public GisettServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
