package com.gisett.exception;

/**
 * Created by tuzes-boloni.kincso on 9/27/2018
 */
public class GisettRetrievalException extends RuntimeException {

	public GisettRetrievalException() {
		super();
	}

	public GisettRetrievalException(String message) {
		super(message);
	}

	public GisettRetrievalException(String message, Throwable cause) {
		super(message, cause);
	}
}
