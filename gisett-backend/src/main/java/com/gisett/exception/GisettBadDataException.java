package com.gisett.exception;

/**
 * Created by tuzes-boloni.kincso on 9/27/2018
 */
public class GisettBadDataException extends RuntimeException {

	public GisettBadDataException() {
		super();
	}

	public GisettBadDataException(String message) {
		super(message);
	}

	public GisettBadDataException(String message, Throwable cause) {
		super(message, cause);
	}
}
