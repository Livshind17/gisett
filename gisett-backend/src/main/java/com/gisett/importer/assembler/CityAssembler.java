package com.gisett.importer.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.CityFromConfig;
import com.gisett.configuration.importer.IndicatorFromConfig;
import com.gisett.domain.model.City;

@Component
public class CityAssembler implements GeneralAssembler<CityFromConfig, City> {

	@Autowired
	private IndicatorAssembler indicatorAssember;

	@Override
	public City createModel(CityFromConfig cityConfig) {
		if (cityConfig != null) {
			City city = new City();
			updateModel(city, cityConfig);
			return city;
		}
		return null;
	}

	@Override
	public City updateModel(City city, CityFromConfig cityConfig) {
		if (city != null && cityConfig != null) {
			city.setKey(cityConfig.getKey());
			city.setName(cityConfig.getCityName());
			city.setCoordinatesNW(cityConfig.getCoordinatesNW());
			city.setCoordinatesSE(cityConfig.getCoordinatesSE());
			if (cityConfig.getIndicators() != null) {
				List<IndicatorFromConfig> indicatorFromConfigs = cityConfig.getIndicators()
				                                                           .stream()
				                                                           .map(indicatorForCity -> ((IndicatorFromConfig) indicatorForCity))
				                                                           .collect(Collectors.toList());
				city.setIndicators(indicatorAssember.createModelList(indicatorFromConfigs));
			}
			if (cityConfig.getPoiTypes() != null) {
				city.setPoiTypes(cityConfig.getPoiTypes());
			}
		}
		return city;
	}

}
