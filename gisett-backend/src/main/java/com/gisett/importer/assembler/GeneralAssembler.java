package com.gisett.importer.assembler;

import java.util.List;
import java.util.stream.Collectors;

import com.gisett.configuration.importer.common.IConfig;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
public interface GeneralAssembler<Config extends IConfig, Model> {

	Model createModel(Config config);

	Model updateModel(Model model, Config config);

	default List<Model> createModelList(final List<Config> configs) {
		if (configs != null) {
			return configs.stream()
			              .filter(config -> !config.isSkip())
			              .map(this::createModel)
			              .collect(Collectors.toList());
		} else {
			return null;
		}
	}
}
