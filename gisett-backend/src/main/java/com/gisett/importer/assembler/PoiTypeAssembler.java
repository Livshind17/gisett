package com.gisett.importer.assembler;

import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.PoiTypeFromConfig;
import com.gisett.domain.model.PoiType;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

@Component
public class PoiTypeAssembler implements GeneralAssembler<PoiTypeFromConfig, PoiType> {

	@Override
	public PoiType createModel(PoiTypeFromConfig poiTypeFromConfig) {
		if (poiTypeFromConfig != null) {
			return new PoiType(poiTypeFromConfig.getKey(), poiTypeFromConfig.getTypeName());
		}
		return null;
	}

	@Override
	public PoiType updateModel(PoiType poiType, PoiTypeFromConfig poiTypeConfig) {
		if (poiType != null && poiTypeConfig != null) {
			poiType.setName(poiTypeConfig.getTypeName());
			poiType.setKey(poiTypeConfig.getKey());
		}
		return poiType;
	}
}
