package com.gisett.importer.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.IndicatorCategoryFromConfig;
import com.gisett.domain.model.IndicatorCategory;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
@Component
public class IndicatorCategoryAssembler implements GeneralAssembler<IndicatorCategoryFromConfig, IndicatorCategory> {
	@Autowired
	private IndicatorGroupAssembler indicatorAssembler;

	@Override
	public IndicatorCategory createModel(IndicatorCategoryFromConfig indicatorInfo) {
		if (indicatorInfo != null) {
			return new IndicatorCategory(indicatorInfo.getKey(), indicatorInfo.getCategoryName(),
			        indicatorAssembler.createModelList(indicatorInfo.getIndicatorGroups()));
		}
		return null;
	}

	@Override
	public IndicatorCategory updateModel(IndicatorCategory indicator, IndicatorCategoryFromConfig indicatorInfo) {
		if (indicator != null && indicatorInfo != null) {
			indicator.setCategory(indicatorInfo.getCategoryName());
			indicator.setKey(indicatorInfo.getKey());
			indicator.setIndicatorGroups(indicatorAssembler.createModelList(indicatorInfo.getIndicatorGroups()));
		}
		return indicator;
	}
}
