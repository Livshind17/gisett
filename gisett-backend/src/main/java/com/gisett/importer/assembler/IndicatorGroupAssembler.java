package com.gisett.importer.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.IndicatorGroupFromConfig;
import com.gisett.domain.model.IndicatorGroup;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
@Component
public class IndicatorGroupAssembler implements GeneralAssembler<IndicatorGroupFromConfig, IndicatorGroup> {
	@Autowired
	private IndicatorAssembler indicatorAssembler;

	@Override
	public IndicatorGroup createModel(IndicatorGroupFromConfig indicatorInfo) {
		if (indicatorInfo != null) {
			return new IndicatorGroup(indicatorInfo.getKey(), indicatorInfo.getGroupName(),
			        indicatorAssembler.createModelList(indicatorInfo.getIndicators()));
		}
		return null;
	}

	@Override
	public IndicatorGroup updateModel(IndicatorGroup indicator, IndicatorGroupFromConfig indicatorInfo) {
		if (indicator != null && indicatorInfo != null) {
			indicator.setGroupName(indicatorInfo.getGroupName());
			indicator.setKey(indicatorInfo.getKey());
			indicator.setIndicators(indicatorAssembler.createModelList(indicatorInfo.getIndicators()));
		}
		return indicator;
	}
}
