package com.gisett.importer.assembler;

import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.IndicatorFromConfig;
import com.gisett.domain.model.Indicator;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
@Component
public class IndicatorAssembler implements GeneralAssembler<IndicatorFromConfig, Indicator> {

	@Override
	public Indicator createModel(IndicatorFromConfig indicatorConfig) {
		if (indicatorConfig != null) {
			Indicator indicator = new Indicator();
			return updateModel(indicator, indicatorConfig);
		}
		return null;
	}

	@Override
	public Indicator updateModel(com.gisett.domain.model.Indicator indicator, IndicatorFromConfig indicatorInfo) {
		if (indicator != null && indicatorInfo != null) {
			indicator.setName(indicatorInfo.getIndicatorName());
			indicator.setKey(indicatorInfo.getKey());
			indicator.setMin(indicatorInfo.getMin());
			indicator.setMax(indicatorInfo.getMax());
			indicator.setIsHighValueBetter(indicatorInfo.getIsHighValueBetter());
		}
		return indicator;
	}
}
