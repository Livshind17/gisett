package com.gisett.importer.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gisett.configuration.importer.PoiCategoriesFromConfig;
import com.gisett.domain.model.PoiCategory;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

@Component
public class PoiCategoryAssembler implements GeneralAssembler<PoiCategoriesFromConfig, PoiCategory> {

	@Autowired
	private PoiTypeAssembler poiTypeAssembler;

	@Override
	public PoiCategory createModel(PoiCategoriesFromConfig poiCategoriesFromConfig) {
		if (poiCategoriesFromConfig != null) {
			return new PoiCategory(poiCategoriesFromConfig.getKey(), poiCategoriesFromConfig.getCategoryName(),
			        poiTypeAssembler.createModelList(poiCategoriesFromConfig.getPoiTypes()));
		}
		return null;
	}

	@Override
	public PoiCategory updateModel(PoiCategory poi, PoiCategoriesFromConfig poiCategoriesFromConfig) {
		if (poi != null && poiCategoriesFromConfig != null) {
			poi.setName(poiCategoriesFromConfig.getCategoryName());
			poi.setKey(poiCategoriesFromConfig.getKey());
			poi.setPoiTypes(poiTypeAssembler.createModelList(poiCategoriesFromConfig.getPoiTypes()));
		}
		return poi;
	}
}
