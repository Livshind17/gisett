package com.gisett.importer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.CityFromConfig;
import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.domain.model.City;
import com.gisett.domain.service.CityService;
import com.gisett.importer.assembler.CityAssembler;

/**
 * Created by juhasz.levente on 8/20/2018.
 */
@Service
public class CityImporter {

	Logger logger = LoggerFactory.getLogger(CityImporter.class);

	@Autowired
	CityAssembler cityAssembler;
	@Autowired
	LocationImporter locationImporter;
	@Autowired
	IndicatorImporter indicatorImporter;
	@Autowired
	PoiImporter poiImporter;
	@Autowired
	PropertyImporter propertyImporter;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@Autowired
	private CityService cityService;

	public void importCities() {
		if (gisettImporterConfig.getCities() == null) {
			logger.info("No citites configured for the importer.");
			return;
		}
		gisettImporterConfig.getCities()
		                    .forEach(city -> {
			                    saveCity(city);
			                    importLocations(city);
			                    importIndicators(city);
			                    importPois(city);
			                    importProperties(city);
		                    });
	}

	private void importLocations(CityFromConfig cityFromConfig) {
		try {
			if (StringUtils.isNotEmpty(cityFromConfig.getLocations())) {
				locationImporter.importLocations(cityFromConfig.getKey(), cityFromConfig.getLocations());
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to import %s's location points!. ", cityFromConfig.getCityName()), e);
		}
	}

	private void importIndicators(CityFromConfig cityFromConfig) {
		try {
			// @formatter:off
			if (cityFromConfig.getIndicators() != null && !cityFromConfig.getIndicators().isEmpty()) {
			// @formatter:on
				indicatorImporter.importIndicators(cityFromConfig);
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to import %s's indicator points!. ", cityFromConfig.getCityName()), e);
		}
	}

	private void importPois(CityFromConfig cityFromConfig) {
		try {
			// @formatter:off
			if (cityFromConfig.getPoiTypes() != null && !cityFromConfig.getPois().isEmpty()) {
			// @formatter:on
				poiImporter.importPois(cityFromConfig.getKey(), cityFromConfig.getPois());
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to import %s's poi points!. ", cityFromConfig.getCityName()), e);
		}
	}

	private void importProperties(CityFromConfig cityFromConfig) {
		try {
			if (StringUtils.isNotEmpty(cityFromConfig.getProperties())) {
				propertyImporter.importProperties(cityFromConfig.getKey(), cityFromConfig.getProperties());
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to import %s's properties!. ", cityFromConfig.getCityName()), e);
		}
	}

	private void saveCity(CityFromConfig cityConfig) {
		City city = cityAssembler.createModel(cityConfig);
		cityService.save(city);
	}

}
