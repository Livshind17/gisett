package com.gisett.importer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by juhasz.levente on 8/20/2018.
 */
@Service
public class GisettImporter {

	@Autowired
	CityImporter cityImporter;
	@Autowired
	IndicatorConfigImporter indicatorConfigImporter;
	@Autowired
	PoiConfigImporter poiConfigImporter;

	public void importGisett() {
		indicatorConfigImporter.importIndicatorConfigs();
		poiConfigImporter.importPoiConfigs();
		cityImporter.importCities();
	}

}
