package com.gisett.importer;

import java.io.File;
import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.domain.model.Location;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.LocationService;

/**
 * Created by juhasz.levente on 8/20/2018.
 */
@Service
public class LocationImporter {

	Logger logger = LoggerFactory.getLogger(LocationImporter.class);

	@Autowired
	private GisettImporterConfig gisettImporterConfig;

	@Autowired
	LocationService service;

	public void importLocations(String cityKey, String importFileName) {
		JSONParser parser = new JSONParser();
		try {
			String filePath = getLocationImportFolder(cityKey) + "/" + importFileName;
			File importFile = new File(filePath);
			if (importFile.exists()) {
				Object obj = parser.parse(new FileReader(importFile));
				JSONObject jsonObject = (JSONObject) obj;
				JSONArray features = (JSONArray) jsonObject.get("features");
				features.forEach(o -> {
					saveLocation(cityKey, (JSONObject) o);
				});
			} else {
				logger.error(String.format("Failed to import locations for city %s! Err: location file does not exist: %s.", cityKey, filePath));
			}
		} catch (Exception e) {
			logger.error("Failed to import locations! Err: ", e);
		}
	}

	private void saveLocation(String cityKey, JSONObject locationJSON) {
		JSONObject attributes = (JSONObject) locationJSON.get("attributes");
		JSONObject geometry = (JSONObject) locationJSON.get("geometry");

		String name = (String) attributes.get("NAME");
		Long fid = (Long) attributes.get("FID");
		Double x = (Double) geometry.get("x");
		Double y = (Double) geometry.get("y");
		Coordinates coordinates = new Coordinates(y, x);

		service.save(new Location(fid, name, cityKey, coordinates));
	}

	private String getLocationImportFolder(String cityKey) {
		return gisettImporterConfig.getDataImportLocationPath() + "/" + cityKey.toLowerCase() + "/locations";
	}
}
