package com.gisett.importer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.domain.model.Address;
import com.gisett.domain.model.Agency;
import com.gisett.domain.model.Amenity;
import com.gisett.domain.model.Category;
import com.gisett.domain.model.HouseType;
import com.gisett.domain.model.Listing;
import com.gisett.domain.model.Property;
import com.gisett.domain.service.IndicatorPointService;
import com.gisett.domain.service.PropertyService;
import com.gisett.exception.GisettRetrievalException;

/**
 * Created by tuzes-boloni.kincso on 10/17/2018
 */

@Service
public class PropertyImporter {

	Logger logger = LoggerFactory.getLogger(PropertyImporter.class);

	@Autowired
	private GisettImporterConfig gisettImporterConfig;

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private IndicatorPointService indicatorPointService;

	public void importProperties(String cityKey, String importFileName) {
		JSONParser parser = new JSONParser();
		logger.info(String.format("Importing properties for city %s", cityKey));
		String filePath = getLocationImportFolder(cityKey) + "/" + importFileName;
		try {
			File importFile = new File(filePath);
			if (importFile.exists()) {
				Object obj = parser.parse(new FileReader(importFile));
				JSONObject jsonObject = (JSONObject) obj;
				JSONArray features = (JSONArray) jsonObject.get("features");
				IntStream stream = features.stream()
				                           .mapToInt(o -> {
					                           try {
						                           return saveProperty(cityKey, (JSONObject) o) != null ? 0 : 1;
					                           } catch (Exception e) {
						                           JSONObject properties = (JSONObject) ((JSONObject) o).get("properties");
						                           logger.error(String.format("Failed to save property cityKey: %s FID: %d", cityKey, properties.get("FID")),
						                                   e);
						                           return 1;
					                           }
				                           });
				long total = features.size();
				long count = stream.sum();
				logger.info(String.format(
				        "Number of failed property imports is %d from total of %d for city %s. Location file: %s",
				        count, total, cityKey, filePath));
			} else {
				logger.error(
				        String.format("Failed to import properties for city %s! Err: location file does not exist: %s.",
				                cityKey, filePath));
			}
		} catch (IOException | ParseException e) {
			logger.error(String.format(
			        "Failed to import properties for city %s! Err: can not read / can not parse location file: %s.",
			        cityKey, filePath));
		} catch (Exception e) {
			logger.error("Failed to import properties for city " + cityKey + "! Err: ", e);
		}
	}

	private Property saveProperty(String cityKey, JSONObject locationJSON) {
		JSONObject properties = (JSONObject) locationJSON.get("properties");
		JSONObject geometry = (JSONObject) locationJSON.get("geometry");

		Long fid = (Long) properties.get("FID");
		Long objectid = (Long) properties.get("OBJECTID");
		Address address = getAddress(properties);
		Integer bedrooms = getIntegerValueOfProperty(properties, "bedrooms");
		Integer yearBuilt = getIntegerValueOfProperty(properties, "year_built");
		String description = (String) properties.get("description");
		List<String> photos = (List<String>) properties.get("photos");
		Integer bathrooms = getIntegerValueOfProperty(properties, "bathrooms");
		Date dateUpdate = new Date((Long) properties.get("date_update"));
		// category
		String categoryValue = (String) properties.get("category");
		Category category = null;
		if (categoryValue.equals("for_sale")) {
			category = Category.SELL;
		}
		if (categoryValue.equals("rent")) {
			category = Category.RENT;
		}
		// houseType
		HouseType houseType = null;
		Long houseTypeValue = (Long) properties.get("type");
		if (houseTypeValue != null) {
			houseType = HouseType.valueOf((int) (long) houseTypeValue);
		}
		// listing
		Listing listing = null;
		Long listingValue = (Long) properties.get("listing");
		if (listingValue != null) {
			listing = Listing.valueOf((int) (long) listingValue);
		}

		Integer lotSurface = getIntegerValueOfProperty(properties, "lot_surface");
		Integer builtSurface = getIntegerValueOfProperty(properties, "built_surface");
		Integer floor = getIntegerValueOfProperty(properties, "floor");
		Double price = (Double) properties.get("price");
		String currency = (String) properties.get("currency");
		Agency agency = getAgency(properties);
		Map<Amenity, Boolean> amenities = getAmenities(properties);
		List<Double> coords = (List<Double>) geometry.get("coordinates");
		GeoJsonPoint coordinates = new GeoJsonPoint(coords.get(0), coords.get(1));

		Property property = new Property(objectid, fid, cityKey, dateUpdate, address, coordinates, agency, bedrooms,
		        bathrooms, lotSurface, builtSurface, floor, yearBuilt, description, photos, price, currency, houseType,
		        category, amenities, listing);
		try {
			property.setNearestIndicatorPoint(
			        indicatorPointService.nearestByPointWithRangeConvert(coordinates.getX(), coordinates.getY(), cityKey));
		} catch (GisettRetrievalException gre) {
			logger.error(String.format("Not found indicator information for property cityKey %s FID %d", cityKey, fid));
		}
		return propertyService.save(property);
	}

	private Integer getIntegerValueOfProperty(JSONObject properties, String propertyName) {
		Long propertyValue = (Long) properties.get(propertyName);
		if (propertyValue != null) {
			return (int) (long) propertyValue;
		}
		return null;
	}

	private Address getAddress(JSONObject properties) {
		String addrLine1 = (String) properties.get("address_line_1");
		String addrLine2 = (String) properties.get("address_line_2");
		String city = (String) properties.get("city");
		String state = (String) properties.get("state");
		String country = (String) properties.get("country");
		String zip = (String) properties.get("zip");
		return new Address(addrLine1, addrLine2, city, state, country, zip);
	}

	private Agency getAgency(JSONObject properties) {
		List<String> landlinePhones = (List<String>) properties.get("landline_phones");
		List<String> mobilePhones = (List<String>) properties.get("mobile_phones");
		List<String> emails = (List<String>) properties.get("emails");
		String agencyName = (String) properties.get("agency_name");
		String agencyContact = (String) properties.get("agency_contact");
		String agencyUrl = (String) properties.get("agency_url");
		return new Agency(agencyName, agencyContact, agencyUrl, landlinePhones, mobilePhones, emails);
	}

	private Map<Amenity, Boolean> getAmenities(JSONObject properties) {
		Map<Amenity, Boolean> amenities = new HashMap<>();
		putAmenity(properties, amenities, "air_condition", Amenity.AIR_CONDITION);
		putAmenity(properties, amenities, "balcony", Amenity.BALCONY);
		putAmenity(properties, amenities, "basement", Amenity.BASEMENT);
		putAmenity(properties, amenities, "bbq_area", Amenity.BBQ_AREA);
		putAmenity(properties, amenities, "dishwash", Amenity.DISHWASHER);
		putAmenity(properties, amenities, "doorman", Amenity.DOORMAN);
		putAmenity(properties, amenities, "dryer", Amenity.DRYER);
		putAmenity(properties, amenities, "elevator", Amenity.ELEVATOR);
		putAmenity(properties, amenities, "fireplace", Amenity.FIREPLACE);
		putAmenity(properties, amenities, "furnished", Amenity.FURNISHED);
		putAmenity(properties, amenities, "garden", Amenity.GARDEN);
		putAmenity(properties, amenities, "gym", Amenity.GYM);
		putAmenity(properties, amenities, "heating", Amenity.HEATING);
		putAmenity(properties, amenities, "laundry", Amenity.LAUNDRY);
		putAmenity(properties, amenities, "parking", Amenity.PARKING);
		putAmenity(properties, amenities, "swimming_pool", Amenity.SWIMMING_POOL);
		putAmenity(properties, amenities, "washer", Amenity.WASHER);
		putAmenity(properties, amenities, "wheelchair_access", Amenity.WHEELCHAIR_ACCESS);
		putAmenity(properties, amenities, "pet_allow_cat", Amenity.PET_ALLOW_CAT);
		putAmenity(properties, amenities, "pet_allow_dog", Amenity.PET_ALLOW_DOG);
		return amenities;
	}

	private void putAmenity(JSONObject properties, Map<Amenity, Boolean> amenityMap, String amenityName, Amenity mapKey) {
		Boolean amenityValue = (Boolean) properties.get(amenityName);
		if (amenityValue != null) {
			amenityMap.put(mapKey, amenityValue);
		}
	}

	private String getLocationImportFolder(String cityKey) {
		return gisettImporterConfig.getDataImportLocationPath() + "/" + cityKey.toLowerCase() + "/properties";
	}
}
