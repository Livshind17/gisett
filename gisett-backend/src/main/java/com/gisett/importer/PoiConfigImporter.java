package com.gisett.importer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.PoiCategoriesFromConfig;
import com.gisett.domain.model.PoiCategory;
import com.gisett.domain.service.PoiCategoryService;
import com.gisett.importer.assembler.PoiCategoryAssembler;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

@Service
public class PoiConfigImporter {

	private Logger logger = LoggerFactory.getLogger(PoiConfigImporter.class);

	@Autowired
	private PoiCategoryService poiConfigService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@Autowired
	private PoiCategoryAssembler assembler;

	public void importPoiConfigs() {
		if (gisettImporterConfig.getPoiConfigs() == null) {
			logger.info("No POI configurations found for the importer.");
			return;
		}

		gisettImporterConfig.getPoiConfigs()
		                    .forEach(poiCategory -> {
			                    savePoiConfig(poiCategory);
		                    });
	}

	private PoiCategory savePoiConfig(PoiCategoriesFromConfig poiCategoriesFromConfig) {
		return poiConfigService.save(assembler.createModel(poiCategoriesFromConfig));
	}
}
