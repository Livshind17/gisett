package com.gisett.importer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonMultiLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonMultiPolygon;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.domain.model.Poi;
import com.gisett.domain.model.PoiLine;
import com.gisett.domain.model.PoiPoint;
import com.gisett.domain.model.PoiPolygon;
import com.gisett.domain.service.PoiService;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

@Service
public class PoiImporter {

	Logger logger = LoggerFactory.getLogger(PoiImporter.class);

	@Autowired
	private GisettImporterConfig gisettImporterConfig;

	@Autowired
	private PoiService service;

	public void importPois(String cityKey, List<String> importFileNames) {
		JSONParser parser = new JSONParser();
		logger.info(String.format("Importing POIs for city %s", cityKey));
		String folderPath = getLocationImportFolder(cityKey) + "/";
		importFileNames.forEach(fileName -> {
			String filePath = folderPath + fileName;
			try {

				logger.info(String.format("Importing POIs for city %s. Location file: %s", cityKey, fileName));

				File importFile = new File(filePath);
				if (importFile.exists()) {
					Object obj = parser.parse(new FileReader(importFile));
					JSONObject jsonObject = (JSONObject) obj;
					JSONArray features = (JSONArray) jsonObject.get("features");
					IntStream stream = features.stream()
					                           .mapToInt(o -> savePoi(cityKey, (JSONObject) o) != null ? 0 : 1);
					long total = features.size();
					long count = stream.sum();
					logger.info(String.format(
					        "Number of failed POI imports is %d from total of %d for city %s. Location file: %s", count,
					        total, cityKey, filePath));
				} else {
					logger.error(
					        String.format("Failed to import pois for city %s! Err: location file does not exist: %s.",
					                cityKey, filePath));
				}
			} catch (IOException | ParseException e) {
				logger.error(String.format(
				        "Failed to import pois for city %s! Err: can not read / can not parse location file: %s.",
				        cityKey, filePath));
			} catch (Exception e) {
				logger.error("Failed to import pois for city " + cityKey + "! Err: ", e);
			}

		});
	}

	private Poi savePoi(String cityKey, JSONObject locationJSON) {
		JSONObject attributes = (JSONObject) locationJSON.get("attributes");
		Long fID = (Long) attributes.get("FID");
		String type = (String) attributes.get("type");
		String description = (String) attributes.get("desc_");
		JSONObject geometry = (JSONObject) locationJSON.get("geometry");

		Poi importedPoi = null;
		try {
			if (geometry.get("x") != null) {
				importedPoi = savePoiPoints(new PoiPoint(fID, cityKey, type, description), geometry);
			} else {
				if (geometry.get("paths") != null) {
					importedPoi = savePoiLines(new PoiLine(fID, cityKey, type, description), geometry);
				} else {
					importedPoi = savePoiPolygons(new PoiPolygon(fID, cityKey, type, description), geometry);
				}
			}
			logger.debug(String.format("%s: Import POI: FID= %d City= %s type=%s name= %s",
			        importedPoi != null ? "Success" : "Error", fID, cityKey, type, description));
			return importedPoi;
		} catch (GisettBadDataException e) {
			logger.error(String.format("Error: Import POI: FID= %d City= %s type=%s name= %s cause: %s", fID,
			        cityKey, type, description, e.getMessage()));
			return null;
		}
	}

	private Poi savePoiPoints(PoiPoint poiPoint, JSONObject geometry) {
		Double x = (Double) geometry.get("x");
		Double y = (Double) geometry.get("y");
		GeoJsonPoint coordinates = new GeoJsonPoint(x, y);
		poiPoint.setCoordinates(coordinates);
		if (gisettImporterConfig.getCheckPoiDuplicates()) {
			List<Poi> pois = service.findPoisNearPointByTypes(poiPoint.getCityKey(), x, y, 15, Arrays.asList(poiPoint.getType()));
			if (!pois.isEmpty()) {
				logger.info(String.format("Duplicates for %s:", poiPoint.toString()));
				pois.forEach(poi -> logger.info(String.format("\t* %s", poi.toString())));
			}
		}
		return service.save(poiPoint);
	}

	private Poi savePoiLines(PoiLine poiLine, JSONObject geometry) {
		List<GeoJsonLineString> lines = new ArrayList<GeoJsonLineString>();
		((JSONArray) geometry.get("paths")).forEach(pathItem -> {
			List<Point> linePoints = new ArrayList<>();
			((JSONArray) pathItem).forEach(linePoint -> {
				linePoints.add(
				        new Point((double) ((JSONArray) linePoint).get(0), (double) ((JSONArray) linePoint).get(1)));
			});
			lines.add(new GeoJsonLineString(linePoints));
		});
		GeoJsonMultiLineString coordinates = new GeoJsonMultiLineString(lines);
		poiLine.setCoordinates(coordinates);
		return service.save(poiLine);
	}

	private Poi savePoiPolygons(PoiPolygon poiPolygon, JSONObject geometry) {
		List<GeoJsonPolygon> polygons = new ArrayList<GeoJsonPolygon>();
		((JSONArray) geometry.get("rings")).forEach(ringItem -> {
			List<Point> pointList = new ArrayList<>();
			((JSONArray) ringItem).forEach(polygonPoints -> {
				pointList.add(new Point((double) ((JSONArray) polygonPoints).get(0),
				        (double) ((JSONArray) polygonPoints).get(1)));
			});
			polygons.add(new GeoJsonPolygon(pointList));
		});
		GeoJsonMultiPolygon coordinates = new GeoJsonMultiPolygon(polygons);
		poiPolygon.setCoordinates(coordinates);
		return service.save(poiPolygon);
	}

	private String getLocationImportFolder(String cityKey) {
		return gisettImporterConfig.getDataImportLocationPath() + "/" + cityKey.toLowerCase() + "/pois";
	}
}
