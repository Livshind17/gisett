package com.gisett.importer;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.CityFromConfig;
import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.IndicatorCategoryFromConfig;
import com.gisett.configuration.importer.IndicatorFromConfig;
import com.gisett.configuration.importer.IndicatorGroupFromConfig;
import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.service.IndicatorPointService;
import com.gisett.util.ExportFileBuilder;
import com.gisett.util.ImportFileProcessor;
import com.gisett.util.PointCalculator;

/**
 * Created by kintzel.levente on 7/2/2018.
 */
@Service
public class IndicatorImporter {

	private Logger logger = LoggerFactory.getLogger(IndicatorImporter.class);

	@Autowired
	private IndicatorPointService indicatorPointService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;

	private ImportFileProcessor importFileProcessor = new ImportFileProcessor();
	private ExportFileBuilder exportFileBuilder = new ExportFileBuilder();
	private PointCalculator pointCalculator = new PointCalculator();
	private String cityKey;

	public IndicatorImporter() throws Exception {
	}

	public void setCityKey(String cityKey) {
		this.cityKey = cityKey;
	}

	public void importIndicators(CityFromConfig cityFromConfig) throws Exception {
		pointCalculator.setViewPort(cityFromConfig.getCoordinatesNW()
		                                          .getLatitude(),
		        cityFromConfig.getCoordinatesNW()
		                      .getLongitude(),
		        cityFromConfig.getCoordinatesSE()
		                      .getLatitude(),
		        cityFromConfig.getCoordinatesSE()
		                      .getLongitude());

		setCityKey(cityFromConfig.getKey());

		logger.info("Importing indicators for city:" + cityKey);
		Boolean exportIndicatorsEnabled = gisettImporterConfig.getExportIndicatorsEnabled();
		logger.info(String.format("IndicatorFromConfig export %s", exportIndicatorsEnabled ? "enabled" : "disabled"));
		if (exportIndicatorsEnabled) {
			File exportFolder = new File(gisettImporterConfig.getDataExportLocationPath());
			if (!exportFolder.exists()) {
				exportIndicatorsEnabled = false;
				logger.error(String.format(
				        "Failed to export indicators for city: %s! Err: export folder does not exist: %s.",
				        cityKey, gisettImporterConfig.getDataExportLocationPath()));
			}
		}
		final Boolean exportEnabled = exportIndicatorsEnabled;

		cityFromConfig.getIndicators()
		              .stream()
		              .filter(indicator -> !indicator.isSkip())
		              .forEach(indicator -> {
			              try {
				              logger.info(String.format("Importing indicators for city: %s indicator: %s input: %s",
				                      cityKey, indicator.getKey(), indicator.getInput()));
				              File importFile = getFile(indicator.getInput(), getTifImportPath(cityKey));
				              if (importFile.exists()) {
					              File geoJSONFile = exportEnabled ? getFile(indicator.getOutput(), getTifExportPath(cityKey)) : null;
					              GridCoverage2D image = importFileProcessor.extract(importFile);

					              Double minValue = indicator.getMin();
					              Double maxValue = indicator.getMax();
					              if (minValue == null || maxValue == null) {
						              Optional<IndicatorFromConfig> configIndicator = getIndicator(indicator.getKey(),
						                      gisettImporterConfig);
						              if (configIndicator.isPresent()) {
							              minValue = minValue != null ? minValue
							                      : configIndicator.get()
							                                       .getMin();
							              maxValue = maxValue != null ? maxValue
							                      : configIndicator.get()
							                                       .getMax();
						              }
					              }

					              int importedPointsNo = readImage(image, geoJSONFile, indicator.getKey(), minValue, maxValue);
					              logger.info(String.format(
					                      "Successfully imported indicators for city: %s indicator: %s. Imported points no: %d",
					                      cityKey, indicator.getKey(), importedPointsNo));
				              } else {
					              logger.error(String.format(
					                      "Failed to import indicators for city: %s indicator: %s! Err: location file does not exist: %s.",
					                      cityKey, indicator.getKey(), indicator.getInput()));
				              }
			              } catch (Exception e) {
				              logger.error(
				                      String.format("Failed to import indicators for city: %s indicator: %s input: %s!",
				                              cityKey, indicator.getKey(), indicator.getInput()),
				                      e);
			              }
		              });
	}

	private Optional<IndicatorFromConfig> getIndicator(String key, GisettImporterConfig config) {
		return gisettImporterConfig.getIndicatorConfigs()
		                           .stream()
		                           .map(IndicatorCategoryFromConfig::getIndicatorGroups)
		                           .flatMap(Collection::stream)
		                           .map(IndicatorGroupFromConfig::getIndicators)
		                           .flatMap(Collection::stream)
		                           .filter(ind -> key.equals(ind.getKey()))
		                           .findFirst();
	}

	private int readImage(GridCoverage2D image, File output, String indicatorName, double minValue, double maxValue)
	        throws Exception {

		FileWriter fw = output != null ? exportFileBuilder.initGeoJSONFile(output, indicatorName) : null;

		Envelope2D rasterEnvelope = image.getEnvelope2D();

		DirectPosition2D cursor = new DirectPosition2D(pointCalculator.getNorthWestCornerX(),
		        pointCalculator.getNorthWestCornerY());
		DirectPosition2D longCursor = new DirectPosition2D(pointCalculator.getNorthWestCornerX(),
		        pointCalculator.getNorthWestCornerY());

		int count = 0;
		int i = 0;

		while (cursor.getY() >= pointCalculator.getViewsMinY()) {

			i++;
			int j = 0;

			while (cursor.getX() <= pointCalculator.getViewsMaxX()) {

				Point2D destination = pointCalculator.getPointToEast(cursor);
				cursor = new DirectPosition2D(destination);

				if (rasterEnvelope.contains(destination)) {

					j++;

					double vals[] = new double[1];
					image.evaluate(destination, vals);

					if (vals[0] <= maxValue && vals[0] >= minValue) {

						count++;
						logger.debug(cityKey + "(" + indicatorName + ":" + count + ")(" + i + "," + j + ").["
						        + cursor.getY() + "," + cursor.getX() + "]=(" + vals[0] + ")");

						if (output != null) {
							exportFileBuilder.writePointToGeoJSON(cursor, vals[0], count, fw);
						}
						saveIndicatorPoint(indicatorName, cursor, vals[0], count);
					}
				}
			}

			Point2D longDestination = pointCalculator.getPointToSouth(longCursor);
			longCursor = new DirectPosition2D(longDestination);
			cursor = new DirectPosition2D(longDestination);
		}

		if (output != null) {
			exportFileBuilder.closeGeoJSONFile(fw);
		}

		return count;
	}

	private String getTifImportPath(String cityKey) {
		return gisettImporterConfig.getDataImportLocationPath() + "/" + cityKey.toLowerCase() + "/indicators/";
	}

	private String getTifExportPath(String cityKey) {
		return gisettImporterConfig.getDataExportLocationPath() + "/" + cityKey.toLowerCase() + "/indicators/";
	}

	private File getFile(String fileName, String path) throws Exception {
		return new File(path + fileName);
	}

	private void saveIndicatorPoint(String imageName, DirectPosition2D cursor, double value, long pointId)
	        throws Exception {
		IndicatorPoint rp = loadExistingPoint(cursor);
		if (rp == null) {
			rp = new IndicatorPoint();
			rp.setId(imageName + pointId);
			rp.setCityKey(cityKey);
			rp.setValue(new HashMap<>());
			rp.setPoint(new GeoJsonPoint(cursor.getX(), cursor.getY()));
		}
		rp.getValue()
		  .put(imageName, value);
		indicatorPointService.save(rp);
	}

	private IndicatorPoint loadExistingPoint(DirectPosition2D point) {
		List<IndicatorPoint> points = indicatorPointService.nearByPoints(point.getX(), point.getY(), 5);
		return !points.isEmpty() ? points.iterator()
		                                 .next()
		        : null;
	}
}
