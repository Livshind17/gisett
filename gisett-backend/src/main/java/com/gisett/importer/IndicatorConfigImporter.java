package com.gisett.importer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.IndicatorCategoryFromConfig;
import com.gisett.domain.model.IndicatorCategory;
import com.gisett.domain.service.IndicatorCategoryService;
import com.gisett.importer.assembler.IndicatorCategoryAssembler;

/**
 * Created by Bartha.Vivien on 8/29/2018.
 */
@Service
public class IndicatorConfigImporter {
	private Logger logger = LoggerFactory.getLogger(IndicatorConfigImporter.class);

	@Autowired
	private IndicatorCategoryService indicatorConfigService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@Autowired
	private IndicatorCategoryAssembler assembler;

	public void importIndicatorConfigs() {
		if (gisettImporterConfig.getIndicatorConfigs() == null) {
			logger.info("No indicators configured for the importer.");
			return;
		}

		gisettImporterConfig.getIndicatorConfigs()
		                    .forEach(indicatorCategory -> {
			                    saveIndicatorConfigs(indicatorCategory);
		                    });
	}

	private IndicatorCategory saveIndicatorConfigs(IndicatorCategoryFromConfig indicatorCategoryFromConfig) {
		return indicatorConfigService.save(assembler.createModel(indicatorCategoryFromConfig));
	}

}
