package com.gisett.mongobee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;

import com.gisett.domain.repository.CityRepository;
import com.gisett.domain.repository.IndicatorCategoryRepository;
import com.gisett.domain.repository.IndicatorPointRepository;
import com.gisett.domain.repository.LocationRepository;
import com.gisett.domain.repository.PoiCategoryRepository;
import com.gisett.domain.repository.PoiRepository;
import com.gisett.domain.repository.PropertyRepository;
import com.gisett.importer.GisettImporter;

/**
 * Created by kintzel.levente on 8/24/2018.
 */
@Component
@ChangeLog(order = "001")
public class InitialChangelog {

	@Bean
	public InitialChangelog getInitialChangeLog(ApplicationContext context) {
		applicationContext = context;
		return this;
	}

	@Autowired
	private static ApplicationContext applicationContext;

	@Autowired
	private GisettImporter gisImporter;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private IndicatorPointRepository indicatorPointRepository;

	@Autowired
	private IndicatorCategoryRepository indicatorCategoryRepository;

	@Autowired
	private PoiCategoryRepository poiCategoryRepository;

	@Autowired
	private PoiRepository poiRepository;

	@Autowired
	private PropertyRepository propertyRepository;

	@ChangeSet(order = "001", id = "dbImport_20181016", author = "Bartha.Vivien")
	public void initDatabase() {
		clearData();
		gisImporter = applicationContext.getBean(GisettImporter.class);
		gisImporter.importGisett();
	}

	protected void clearData() {
		cityRepository = applicationContext.getBean(CityRepository.class);
		locationRepository = applicationContext.getBean(LocationRepository.class);
		indicatorPointRepository = applicationContext.getBean(IndicatorPointRepository.class);
		indicatorCategoryRepository = applicationContext.getBean(IndicatorCategoryRepository.class);
		poiCategoryRepository = applicationContext.getBean(PoiCategoryRepository.class);
		poiRepository = applicationContext.getBean(PoiRepository.class);
		propertyRepository = applicationContext.getBean(PropertyRepository.class);

		cityRepository.deleteAll();
		locationRepository.deleteAll();
		indicatorPointRepository.deleteAll();
		indicatorCategoryRepository.deleteAll();
		poiCategoryRepository.deleteAll();
		poiRepository.deleteAll();
		propertyRepository.deleteAll();
	}

}
