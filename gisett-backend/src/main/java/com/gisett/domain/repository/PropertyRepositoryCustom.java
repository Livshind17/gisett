package com.gisett.domain.repository;

import org.springframework.data.domain.Page;

import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;

/**
 * Created by Bartha.Vivien on 10/17/2018.
 */
public interface PropertyRepositoryCustom {

	Page<Property> findFilteredPropertyList(PropertyFilter filter, String cityKey, Integer page, Integer size,
	        Double latitude, Double longitude, Double distance);
}
