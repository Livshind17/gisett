package com.gisett.domain.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.gisett.domain.model.IndicatorPoint;

/**
 * Created by kintzel.levente on 7/6/2018.
 */
public interface IndicatorPointRepository extends MongoRepository<IndicatorPoint, String> {

	@Query(value = "{\"point\":\n" +
	        "       { $near :\n" +
	        "          {\n" +
	        "            $geometry : {\n" +
	        "               type : \"Point\" ,\n" +
	        "               coordinates : [ ?0, ?1] \n" +
	        "          }," +
	        "          $maxDistance: ?2\n" +
	        "       }}}")
	public List<IndicatorPoint> nearByPoints(double longitude, double latitude, double distance);

	@Query(value = "{\"point\":\n" +
	        "       { $geoWithin: \n" +
	        "          {\n" +
	        "            $box : [\n" +
	        "               [ ?0, ?1 ], \n" +
	        "               [ ?2, ?3 ] \n" +
	        "          ]\n" +
	        "       }}}")
	public List<IndicatorPoint> withinSquarePoints(double bottomLeftLng, double bottomLeftLat, double upperRightLng, double upperRightLat);

	@Query(value = "{\"point\":\n" +
	        "       { $near :\n" +
	        "          {\n" +
	        "            $geometry : {\n" +
	        "               type : \"Point\" ,\n" +
	        "               coordinates : [ ?0, ?1] \n" +
	        "          }," +
	        "          $maxDistance: 220\n" +
	        "       }}, cityKey: { $eq :?2}}")
	public List<IndicatorPoint> nearestByPoint(double longitude, double latitude, String cityKey);
}
