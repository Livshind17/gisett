package com.gisett.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gisett.domain.model.City;

/**
 * Created by juhasz.levente on 8/17/2018.
 */
public interface CityRepository extends MongoRepository<City, String> {

	City findByKey(String key);
}
