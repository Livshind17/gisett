package com.gisett.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gisett.domain.model.Property;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public interface PropertyRepository extends MongoRepository<Property, String>, PropertyRepositoryCustom {
}
