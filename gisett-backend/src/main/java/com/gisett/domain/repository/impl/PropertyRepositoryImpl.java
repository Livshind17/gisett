package com.gisett.domain.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.util.ObjectUtils;

import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;
import com.gisett.domain.repository.PropertyRepositoryCustom;

/**
 * Created by Bartha.Vivien on 10/16/2018.
 */
public class PropertyRepositoryImpl implements PropertyRepositoryCustom {

	private static final String ONE_VALUE_FROM_ARRAY = "in";
	private static final String ALL_VALUE_FROM_ARRAY = "all";
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Page<Property> findFilteredPropertyList(PropertyFilter filter, String cityKey, Integer page, Integer size,
	        Double latitude, Double longitude, Double distance) {

		Pageable pageable = PageRequest.of(page, size);
		Query filterQuery = buildQueryWithFilterCriteria(filter, cityKey);

		if (longitude != null && latitude != null) {
			filterQuery.addCriteria(
			        Criteria.where("coordinates")
			                .near(new GeoJsonPoint(longitude, latitude))
			                .maxDistance(distance));
		}
		filterQuery.with(pageable);
		long responseCount = mongoTemplate.count(filterQuery, Property.class);
		List<Property> list = mongoTemplate.find(filterQuery, Property.class);

		return PageableExecutionUtils.getPage(list, pageable, () -> responseCount);
	}

	private Query buildQueryWithFilterCriteria(PropertyFilter filter, String cityKey) {
		Query filterQuery = new Query();
		filterQuery.addCriteria(Criteria.where("cityKey")
		                                .is(cityKey));

		setBetweenCriteria(filterQuery, "price", filter.getFromPrice(), filter.getToPrice());
		setBetweenCriteria(filterQuery, "floor", filter.getFromFloor(), filter.getToFloor());
		setBetweenCriteria(filterQuery, "yearBuilt", filter.getFromYear(), filter.getToYear());
		setBetweenCriteria(filterQuery, "builtSurface", filter.getFromSurface(), filter.getToSurface());

		setCriteriaWithArray(filterQuery, "bathrooms", ONE_VALUE_FROM_ARRAY, filter.getBathCount());
		setCriteriaWithArray(filterQuery, "bedrooms", ONE_VALUE_FROM_ARRAY, filter.getBedCount());
		setCriteriaWithArray(filterQuery, "houseType", ONE_VALUE_FROM_ARRAY, filter.getHouseType());
		setCriteriaWithArray(filterQuery, "amenities", ALL_VALUE_FROM_ARRAY, filter.getAmenities());
		setCriteriaWithArray(filterQuery, "amenities", ALL_VALUE_FROM_ARRAY, filter.getPetAllows());

		if (filter.getCategory() != null) {
			filterQuery.addCriteria(Criteria.where("category")
			                                .is(filter.getCategory()));
		}
		if (filter.getListing() != null) {
			filterQuery.addCriteria(Criteria.where("listing")
			                                .is(filter.getListing()));
		}

		return filterQuery;
	}

	private void setCriteriaIs(Query filterQuery, String attribute, Integer requestValue) {
		if (requestValue != null) {
			filterQuery.addCriteria(Criteria.where(attribute)
			                                .is(requestValue));
		}
	}

	private void setCriteriaWithArray(Query query, String attribute, String method, List<?> arrayOfValue) {

		if (!ObjectUtils.isEmpty(arrayOfValue)) {
			if (method.equals(ONE_VALUE_FROM_ARRAY)) {
				query.addCriteria(Criteria.where(attribute)
				                          .in(arrayOfValue));
			} else {
				if (method.equals(ALL_VALUE_FROM_ARRAY)) {
					arrayOfValue
					            .forEach((value) -> query.addCriteria(Criteria.where(attribute + "." + value)
					                                                          .is(true)));
				}
			}
		}
	}

	private void setBetweenCriteria(Query query, String attribute, Integer fromValue, Integer toValue) {

		if (fromValue != null && toValue != null) {
			query.addCriteria(Criteria.where(attribute)
			                          .gte(fromValue)
			                          .lte(toValue));
		} else {
			if (fromValue != null) {
				query.addCriteria(Criteria.where(attribute)
				                          .gte(fromValue));
			}
			if (toValue != null) {
				query.addCriteria(Criteria.where(attribute)
				                          .lte(toValue));
			}
		}
	}

}
