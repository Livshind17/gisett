package com.gisett.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gisett.domain.model.PoiCategory;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

@Repository
public interface PoiCategoryRepository extends MongoRepository<PoiCategory, String> {
}
