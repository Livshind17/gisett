package com.gisett.domain.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gisett.domain.model.Location;

/**
 * Created by juhasz.levente on 8/21/2018.
 */
public interface LocationRepository extends MongoRepository<Location, String> {

	List<Location> findByNameLikeIgnoreCaseAndCityKey(String name, String cityKey);
}
