package com.gisett.domain.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.gisett.domain.model.Poi;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */
public interface PoiRepository extends MongoRepository<Poi, String> {

	@Query(value = "{\"coordinates\":\n" +
	        "       { $near :\n" +
	        "          {\n" +
	        "            $geometry : {\n" +
	        "               type : \"Point\" ,\n" +
	        "               coordinates : [ ?1, ?2] \n" +
	        "          }," +
	        "          $maxDistance: ?3\n" +
	        "       }}," +
	        "\"type\": {\"$in\": ?4}," + "\"cityKey\": ?0}" +
	        "}")
	List<Poi> findPoisNearPointByTypes(String cityKey, double lng, double lat, double distance,
	        List<String> types);

}
