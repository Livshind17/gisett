package com.gisett.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gisett.domain.model.IndicatorCategory;

/**
 * Created by Bartha.Vivien on 8/29/2018.
 */
@Repository
public interface IndicatorCategoryRepository extends MongoRepository<IndicatorCategory, String> {

}
