package com.gisett.domain.model;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

public class PoiPoint extends Poi {

	public PoiPoint(Long fid, String cityKey, String type, String description, GeoJsonPoint coordinates) {
		super(fid, cityKey, type, description, coordinates);
	}

	public PoiPoint() {
	}

	public PoiPoint(Long fid, String cityKey, String type, String description) {
		super(fid, cityKey, type, description);
	}

	public String toString() {
		GeoJsonPoint point = (GeoJsonPoint) getCoordinates();
		return String.format("PoiPoint {FID: %d, name: %s, type: %s, city: %s, lng: %f, lat: %f}", getFid(), getName(), getType(), getCityKey(), point.getX(),
		        point.getY());
	}
}
