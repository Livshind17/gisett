package com.gisett.domain.model.commons;

public abstract class KeyName {

	protected String key;
	protected String name;

	public KeyName() {
	}

	public KeyName(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
