package com.gisett.domain.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.model.commons.KeyName;

/**
 * Created by juhasz.levente on 8/17/2018.
 */

@Document(collection = "cities")
public class City extends KeyName {
	@Id
	private ObjectId id;
	private Coordinates coordinatesNW;
	private Coordinates coordinatesSE;
	private List<Indicator> indicators;
	private List<String> poiTypes;

	public City() {
	}

	public City(String key, String name, Coordinates coordinatesNW, Coordinates coordinatesSE) {
		super(key, name);
		this.coordinatesNW = coordinatesNW;
		this.coordinatesSE = coordinatesSE;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Coordinates getCoordinatesNW() {
		return coordinatesNW;
	}

	public void setCoordinatesNW(Coordinates coordinatesNW) {
		this.coordinatesNW = coordinatesNW;
	}

	public Coordinates getCoordinatesSE() {
		return coordinatesSE;
	}

	public void setCoordinatesSE(Coordinates coordinatesSE) {
		this.coordinatesSE = coordinatesSE;
	}

	public List<Indicator> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<Indicator> indicators) {
		this.indicators = indicators;
	}

	public List<String> getPoiTypes() {
		return poiTypes;
	}

	public void setPoiTypes(List<String> poiTypes) {
		this.poiTypes = poiTypes;
	}
}
