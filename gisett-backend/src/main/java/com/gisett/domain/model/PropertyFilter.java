package com.gisett.domain.model;

import java.util.List;

/**
 * Created by Bartha.Vivien on 10/16/2018.
 */
public class PropertyFilter {
	private Integer fromPrice;
	private Integer toPrice;
	private List<Integer> bedCount;
	private List<Integer> bathCount;
	private Integer fromSurface;
	private Integer toSurface;
	private Integer fromFloor;
	private Integer toFloor;
	private Integer fromYear;
	private Integer toYear;
	private Category category;
	private Listing listing;
	private List<Amenity> amenities;
	private List<Amenity> petAllows;
	private List<HouseType> houseType;

	public Integer getFromPrice() {
		return fromPrice;
	}

	public void setFromPrice(Integer fromPrice) {
		this.fromPrice = fromPrice;
	}

	public Integer getToPrice() {
		return toPrice;
	}

	public void setToPrice(Integer toPrice) {
		this.toPrice = toPrice;
	}

	public List<Integer> getBedCount() {
		return bedCount;
	}

	public void setBedCount(List<Integer> bedCount) {
		this.bedCount = bedCount;
	}

	public List<Integer> getBathCount() {
		return bathCount;
	}

	public void setBathCount(List<Integer> bathCount) {
		this.bathCount = bathCount;
	}

	public Integer getFromSurface() {
		return fromSurface;
	}

	public void setFromSurface(Integer fromSurface) {
		this.fromSurface = fromSurface;
	}

	public Integer getToSurface() {
		return toSurface;
	}

	public void setToSurface(Integer toSurface) {
		this.toSurface = toSurface;
	}

	public Integer getFromFloor() {
		return fromFloor;
	}

	public void setFromFloor(Integer fromFloor) {
		this.fromFloor = fromFloor;
	}

	public Integer getToFloor() {
		return toFloor;
	}

	public void setToFloor(Integer toFloor) {
		this.toFloor = toFloor;
	}

	public Integer getFromYear() {
		return fromYear;
	}

	public void setFromYear(Integer fromYear) {
		this.fromYear = fromYear;
	}

	public Integer getToYear() {
		return toYear;
	}

	public void setToYear(Integer toYear) {
		this.toYear = toYear;
	}

	public Listing getListing() {
		return listing;
	}

	public void setListing(Listing listing) {
		this.listing = listing;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Amenity> getAmenities() {
		return amenities;
	}

	public void setAmenities(List<Amenity> amenities) {
		this.amenities = amenities;
	}

	public List<Amenity> getPetAllows() {
		return petAllows;
	}

	public void setPetAllows(List<Amenity> petAllows) {
		this.petAllows = petAllows;
	}

	public List<HouseType> getHouseType() {
		return houseType;
	}

	public void setHouseType(List<HouseType> houseType) {
		this.houseType = houseType;
	}
}
