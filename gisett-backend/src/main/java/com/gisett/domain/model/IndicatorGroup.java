package com.gisett.domain.model;

import java.util.List;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */

public class IndicatorGroup {
	private String key;
	private String groupName;
	private List<Indicator> indicators;

	public IndicatorGroup(String key, String groupName, List<Indicator> indicators) {
		this.key = key;
		this.groupName = groupName;
		this.indicators = indicators;
	}

	public List<Indicator> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<Indicator> indicators) {
		this.indicators = indicators;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}