package com.gisett.domain.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gisett.domain.model.commons.BaseImportedIdentifier;
import com.gisett.domain.model.commons.Coordinates;

/**
 * Created by juhasz.levente on 8/21/2018.
 */
@Document(collection = "locations")
public class Location extends BaseImportedIdentifier {
	@Id
	private String id;
	private String name;
	private String cityKey;
	private Coordinates coordinates;

	public Location(Long fid, String name, String cityKey, Coordinates coordinates) {
		this.name = name;
		this.cityKey = cityKey;
		this.coordinates = coordinates;
		setFid(fid);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public String getCityKey() {
		return cityKey;
	}

	public void setCityKey(String cityKey) {
		this.cityKey = cityKey;
	}
}
