package com.gisett.domain.model;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by kintzel.levente on 7/6/2018.
 */
@Document(collection = "indicatorpoints300")
public class IndicatorPoint {

	@Id
	private String id;
	private String cityKey;
	private Map<String, Double> value;
	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	private GeoJsonPoint point;

	public IndicatorPoint() {
	}

	public IndicatorPoint(GeoJsonPoint point) {
		this.point = point;
	}

	public IndicatorPoint(GeoJsonPoint point, Map<String, Double> value) {
		this.point = point;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCityKey() {
		return cityKey;
	}

	public void setCityKey(String cityKey) {
		this.cityKey = cityKey;
	}

	public Map<String, Double> getValue() {
		return value;
	}

	public void setValue(Map<String, Double> value) {
		this.value = value;
	}

	public GeoJsonPoint getPoint() {
		return point;
	}

	public void setPoint(GeoJsonPoint point) {
		this.point = point;
	}
}
