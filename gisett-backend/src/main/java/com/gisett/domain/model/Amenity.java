package com.gisett.domain.model;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public enum Amenity {
	AIR_CONDITION,
	BALCONY,
	BASEMENT,
	BBQ_AREA,
	DISHWASHER,
	DOORMAN,
	DRYER,
	ELEVATOR,
	FIREPLACE,
	FURNISHED,
	GARDEN,
	GYM,
	HEATING,
	LAUNDRY,
	PARKING,
	SWIMMING_POOL,
	WASHER,
	WHEELCHAIR_ACCESS,
	PET_ALLOW_CAT,
	PET_ALLOW_DOG
}
