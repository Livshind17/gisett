package com.gisett.domain.model.commons;

import com.fasterxml.jackson.annotation.JsonValue;

public enum GeometryType {

	POINT, LINE, POLYGON;

	@JsonValue
	public int getJsonValue() {
		return this.ordinal();
	}
}
