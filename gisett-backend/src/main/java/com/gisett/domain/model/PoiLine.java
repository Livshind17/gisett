package com.gisett.domain.model;

import org.springframework.data.mongodb.core.geo.GeoJsonMultiLineString;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

public class PoiLine extends Poi {

	public PoiLine() {
	}

	public PoiLine(Long fid, String cityKey, String type, String description, GeoJsonMultiLineString coordinates) {
		super(fid, cityKey, type, description, coordinates);
	}

	public PoiLine(Long fid, String cityKey, String type, String description) {
		super(fid, cityKey, type, description);
	}
}
