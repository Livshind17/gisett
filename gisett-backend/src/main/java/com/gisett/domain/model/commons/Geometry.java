package com.gisett.domain.model.commons;

import java.util.List;

public class Geometry {

	private GeometryType type;
	private List<List<Coordinates>> coordinates;

	public GeometryType getType() {
		return type;
	}

	public void setType(GeometryType type) {
		this.type = type;
	}

	public List<List<Coordinates>> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<List<Coordinates>> coordinates) {
		this.coordinates = coordinates;
	}
}
