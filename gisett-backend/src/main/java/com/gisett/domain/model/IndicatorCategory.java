package com.gisett.domain.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Bartha.Vivien on 8/29/2018.
 */
@Document(collection = "indicator-configs")
public class IndicatorCategory {
	@Id
	private String id;
	private String key;
	private String category;
	private List<IndicatorGroup> indicatorGroups;

	public IndicatorCategory(String key, String category, List<IndicatorGroup> indicatorGroups) {
		this.key = key;
		this.category = category;
		this.indicatorGroups = indicatorGroups;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<IndicatorGroup> getIndicatorGroups() {
		return indicatorGroups;
	}

	public void setIndicatorGroups(List<IndicatorGroup> indicatorGroups) {
		this.indicatorGroups = indicatorGroups;
	}
}
