package com.gisett.domain.model.commons;

public interface IImportedIdentifier {

	void setFid(Long fid);

	Long getFid();
}
