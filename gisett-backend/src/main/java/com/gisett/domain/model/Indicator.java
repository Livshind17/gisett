package com.gisett.domain.model;

import com.gisett.domain.model.commons.KeyName;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
public class Indicator extends KeyName {
	private Double min;
	private Double max;
	private Boolean isHighValueBetter;

	public Indicator() {

	}

	public Indicator(String key, String name) {
		super(key, name);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Boolean getIsHighValueBetter() {
		return isHighValueBetter;
	}

	public void setIsHighValueBetter(Boolean highValueBetter) {
		isHighValueBetter = highValueBetter;
	}
}