package com.gisett.domain.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gisett.domain.model.commons.KeyName;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

@Document(collection = "poi-configs")
public class PoiCategory extends KeyName {

	@Id
	private String id;
	private List<PoiType> poiTypes;

	public PoiCategory(String key, String name, List<PoiType> poiTypes) {
		this.key = key;
		this.name = name;
		this.poiTypes = poiTypes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<PoiType> getPoiTypes() {
		return poiTypes;
	}

	public void setPoiTypes(List<PoiType> poiTypes) {
		this.poiTypes = poiTypes;
	}
}
