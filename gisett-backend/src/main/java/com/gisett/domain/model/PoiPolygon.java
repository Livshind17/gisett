package com.gisett.domain.model;

import org.springframework.data.mongodb.core.geo.GeoJsonMultiPolygon;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

public class PoiPolygon extends Poi {

	public PoiPolygon() {
	}

	public PoiPolygon(Long fid, String cityKey, String type, String description, GeoJsonMultiPolygon coordinates) {
		super(fid, cityKey, type, description, coordinates);
	}

	public PoiPolygon(Long fid, String cityKey, String type, String description) {
		super(fid, cityKey, type, description);
	}
}
