package com.gisett.domain.model;

import java.util.List;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public class Agency {
	private String name;
	private String contact;
	private String url;
	private List<String> landlinePhones;
	private List<String> mobilePhones;
	private List<String> emails;

	public Agency(String name, String contact, String url, List<String> landlinePhones, List<String> mobilePhones,
	        List<String> emails) {
		this.name = name;
		this.contact = contact;
		this.url = url;
		this.landlinePhones = landlinePhones;
		this.mobilePhones = mobilePhones;
		this.emails = emails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getLandlinePhones() {
		return landlinePhones;
	}

	public void setLandlinePhones(List<String> landlinePhones) {
		this.landlinePhones = landlinePhones;
	}

	public List<String> getMobilePhones() {
		return mobilePhones;
	}

	public void setMobilePhones(List<String> mobilePhones) {
		this.mobilePhones = mobilePhones;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
}
