package com.gisett.domain.model.commons;

/**
 * Created by Bartha.Vivien on 10/18/2018.
 */
public abstract class ExternalIdentifier extends BaseImportedIdentifier {
	private Long objectId;

	public ExternalIdentifier(){}

	public ExternalIdentifier(Long objectId, Long fid) {
		this.objectId = objectId;
		setFid(fid);
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
}
