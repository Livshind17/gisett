package com.gisett.domain.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gisett.domain.model.commons.ExternalIdentifier;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
@Document(collection = "properties")
public class Property extends ExternalIdentifier {
	@Id
	private ObjectId id;

	private String cityKey;
	private Date dateUpdate;
	private Address propertyAddress;
	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	private GeoJsonPoint coordinates;
	private Agency agency;

	private Integer bedrooms;
	private Integer bathrooms;
	private Integer lotSurface;
	private Integer builtSurface;
	private Integer floor;
	private Integer yearBuilt;
	private String description;
	private List<String> photos;

	private Double price;
	private String currency;

	private HouseType houseType;
	private Category category;

	private Map<Amenity, Boolean> amenities;
	private Listing listing;
	private IndicatorPoint nearestIndicatorPoint;

	public Property() {
	}

	public Property(Long objectId, Long fid, String cityKey, Date dateUpdate, Address propertyAddress,
	        GeoJsonPoint coordinates, Agency agency, Integer bedrooms, Integer bathrooms, Integer lotSurface,
	        Integer builtSurface, Integer floor, Integer yearBuilt, String description, List<String> photos,
	        Double price, String currency, HouseType houseType, Category category, Map<Amenity, Boolean> amenities,
	        Listing listing) {

		super(objectId, fid);

		this.id = id;
		this.cityKey = cityKey;
		this.dateUpdate = dateUpdate;
		this.propertyAddress = propertyAddress;
		this.coordinates = coordinates;
		this.agency = agency;
		this.bedrooms = bedrooms;
		this.bathrooms = bathrooms;
		this.lotSurface = lotSurface;
		this.builtSurface = builtSurface;
		this.floor = floor;
		this.yearBuilt = yearBuilt;
		this.description = description;
		this.photos = photos;
		this.price = price;
		this.currency = currency;
		this.houseType = houseType;
		this.category = category;
		this.amenities = amenities;
		this.listing = listing;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCityKey() {
		return cityKey;
	}

	public void setCityKey(String cityKey) {
		this.cityKey = cityKey;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public Address getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(Address propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public GeoJsonPoint getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(GeoJsonPoint coordinates) {
		this.coordinates = coordinates;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	public Integer getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Integer getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getLotSurface() {
		return lotSurface;
	}

	public void setLotSurface(Integer lotSurface) {
		this.lotSurface = lotSurface;
	}

	public Integer getBuiltSurface() {
		return builtSurface;
	}

	public void setBuiltSurface(Integer builtSurface) {
		this.builtSurface = builtSurface;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public Integer getYearBuilt() {
		return yearBuilt;
	}

	public void setYearBuilt(Integer yearBuilt) {
		this.yearBuilt = yearBuilt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getPhotos() {
		return photos;
	}

	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public HouseType getHouseType() {
		return houseType;
	}

	public void setHouseType(HouseType houseType) {
		this.houseType = houseType;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Map<Amenity, Boolean> getAmenities() {
		return amenities;
	}

	public void setAmenities(Map<Amenity, Boolean> amenities) {
		this.amenities = amenities;
	}

	public Listing getListing() {
		return listing;
	}

	public void setListing(Listing listing) {
		this.listing = listing;
	}

	public IndicatorPoint getNearestIndicatorPoint() {
		return nearestIndicatorPoint;
	}

	public void setNearestIndicatorPoint(IndicatorPoint nearestIndicatorPoint) {
		this.nearestIndicatorPoint = nearestIndicatorPoint;
	}
}
