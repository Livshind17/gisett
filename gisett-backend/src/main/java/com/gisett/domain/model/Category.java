package com.gisett.domain.model;

/**
 * Created by Bartha.Vivien on 10/17/2018.
 */
public enum Category {
	RENT,
	SELL
}
