package com.gisett.domain.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public enum Listing {
	PRICE_DROP(6),
	NEW_CONSTRUCTION(2),
	RESALE(0),
	NEW_LISTINGS(4),
	FORECLOSURES(3),
	OPEN_HOUSES(5),
	FOR_SELL_BY_OWNER(1);
	private final int value;
	private static Map map = new HashMap<>();

	Listing(final int newValue) {
		value = newValue;
	}

	static {
		for (Listing listing : Listing.values()) {
			map.put(listing.value, listing);
		}
	}

	public static Listing valueOf(int listing) {
		return (Listing) map.get(listing);
	}

	public int getValue() {
		return value;
	}
}
