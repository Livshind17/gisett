package com.gisett.domain.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gisett.domain.model.commons.BaseImportedIdentifier;

/**
 * Created by tuzes-boloni.kincso on 9/25/2018
 */

@Document(collection = "pois")
public abstract class Poi extends BaseImportedIdentifier {

	@Id
	private String id;
	private String cityKey;
	private String type;
	private String name;
	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	private GeoJson coordinates;

	public Poi(Long fid, String cityKey, String type, String name) {
		this.cityKey = cityKey;
		this.type = type;
		this.name = name;
		setFid(fid);
	}

	public Poi(Long fid, String cityKey, String type, String name, GeoJson coordinates) {
		this.cityKey = cityKey;
		this.type = type;
		this.name = name;
		this.coordinates = coordinates;
		setFid(fid);
	}

	public Poi() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCityKey() {
		return cityKey;
	}

	public void setCityKey(String cityKey) {
		this.cityKey = cityKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GeoJson getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(GeoJson coordinates) {
		this.coordinates = coordinates;
	}
}
