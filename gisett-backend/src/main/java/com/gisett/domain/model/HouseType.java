package com.gisett.domain.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public enum HouseType {
	UNDEFINED(0),
	HOUSE(1),
	APARTMENT(2),
	TWINHOUSE(3),
	CONDO(4),
	MULTI_FAMILY(5),
	MOBILE_MANUFACTURED(6),
	ROOMMATES(7),
	ASSISTED_LIVING(8),
	STUDIO_LOFT(9),
	LAND(10),
	GARDEN_APARTMENT(11),
	PENTHOUSE(12),
	OTHER(13);

	private final int value;
	private static Map map = new HashMap<>();

	HouseType(final int newValue) {
		value = newValue;
	}

	static {
		for (HouseType houseType : HouseType.values()) {
			map.put(houseType.value, houseType);
		}
	}

	public static HouseType valueOf(int houseType) {
		return (HouseType) map.get(houseType);
	}

	public int getValue() {
		return value;
	}
}
