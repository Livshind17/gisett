package com.gisett.domain.model.commons;

public abstract class BaseImportedIdentifier implements IImportedIdentifier {

	private Long fid;

	public Long getFid() {
		return fid;
	}

	public void setFid(Long fid) {
		this.fid = fid;
	}
}
