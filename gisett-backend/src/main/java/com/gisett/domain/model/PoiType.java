package com.gisett.domain.model;

import com.gisett.domain.model.commons.KeyName;

/**
 * Created by tuzes-boloni.kincso on 9/20/2018
 */

public class PoiType extends KeyName {

	public PoiType(String key, String name) {
		super(key, name);
	}
}
