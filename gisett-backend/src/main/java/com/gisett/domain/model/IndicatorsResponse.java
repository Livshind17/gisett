package com.gisett.domain.model;

import com.gisett.domain.model.commons.Coordinates;

/**
 * Created by tuzes-boloni.kincso on 10/3/2018
 */
public class IndicatorsResponse {

	private Coordinates coordinates;
	private int columnCount;
	private String indicatorsEncoded;

	public IndicatorsResponse(String indicatorsEncoded, Coordinates coordinates, int columnCount) {
		this.coordinates = coordinates;
		this.columnCount = columnCount;
		this.indicatorsEncoded = indicatorsEncoded;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public int getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(int columnCount) {
		this.columnCount = columnCount;
	}

	public String getIndicatorsEncoded() {
		return indicatorsEncoded;
	}

	public void setIndicatorsEncoded(String indicatorsEncoded) {
		this.indicatorsEncoded = indicatorsEncoded;
	}
}
