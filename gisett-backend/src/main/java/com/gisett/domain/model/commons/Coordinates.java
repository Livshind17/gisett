package com.gisett.domain.model.commons;

/**
 * Created by kintzel.levente on 7/16/2018.
 */
public class Coordinates {

	private double latitude;
	private double longitude;

	public Coordinates() {
	}

	public Coordinates(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
