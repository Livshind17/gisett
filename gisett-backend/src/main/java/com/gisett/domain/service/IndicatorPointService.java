package com.gisett.domain.service;

import java.util.List;

import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */
public interface IndicatorPointService {

	List<IndicatorPoint> nearByPoints(double longitude, double latitude, double distance);

	IndicatorPoint nearestByPoint(double longitude, double latitude, String cityKey);

	IndicatorPoint nearestByPointWithRangeConvert(double longitude, double latitude, String cityKey);

	List<IndicatorPoint> withinSquarePoints(double bottomLeftLng, double bottomLeftLat, double upperRightLng,
	        double upperRightLat);

	IndicatorPoint save(IndicatorPoint indicatorPoint);

	IndicatorsResponse getIndicatorsForHeatmap(String cityKey, double locationPointLng, double locationPointLat,
	        double radius);
}
