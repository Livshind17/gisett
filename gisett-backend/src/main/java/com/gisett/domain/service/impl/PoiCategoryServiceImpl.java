package com.gisett.domain.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.domain.model.PoiCategory;
import com.gisett.domain.repository.PoiCategoryRepository;
import com.gisett.domain.service.PoiCategoryService;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class PoiCategoryServiceImpl implements PoiCategoryService {

	private Logger logger = LoggerFactory.getLogger(PoiCategoryServiceImpl.class);

	@Autowired
	PoiCategoryRepository repository;

	@Override
	public PoiCategory save(PoiCategory poiConfig) {
		try {
			return repository.save(poiConfig);
		} catch (DataIntegrityViolationException e) {
			logger.error("Save failed in PoiConfigService", e);
			throw new GisettBadDataException("Save failed in PoiConfigService", e);
		}
	}
}
