package com.gisett.domain.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.domain.model.Poi;
import com.gisett.domain.repository.PoiRepository;
import com.gisett.domain.service.PoiService;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class PoiServiceImpl implements PoiService {

	private Logger logger = LoggerFactory.getLogger(PoiServiceImpl.class);

	@Autowired
	PoiRepository poiRepository;

	@Override
	public Poi save(Poi poi) {
		try {
			return poiRepository.save(poi);
		} catch (DataIntegrityViolationException e) {
			String logMessage = "Save failed in PoiService";
			logger.error(logMessage, e);
			throw new GisettBadDataException(logMessage, e);
		}
	}

	@Override
	public List<Poi> findPoisNearPointByTypes(String cityKey, double lng, double lat, double distance,
	        List<String> types) {
		try {
			return poiRepository.findPoisNearPointByTypes(cityKey, lng, lat, distance, types);
		} catch (DataIntegrityViolationException e) {
			String logMessage = "FindNearPointByCategories failed in PoiService";
			logger.error(logMessage, e);
			throw new GisettBadDataException(logMessage, e);
		}
	}
}
