package com.gisett.domain.service;

import java.util.List;

import com.gisett.domain.model.IndicatorCategory;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

public interface IndicatorCategoryService {

	List<IndicatorCategory> findAll();

	IndicatorCategory save(IndicatorCategory indicatorCategory);
}
