package com.gisett.domain.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.domain.model.Location;
import com.gisett.domain.repository.LocationRepository;
import com.gisett.domain.service.LocationService;
import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettServiceException;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class LocationServiceImpl implements LocationService {

	private Logger logger = LoggerFactory.getLogger(LocationService.class);

	@Autowired
	LocationRepository repository;

	@Override
	public List<Location> findByNameLikeAndCityKey(String name, String cityKey) {
		try {
			return repository.findByNameLikeIgnoreCaseAndCityKey(name, cityKey);
		} catch (DataAccessException e) {
			logger.error("findByNameLikeAndCityKey failed in LocationService", e);
			throw new GisettServiceException("FindAll failed in LocationService", e);
		}
	}

	@Override
	public Location save(Location location) {
		try {
			return repository.save(location);
		} catch (DataIntegrityViolationException e) {
			logger.error("save failed in LocationService", e);
			throw new GisettBadDataException("Save failed in LocationService", e);
		}
	}
}
