package com.gisett.domain.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;
import com.gisett.domain.repository.PropertyRepository;
import com.gisett.domain.service.PropertyService;
import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettServiceException;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
@Service
public class PropertyServiceImpl implements PropertyService {

	private Logger logger = LoggerFactory.getLogger(PropertyServiceImpl.class);
	private final PropertyRepository propertyRepository;

	@Autowired
	public PropertyServiceImpl(PropertyRepository propertyRepository) {
		this.propertyRepository = propertyRepository;
	}

	@Override
	public Property save(Property property) {
		try {
			return propertyRepository.save(property);
		} catch (DataIntegrityViolationException e) {
			String logMessage = "Save failed in PropertyService";
			logger.error(logMessage, e);
			throw new GisettBadDataException(logMessage, e);
		}
	}

	@Override
	public Page<Property> findAllFiltered(PropertyFilter filter, String cityKey, Integer page, Integer size,
	        Double latitude, Double longitude, Double distance) {
		try {
			return propertyRepository.findFilteredPropertyList(filter, cityKey, page, size, latitude, longitude, distance);
		} catch (DataAccessException e) {
			String logMessage = "FindAllFiltered failed in PropertyService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}
}
