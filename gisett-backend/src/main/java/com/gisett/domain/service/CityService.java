package com.gisett.domain.service;

import java.util.List;

import com.gisett.configuration.appdata.CityData;
import com.gisett.domain.model.City;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */
public interface CityService {

	List<City> findAll();

	City findByKey(String key);

	City save(City city);

	CityData getCityData(String key);
}
