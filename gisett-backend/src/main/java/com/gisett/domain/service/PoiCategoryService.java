package com.gisett.domain.service;

import com.gisett.domain.model.PoiCategory;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */
public interface PoiCategoryService {

	PoiCategory save(PoiCategory poiConfig);
}
