package com.gisett.domain.service;

import java.util.List;

import com.gisett.domain.model.Poi;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

public interface PoiService {

	Poi save(Poi poi);

	List<Poi> findPoisNearPointByTypes(String cityKey, double lng, double lat, double distance,
	        List<String> types);
}
