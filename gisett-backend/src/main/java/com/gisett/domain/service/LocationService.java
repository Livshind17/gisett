package com.gisett.domain.service;

import java.util.List;

import com.gisett.domain.model.Location;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */
public interface LocationService {

	List<Location> findByNameLikeAndCityKey(String name, String cityKey);

	Location save(Location location);
}
