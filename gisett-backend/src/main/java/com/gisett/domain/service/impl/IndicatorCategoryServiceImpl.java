package com.gisett.domain.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.domain.model.IndicatorCategory;
import com.gisett.domain.repository.IndicatorCategoryRepository;
import com.gisett.domain.service.IndicatorCategoryService;
import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettServiceException;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class IndicatorCategoryServiceImpl implements IndicatorCategoryService {

	private Logger logger = LoggerFactory.getLogger(IndicatorCategoryServiceImpl.class);

	@Autowired
	IndicatorCategoryRepository repository;

	@Override
	public List<IndicatorCategory> findAll() {
		try {
			return repository.findAll();
		} catch (DataAccessException e) {
			logger.error("FindAll failed in IndicatorCategoryService", e);
			throw new GisettServiceException("FindAll failed in PoiService", e);
		}
	}

	@Override
	public IndicatorCategory save(IndicatorCategory indicatorCategory) {
		try {
			return repository.save(indicatorCategory);
		} catch (DataIntegrityViolationException e) {
			logger.error("Save failed in IndicatorCategoryService", e);
			throw new GisettBadDataException("Save failed in IndicatorCategoryService", e);
		}
	}
}
