package com.gisett.domain.service.impl;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.geotools.geometry.DirectPosition2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.configuration.appdata.CityData;
import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;
import com.gisett.domain.repository.IndicatorPointRepository;
import com.gisett.domain.service.CityService;
import com.gisett.domain.service.IndicatorPointService;
import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettRetrievalException;
import com.gisett.exception.GisettServiceException;
import com.gisett.util.IndicatorResponseCreator;
import com.gisett.util.PointCalculator;
import com.gisett.util.RangeConverter;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class IndicatorPointServiceImpl implements IndicatorPointService {

	private Logger logger = LoggerFactory.getLogger(IndicatorPointServiceImpl.class);

	@Autowired
	private IndicatorPointRepository repository;

	@Autowired
	private CityService cityService;

	private PointCalculator pointCalculator = new PointCalculator();
	private RangeConverter rangeConverter = new RangeConverter();
	IndicatorResponseCreator responseCreator = new IndicatorResponseCreator();

	@Override
	public List<IndicatorPoint> nearByPoints(double longitude, double latitude, double distance) {
		try {
			return repository.nearByPoints(longitude, latitude, distance);
		} catch (DataAccessException e) {
			String logMessage = "NearByPoints failed in IndicatorPointService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public List<IndicatorPoint> withinSquarePoints(double bottomLeftLng, double bottomLeftLat, double upperRightLng,
	        double upperRightLat) {
		try {
			return repository.withinSquarePoints(bottomLeftLng, bottomLeftLat, upperRightLng, upperRightLat);
		} catch (DataAccessException e) {
			String logMessage = "GeoWithinPoints failed in IndicatorPointService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public IndicatorPoint nearestByPoint(double longitude, double latitude, String cityKey) {
		List<IndicatorPoint> indicatorPoints = repository.nearestByPoint(longitude, latitude, cityKey);
		try {
			if (!indicatorPoints.isEmpty()) {
				return indicatorPoints.get(0);
			}
			throw new GisettRetrievalException("Not found the nearest coordinates, check the cityKey parameter");
		} catch (DataAccessException e) {
			String logMessage = "nearestByCoordinates failed in IndicatorPointService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public IndicatorPoint nearestByPointWithRangeConvert(double longitude, double latitude, String cityKey) {
		List<IndicatorPoint> indicatorPoints = repository.nearestByPoint(longitude, latitude, cityKey);
		try {
			if (!indicatorPoints.isEmpty()) {
				IndicatorPoint indicatorPoint = indicatorPoints.get(0);
				CityData cityData = cityService.getCityData(cityKey);
				//@formatter:off
				Map<String, Double> newIndicatorMap = indicatorPoint.getValue()
				                                                    .entrySet()
				                                                    .stream()
				                                                    .collect(Collectors.toMap(Map.Entry::getKey, i -> {
					                                                    return (double) rangeConverter.convertIndicatorRanges(
					                                                                                          cityData.getIndicators()
					                                                                                                  .get(i.getKey()),
					                                                                                          i.getValue());
				                                                    }));
				//@formatter:on
				indicatorPoint.setValue(newIndicatorMap);
				return indicatorPoint;
			}
			throw new GisettRetrievalException("Not found the nearest coordinates, check the cityKey parameter");
		} catch (DataAccessException e) {
			String logMessage = "nearestByCoordinates failed in IndicatorPointService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public IndicatorPoint save(IndicatorPoint indicatorPoint) {
		try {
			return repository.save(indicatorPoint);
		} catch (DataIntegrityViolationException e) {
			String logMessage = "Save failed in IndicatorPointService";
			logger.error(logMessage, e);
			throw new GisettBadDataException(logMessage, e);
		}
	}

	@Override
	public IndicatorsResponse getIndicatorsForHeatmap(String cityKey, double locationPointLng, double locationPointLat,
	        double radius) {
		long startTime = System.currentTimeMillis();
		logger.debug("location point: " + locationPointLng + "-" + locationPointLat);
		DirectPosition2D referencePoint = new DirectPosition2D(locationPointLng, locationPointLat);
		Point2D bottomLeftPoint = pointCalculator.getPointToDirection(referencePoint, 180 + 45, radius);
		Point2D upperRightPoint = pointCalculator.getPointToDirection(referencePoint, 90 - 45, radius);
		List<IndicatorPoint> indicatorPoints = withinSquarePoints(bottomLeftPoint.getX(), bottomLeftPoint.getY(),
		        upperRightPoint.getX(), upperRightPoint.getY());
		logger.debug("Corners: " + bottomLeftPoint.getX() + " " + bottomLeftPoint.getY() + " - "
		        + upperRightPoint.getX() + " " + upperRightPoint.getY());

		if (cityService.getCityData(cityKey) == null) {
			String logMessage = "Can not find any city with the city key " + cityKey;
			logger.error(logMessage);
			throw new GisettRetrievalException(logMessage);
		}
		IndicatorsResponse response = responseCreator.createResponse(cityService.getCityData(cityKey), indicatorPoints);

		long stopTime = System.currentTimeMillis();
		logger.debug(" Get indicators for heatmap response creation took : " + (stopTime - startTime) + " ms");
		return response;
	}
}
