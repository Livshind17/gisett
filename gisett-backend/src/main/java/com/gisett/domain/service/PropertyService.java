package com.gisett.domain.service;

import org.springframework.data.domain.Page;

import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;

/**
 * Created by Bartha.Vivien on 10/15/2018.
 */
public interface PropertyService {

	Property save(Property property);

	Page<Property> findAllFiltered(PropertyFilter filter, String cityKey, Integer page, Integer size, Double latitude,
	        Double longitude, Double distance);
}
