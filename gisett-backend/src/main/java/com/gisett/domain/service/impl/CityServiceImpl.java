package com.gisett.domain.service.impl;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.gisett.configuration.appdata.CityData;
import com.gisett.configuration.appdata.IndicatorCategoryData;
import com.gisett.configuration.appdata.IndicatorData;
import com.gisett.configuration.appdata.IndicatorGroupData;
import com.gisett.configuration.appdata.PoiCategoryData;
import com.gisett.domain.model.City;
import com.gisett.domain.model.Indicator;
import com.gisett.domain.model.IndicatorCategory;
import com.gisett.domain.model.IndicatorGroup;
import com.gisett.domain.model.PoiCategory;
import com.gisett.domain.model.PoiType;
import com.gisett.domain.repository.CityRepository;
import com.gisett.domain.repository.IndicatorCategoryRepository;
import com.gisett.domain.repository.PoiCategoryRepository;
import com.gisett.domain.service.CityService;
import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettRetrievalException;
import com.gisett.exception.GisettServiceException;

/**
 * Created by tuzes-boloni.kincso on 9/26/2018
 */

@Service
public class CityServiceImpl implements CityService {

	private Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);

	@Autowired
	CityRepository cityRepository;

	@Autowired
	IndicatorCategoryRepository indicatorCategoryRepository;

	@Autowired
	PoiCategoryRepository poiCategoryRepository;

	@Override
	public List<City> findAll() {
		try {
			return cityRepository.findAll();
		} catch (DataAccessException e) {
			String logMessage = "FindAll failed in CityService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public City findByKey(String key) {
		try {
			return cityRepository.findByKey(key);
		} catch (DataAccessException e) {
			String logMessage = "FindByKey failed in CityService";
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	@Override
	public City save(City city) {
		try {
			return cityRepository.save(city);
		} catch (DataIntegrityViolationException e) {
			String logMessage = "Save failed in CityService";
			logger.error(logMessage, e);
			throw new GisettBadDataException(logMessage, e);
		}
	}

	@Override
	public CityData getCityData(String key) {
		try {
			CityData cityData = new CityData();

			City city = cityRepository.findByKey(key);
			if (city == null) {
				String logMessage = "Can not find any city with the city key " + key;
				logger.error(logMessage);
				throw new GisettRetrievalException(logMessage);
			}

			// set common data
			cityData.setKey(city.getKey());
			cityData.setCoordinatesNW(city.getCoordinatesNW());
			cityData.setCoordinatesSE(city.getCoordinatesSE());

			// set indicators
			List<IndicatorCategory> indicatorCategories = indicatorCategoryRepository.findAll();
			cityData.setIndicators(getIndicators(city.getIndicators(), indicatorCategories));

			// set indicator categories
			cityData.setIndicatorCategories(getIndicatorCategoryData(indicatorCategories, cityData.getIndicators()));

			// set pois
			List<PoiCategory> poiCategories = poiCategoryRepository.findAll();
			cityData.setPoiCategories(getPoiTypes(city.getPoiTypes(), poiCategories));

			return cityData;
		} catch (DataAccessException e) {
			String logMessage = "Failed to construct CityData for city:" + key;
			logger.error(logMessage, e);
			throw new GisettServiceException(logMessage, e);
		}
	}

	/**
	 * Creates the indicator data map for a city knowing the list of city indicators
	 * and the list of categories available in the system.
	 *
	 * @param indicators
	 *            city indicator list
	 * @param indicatorCategories
	 *            indicator categories
	 * @return the map with city indicator data, the key is the indicator key
	 */
	private Map<String, IndicatorData> getIndicators(List<Indicator> indicators,
	        List<IndicatorCategory> indicatorCategories) {
		Map<String, IndicatorData> indicatorDataMap = new LinkedHashMap<>();
		indicators.forEach(indicator -> {
			Optional<Indicator> indicatorModelOpt = getIndicator(indicatorCategories, indicator.getKey());
			IndicatorData data = new IndicatorData();
			data.setKey(indicator.getKey());
			data.setMin(indicator.getMin());
			data.setMax(indicator.getMax());
			if (indicatorModelOpt.isPresent()) {
				Indicator indicatorModel = indicatorModelOpt.get();
				data.setHighValueBetter(indicatorModel.getIsHighValueBetter());
				data.setMin(data.getMin() != null ? data.getMin() : indicatorModel.getMin());
				data.setMax(data.getMax() != null ? data.getMax() : indicatorModel.getMax());
			}
			indicatorDataMap.put(indicator.getKey(), data);
		});
		return indicatorDataMap;
	}

	private List<PoiCategoryData> getPoiTypes(List<String> cityPoiTypes, List<PoiCategory> allPoiCategories) {
		List<PoiCategoryData> poiCategoryList = allPoiCategories.stream()
		                                                        .map(poiCategory -> {
			                                                        PoiCategoryData poiCategoryData = new PoiCategoryData(poiCategory.getKey());
			                                                        List<String> dataPoiTypes = poiCategory.getPoiTypes()
			                                                                                               .stream()
			                                                                                               .filter(poiType -> cityPoiTypes.contains(
			                                                                                                       poiType.getKey()))
			                                                                                               .map(PoiType::getKey)
			                                                                                               .collect(Collectors.toList());
			                                                        poiCategoryData.setPoiTypes(dataPoiTypes);
			                                                        return poiCategoryData;
		                                                        })
		                                                        .collect(Collectors.toList());
		return poiCategoryList;
	}

	/**
	 * Creates the list of indicator category data.
	 *
	 * @param indicatorCategories
	 *            indicator category list for whole system
	 * @param indicators
	 *            the indicators map. We will need it to determine which indicator
	 *            group is available for our city
	 * @return the list of indicator category data.
	 */
	private List<IndicatorCategoryData> getIndicatorCategoryData(List<IndicatorCategory> indicatorCategories,
	        Map<String, IndicatorData> indicators) {
		return indicatorCategories.stream()
		                          .map(indicatorCategory -> {
			                          IndicatorCategoryData data = new IndicatorCategoryData();
			                          data.setKey(indicatorCategory.getKey());
			                          data.setIndicatorGroups(getIndicatorGroupData(indicatorCategory.getIndicatorGroups(), indicators));
			                          return data;
		                          })
		                          .collect(Collectors.toList());
	}

	/**
	 * Creates the list of indicator group data.
	 *
	 * @param indicatorGroups
	 *            the indicator group list
	 * @param indicators
	 *            the indicators map. We will need it to determine which indicator
	 *            group is available for our city
	 * @return the list of indicator group data.
	 */
	private List<IndicatorGroupData> getIndicatorGroupData(List<IndicatorGroup> indicatorGroups,
	        Map<String, IndicatorData> indicators) {
		return indicatorGroups.stream()
		                      .map(indicatorGroup -> {
			                      IndicatorGroupData data = new IndicatorGroupData();
			                      data.setKey(indicatorGroup.getKey());
			                      data.setIndicators(indicatorGroup.getIndicators()
			                                                       .stream()
			                                                       .map(Indicator::getKey)
			                                                       .filter(key -> indicators.containsKey(key))
			                                                       .collect(Collectors.toList()));
			                      data.setActiveForCity(!data.getIndicators()
			                                                 .isEmpty());
			                      return data;
		                      })
		                      .collect(Collectors.toList());
	}

	/**
	 * Gets an indicator from the indicator config hierarchy
	 *
	 * @param indicatorCategories
	 *            list of indicator categories
	 * @param indicatorKey
	 *            the indicator key we are looking for
	 * @return the indicator found
	 */
	private Optional<Indicator> getIndicator(List<IndicatorCategory> indicatorCategories, String indicatorKey) {
		return indicatorCategories.stream()
		                          .map(IndicatorCategory::getIndicatorGroups)
		                          .flatMap(Collection::stream)
		                          .map(IndicatorGroup::getIndicators)
		                          .flatMap(Collection::stream)
		                          .filter(ind -> indicatorKey.equals(ind.getKey()))
		                          .findFirst();
	}
}
