package com.gisett.util;

import java.awt.geom.Point2D;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.geotools.referencing.GeodeticCalculator;

import com.gisett.domain.model.commons.Coordinates;

/**
 * Created by Bartha.Vivien on 8/31/2018.
 */
public class PointCalculator {

    private GeodeticCalculator calc = new GeodeticCalculator();
    private DirectPosition2D northWestCorner;
    private DirectPosition2D southEastCorner;
    private Envelope2D fullViewPort;

    public static final double INDICATOR_STEP_DISTANCE = 300;

    public void setViewPort(double latNW, double longNW, double latSE, double longSE) {
        northWestCorner = new DirectPosition2D(longNW, latNW);
        southEastCorner = new DirectPosition2D(longSE, latSE);
        fullViewPort = new Envelope2D(northWestCorner, southEastCorner);
    }

    public Point2D getPointToDirection(DirectPosition2D cursor, int azimuth, double radius) {
        calc.setStartingGeographicPoint(cursor);
        calc.setDirection(azimuth, radius);

        return calc.getDestinationGeographicPoint();
    }

    public Point2D getPointToEast(DirectPosition2D cursor) {
        calc.setStartingGeographicPoint(cursor);
        calc.setDirection(90, INDICATOR_STEP_DISTANCE);

        return calc.getDestinationGeographicPoint();
    }

    public Point2D getPointToSouth(DirectPosition2D cursor) {
        calc.setStartingGeographicPoint(cursor);
        calc.setDirection(180, INDICATOR_STEP_DISTANCE);

        return calc.getDestinationGeographicPoint();
    }

    public double getViewsMaxX() {
        return fullViewPort.getMaxX();
    }

    public double getViewsMinY() {
        return fullViewPort.getMinY();
    }

    public DirectPosition2D getNorthWestCorner() {
        return northWestCorner;
    }

    public double getNorthWestCornerX() {
        return northWestCorner.getX();
    }

    public double getNorthWestCornerY() {
        return northWestCorner.getY();
    }

    public double getDistance(Coordinates start, Coordinates end) {
        calc.setStartingGeographicPoint(start.getLongitude(), start.getLatitude());
        calc.setDestinationGeographicPoint(end.getLongitude(), end.getLatitude());
        return calc.getOrthodromicDistance();
    }
}
