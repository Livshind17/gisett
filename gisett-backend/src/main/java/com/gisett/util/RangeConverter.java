package com.gisett.util;

import com.gisett.configuration.appdata.IndicatorData;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by tuzes-boloni.kincso on 9/28/2018
 */
public class RangeConverter {

	private int rangeMin = 0;
	private int rangeMax = 100;

	public byte convertToAnotherRange(Double value, Double actualRangeMin, Double actualRangeMax) {
		long convertedValue = Math
		                          .round(Math.abs((value - actualRangeMin) / (actualRangeMax - actualRangeMin)) * (rangeMax - rangeMin)
		                                  + rangeMin);
		return (byte) convertedValue;
	}

	public byte convertIndicatorRanges(IndicatorData indicatorData, Double indicatorValue) {
		Double min = indicatorData.getMin();
		Double max = indicatorData.getMax();
		if (indicatorValue < min || indicatorValue > max || min > max) {
			throw new GisettBadDataException("Error in convertToAnotherRange! Invalid range or value out of range!");
		}
		boolean isHighValueBetter = (indicatorData.getIsHighValueBetter() == null) ? true
		        : indicatorData.getIsHighValueBetter();
		double rangeStart = isHighValueBetter ? min : max;
		double rangeEnd = isHighValueBetter ? max : min;
		return convertToAnotherRange(indicatorValue, rangeStart, rangeEnd);
	}

	public int getRangeMin() {
		return rangeMin;
	}

	public void setRangeMin(int rangeMin) {
		rangeMin = rangeMin;
	}

	public int getRangeMax() {
		return rangeMax;
	}

	public void setRangeMax(int rangeMax) {
		rangeMax = rangeMax;
	}
}