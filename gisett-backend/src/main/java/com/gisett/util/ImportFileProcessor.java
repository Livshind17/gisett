package com.gisett.util;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.OverviewPolicy;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.opengis.parameter.GeneralParameterValue;
import org.opengis.parameter.ParameterValue;

import java.io.File;

/**
 * Created by Bartha.Vivien on 8/31/2018.
 */

public class ImportFileProcessor {

    public GridCoverage2D extract(File input) throws Exception {

        ParameterValue<OverviewPolicy> policy = AbstractGridFormat.OVERVIEW_POLICY
                .createValue();
        policy.setValue(OverviewPolicy.IGNORE);

        ParameterValue<String> gridsize = AbstractGridFormat.SUGGESTED_TILE_SIZE.createValue();

        ParameterValue<Boolean> useJaiRead = AbstractGridFormat.USE_JAI_IMAGEREAD.createValue();
        useJaiRead.setValue(true);

        GeoTiffReader reader = new GeoTiffReader(input);

        return reader.read(new GeneralParameterValue[]{policy, gridsize, useJaiRead});
    }

}
