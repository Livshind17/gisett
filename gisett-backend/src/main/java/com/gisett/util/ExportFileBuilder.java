package com.gisett.util;

import com.vividsolutions.jts.geom.Point;
import org.geotools.data.DataUtilities;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;

/**
 * Created by Bartha.Vivien on 8/31/2018.
 */
public class ExportFileBuilder {

    private GeometryBuilder builder = new GeometryBuilder();
    private final SimpleFeatureType TYPE = DataUtilities.createType("gisett", "GRID_CODE:double,location:Point");

    public ExportFileBuilder() throws Exception {
    }

    public FileWriter initGeoJSONFile(File output, String indicatorName) throws Exception {
        output.createNewFile();
        FileWriter fw = new FileWriter(output);
        fw.write("{\"type\": \"FeatureCollection\",\n\"name\":\"" + indicatorName + "\",\n");
        fw.write("\"crs\": { \"type\": \"name\", \"properties\": { \"name\": \"urn:ogc:def:crs:OGC:1.3:CRS84\" } },\n");
        fw.write("\"features\": [\n");
        return fw;
    }

    public void writePointToGeoJSON(DirectPosition2D cursor, double value, long pointId, FileWriter fw) throws Exception {
        FeatureJSON fjson = new FeatureJSON(new GeometryJSON(14));
        StringWriter writer = new StringWriter();
        final Point point = builder.point(cursor.getX(), cursor.getY());
        SimpleFeatureBuilder fBuild = new SimpleFeatureBuilder(TYPE);
        fBuild.add(value);
        fBuild.add(point);
        SimpleFeature feature = fBuild.buildFeature("" + pointId);
        fjson.writeFeature(feature, writer);
        if (pointId != 1) {
            fw.write("," + writer.toString() + "\n");
        } else {
            fw.write(writer.toString() + "\n");
        }
    }

    public void closeGeoJSONFile(FileWriter fw) throws Exception {
        fw.write("]}");
        fw.close();
    }
}
