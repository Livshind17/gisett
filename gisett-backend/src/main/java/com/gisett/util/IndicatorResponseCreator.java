package com.gisett.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gisett.configuration.appdata.CityData;
import com.gisett.configuration.appdata.IndicatorData;
import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.exception.GisettServiceException;

/**
 * Created by tuzes-boloni.kincso on 10/1/2018
 */

public class IndicatorResponseCreator {

	private Logger logger = LoggerFactory.getLogger(IndicatorResponseCreator.class);

	private RangeConverter rangeConverter = new RangeConverter();
	private PointCalculator pointCalculator = new PointCalculator();

	protected int savePointFailNr = 0;
	protected int columnCount;
	protected Coordinates upperLeftCoord;
	protected int indicatorReprByteSize;
	protected List<String> cityIndicatorKeys = new ArrayList<>();
	protected Map<String, IndicatorData> cityIndicatorData;

	public IndicatorsResponse createResponse(CityData cityData, List<IndicatorPoint> points) {
		// initialize process
		setIndicatorDataByCity(cityData);

		byte[][][] pointMatrix = createPointMatrix(points);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for (byte[][] matrixRow : pointMatrix) {
			for (byte[] matrixItem : matrixRow) {
				baos = writeToByteArrayOutputStream(baos, matrixItem);
			}
		}
		logger.debug("Response byte array for all indicator points: " + Arrays.toString(baos.toByteArray()));
		String encodedIndicators = Base64.getEncoder().encodeToString(baos.toByteArray());
		return new IndicatorsResponse(encodedIndicators, this.upperLeftCoord, this.columnCount);
	}

	protected byte[][][] createPointMatrix(List<IndicatorPoint> points) {
		List<Coordinates> corners = getMatrixCorners(points);
		//column count
		int upperColumnCount = (int) Math.round(
				pointCalculator.getDistance(corners.get(1), corners.get(0)) / PointCalculator.INDICATOR_STEP_DISTANCE)
				+ 1;
		int bottomColumnCount = (int) Math.round(
				pointCalculator.getDistance(corners.get(3), corners.get(2)) / PointCalculator.INDICATOR_STEP_DISTANCE)
				+ 1;
		this.columnCount = upperColumnCount > bottomColumnCount ? upperColumnCount : bottomColumnCount;
		//row count
		int leftRowCount = (int) Math.round(
				pointCalculator.getDistance(corners.get(2), corners.get(0)) / PointCalculator.INDICATOR_STEP_DISTANCE)
				+ 1;
		int rightRowCount = (int) Math.round(
				pointCalculator.getDistance(corners.get(3), corners.get(1)) / PointCalculator.INDICATOR_STEP_DISTANCE)
				+ 1;
		int rowCount = leftRowCount > rightRowCount ? leftRowCount : rightRowCount;

		logger.debug("Upper Column count - corner1-corner0: " + upperColumnCount);
		logger.debug("Bottom Column count - corner3-corner2: " + bottomColumnCount);
		logger.debug("Left Row count - corner2-corner0: " + leftRowCount);
		logger.debug("Right Row count - corner3-corner1: " + rightRowCount);
		logger.debug("Column count: " + columnCount);
		logger.debug("Row count: " + rowCount);

		byte pointMatrix[][][] = new byte[rowCount][columnCount][];
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < columnCount; j++) {
				pointMatrix[i][j] = new byte[indicatorReprByteSize];
			}
		}
		return fillPointMatrix(points, pointMatrix, corners);
	}

	private byte[][][] fillPointMatrix(List<IndicatorPoint> points, byte[][][] pointMatrix, List<Coordinates> corners) {
		Coordinates nWCoord = corners.get(0);
		points.forEach(point -> {
			double x = point.getPoint().getX();
			double y = point.getPoint().getY();

			int j = (int) Math.round(
					pointCalculator.getDistance(new Coordinates(y, x), new Coordinates(y, nWCoord.getLongitude()))
							/ PointCalculator.INDICATOR_STEP_DISTANCE);
			int i = (int) Math
					.round(pointCalculator.getDistance(new Coordinates(nWCoord.getLatitude(), x), new Coordinates(y, x))
							/ PointCalculator.INDICATOR_STEP_DISTANCE);
			try {
				pointMatrix[i][j] = createMatrixValueForPoint(point);
			} catch (Exception e) {
				logger.error("Failed to save the value of point (" + x + ", " + y + ") to the matrix's [" + i + "][" + j
						+ "] position.");
				savePointFailNr++;
			}
		});
		return pointMatrix;
	}

	protected byte[] createMatrixValueForPoint(IndicatorPoint point) {
		// active indicators from city indicators for this point
		byte[] pointActIndicators = new byte[indicatorReprByteSize];
		Set<String> pointIndicatorKeys = point.getValue().keySet();
		StringBuilder pointActIndString = new StringBuilder(cityIndicatorKeys.stream()
				.map(cityIndKey -> pointIndicatorKeys.contains(cityIndKey) ? "1" : "0").collect(Collectors.joining()));
		while (pointActIndString.length() % 8 != 0) {
			pointActIndString.append("0");
		}
		logger.debug("Active indicators for this point - bit string: " + pointActIndString);
		int index = 0;
		while (index < pointActIndString.length()) {
			pointActIndicators[index / 8] = (byte) Integer.parseInt(pointActIndString.substring(index, index + 8), 2);
			index += 8;
		}
		logger.debug("Active indicators for this point - byte array: " + Arrays.toString(pointActIndicators));

		// indicator values for this point
		List<Byte> indicatorPointValueCompressedList = cityIndicatorKeys.stream()
				.filter(cityIndicatorKey -> point.getValue().get(cityIndicatorKey) != null)
				.map(cityIndicatorKey -> converter(cityIndicatorKey, point.getValue().get(cityIndicatorKey)))
				.collect(Collectors.toList());

		// this is required because stream is not handling byte[] just int[]
		byte[] indicatorValues = new byte[pointIndicatorKeys.size()];
		int j = 0;
		for (Byte listItem : indicatorPointValueCompressedList) {
			indicatorValues[j++] = listItem;
		}
		logger.debug("Indicator values for point: " + Arrays.toString(indicatorValues));

		// creating a single byte[] with active indicators + indicator values for this
		// point
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos = writeToByteArrayOutputStream(baos, pointActIndicators);
		baos = writeToByteArrayOutputStream(baos, indicatorValues);
		logger.debug("Byte array created for point: " + Arrays.toString(baos.toByteArray()));
		return baos.toByteArray();
	}

	protected byte converter(String indicatorKey, Double indicatorValue) {
		IndicatorData indicatorData = cityIndicatorData.get(indicatorKey);
		return rangeConverter.convertIndicatorRanges(indicatorData, indicatorValue);
	}

	private ByteArrayOutputStream writeToByteArrayOutputStream(ByteArrayOutputStream baos, byte[] itemToWrite) {
		try {
			baos.write(itemToWrite);
		} catch (IOException e) {
			logger.error("Writing to ByteArrayOutputStream failed in IndicatorResponseCreator", e);
			throw new GisettServiceException("Writing to ByteArrayOutputStream failed in IndicatorResponseCreator", e);
		}
		return baos;
	}

	protected List<Coordinates> getMatrixCorners(List<IndicatorPoint> points) {
		Coordinates upperLeft = new Coordinates((-Double.MAX_VALUE), Double.MAX_VALUE);
		Coordinates bottomRight = new Coordinates(Double.MAX_VALUE, (-Double.MAX_VALUE));
		for (IndicatorPoint point : points) {
			double coordinate = point.getPoint().getX();
			if (coordinate < upperLeft.getLongitude()) {
				upperLeft.setLongitude(coordinate);
			}
			if (coordinate > bottomRight.getLongitude()) {
				bottomRight.setLongitude(coordinate);
			}
			coordinate = point.getPoint().getY();
			if (coordinate > upperLeft.getLatitude()) {
				upperLeft.setLatitude(coordinate);
			}
			if (coordinate < bottomRight.getLatitude()) {
				bottomRight.setLatitude(coordinate);
			}
		}
		this.upperLeftCoord = upperLeft;
		Coordinates upperRight = new Coordinates(upperLeft.getLatitude(), bottomRight.getLongitude());
		Coordinates bottomLeft = new Coordinates(bottomRight.getLatitude(), upperLeft.getLongitude());
		logger.debug("Corners of point matrix: ");
		logger.debug(upperLeft.getLongitude() + " - " + upperLeft.getLatitude());
		logger.debug(upperRight.getLongitude() + " - " + upperRight.getLatitude());
		logger.debug(bottomLeft.getLongitude() + " - " + bottomLeft.getLatitude());
		logger.debug(bottomRight.getLongitude() + " - " + bottomRight.getLatitude());
		return Arrays.asList(upperLeft, upperRight, bottomLeft, bottomRight);
	}

	protected void setIndicatorDataByCity(CityData city) {
		this.cityIndicatorKeys = city.getIndicators().values().stream().map(IndicatorData::getKey)
				.collect(Collectors.toList());
		logger.debug(cityIndicatorKeys.toString());
		this.indicatorReprByteSize = (cityIndicatorKeys.size() + 7) / 8;
		this.cityIndicatorData = city.getIndicators();
		cityIndicatorData.forEach((key, value) -> logger.debug(key + " - " + value.getKey()));
	}
}
