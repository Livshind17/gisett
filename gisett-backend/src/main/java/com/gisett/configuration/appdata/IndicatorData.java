package com.gisett.configuration.appdata;

public class IndicatorData {

	private String key;
	private Double min;
	private Double max;
	private Boolean isHighValueBetter;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Boolean getIsHighValueBetter() {
		return isHighValueBetter;
	}

	public void setHighValueBetter(Boolean highValueBetter) {
		isHighValueBetter = highValueBetter;
	}
}
