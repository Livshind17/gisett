package com.gisett.configuration.appdata;

import java.util.List;

public class IndicatorCategoryData {

	private String key;
	private List<IndicatorGroupData> indicatorGroups;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<IndicatorGroupData> getIndicatorGroups() {
		return indicatorGroups;
	}

	public void setIndicatorGroups(List<IndicatorGroupData> indicatorGroups) {
		this.indicatorGroups = indicatorGroups;
	}
}
