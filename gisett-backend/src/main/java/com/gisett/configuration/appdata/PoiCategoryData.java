package com.gisett.configuration.appdata;

import java.util.List;

/**
 * Created by tuzes-boloni.kincso on 10/10/2018
 */
public class PoiCategoryData {

	private String key;
	private List<String> poiTypes;

	public PoiCategoryData() {
	}

	public PoiCategoryData(String key) {
		this.key = key;
	}

	public PoiCategoryData(String key, List<String> poiTypes) {
		this.key = key;
		this.poiTypes = poiTypes;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<String> getPoiTypes() {
		return poiTypes;
	}

	public void setPoiTypes(List<String> poiTypes) {
		this.poiTypes = poiTypes;
	}
}
