package com.gisett.configuration.appdata;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.gisett.domain.model.commons.Coordinates;

public class CityData {

	private String key;
	private Coordinates coordinatesNW;
	private Coordinates coordinatesSE;

	private Map<String, IndicatorData> indicators = new LinkedHashMap<>();

	private List<IndicatorCategoryData> indicatorCategories = new ArrayList<>();

	private List<PoiCategoryData> poiCategories = new ArrayList<>();

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Map<String, IndicatorData> getIndicators() {
		return indicators;
	}

	public void setIndicators(Map<String, IndicatorData> indicators) {
		this.indicators = indicators;
	}

	public List<IndicatorCategoryData> getIndicatorCategories() {
		return indicatorCategories;
	}

	public void setIndicatorCategories(List<IndicatorCategoryData> indicatorCategories) {
		this.indicatorCategories = indicatorCategories;
	}

	public List<PoiCategoryData> getPoiCategories() {
		return poiCategories;
	}

	public void setPoiCategories(List<PoiCategoryData> poiCategories) {
		this.poiCategories = poiCategories;
	}

	public Coordinates getCoordinatesNW() {
		return coordinatesNW;
	}

	public void setCoordinatesNW(Coordinates coordinatesNW) {
		this.coordinatesNW = coordinatesNW;
	}

	public Coordinates getCoordinatesSE() {
		return coordinatesSE;
	}

	public void setCoordinatesSE(Coordinates coordinatesSE) {
		this.coordinatesSE = coordinatesSE;
	}
}
