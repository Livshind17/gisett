package com.gisett.configuration.appdata;

import java.util.List;

public class IndicatorGroupData {

	private String key;
	private Boolean isActiveForCity;
	private List<String> indicators;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getActiveForCity() {
		return isActiveForCity;
	}

	public void setActiveForCity(Boolean activeForCity) {
		isActiveForCity = activeForCity;
	}

	public List<String> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<String> indicators) {
		this.indicators = indicators;
	}
}
