package com.gisett.configuration.importer;

import java.util.List;

import com.gisett.configuration.importer.common.BaseKeyConfig;

/**
 * Created by Bartha.Vivien on 8/29/2018.
 */
public class IndicatorCategoryFromConfig extends BaseKeyConfig {

	private String categoryName;
	private List<IndicatorGroupFromConfig> indicatorGroups;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<IndicatorGroupFromConfig> getIndicatorGroups() {
		return indicatorGroups;
	}

	public void setIndicatorGroups(List<IndicatorGroupFromConfig> indicatorGroups) {
		this.indicatorGroups = indicatorGroups;
	}
}
