package com.gisett.configuration.importer.common;

/**
 * Created by tuzes-boloni.kincso on 10/9/2018
 */
public interface IKeyConfig extends IConfig {

	String getKey();

	void setKey(String key);
}
