package com.gisett.configuration.importer;

/**
 * Created by kintzel.levente on 7/16/2018.
 */
public class IndicatorForCity extends IndicatorFromConfig {

	private String input;
	private String output;

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}
