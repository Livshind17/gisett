package com.gisett.configuration.importer;

import java.util.List;

import com.gisett.configuration.importer.common.BaseKeyConfig;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

public class PoiCategoriesFromConfig extends BaseKeyConfig {

	private String categoryName;
	private List<PoiTypeFromConfig> poiTypes;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<PoiTypeFromConfig> getPoiTypes() {
		return poiTypes;
	}

	public void setPoiTypes(List<PoiTypeFromConfig> poiTypes) {
		this.poiTypes = poiTypes;
	}
}
