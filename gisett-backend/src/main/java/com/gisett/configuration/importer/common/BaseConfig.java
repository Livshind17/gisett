package com.gisett.configuration.importer.common;

public abstract class BaseConfig implements IConfig {

	protected boolean skip;

	public boolean isSkip() {
		return skip;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}
}
