package com.gisett.configuration.importer;

import java.util.List;

import com.gisett.configuration.importer.common.BaseKeyConfig;
import com.gisett.domain.model.commons.Coordinates;

/**
 * Created by juhasz.levente on 8/17/2018.
 */
public class CityFromConfig extends BaseKeyConfig {

	private String cityName;
	private Coordinates coordinatesNW;
	private Coordinates coordinatesSE;
	private List<IndicatorForCity> indicators;
	private String locations;
	private List<String> pois;
	private List<String> poiTypes;
	private String properties;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Coordinates getCoordinatesNW() {
		return coordinatesNW;
	}

	public void setCoordinatesNW(Coordinates coordinatesNW) {
		this.coordinatesNW = coordinatesNW;
	}

	public Coordinates getCoordinatesSE() {
		return coordinatesSE;
	}

	public void setCoordinatesSE(Coordinates coordinatesSE) {
		this.coordinatesSE = coordinatesSE;
	}

	public List<IndicatorForCity> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<IndicatorForCity> indicators) {
		this.indicators = indicators;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public List<String> getPois() {
		return pois;
	}

	public void setPois(List<String> pois) {
		this.pois = pois;
	}

	public List<String> getPoiTypes() {
		return poiTypes;
	}

	public void setPoiTypes(List<String> poiTypes) {
		this.poiTypes = poiTypes;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}
}
