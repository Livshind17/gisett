package com.gisett.configuration.importer;

import java.util.List;

import com.gisett.configuration.importer.common.BaseKeyConfig;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */

public class IndicatorGroupFromConfig extends BaseKeyConfig {

	private String groupName;
	private List<IndicatorFromConfig> indicators;

	public List<IndicatorFromConfig> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<IndicatorFromConfig> indicatorFromConfigs) {
		this.indicators = indicatorFromConfigs;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}
}
