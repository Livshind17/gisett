package com.gisett.configuration.importer;

import com.gisett.configuration.importer.common.BaseKeyConfig;

/**
 * Created by Bartha.Vivien on 9/7/2018.
 */
public class IndicatorFromConfig extends BaseKeyConfig {

	private String indicatorName;
	private Double min;
	private Double max;
	private Boolean isHighValueBetter;

	public String getIndicatorName() {
		return indicatorName;
	}

	public void setIndicatorName(String indicatorName) {
		this.indicatorName = indicatorName;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Boolean getIsHighValueBetter() {
		return isHighValueBetter;
	}

	public void setIsHighValueBetter(Boolean highValueBetter) {
		isHighValueBetter = highValueBetter;
	}
}
