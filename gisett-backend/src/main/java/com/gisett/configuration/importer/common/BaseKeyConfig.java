package com.gisett.configuration.importer.common;

/**
 * Created by tuzes-boloni.kincso on 10/9/2018
 */
public class BaseKeyConfig extends BaseConfig implements IKeyConfig {

	protected String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
