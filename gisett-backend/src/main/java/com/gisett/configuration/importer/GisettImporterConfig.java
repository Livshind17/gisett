package com.gisett.configuration.importer;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by kintzel.levente on 7/16/2018.
 */

@ConfigurationProperties(prefix = "gisett")
@Configuration
public class GisettImporterConfig {

	private String dataImportLocationPath;

	private String dataExportLocationPath;

	private Boolean exportIndicatorsEnabled;
	private Boolean checkPoiDuplicates;

	private List<CityFromConfig> cities;
	private List<IndicatorCategoryFromConfig> indicatorConfigs;
	private List<PoiCategoriesFromConfig> poiConfigs;

	public List<IndicatorCategoryFromConfig> getIndicatorConfigs() {
		return indicatorConfigs;
	}

	public void setIndicatorConfigs(List<IndicatorCategoryFromConfig> indicatorConfigs) {
		this.indicatorConfigs = indicatorConfigs;
	}

	public List<PoiCategoriesFromConfig> getPoiConfigs() {
		return poiConfigs;
	}

	public void setPoiConfigs(List<PoiCategoriesFromConfig> poiConfigs) {
		this.poiConfigs = poiConfigs;
	}

	public String getDataImportLocationPath() {
		return dataImportLocationPath;
	}

	public void setDataImportLocationPath(String dataImportLocationPath) {
		this.dataImportLocationPath = dataImportLocationPath;
	}

	public String getDataExportLocationPath() {
		return dataExportLocationPath;
	}

	public void setDataExportLocationPath(String dataExportLocationPath) {
		this.dataExportLocationPath = dataExportLocationPath;
	}

	public Boolean getExportIndicatorsEnabled() {
		return exportIndicatorsEnabled;
	}

	public void setExportIndicatorsEnabled(Boolean exportIndicatorsEnabled) {
		this.exportIndicatorsEnabled = exportIndicatorsEnabled;
	}

	public List<CityFromConfig> getCities() {
		return cities;
	}

	public void setCities(List<CityFromConfig> cities) {
		this.cities = cities;
	}

	public Boolean getCheckPoiDuplicates() {
		return checkPoiDuplicates;
	}

	public void setCheckPoiDuplicates(Boolean checkPoiDuplicates) {
		this.checkPoiDuplicates = checkPoiDuplicates;
	}
}
