package com.gisett.configuration.importer;

import com.gisett.configuration.importer.common.BaseKeyConfig;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */
public class PoiTypeFromConfig extends BaseKeyConfig {

	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
