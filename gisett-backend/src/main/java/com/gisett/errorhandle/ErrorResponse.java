package com.gisett.errorhandle;

import java.util.Date;

import org.springframework.http.HttpStatus;

/**
 * Created by Bartha.Vivien on 10/17/2018.
 */
public class ErrorResponse {
	private HttpStatus status;
	private String errorDescription;
	private String errorMessage;
	private Date timestamp;

	ErrorResponse(HttpStatus status, String errorMessage) {
		this.status = status;
		this.errorMessage = errorMessage;
		this.timestamp = new Date();
	}

	ErrorResponse(HttpStatus status, String errorDescription, String errorMessage) {
		this.status = status;
		this.errorDescription = errorDescription;
		this.errorMessage = errorMessage;
		this.timestamp = new Date();
	}

	String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
