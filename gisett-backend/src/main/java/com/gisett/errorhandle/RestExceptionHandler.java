package com.gisett.errorhandle;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.gisett.exception.GisettBadDataException;
import com.gisett.exception.GisettRetrievalException;
import com.gisett.exception.GisettServiceException;

/**
 * Created by Bartha.Vivien on 10/17/2018.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(GisettBadDataException.class)
	protected ResponseEntity<Object> handleGisettBadDataExceptionApiLayer(GisettBadDataException ex) {
		String errorMessage = "GisettBadDataException was thrown. The requested parameters are missing or incorrect.";
		return buildResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage(), errorMessage));
	}

	@ExceptionHandler(GisettServiceException.class)
	protected ResponseEntity<Object> handleGisettServiceExceptionApiLayer(GisettServiceException ex) {
		String errorMessage = "GisettServiceException was thrown. An internal server error happened. Please try later.";
		return buildResponseEntity(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), errorMessage));
	}

	@ExceptionHandler(GisettRetrievalException.class)
	protected ResponseEntity<Object> handleGisettRetrievalExceptionApiLayer(GisettRetrievalException ex) {
		String errorMessage = "GisettRetrievalException was thrown. Data not found with the given parameters. Please check the coordinates or the cityKey parameter.";
		return buildResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage(), errorMessage));
	}

	private ResponseEntity<Object> buildResponseEntity(ErrorResponse apiError) {
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}
}
