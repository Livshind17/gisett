package com.gisett.api.assembler;

import org.springframework.stereotype.Component;

import com.gisett.api.dto.LocationDTO;
import com.gisett.domain.model.Location;

@Component
public class LocationDTOAssember implements IControllerDTOAssembler<LocationDTO, Location> {

	public LocationDTO createDTO(Location location) {
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setName(location.getName());
		locationDTO.setCoordinates(location.getCoordinates());
		return locationDTO;
	}
}
