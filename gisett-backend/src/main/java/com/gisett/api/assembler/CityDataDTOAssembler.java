package com.gisett.api.assembler;

import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.gisett.api.dto.CityDataDTO;
import com.gisett.configuration.appdata.CityData;
import com.gisett.configuration.appdata.IndicatorData;

@Component
public class CityDataDTOAssembler implements IControllerDTOAssembler<CityDataDTO, CityData> {

	public CityDataDTO createDTO(CityData data) {
		CityDataDTO dataDTO = new CityDataDTO();
		dataDTO.setKey(data.getKey());
		dataDTO.setCoordinatesNW(data.getCoordinatesNW());
		dataDTO.setCoordinatesSE(data.getCoordinatesSE());
		dataDTO.setIndicatorCategories(data.getIndicatorCategories());
		dataDTO.setIndicators(
		        data.getIndicators()
		            .values()
		            .stream()
		            .map(IndicatorData::getKey)
		            .collect(Collectors.toList()));
		dataDTO.setPoiCategories(data.getPoiCategories());
		return dataDTO;
	}

}
