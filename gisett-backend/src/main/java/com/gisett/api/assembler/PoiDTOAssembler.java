package com.gisett.api.assembler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.mongodb.core.geo.GeoJsonLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonMultiLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonMultiPolygon;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.stereotype.Component;

import com.gisett.api.dto.PoiDTO;
import com.gisett.domain.model.Poi;
import com.gisett.domain.model.PoiLine;
import com.gisett.domain.model.PoiPoint;
import com.gisett.domain.model.PoiPolygon;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.model.commons.Geometry;
import com.gisett.domain.model.commons.GeometryType;

/**
 * Created by tuzes-boloni.kincso on 10/9/2018
 */
@Component
public class PoiDTOAssembler implements IControllerDTOAssembler<PoiDTO, Poi> {

	public PoiDTO createDTO(Poi poi) {
		PoiDTO poiDTO = new PoiDTO();
		poiDTO.setName(poi.getName());
		poiDTO.setType(poi.getType());
		updateGeometry(poiDTO, poi);
		return poiDTO;
	}

	private void updateGeometry(PoiDTO dto, Poi model) {
		Geometry geom = new Geometry();
		if (model instanceof PoiPoint) {
			geom.setType(GeometryType.POINT);
			GeoJsonPoint jsonPoint = (GeoJsonPoint) model.getCoordinates();
			Coordinates point = new Coordinates(jsonPoint.getY(), jsonPoint.getX());
			List<Coordinates> points = Arrays.asList(point);
			geom.setCoordinates(Arrays.asList(points));
		} else if (model instanceof PoiLine) {
			geom.setType(GeometryType.LINE);
			GeoJsonMultiLineString lines = (GeoJsonMultiLineString) model.getCoordinates();
			geom.setCoordinates(getPointsFromMultiLine(lines));
		} else if (model instanceof PoiPolygon) {
			geom.setType(GeometryType.POLYGON);
			GeoJsonMultiPolygon polygons = (GeoJsonMultiPolygon) model.getCoordinates();
			geom.setCoordinates(getPointsFromMultiPolygon(polygons));
		}
		dto.setGeometry(geom);
	}

	private List<List<Coordinates>> getPointsFromMultiPolygon(GeoJsonMultiPolygon polygons) {
		return polygons.getCoordinates()
		               .stream()
		               .map(GeoJsonPolygon::getCoordinates)
		               .flatMap(Collection::stream)
		               .map(line -> getPointsFromLine(line))
		               .collect(Collectors.toList());

	}

	private List<List<Coordinates>> getPointsFromMultiLine(GeoJsonMultiLineString multiLine) {
		List<List<Coordinates>> coordinates = new ArrayList<>();
		multiLine.getCoordinates()
		         .forEach(line -> coordinates.add(getPointsFromLine(line)));
		return coordinates;
	}

	private List<Coordinates> getPointsFromLine(GeoJsonLineString line) {
		return line.getCoordinates()
		           .stream()
		           .map(point -> new Coordinates(point.getY(), point.getX()))
		           .collect(Collectors.toList());
	}
}
