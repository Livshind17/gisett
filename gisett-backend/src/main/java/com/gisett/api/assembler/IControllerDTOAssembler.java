package com.gisett.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

public interface IControllerDTOAssembler<DTO, Model> {

	DTO createDTO(Model model);

	default List<DTO> createDTOList(List<Model> models) {
		return models.stream()
		             .map(model -> createDTO(model))
		             .collect(Collectors.toList());
	}
}
