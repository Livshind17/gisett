package com.gisett.api.dto;

import com.gisett.domain.model.commons.Geometry;

/**
 * Created by tuzes-boloni.kincso on 10/9/2018
 */
public class PoiDTO {

	private String type;
	private String name;
	private Geometry geometry;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}
}
