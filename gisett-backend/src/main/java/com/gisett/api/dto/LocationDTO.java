package com.gisett.api.dto;

import com.gisett.domain.model.commons.Coordinates;

public class LocationDTO {

	private String name;
	private Coordinates coordinates;

	public LocationDTO() {
	}

	public LocationDTO(String name, Coordinates coordinates) {
		this.name = name;
		this.coordinates = coordinates;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
}
