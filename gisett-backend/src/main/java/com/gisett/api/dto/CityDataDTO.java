package com.gisett.api.dto;

import java.util.List;

import com.gisett.configuration.appdata.IndicatorCategoryData;
import com.gisett.configuration.appdata.PoiCategoryData;
import com.gisett.domain.model.commons.Coordinates;

public class CityDataDTO {

	private String key;
	private Coordinates coordinatesNW;
	private Coordinates coordinatesSE;
	private List<String> indicators;
	private List<IndicatorCategoryData> indicatorCategories;
	private List<PoiCategoryData> poiCategories;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Coordinates getCoordinatesNW() {
		return coordinatesNW;
	}

	public void setCoordinatesNW(Coordinates coordinatesNW) {
		this.coordinatesNW = coordinatesNW;
	}

	public Coordinates getCoordinatesSE() {
		return coordinatesSE;
	}

	public void setCoordinatesSE(Coordinates coordinatesSE) {
		this.coordinatesSE = coordinatesSE;
	}

	public List<String> getIndicators() {
		return indicators;
	}

	public void setIndicators(List<String> indicators) {
		this.indicators = indicators;
	}

	public List<IndicatorCategoryData> getIndicatorCategories() {
		return indicatorCategories;
	}

	public void setIndicatorCategories(List<IndicatorCategoryData> indicatorCategories) {
		this.indicatorCategories = indicatorCategories;
	}

	public List<PoiCategoryData> getPoiCategories() {
		return poiCategories;
	}

	public void setPoiCategories(List<PoiCategoryData> poiCategories) {
		this.poiCategories = poiCategories;
	}
}
