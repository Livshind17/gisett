package com.gisett.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gisett.api.assembler.PoiDTOAssembler;
import com.gisett.api.dto.PoiDTO;
import com.gisett.domain.model.Poi;
import com.gisett.domain.service.PoiService;

/**
 * Created by tuzes-boloni.kincso on 10/9/2018
 */

@RestController
@RequestMapping("/api")
public class PoiController {

	private Logger logger = LoggerFactory.getLogger(PoiController.class);

	@Autowired
	PoiService poiService;

	@Autowired
	PoiDTOAssembler poiDTOAssembler;

	@RequestMapping(value = "/cities/{cityKey}/pois/points", method = RequestMethod.GET)
	public List<PoiDTO> findPoisNearPointByTypes(@PathVariable String cityKey, @RequestParam("lng") double lng,
	        @RequestParam("lat") double lat, @RequestParam("distance") double distance,
	        @RequestParam("types") List<String> types) {
		long startTime = System.currentTimeMillis();
		List<Poi> poiList = poiService.findPoisNearPointByTypes(cityKey, lng, lat, distance, types);
		logger.debug(" TIME to load POIs: " + (System.currentTimeMillis() - startTime));
		List<PoiDTO> poiDTOList = poiDTOAssembler.createDTOList(poiList);
		logger.debug(" TIME to return POIs: " + (System.currentTimeMillis() - startTime));
		return poiDTOList;
	}
}
