package com.gisett.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gisett.api.assembler.CityDataDTOAssembler;
import com.gisett.api.dto.CityDataDTO;
import com.gisett.domain.model.City;
import com.gisett.domain.service.CityService;

/**
 * Created by juhasz.levente on 8/17/2018.
 */
@RestController
@RequestMapping("/api/cities")
public class CityController {

	private Logger logger = LoggerFactory.getLogger(CityController.class);

	@Autowired
	private CityService service;

	@Autowired
	private CityDataDTOAssembler assembler;

	@RequestMapping(method = RequestMethod.GET)
	public List<String> getAllCity() {
		List<String> cities = new ArrayList<>();
		cities = service.findAll()
		                .stream()
		                .map(City::getKey)
		                .collect(Collectors.toList());
		return cities;
	}

	@RequestMapping(value = "/{cityKey}/configurations", method = RequestMethod.GET)
	public CityDataDTO getCityConfigurations(@PathVariable("cityKey") String cityKey) {
		CityDataDTO cityDataDTO = new CityDataDTO();
		cityDataDTO = assembler.createDTO(service.getCityData(cityKey));
		return cityDataDTO;
	}
}
