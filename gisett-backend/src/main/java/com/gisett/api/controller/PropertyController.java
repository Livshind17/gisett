package com.gisett.api.controller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;
import com.gisett.domain.service.PropertyService;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by Bartha.Vivien on 10/16/2018.
 */
@RestController
@RequestMapping("/api")
public class PropertyController {

	private static final Integer PAGE_SIZE = 1000;
	private static final Integer PAGE_NUMBER = 0;
	private static final Integer DISTANCE = 1000;

	private Logger logger = LoggerFactory.getLogger(PropertyController.class);

	@Autowired
	private PropertyService service;

	@RequestMapping(value = "/cities/{cityKey}/properties", method = RequestMethod.POST)
	public Page<Property> getFilteredPropertyList(@RequestBody PropertyFilter filter,
	        @PathVariable("cityKey") String cityKey, @RequestParam("lng") Double lng, @RequestParam("lat") Double lat,
	        @RequestParam("distance") Double distance, @RequestParam("size") Integer size) {

		if (StringUtils.isBlank(cityKey)) {
			throw new GisettBadDataException("Property filter: CityKey is either null, empty or whitespace!");
		}

		size = size == null ? PAGE_SIZE : size;
		distance = distance == null ? DISTANCE : distance;

		long startTime = System.currentTimeMillis();
		Page<Property> properties = service.findAllFiltered(filter, cityKey, PAGE_NUMBER, size, lat, lng, distance);
		long stopTime = System.currentTimeMillis();
		logger.debug(" TIME to execute getFilteredPropertyList: " + (stopTime - startTime));
		return properties;
	}
}
