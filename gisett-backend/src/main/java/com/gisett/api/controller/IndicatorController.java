package com.gisett.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;
import com.gisett.domain.service.IndicatorPointService;

/**
 * Created by Bartha.Vivien on 8/29/2018.
 */
@RestController
@RequestMapping("/api")
public class IndicatorController {

	private Logger logger = LoggerFactory.getLogger(IndicatorController.class);

	@Autowired
	private IndicatorPointService indicatorPointService;

	// This was a temporary solution to render some heatmap
	@Deprecated
	@RequestMapping(value = "/indicators/nearByPoints/", method = RequestMethod.GET)
	public List<IndicatorPoint> getNearByIndicatorPoints(@RequestParam("lng") double lng,
	        @RequestParam("lat") double lat, @RequestParam("distance") double distance) {
		long startTime = System.currentTimeMillis();
		List<IndicatorPoint> list = indicatorPointService.nearByPoints(lng, lat, distance);
		long stopTime = System.currentTimeMillis();
		logger.debug(" TIME to execute nearByPoints: " + (stopTime - startTime));
		return list;
	}

	@RequestMapping(value = "/cities/{cityKey}/indicators/nearest-point", method = RequestMethod.GET)
	public IndicatorPoint getNearestIndicatorPoint(@PathVariable("cityKey") String cityKey,
	        @RequestParam("lng") double lng, @RequestParam("lat") double lat) {
		long startTime = System.currentTimeMillis();
		IndicatorPoint indicatorPoint = indicatorPointService.nearestByPointWithRangeConvert(lng, lat, cityKey);
		long stopTime = System.currentTimeMillis();
		logger.debug(" TIME to execute nearestByCoordinates: " + (stopTime - startTime));
		return indicatorPoint;
	}

	@Deprecated
	@RequestMapping(value = "/indicators/withinSquarePoints/", method = RequestMethod.GET)
	public List<IndicatorPoint> getWithinSquarePoints(@RequestParam("bottomLeftLng") double bottomLeftLng,
	        @RequestParam("bottomLeftLat") double bottomLeftLat, @RequestParam("upperRightLng") double upperRightLng,
	        @RequestParam("upperRightLat") double upperRightLat) {
		long startTime = System.currentTimeMillis();
		List<IndicatorPoint> list = indicatorPointService.withinSquarePoints(bottomLeftLng, bottomLeftLat,
		        upperRightLng, upperRightLat);
		long stopTime = System.currentTimeMillis();
		logger.debug(" TIME to execute nearByPoints: " + (stopTime - startTime));
		return list;
	}

	@RequestMapping(value = "/cities/{cityKey}/indicators/points", method = RequestMethod.GET)
	public IndicatorsResponse getIndicatorsForHeatmap(@PathVariable("cityKey") String cityKey,
	        @RequestParam("lng") double pointLng, @RequestParam("lat") double pointLat,
	        @RequestParam("distance") double distance) {
		return indicatorPointService.getIndicatorsForHeatmap(cityKey, pointLng, pointLat, distance);
	}
}
