package com.gisett.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gisett.api.assembler.LocationDTOAssember;
import com.gisett.api.dto.LocationDTO;
import com.gisett.domain.service.LocationService;

/**
 * Created by juhasz.levente on 8/21/2018.
 */
@RestController
@RequestMapping("/api")
public class LocationController {

	@Autowired
	LocationService service;

	@Autowired
	LocationDTOAssember locationAssembler;

	@RequestMapping(value = "/cities/{cityKey}/locations", method = RequestMethod.GET)
	public List<LocationDTO> getLocations(@PathVariable("cityKey") String cityKey, @RequestParam("name") String name) {
		return locationAssembler.createDTOList(service.findByNameLikeAndCityKey(name, cityKey));
	}

}
