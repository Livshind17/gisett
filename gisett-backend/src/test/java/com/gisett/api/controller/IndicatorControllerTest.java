package com.gisett.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.NestedServletException;

import com.gisett.configuration.util.SimpleCORSFilter;
import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.IndicatorPointService;
import com.gisett.exception.GisettRetrievalException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tuzes-boloni.kincso on 10/23/2018
 */
public class IndicatorControllerTest {

	private static final String CITY_KEY = "US-CA-LA";
	private static final Coordinates COORD = new Coordinates(-118.653, 34.527);
	private static final double RADIUS = 10000;

	private MockMvc mockMvc;

	@Mock
	private IndicatorPointService indicatorPointService;
	@InjectMocks
	private IndicatorController indicatorController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(indicatorController)
		                         .addFilters(new SimpleCORSFilter())
		                         .build();
	}

	@Test
	public void getNearestIndicatorPointTest() throws Exception {
		Map<String, Double> indicators = new HashMap<>();
		indicators.put("asthma", 56.0);
		when(indicatorPointService.nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY))
		                                                                                                               .thenReturn(new IndicatorPoint(
		                                                                                                                       new GeoJsonPoint(
		                                                                                                                               COORD.getLongitude(),
		                                                                                                                               COORD.getLatitude()),
		                                                                                                                       indicators));

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		mockMvc.perform(get("/api/cities/{cityKey}/indicators/nearest-point", CITY_KEY).params(parameters))
		       .andExpect(jsonPath("$.point.y").value(COORD.getLatitude()))
		       .andExpect(jsonPath("$.point.x").value(COORD.getLongitude()))
		       .andExpect(jsonPath("$.value.asthma").value(56.0));

		verify(indicatorPointService, times(1)).nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY);
		verifyNoMoreInteractions(indicatorPointService);
	}

	@Test(expected = NestedServletException.class)
	public void getNearestIndicatorPointTestRetrievalException() throws Exception {
		Map<String, Double> indicators = new HashMap<>();
		indicators.put("asthma", 56.0);
		when(indicatorPointService.nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY))
		                                                                                                               .thenThrow(
		                                                                                                                       GisettRetrievalException.class);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		mockMvc.perform(get("/api/cities/{cityKey}/indicators/nearest-point", CITY_KEY).params(parameters))
		       .andExpect(status().isNotFound())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8));

		verify(indicatorPointService, times(1)).nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY);
		verifyNoMoreInteractions(indicatorPointService);
	}

	@Test(expected = NestedServletException.class)
	public void getNearestIndicatorPointTestServiceException() throws Exception {
		Map<String, Double> indicators = new HashMap<>();
		indicators.put("asthma", 56.0);
		when(indicatorPointService.nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY))
		                                                                                                               .thenThrow(
		                                                                                                                       GisettRetrievalException.class);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		mockMvc.perform(get("/api/cities/{cityKey}/indicators/nearest-point", CITY_KEY).params(parameters))
		       .andExpect(status().isInternalServerError())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8));

		verify(indicatorPointService, times(1)).nearestByPointWithRangeConvert(COORD.getLongitude(), COORD.getLatitude(), CITY_KEY);
		verifyNoMoreInteractions(indicatorPointService);
	}

	@Test
	public void getIndicatorsForHeatmapTest() throws Exception {
		IndicatorsResponse response = new IndicatorsResponse("Encoded Response", COORD, 15);
		when(indicatorPointService.getIndicatorsForHeatmap(CITY_KEY, COORD.getLongitude(), COORD.getLatitude(), RADIUS))
		                                                                                                                .thenReturn(response);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(RADIUS));
		mockMvc.perform(get("/api/cities/{cityKey}/indicators/points", CITY_KEY).params(parameters))
		       .andExpect(jsonPath("$.indicatorsEncoded").value("Encoded Response"))
		       .andExpect(jsonPath("$.coordinates.latitude").value(COORD.getLatitude()))
		       .andExpect(jsonPath("$.coordinates.longitude").value(COORD.getLongitude()))
		       .andExpect(jsonPath("$.columnCount").value(15));

		verify(indicatorPointService, times(1)).getIndicatorsForHeatmap(CITY_KEY, COORD.getLongitude(),
		        COORD.getLatitude(), RADIUS);
		verifyNoMoreInteractions(indicatorPointService);
	}

	@Test(expected = NestedServletException.class)
	public void getIndicatorsForHeatmapTestRetrievalException() throws Exception {
		when(indicatorPointService.getIndicatorsForHeatmap(CITY_KEY, COORD.getLongitude(), COORD.getLatitude(), RADIUS))
		                                                                                                                .thenThrow(
		                                                                                                                        GisettRetrievalException.class);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(RADIUS));
		mockMvc.perform(get("/api/cities/{cityKey}/indicators/points", CITY_KEY).params(parameters))
		       .andExpect(status().isNotFound());

		verify(indicatorPointService, times(1)).getIndicatorsForHeatmap(CITY_KEY, COORD.getLongitude(),
		        COORD.getLatitude(), RADIUS);
		verifyNoMoreInteractions(indicatorPointService);
	}
}
