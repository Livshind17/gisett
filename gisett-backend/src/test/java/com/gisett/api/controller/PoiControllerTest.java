package com.gisett.api.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.NestedServletException;

import com.gisett.api.assembler.PoiDTOAssembler;
import com.gisett.api.dto.PoiDTO;
import com.gisett.configuration.util.SimpleCORSFilter;
import com.gisett.domain.model.Poi;
import com.gisett.domain.model.PoiPoint;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.model.commons.Geometry;
import com.gisett.domain.service.PoiService;
import com.gisett.exception.GisettBadDataException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tuzes-boloni.kincso on 10/29/2018
 */
public class PoiControllerTest {

	private static final String CITY_KEY = "US-CA-LA";
	private static final Coordinates COORD = new Coordinates(34.527, -118.653);
	private static final double DISTANCE = 10000;
	private static final List<String> TYPES = new ArrayList<>(Arrays.asList("train_stations"));
	private static final String NAME = "name";

	private List<Poi> pois;
	private List<PoiDTO> poiDTOs;
	private MockMvc mockMvc;

	@Mock
	private PoiService poiService;
	@Mock
	private PoiDTOAssembler poiAssembler;
	@InjectMocks
	private PoiController poiController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
		                         .standaloneSetup(poiController)
		                         .addFilters(new SimpleCORSFilter())
		                         .build();

		pois = new ArrayList<>(Arrays.asList(new PoiPoint()));
		Geometry geometry = new Geometry();
		geometry.setCoordinates(Arrays.asList(Arrays.asList(COORD)));
		PoiDTO poiDTO = new PoiDTO();
		poiDTO.setType(TYPES.get(0));
		poiDTO.setName(NAME);
		poiDTO.setGeometry(geometry);
		poiDTOs = new ArrayList<>(Arrays.asList(poiDTO));
	}

	@Test
	public void findPoisNearPointByTypesTest() throws Exception {
		when(poiService.findPoisNearPointByTypes(CITY_KEY, COORD.getLongitude(), COORD.getLatitude(), DISTANCE, TYPES)).thenReturn(pois);
		when(poiAssembler.createDTOList(pois)).thenReturn(poiDTOs);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(DISTANCE));
		parameters.add("types", TYPES.get(0));
		mockMvc.perform(get("/api/cities/{cityKey}/pois/points", CITY_KEY).params(parameters))
		       .andExpect(status().isOk())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(jsonPath("$", Matchers.hasSize(poiDTOs.size())))
		       .andExpect(jsonPath("$[0].name").value(NAME))
		       .andExpect(jsonPath("$[0].type").value(TYPES.get(0)))
		       .andExpect(jsonPath("$[0].geometry.coordinates[0][0].latitude").value(COORD.getLatitude()))
		       .andExpect(jsonPath("$[0].geometry.coordinates[0][0].longitude").value(COORD.getLongitude()));

		verify(poiAssembler, times(1)).createDTOList(pois);
		verifyNoMoreInteractions(poiAssembler);
	}

	@Test(expected = NestedServletException.class)
	public void getLocationsTestServiceException() throws Exception {
		when(poiService.findPoisNearPointByTypes(CITY_KEY, COORD.getLongitude(), COORD.getLatitude(), DISTANCE, TYPES)).thenThrow(GisettBadDataException.class);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(DISTANCE));
		parameters.add("types", TYPES.get(0));
		mockMvc.perform(get("/api/cities/{cityKey}/pois/points", CITY_KEY).params(parameters))
		       .andExpect(status().isBadRequest());
	}
}
