package com.gisett.api.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import com.gisett.api.assembler.LocationDTOAssember;
import com.gisett.api.dto.LocationDTO;
import com.gisett.configuration.util.SimpleCORSFilter;
import com.gisett.domain.model.Location;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.LocationService;
import com.gisett.exception.GisettServiceException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tuzes-boloni.kincso on 9/4/2018
 */
public class LocationControllerTest {

	private static final String KEY1 = "LA";
	private static final String KEY2 = "NY";
	private static final String NAME1 = "Hollywood";
	private static final String NAME2 = "Manhattan";

	private List<Location> locations;
	private List<LocationDTO> locationDTOs;
	private MockMvc mockMvc;

	@Mock
	private LocationService locationService;
	@Mock
	private LocationDTOAssember locationAssembler;
	@InjectMocks
	private LocationController locationController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
		                         .standaloneSetup(locationController)
		                         .addFilters(new SimpleCORSFilter())
		                         .build();

		Location location1 = new Location(1L, NAME1, KEY1, new Coordinates(5, 10));
		LocationDTO locationDTO1 = new LocationDTO(NAME1, new Coordinates(5, 10));
		Location location2 = new Location(2L, NAME2, KEY2, new Coordinates(35, 50));
		LocationDTO locationDTO2 = new LocationDTO(NAME2, new Coordinates(35, 50));
		locations = new ArrayList<>(Arrays.asList(location1, location2));
		locationDTOs = new ArrayList<>(Arrays.asList(locationDTO1, locationDTO2));
	}

	@Test
	public void getLocationsTest() throws Exception {
		when(locationService.findByNameLikeAndCityKey(NAME1, KEY1)).thenReturn(locations);
		when(locationAssembler.createDTOList(locations)).thenReturn(locationDTOs);

		mockMvc.perform(get("/api/cities/{cityKey}/locations", KEY1).param("name", NAME1))
		       .andExpect(status().isOk())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(jsonPath("$", Matchers.hasSize(locations.size())))
		       .andExpect(jsonPath("$[0].name").value(NAME1))
		       .andExpect(jsonPath("$[0].coordinates.latitude").value(5))
		       .andExpect(jsonPath("$[0].coordinates.longitude").value(10))
		       .andExpect(jsonPath("$[1].name").value(NAME2))
		       .andExpect(jsonPath("$[1].coordinates.latitude").value(35))
		       .andExpect(jsonPath("$[1].coordinates.longitude").value(50));

		verify(locationService, times(1)).findByNameLikeAndCityKey(NAME1, KEY1);
		verifyNoMoreInteractions(locationService);
		verify(locationAssembler, times(1)).createDTOList(locations);
		verifyNoMoreInteractions(locationAssembler);
	}

	@Test
	public void getLocationsTestWithEmptyListResult() throws Exception {
		when(locationService.findByNameLikeAndCityKey(NAME1, KEY1)).thenReturn(Collections.emptyList());
		when(locationAssembler.createDTOList(Collections.emptyList())).thenReturn(Collections.emptyList());

		mockMvc.perform(get("/api/cities/{cityKey}/locations", KEY1).param("name", NAME1))
		       .andExpect(jsonPath("$", Matchers.hasSize(Collections.emptyList()
		                                                            .size())));

		verify(locationService, times(1)).findByNameLikeAndCityKey(NAME1, KEY1);
		verifyNoMoreInteractions(locationService);
		verify(locationAssembler, times(1)).createDTOList(Collections.emptyList());
		verifyNoMoreInteractions(locationAssembler);
	}

	@Test(expected = NestedServletException.class)
	public void getLocationsTestServiceException() throws Exception {
		when(locationService.findByNameLikeAndCityKey(NAME1, KEY1)).thenThrow(GisettServiceException.class);

		mockMvc.perform(get("/api/cities/{cityKey}/locations", KEY1).param("name", NAME1))
		       .andExpect(status().isInternalServerError());

		verify(locationService, times(1)).findByNameLikeAndCityKey(NAME1, KEY1);
		verifyNoMoreInteractions(locationService);
	}

}
