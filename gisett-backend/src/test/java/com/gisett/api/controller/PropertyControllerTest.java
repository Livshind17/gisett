package com.gisett.api.controller;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.NestedServletException;

import com.google.gson.Gson;

import com.gisett.configuration.util.SimpleCORSFilter;
import com.gisett.domain.model.Category;
import com.gisett.domain.model.Property;
import com.gisett.domain.model.PropertyFilter;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.PropertyService;
import com.gisett.exception.GisettServiceException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tuzes-boloni.kincso on 10/29/2018
 */
public class PropertyControllerTest {

	private static final String CITY_KEY = "US-CA-LA";
	private static final Coordinates COORD = new Coordinates(34.527, -118.653);
	private static final double DISTANCE = 10000;
	private static final Integer SIZE = 15;
	private static final String DESC = "desc";

	private Page<Property> properties;
	private PropertyFilter filter;
	private MockMvc mockMvc;

	@Mock
	private PropertyService propertyService;
	@InjectMocks
	private PropertyController propertyController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
		                         .standaloneSetup(propertyController)
		                         .addFilters(new SimpleCORSFilter())
		                         .build();
		Property property = new Property();
		property.setBedrooms(2);
		property.setDescription(DESC);
		property.setCategory(Category.RENT);
		properties = new PageImpl<>(Arrays.asList(property));
		filter = new PropertyFilter();
		filter.setToPrice(50000);
	}

	@Test
	public void getFilteredPropertyListTest() throws Exception {
		when(propertyService.findAllFiltered(any(PropertyFilter.class), eq(CITY_KEY), eq(0), eq(SIZE), eq(COORD.getLatitude()), eq(COORD.getLongitude()),
		        eq(DISTANCE))).thenReturn(properties);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(DISTANCE));
		parameters.add("size", Integer.toString(SIZE));
		mockMvc.perform((post("/api/cities/{cityKey}/properties", CITY_KEY).params(parameters))
		                                                                                   .contentType(MediaType.APPLICATION_JSON_UTF8)
		                                                                                   .content(new Gson().toJson(filter))
		                                                                                   .contentType(MediaType.APPLICATION_JSON_UTF8)
		                                                                                   .accept(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(status().isOk())
		       .andExpect(jsonPath("$.content[0].bedrooms").value(2))
		       .andExpect(jsonPath("$.content[0].description").value(DESC))
		       .andExpect(jsonPath("$.content[0].category").value("RENT"));

		verify(propertyService, times(1)).findAllFiltered(any(PropertyFilter.class), eq(CITY_KEY), eq(0), eq(SIZE), eq(COORD.getLatitude()),
		        eq(COORD.getLongitude()), eq(DISTANCE));
		verifyNoMoreInteractions(propertyService);
	}

	@Test(expected = NestedServletException.class)
	public void getFilteredPropertyListServiceExceptionTest() throws Exception {
		when(propertyService.findAllFiltered(any(PropertyFilter.class), eq(CITY_KEY), eq(0), eq(SIZE), eq(COORD.getLatitude()), eq(COORD.getLongitude()),
		        eq(DISTANCE))).thenThrow(GisettServiceException.class);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("lng", Double.toString(COORD.getLongitude()));
		parameters.add("lat", Double.toString(COORD.getLatitude()));
		parameters.add("distance", Double.toString(DISTANCE));
		parameters.add("size", Integer.toString(SIZE));
		mockMvc.perform((post("/api/cities/{cityKey}/properties", CITY_KEY).params(parameters))
		                                                                                   .contentType(MediaType.APPLICATION_JSON_UTF8)
		                                                                                   .content(new Gson().toJson(filter))
		                                                                                   .contentType(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(status().isInternalServerError());
	}
}
