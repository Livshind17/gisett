package com.gisett.api.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import com.gisett.api.assembler.CityDataDTOAssembler;
import com.gisett.api.dto.CityDataDTO;
import com.gisett.configuration.appdata.PoiCategoryData;
import com.gisett.configuration.util.SimpleCORSFilter;
import com.gisett.domain.model.City;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.CityService;
import com.gisett.exception.GisettRetrievalException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tuzes-boloni.kincso on 9/4/2018
 */
public class CityControllerTest {

	private static final String KEY = "LA";
	private static final String NAME = "Los Angeles";
	private static final Coordinates COORD_NW = new Coordinates(5, 10);
	private static final Coordinates COORD_SE = new Coordinates(15, 30);
	private static final List<PoiCategoryData> POI_CATEGORY_LIST = Arrays.asList(new PoiCategoryData("health"));

	private List<City> cities;
	private CityDataDTO cityDataDTO;
	private MockMvc mockMvc;

	@Mock
	private CityService cityService;
	@Mock
	private CityDataDTOAssembler assembler;
	@InjectMocks
	private CityController cityController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
		                         .standaloneSetup(cityController)
		                         .addFilters(new SimpleCORSFilter())
		                         .build();

		// creating dummy city list
		City ny = new City("NY", "New York", new Coordinates(4, 11), new Coordinates(20, 35));
		City la = new City(KEY, NAME, COORD_NW, COORD_SE);
		cities = new ArrayList<>(Arrays.asList(ny, la));

		// creating dummy city data
		cityDataDTO = new CityDataDTO();
		cityDataDTO.setKey(KEY);
		cityDataDTO.setPoiCategories(POI_CATEGORY_LIST);
		cityDataDTO.setIndicators(new ArrayList<String>());
	}

	@Test
	public void getAllCityTest() throws Exception {
		when(cityService.findAll()).thenReturn(cities);
		mockMvc.perform(get("/api/cities"))
		       .andExpect(status().isOk())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(jsonPath("$", Matchers.hasSize(cities.size())))
		       .andExpect(jsonPath("$[1]").value(KEY));

		verify(cityService, times(1)).findAll();
		verifyNoMoreInteractions(cityService);
	}

	@Test
	public void getCityConfigurationsTest() throws Exception {
		when(assembler.createDTO(any())).thenReturn(cityDataDTO);
		mockMvc.perform(get("/api/cities/{cityKey}/configurations", KEY))
		       .andExpect(status().isOk())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8))
		       .andExpect(jsonPath("key").value(KEY))
		       .andExpect(jsonPath("poiCategories[0].key").value(POI_CATEGORY_LIST.get(0)
		                                                                          .getKey()))
		       .andExpect(jsonPath("indicators").isEmpty())
		       .andExpect(jsonPath("indicatorCategories").isEmpty());

		verify(assembler, times(1)).createDTO(any());
		verifyNoMoreInteractions(assembler);
	}

	@Test(expected = NestedServletException.class)
	public void getCityConfigurationsTestRetrievalException() throws Exception {
		when(assembler.createDTO(any())).thenThrow(GisettRetrievalException.class);
		mockMvc.perform(get("/api/cities/{cityKey}/configurations", KEY))
		       .andExpect(status().isNotFound())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8));

		verify(assembler, times(1)).createDTO(any());
		verifyNoMoreInteractions(assembler);
	}

	@Test(expected = NestedServletException.class)
	public void getCityConfigurationsTestServiceException() throws Exception {
		when(assembler.createDTO(any())).thenThrow(GisettRetrievalException.class);
		mockMvc.perform(get("/api/cities/{cityKey}/configurations", KEY))
		       .andExpect(status().isInternalServerError())
		       .andExpect(MockMvcResultMatchers.content()
		                                       .contentType(MediaType.APPLICATION_JSON_UTF8));

		verify(assembler, times(1)).createDTO(any());
		verifyNoMoreInteractions(assembler);
	}
}
