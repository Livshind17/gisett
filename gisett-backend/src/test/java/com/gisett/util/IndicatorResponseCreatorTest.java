package com.gisett.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import com.gisett.configuration.appdata.CityData;
import com.gisett.configuration.appdata.IndicatorData;
import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.IndicatorsResponse;
import com.gisett.domain.model.commons.Coordinates;

import static org.junit.Assert.assertEquals;

/**
 * Created by tuzes-boloni.kincso on 10/23/2018
 */
public class IndicatorResponseCreatorTest {

	private static final Double DELTA = 0.001;
	private IndicatorResponseCreator responseCreator;

	@Before
	public void setUp() {
		responseCreator = new IndicatorResponseCreator();
	}

	// setIndicatorDataByCity tests
	@Test
	public void setIndicatorDataByCityTest() {
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("transport", createIndicatorData("bus", 0.0, 50.0, true));
		indicators.put("health", createIndicatorData("hospitals", 0.0, 50.0, true));

		CityData cityData = new CityData();
		cityData.setIndicators(indicators);

		responseCreator.setIndicatorDataByCity(cityData);
		assertEquals(Arrays.asList("hospitals", "bus"), responseCreator.cityIndicatorKeys);
		assertEquals(indicators, responseCreator.cityIndicatorData);
		assertEquals(1, responseCreator.indicatorReprByteSize);
	}

	@Test
	public void setIndicatorDataByCityTestReprByteSize() {
		CityData cityData = new CityData();
		cityData.setIndicators(createDummyIndicatorDataMap(9));
		responseCreator.setIndicatorDataByCity(cityData);
		assertEquals(2, responseCreator.indicatorReprByteSize);

		cityData.setIndicators(createDummyIndicatorDataMap(100));
		responseCreator.setIndicatorDataByCity(cityData);
		assertEquals(13, responseCreator.indicatorReprByteSize);

		cityData.setIndicators(createDummyIndicatorDataMap(0));
		responseCreator.setIndicatorDataByCity(cityData);
		assertEquals(0, responseCreator.indicatorReprByteSize);
	}

	// getMatrixCorners tests
	@Test
	public void getMatrixCornersTestMorePoints() {
		List<IndicatorPoint> points = new ArrayList<>();
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.605997, 29.669324)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.661623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.62348, 29.664623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.599348, 29.661623)));

		List<Coordinates> corners = responseCreator.getMatrixCorners(points);
		assertEqualCoordinates(new Coordinates(29.671623, -95.62348), corners.get(0));
		assertEqualCoordinates(new Coordinates(29.671623, -95.599348), corners.get(1));
		assertEqualCoordinates(new Coordinates(29.661623, -95.62348), corners.get(2));
		assertEqualCoordinates(new Coordinates(29.661623, -95.599348), corners.get(3));
		assertEquals(corners.get(0), responseCreator.upperLeftCoord);
	}

	@Test
	public void getMatrixCornersTestTwoPoints() {
		List<IndicatorPoint> points = new ArrayList<>();
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.605997, 29.652324)));

		List<Coordinates> corners = responseCreator.getMatrixCorners(points);
		assertEqualCoordinates(new Coordinates(29.671623, -95.619348), corners.get(0));
		assertEqualCoordinates(new Coordinates(29.671623, -95.605997), corners.get(1));
		assertEqualCoordinates(new Coordinates(29.652324, -95.619348), corners.get(2));
		assertEqualCoordinates(new Coordinates(29.652324, -95.605997), corners.get(3));
		assertEquals(corners.get(0), responseCreator.upperLeftCoord);
	}

	@Test
	public void getMatrixCornersTestOnePoint() {
		List<IndicatorPoint> points = Arrays.asList(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));

		List<Coordinates> corners = responseCreator.getMatrixCorners(points);
		corners.stream().forEach(corner -> assertEqualCoordinates(new Coordinates(29.671623, -95.619348), corner));
		assertEquals(corners.get(0), responseCreator.upperLeftCoord);
	}

	@Test
	public void getMatrixCornersTestMoreSamePoints() {
		List<IndicatorPoint> points = new ArrayList<>();
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));

		List<Coordinates> corners = responseCreator.getMatrixCorners(points);
		corners.stream().forEach(corner -> assertEqualCoordinates(new Coordinates(29.671623, -95.619348), corner));
		assertEquals(corners.get(0), responseCreator.upperLeftCoord);
	}

	// createPointMatrix tests
	@Test
	public void createPointMatrixTestOnePoint() {
		List<IndicatorPoint> points = Arrays.asList(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		responseCreator.createPointMatrix(points);
		assertEquals(1, responseCreator.columnCount);
	}

	@Test
	public void createPointMatrixTestMorePoints() {
		List<IndicatorPoint> points = new ArrayList<>();
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.605997, 29.669324)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.661623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.62348, 29.664623)));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.599348, 29.661623)));

		responseCreator.createPointMatrix(points);
		assertEquals(9, responseCreator.columnCount);
	}

	// fillPointMatrix tests
	@Test
	public void savePointFailureInFillPointMatrixTest() {
		Map<String, Double> value = new HashMap<>();
		value.put("asthma", 5.5);
		List<IndicatorPoint> points = new ArrayList<>();
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623), value));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.605997, 29.669324), value));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.661623), value));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.62348, 29.664623), value));
		points.add(new IndicatorPoint(new GeoJsonPoint(-95.599348, 29.661623), value));

		responseCreator.createPointMatrix(points); // calls fillPointMatrix
		assertEquals(0, responseCreator.savePointFailNr);
	}

	@Test
	public void savePointFailureInFillPointMatrixTestMorePoints() {
		Map<String, Double> value = new HashMap<>();
		value.put("asthma", 5.5);
		List<IndicatorPoint> points = Collections.nCopies(1000, new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623), value));

		responseCreator.createPointMatrix(points);
		assertEquals(0, responseCreator.savePointFailNr);
	}

	// converter tests
	@Test
	public void converterTest() {
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("transport", createIndicatorData("bus", 100.0, 200.0, true));
		responseCreator.cityIndicatorData = indicators;

		assertEquals((byte) 50, responseCreator.converter("transport", 150.0));
	}

	// createMatrixValueForPoint tests
	@Test
	public void createMatrixValueForPointTestOneActiveIndicator() {
		responseCreator.indicatorReprByteSize = 1;
		responseCreator.cityIndicatorKeys = Arrays.asList("transport", "health");
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("transport", createIndicatorData("bus", 100.0, 200.0, true));
		responseCreator.cityIndicatorData = indicators;

		IndicatorPoint point = new IndicatorPoint();
		Map<String, Double> value = new HashMap<>();
		value.put("transport", 150.0);
		point.setValue(value);

		byte[] expectedResponse = { -128, 50 };
		byte[] actual = responseCreator.createMatrixValueForPoint(point);
		assertEquals(2, actual.length);
		for (int i = 0; i < actual.length; i++) {
			assertEquals(expectedResponse[i], actual[i]);
		}
	}

	@Test
	public void createMatrixValueForPointTestMoreActiveIndicators() {
		responseCreator.indicatorReprByteSize = 1;
		responseCreator.cityIndicatorKeys = Arrays.asList("transport", "health", "education", "risk", "shopping");
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("transport", createIndicatorData("bus", 100.0, 200.0, true));
		indicators.put("health", createIndicatorData("bus", 100.0, 200.0, true));
		indicators.put("shopping", createIndicatorData("bus", 100.0, 200.0, true));
		indicators.put("education", createIndicatorData("bus", 100.0, 200.0, true));
		indicators.put("risk", createIndicatorData("bus", 100.0, 200.0, true));
		responseCreator.cityIndicatorData = indicators;

		IndicatorPoint point = new IndicatorPoint();
		Map<String, Double> value = new HashMap<>();
		value.put("transport", 150.0);
		value.put("health", 200.0);
		value.put("education", 150.0);
		value.put("risk", 200.0);
		value.put("shopping", 100.0);
		point.setValue(value);

		byte[] expectedResponse = { (byte) 248, 50, 100, 50, 100, 0 };
		byte[] actual = responseCreator.createMatrixValueForPoint(point);
		assertEquals(6, actual.length);
		for (int i = 0; i < actual.length; i++) {
			assertEquals(expectedResponse[i], actual[i]);
		}
	}

	@Test
	public void createMatrixValueForPointTestBiggerReprByteSize() {
		responseCreator.indicatorReprByteSize = 5;
		responseCreator.cityIndicatorKeys = Arrays.asList("transport", "health", "education", "risk", "shopping");
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("transport", createIndicatorData("bus", 100.0, 200.0, true));
		responseCreator.cityIndicatorData = indicators;

		IndicatorPoint point = new IndicatorPoint();
		Map<String, Double> value = new HashMap<>();
		value.put("transport", 150.0);
		point.setValue(value);

		byte[] expectedResponse = { -128, 0, 0, 0, 0, 50 };
		byte[] actual = responseCreator.createMatrixValueForPoint(point);
		assertEquals(6, actual.length);
		for (int i = 0; i < actual.length; i++) {
			assertEquals(expectedResponse[i], actual[i]);
		}
	}

	// createResponseTests
	@Test
	public void createResponseTest() {
		// cityData
		Map<String, IndicatorData> indicators = new HashMap<>();
		indicators.put("bus", createIndicatorData("bus", 100.0, 200.0, true));
		indicators.put("asthma", createIndicatorData("asthma", 100.0, 200.0, true));
		indicators.put("mall", createIndicatorData("mall", 100.0, 200.0, true));
		indicators.put("tsunami", createIndicatorData("tsunami", 100.0, 200.0, true));
		CityData cityData = new CityData();
		cityData.setIndicators(indicators);

		// indicator point list
		List<IndicatorPoint> points = new ArrayList<>();
		// first point
		Map<String, Double> value = new HashMap<>();
		value.put("asthma", 200.0);
		value.put("mall", 200.0);
		value.put("tsunami", 100.0);
		IndicatorPoint point = new IndicatorPoint(new GeoJsonPoint(-95.619348, 29.671623), value);
		points.add(point);
		// second point
		Map<String, Double> value2 = new HashMap<>();
		value2.put("bus", 150.0);
		IndicatorPoint point2 = new IndicatorPoint(new GeoJsonPoint(-95.618348, 29.674624), value2);
		points.add(point2);

		IndicatorsResponse response = responseCreator.createResponse(cityData, points);
		assertEquals(1, response.getColumnCount());
		assertEquals(4, responseCreator.cityIndicatorKeys.size());

		byte[] byteArray = { -128, 50, 112, 0, 100, 100 };
		assertEquals(Base64.getEncoder().encodeToString(byteArray), response.getIndicatorsEncoded());
	}

	private void assertEqualCoordinates(Coordinates expected, Coordinates actual) {
		assertEquals(expected.getLatitude(), actual.getLatitude(), DELTA);
		assertEquals(expected.getLongitude(), actual.getLongitude(), DELTA);
	}

	private IndicatorData createIndicatorData(String key, Double min, Double max, boolean highValueBetter) {
		IndicatorData indicatorData = new IndicatorData();
		indicatorData.setKey(key);
		indicatorData.setMax(max);
		indicatorData.setMin(min);
		indicatorData.setHighValueBetter(highValueBetter);
		return indicatorData;
	}

	private Map<String, IndicatorData> createDummyIndicatorDataMap(int size) {
		Map<String, IndicatorData> indicators = new HashMap<>();
		IntStream.range(0, size)
		         .boxed()
		         .collect(Collectors.toList())
		         .stream()
		         .forEach(i -> indicators.put(Integer.toString(i), new IndicatorData()));
		return indicators;
	}
}
