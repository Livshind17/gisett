package com.gisett.util;

import java.awt.geom.Point2D;

import org.geotools.geometry.DirectPosition2D;
import org.junit.Before;
import org.junit.Test;

import com.gisett.domain.model.commons.Coordinates;

import static org.junit.Assert.assertEquals;

/**
 * Created by tuzes-boloni.kincso on 10/23/2018
 */
public class PointCalculatorTest {

	private PointCalculator pointCalculator;
	private static final Double DELTA = 0.02;
	private static final Double DELTA_DISTANCE = 10.0; // meters

	@Before
	public void setUp() {
		pointCalculator = new PointCalculator();
	}

	// getPointToDirection tests
	@Test
	public void getPointToDirectionTest() {
		Point2D point = pointCalculator.getPointToDirection(new DirectPosition2D(23.591423, 46.770439), 112, 167460);
		assertEquals(25.591423, point.getX(), DELTA);
		assertEquals(46.170439, point.getY(), DELTA);
	}

	@Test
	public void getPointToDirectionTestZeroAzimuthAndRadius() {
		Point2D point = pointCalculator.getPointToDirection(new DirectPosition2D(23.591423, 46.770439), 0, 0);
		assertEquals(23.591423, point.getX(), DELTA);
		assertEquals(46.770439, point.getY(), DELTA);
	}

	@Test
	public void getPointToDirectionTestNegativeAzimuth() {
		Point2D point = pointCalculator.getPointToDirection(new DirectPosition2D(23.591423, 46.770439), -360, 0);
		assertEquals(23.591423, point.getX(), DELTA);
		assertEquals(46.770439, point.getY(), DELTA);
	}

	// getPointToEast tests
	@Test
	public void getPointToEastTest() {
		Point2D point = pointCalculator.getPointToEast(new DirectPosition2D(23.591423, 46.770439));
		assertEquals(23.595324, point.getX(), DELTA);
		assertEquals(46.770439, point.getY(), DELTA);
	}

	// getPointToSouth tests
	@Test
	public void getPointToSouthTest() {
		Point2D point = pointCalculator.getPointToSouth(new DirectPosition2D(23.591423, 46.770439));
		assertEquals(23.58745, point.getX(), DELTA);
		assertEquals(46.770439, point.getY(), DELTA);
	}

	// getDistance tests
	@Test
	public void getDistanceTestDifferentPoints() {
		assertEquals(22250, pointCalculator.getDistance(new Coordinates(46.770439, 23.591423),
				new Coordinates(46.570439, 23.601423)), DELTA_DISTANCE);
	}

	@Test
	public void getDistanceTestSamePoints() {
		assertEquals(0, pointCalculator.getDistance(new Coordinates(46.770439, 23.591423),
				new Coordinates(46.770439, 23.591423)), DELTA_DISTANCE);
	}
}
