package com.gisett.util;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gisett.configuration.appdata.IndicatorData;
import com.gisett.exception.GisettBadDataException;

/**
 * Created by tuzes-boloni.kincso on 10/23/2018
 */

public class RangeConverterTest {

	private RangeConverter rangeConverter;

	@Before
	public void setUp() {
		rangeConverter = new RangeConverter();
	}

	// convertToAnotherRange tests
	@Test
	public void convertToAnotherRangeTestWithSameRange() {
		List<Double> values = Arrays.asList(0.0, 50.0, 100.0);
		values.stream().forEach(
				value -> Assert.assertEquals((byte) value.intValue(), rangeConverter.convertToAnotherRange(value,
						new Double(rangeConverter.getRangeMin()), new Double(rangeConverter.getRangeMax()))));
	}

	@Test
	public void convertToAnotherRangeTestWithOtherRanges() {
		List<Double> values = Arrays.asList(50.0, 100.0, 150.0);

		// actualRange part of given range
		Object[] actual = values.stream()
		                        .map(value -> rangeConverter.convertToAnotherRange((value), 50.0, 150.0))
		                        .toArray();
		Byte[] expected = { 0, 50, 100 };
		Assert.assertTrue(Arrays.equals(expected, actual));

		// actualRange wider than given range
		actual = values.stream()
		               .map(value -> rangeConverter.convertToAnotherRange((value), -50.0, 150.0))
		               .toArray();
		Byte[] expected1 = { 50, 75, 100 };
		Assert.assertTrue(Arrays.equals(expected1, actual));

		// actualRange narrower than given range
		values = Arrays.asList(50.0, 75.0);
		actual = values.stream()
		               .map(value -> rangeConverter.convertToAnotherRange((value), 25.0, 75.0))
		               .toArray();
		Byte[] expected2 = { 50, 100 };
		Assert.assertTrue(Arrays.equals(expected2, actual));
	}

	// convertIndicatorRanges tests
	@Test
	public void convertIndicatorRangesTestHighValueBetter() {
		Assert.assertEquals((byte) 0.0, rangeConverter.convertIndicatorRanges(createIndicatorData(true, 50.0, 150.0), 50.0));
	}

	@Test
	public void convertIndicatorRangesTestHighValueNull() {
		Assert.assertEquals((byte) 0.0, rangeConverter.convertIndicatorRanges(createIndicatorData(null, 50.0, 150.0), 50.0));
	}

	@Test
	public void convertIndicatorRangesTestLowValueBetter() {
		Assert.assertEquals((byte) 100.0, rangeConverter.convertIndicatorRanges(createIndicatorData(false, 50.0, 150.0), 50.0));
	}

	@Test(expected = GisettBadDataException.class)
	public void convertIndicatorRangesTestInvalidRangeLimits() {
		rangeConverter.convertIndicatorRanges(createIndicatorData(false, 150.0, 50.0), 50.0);
	}

	@Test(expected = GisettBadDataException.class)
	public void convertIndicatorRangesTestValueOutOfRange() {
		rangeConverter.convertIndicatorRanges(createIndicatorData(false, 50.0, 100.0), 20.0);
	}

	private IndicatorData createIndicatorData(Boolean highValueBetter, double min, double max) {
		IndicatorData indicatorData = new IndicatorData();
		indicatorData.setHighValueBetter(highValueBetter);
		indicatorData.setMin(min);
		indicatorData.setMax(max);
		return indicatorData;
	}
}
