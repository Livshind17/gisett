package com.gisett.importer;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gisett.configuration.importer.CityFromConfig;
import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.IndicatorForCity;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.IndicatorPointService;
import com.gisett.util.ExportFileBuilder;
import com.gisett.util.ImportFileProcessor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Bartha.Vivien on 8/31/2018.
 */

@RunWith(SpringRunner.class)
public class IndicatorImporterTest {
	@TestConfiguration
	static class IndicatorImporterContextConfiguration {

		@Bean
		public IndicatorImporter importer() throws Exception {
			return new IndicatorImporter();
		}

		@Bean
		public GisettImporterConfig config() {
			return new GisettImporterConfig();
		}
	}

	@MockBean
	private IndicatorPointService indicatorPointService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@Autowired
	private IndicatorImporter indicatorImporter;
	private ImportFileProcessor importFileProcessor;
	private ExportFileBuilder exportFileBuilder;

	@Before
	public void setUp() throws Exception {
		importFileProcessor = new ImportFileProcessor();
		exportFileBuilder = new ExportFileBuilder();
		gisettImporterConfig.setDataImportLocationPath("./resources/data/");
		gisettImporterConfig.setDataExportLocationPath("./resources/");
		gisettImporterConfig.setExportIndicatorsEnabled(false);
	}

	@Test
	public void importIndicatorsInvalidPathTest() throws Exception {
		Assert.assertNotNull(indicatorPointService);
		Assert.assertNotNull(indicatorImporter);
		Assert.assertNotNull(gisettImporterConfig);

		CityFromConfig cityFromConfig = newCity();
		indicatorImporter.importIndicators(cityFromConfig);
		gisettImporterConfig.setDataImportLocationPath("");

		verify(indicatorPointService, times(0)).save(any());
	}

	private CityFromConfig newCity() {
		CityFromConfig cityFromConfig = new CityFromConfig();
		cityFromConfig.setKey("LA");
		cityFromConfig.setCityName("Los Angeles");
		Coordinates c1 = new Coordinates();
		Coordinates c2 = new Coordinates();
		c1.setLatitude(34.3517752496856);
		c1.setLongitude(-118.66642603040589);
		c2.setLatitude(33.69853884919837);
		c2.setLongitude(-117.65147831535245);
		cityFromConfig.setCoordinatesNW(c1);
		cityFromConfig.setCoordinatesSE(c2);

		IndicatorForCity indicatorForCity1 = new IndicatorForCity();
		indicatorForCity1.setIndicatorName("la2_lowbirth1");
		indicatorForCity1.setInput("la2_lowbirth");
		indicatorForCity1.setOutput("output_la2_lowbirth");
		indicatorForCity1.setMax(33.35177533626097);
		indicatorForCity1.setMin(-115.66316523956226);

		IndicatorForCity indicatorForCity2 = new IndicatorForCity();
		indicatorForCity2.setIndicatorName("la2_lowbirth2");
		indicatorForCity2.setInput("la2_lowbirth");
		indicatorForCity2.setOutput("output_la2_lowbirth");
		indicatorForCity2.setMax(34.35177520626097);
		indicatorForCity2.setMin(-118.66316523956226);

		cityFromConfig.setIndicators(new ArrayList<>(Arrays.asList(indicatorForCity1, indicatorForCity2)));
		return cityFromConfig;
	}
}