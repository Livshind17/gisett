package com.gisett.importer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.IndicatorCategoryFromConfig;
import com.gisett.domain.service.IndicatorCategoryService;
import com.gisett.importer.assembler.IndicatorCategoryAssembler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by Bartha.Vivien on 8/30/2018.
 */
@RunWith(SpringRunner.class)
public class IndicatorConfigImporterTest {
	@TestConfiguration
	static class IndicatorConfigImporterTestContextConfiguration {

		@Bean
		public IndicatorConfigImporter importer() {
			return new IndicatorConfigImporter();
		}

		@Bean
		public GisettImporterConfig config() {
			return new GisettImporterConfig();
		}
	}

	@Autowired
	private IndicatorConfigImporter indicatorConfigImporter;
	@MockBean
	private IndicatorCategoryService indicatorConfigService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@MockBean
	private IndicatorCategoryAssembler assembler;

	@Before
	public void setUp() {
		List<IndicatorCategoryFromConfig> configs = new ArrayList<>();
		IndicatorCategoryFromConfig i = new IndicatorCategoryFromConfig();
		i.setCategoryName("Health");
		i.setIndicatorGroups(new ArrayList<>());
		configs.add(i);
		configs.add(i);
		configs.add(i);
		configs.add(i);
		gisettImporterConfig.setIndicatorConfigs(configs);
	}

	@Test
	public void importIndicatorsCheckInvocationNumberTest() {
		Assert.assertNotNull(indicatorConfigService);
		Assert.assertNotNull(indicatorConfigImporter);

		indicatorConfigImporter.importIndicatorConfigs();

		verify(indicatorConfigService, times(4)).save(any());
		verifyNoMoreInteractions(indicatorConfigService);
	}

	@Test
	public void importIndicatorsFailTest() {
		gisettImporterConfig.setIndicatorConfigs(null);
		indicatorConfigImporter.importIndicatorConfigs();

		verify(indicatorConfigService, times(0)).save(any());
		verifyNoMoreInteractions(indicatorConfigService);
	}
}