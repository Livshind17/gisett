package com.gisett.importer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.PoiCategoriesFromConfig;
import com.gisett.domain.service.PoiCategoryService;
import com.gisett.importer.assembler.PoiCategoryAssembler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by tuzes-boloni.kincso on 9/24/2018
 */

@RunWith(SpringRunner.class)
public class PoiConfigImporterTest {

	@TestConfiguration
	static class PoiConfigImporterTestContextConfiguration {

		@Bean
		public PoiConfigImporter importer() {
			return new PoiConfigImporter();
		}

		@Bean
		public GisettImporterConfig config() {
			return new GisettImporterConfig();
		}
	}

	@Autowired
	private PoiConfigImporter poiConfigImporter;
	@MockBean
	private PoiCategoryService poiConfigService;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@MockBean
	private PoiCategoryAssembler assembler;

	@Before
	public void setUp() {
		List<PoiCategoriesFromConfig> configs = new ArrayList<>();
		PoiCategoriesFromConfig poi = new PoiCategoriesFromConfig();
		poi.setCategoryName("Health");
		poi.setPoiTypes(new ArrayList<>());
		configs.add(poi);
		configs.add(poi);
		gisettImporterConfig.setPoiConfigs(configs);
	}

	@Test
	public void importPoisCheckInvocationNumberTest() {
		Assert.assertNotNull(poiConfigService);
		Assert.assertNotNull(poiConfigImporter);

		poiConfigImporter.importPoiConfigs();

		verify(poiConfigService, times(2)).save(any());
		verifyNoMoreInteractions(poiConfigService);
	}

	@Test
	public void importPoisFailTest() {
		gisettImporterConfig.setPoiConfigs(null);
		poiConfigImporter.importPoiConfigs();

		verify(poiConfigService, times(0)).save(any());
		verifyNoMoreInteractions(poiConfigService);
	}
}
