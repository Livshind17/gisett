package com.gisett.importer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gisett.configuration.importer.CityFromConfig;
import com.gisett.configuration.importer.GisettImporterConfig;
import com.gisett.configuration.importer.IndicatorForCity;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.service.CityService;
import com.gisett.domain.service.IndicatorCategoryService;
import com.gisett.importer.assembler.CityAssembler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by Bartha.Vivien on 8/30/2018.
 */
@RunWith(SpringRunner.class)
public class CityImporterTest {
	@Autowired
	private CityImporter cityImporter;
	@Autowired
	private GisettImporterConfig gisettImporterConfig;
	@MockBean
	private PoiImporter poiImporter;
	@MockBean
	private PropertyImporter propertyImporter;
	@MockBean
	private LocationImporter locationImporter;
	@MockBean
	private IndicatorImporter indicatorImporter;
	@MockBean
	private CityService cityService;
	@MockBean
	private CityAssembler cityAssembler;
	@MockBean
	private IndicatorCategoryService indicatorConfigService;
	private List<CityFromConfig> cities;

	@TestConfiguration
	static class IndicatorConfigImporterTestContextConfiguration {

		@Bean
		public GisettImporterConfig config() {
			return new GisettImporterConfig();
		}

		@Bean
		public CityImporter cityImporter() {
			return new CityImporter();
		}
	}

	@Before
	public void setUp() {
		cities = new ArrayList<>();
		CityFromConfig i = new CityFromConfig();
		i.setKey("LA");
		i.setCityName("Los Angeles");
		i.setLocations("Beverly Hills");
		i.setCoordinatesNW(new Coordinates());
		i.setCoordinatesSE(new Coordinates());
		i.setIndicators(new ArrayList<>(Arrays.asList(new IndicatorForCity(), new IndicatorForCity())));
		i.setPois(new ArrayList<>(Arrays.asList("LA_pois.json")));
		i.setPoiTypes(new ArrayList<>(Arrays.asList("bakeries")));
		i.setProperties("LA_properties.geojson");
		cities.add(i);
		cities.add(i);
	}

	@Test
	public void importCitiesEmptyListTest() {
		gisettImporterConfig.setIndicatorConfigs(null);
		cityImporter.importCities();

		verify(cityService, times(0)).save(any());
		verifyNoMoreInteractions(cityService);
	}

	@Test
	public void importCitiesInvocationNumberTest() throws Exception {
		gisettImporterConfig.setCities(cities);
		cityImporter.importCities();

		verify(locationImporter, times(2)).importLocations(anyString(), anyString());
		verify(indicatorImporter, times(2)).importIndicators(any(CityFromConfig.class));
		verify(poiImporter, times(2)).importPois(anyString(), anyList());
		verify(propertyImporter, times(2)).importProperties(anyString(), anyString());
		verify(cityService, times(2)).save(any());
		verifyNoMoreInteractions(cityService);
	}

}