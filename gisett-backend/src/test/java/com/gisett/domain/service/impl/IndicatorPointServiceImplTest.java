package com.gisett.domain.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit4.SpringRunner;

import com.vividsolutions.jts.util.Assert;

import com.gisett.domain.model.IndicatorPoint;
import com.gisett.domain.model.commons.Coordinates;
import com.gisett.domain.repository.IndicatorPointRepository;
import com.gisett.domain.service.CityService;
import com.gisett.domain.service.IndicatorPointService;

/**
 * Created by tuzes-boloni.kincso on 10/23/2018
 */

@RunWith(SpringRunner.class)
public class IndicatorPointServiceImplTest {

	private static final Coordinates WS_COORD = new Coordinates(-118.569, 34.897);
	private static final Coordinates NE_COORD = new Coordinates(-117.569, 33.897);
	private static final String CITY_KEY = "US-CA-LA";
	private static final String INDICATOR_KEY = "asthma";
	private static final HashMap<String, Double> INDICATOR_VALUES = new HashMap<String, Double>();
	private static final GeoJsonPoint POINT = new GeoJsonPoint(-118.55, 33.45);

	@TestConfiguration
	static class IndicatorPointServiceImplTestContextConfiguration {
		@Bean
		public IndicatorPointService service() {
			return new IndicatorPointServiceImpl();
		}
	}

	@Autowired
	private IndicatorPointService indicatorPointService;
	@MockBean
	private IndicatorPointRepository indicatorPointRepository;
	@MockBean
	private CityService cityService;

	@Before
	public void setUp() {
		INDICATOR_VALUES.put("asthma", 20.0);

		IndicatorPoint indicatorPoint = new IndicatorPoint();
		indicatorPoint.setCityKey(CITY_KEY);
		indicatorPoint.setPoint(POINT);
		indicatorPoint.setValue(INDICATOR_VALUES);

		Mockito.when(indicatorPointRepository.withinSquarePoints(WS_COORD.getLongitude(), WS_COORD.getLatitude(),
				NE_COORD.getLongitude(), NE_COORD.getLatitude())).thenReturn(Arrays.asList(indicatorPoint));
	}

	@Test
	public void withinSquarePointsTest() {
		List<IndicatorPoint> points = indicatorPointService.withinSquarePoints(WS_COORD.getLongitude(),
				WS_COORD.getLatitude(), NE_COORD.getLongitude(), NE_COORD.getLatitude());
		Assert.equals(1, points.size());
		Assert.equals(CITY_KEY, points.get(0).getCityKey());
		Assert.equals(POINT, points.get(0).getPoint());
		Assert.equals(INDICATOR_VALUES.get(INDICATOR_KEY), points.get(0).getValue().get(INDICATOR_KEY));
	}
}
