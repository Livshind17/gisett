import './resources/css/index.css';

import App from './App';
import common_en from './resources/translations/en/common.json';
import common_he from './resources/translations/he/common.json';
import data_en from './resources/translations/en/backendResponses.json';
import i18next from 'i18next';
import { I18nextProvider } from 'react-i18next';
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

i18next.init({
    interpolation: { escapeValue: false }, // React already does escaping
    fallbackLng: 'en',
    lng: 'en',
    resources: {
        en: {
            common: common_en,
            data: data_en
        },
        he: {
            common: common_he
        }
    },
    ns: ['common', 'data'],
    defaultNS: 'common',
    fallbackNS: 'data',
    debug: true
});

ReactDOM.render(
    <I18nextProvider i18n={i18next}>
        <App />
    </I18nextProvider>,
    document.getElementById('root')
);
registerServiceWorker();
