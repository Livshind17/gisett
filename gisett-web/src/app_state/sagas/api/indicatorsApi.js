import axios from 'axios';
import { stringifyCoordinateForAPI } from '../../../utils/commons/Helper';

export function fetchEncodedIndicatorMapsByCityKey(cityKey, distance, coordinates) {
    const stringifiedCoordinate = stringifyCoordinateForAPI(coordinates);
    return axios.get(
        process.env.REACT_APP_API_URL +
            '/cities/' +
            cityKey +
            '/indicators/points?' +
            stringifiedCoordinate +
            '&distance=' +
            distance
    );
}

export function getNearestIndicatorPoint(cityKey, coordinate) {
    let coord;
    if (Array.isArray(coordinate)) {
        coord = 'lat=' + coordinate[0] + '&lng=' + coordinate[1];
    }
    if (typeof coordinate === Object) {
        coord = 'lat=' + coordinate.lat + '&lng=' + coordinate.lng;
    }
    if (typeof coordinate === 'string') {
        coord = coordinate;
    }
    return axios.get(
        process.env.REACT_APP_API_URL + '/cities/' + cityKey + '/indicators/nearest-point?' + coord
    );
}
