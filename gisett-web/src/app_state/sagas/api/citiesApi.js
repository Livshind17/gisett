import axios from 'axios';

export function fetchAllCities() {
    return axios.get(process.env.REACT_APP_API_URL + '/cities');
}

export function fetchCityConfigurationsByCityKey(cityKey) {
    return axios.get(process.env.REACT_APP_API_URL + '/cities/' + cityKey + '/configurations');
}
