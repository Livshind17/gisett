import axios from 'axios';

export function fetchLocationsByCityKeyAndLocationName(cityKey, name) {
    return axios.get(process.env.REACT_APP_API_URL + '/cities/' + cityKey + '/locations?name=' + name);
}
