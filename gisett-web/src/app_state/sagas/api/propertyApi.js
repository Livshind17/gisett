import axios from 'axios';
import { stringifyCoordinateForAPI } from '../../../utils/commons/Helper';

export function filterProperties(cityKey, coordinates, distance, size, backendFilterObj) {
    const stringifiedCoordinate = stringifyCoordinateForAPI(coordinates);
    return axios
        .post(
            process.env.REACT_APP_API_URL +
                '/cities/' +
                cityKey +
                '/properties?' +
                stringifiedCoordinate +
                '&distance=' +
                distance +
                '&size=' +
                size,
            backendFilterObj
        )
        .then(res => {
            return res.data;
        });
}
