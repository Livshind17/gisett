import axios from 'axios';
import { stringifyCoordinateForAPI } from '../../../utils/commons/Helper';

export function fetchPoisByPoiTypesAndCoordsAndDistance(cityKey, coordinates, distance, activePoiTypes) {
    const stringifiedCoordinate = stringifyCoordinateForAPI(coordinates);
    return axios.get(
        process.env.REACT_APP_API_URL +
            '/cities/' +
            cityKey +
            '/pois/points?types=' +
            activePoiTypes.join() +
            '&' +
            stringifiedCoordinate +
            '&distance=' +
            distance
    );
}
