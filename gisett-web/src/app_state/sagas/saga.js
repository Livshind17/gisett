import * as gc from '../../utils/GisettConstants';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { fetchAllCities, fetchCityConfigurationsByCityKey } from './api/citiesApi';
import { fetchEncodedIndicatorMapsByCityKey, getNearestIndicatorPoint } from './api/indicatorsApi';
import { fetchLocationsByCityKeyAndLocationName } from './api/locationsApi';
import { fetchPoisByPoiTypesAndCoordsAndDistance } from './api/poiApi';
import { filterProperties } from './api/propertyApi';
import { getFilterObjectForBackend } from '../../utils/properties/filterObjectCustomization';
import { getReferenceCoordinate } from '../../utils/commons/Helper';

function* fetchAllCitiesRequested() {
    try {
        const cities = yield call(fetchAllCities);
        yield put({ type: gc.FETCH_ALL_CITIES_FULLFILLED, cities: cities });
    } catch (error) {
        yield put({ type: gc.FETCH_ALL_CITIES_REJECTED, error: error });
    }
}

function* fetchLocationRequested(action) {
    try {
        const locations = yield call(fetchLocationsByCityKeyAndLocationName, action.key, action.locName);
        yield put({ type: gc.FETCH_LOCATIONS_FULLFILLED, payload: locations });
    } catch (error) {
        yield put({ type: gc.FETCH_LOCATIONS_REJECTED, payload: error });
    }
}

function* fetchCityConfigurationsRequested(action) {
    try {
        const configurations = yield call(fetchCityConfigurationsByCityKey, action.cityKey);
        yield put({
            type: gc.FETCH_CITY_CONFIGURATIONS_FULLFILLED,
            cityConfigurations: configurations
        });
        // refresh the map layers: heatmap, poi, property
        yield put({ type: gc.REFRESH_MAP_LAYERS });
    } catch (error) {
        yield put({
            type: gc.FETCH_CITY_CONFIGURATIONS_REJECTED,
            error: error
        });
    }
}

function* fetchEncodedIndicatorMapsRequested(action) {
    try {
        const indicatorPoints = yield call(
            fetchEncodedIndicatorMapsByCityKey,
            action.cityKey,
            action.distance,
            action.coordinate
        );
        yield put({
            type: gc.FETCH_ALL_ENCODED_INDICATOR_FULLFILLED,
            payload: indicatorPoints
        });
    } catch (error) {
        yield put({
            type: gc.FETCH_ALL_ENCODED_INDICATOR_REJECTED,
            payload: error
        });
    }
}

function* fetchAllPoiRequested(action) {
    const cities = yield select(state => state.cities);
    const distance = yield select(state => state.heatMaps.settings.distance);
    const locationCoordinate = yield select(state => state.locations.locationCoordinate);
    const coordinate = getReferenceCoordinate(cities.cityCenterCoordinate, locationCoordinate);
    try {
        const pois = yield call(
            fetchPoisByPoiTypesAndCoordsAndDistance,
            cities.selectedCityKey,
            coordinate,
            distance,
            action.poiTypes
        );
        yield put({ type: gc.FETCH_ALL_POIS_FULLFILLED, payload: pois.data, poiTypes: action.poiTypes });
    } catch (error) {
        yield put({
            type: gc.FETCH_ALL_POIS_REJECTED,
            payload: error
        });
    }
}

function* filterAllPropertiesRequested(action) {
    try {
        const properties = yield call(
            filterProperties,
            action.cityKey,
            action.coordinate,
            action.distance,
            action.size,
            action.backendFilterObj
        );
        yield put({
            type: gc.FETCH_ALL_PROPERTIES_FULLFILLED,
            payload: properties,
            returnFilter: action.filterObject,
            selectedSortType: action.selectedSortType,
            indicators: action.indicators
        });
    } catch (error) {
        yield put({ type: gc.FETCH_ALL_PROPERTIES_REJECTED, payload: error });
    }
}

function* fetchNearestIndicatorMap(action) {
    try {
        const indicator = yield call(getNearestIndicatorPoint, action.cityKey, action.coordinate);
        yield put({
            type: gc.FETCH_NEAREST_INDICATOR_FULLFILLED,
            payload: indicator.data,
            coordinate: action.coordinate
        });
    } catch (error) {
        yield put({ type: gc.FETCH_NEAREST_INDICATOR_REJECTED, payload: error });
    }
}

function* goToMainPage() {
    yield put({ type: gc.RESET_CITY });
    yield put({ type: gc.RESET_LOCATION });
    yield put({ type: gc.RESET_STAGE });
}

function* setLocationAndRefreshMapLayers(action) {
    yield put({ type: gc.SET_LOCATION, location: action.location });
    // refresh the map layers: heatmap, poi, property
    yield put({ type: gc.REFRESH_MAP_LAYERS });
}

function* updateHeatMap(param) {
    yield put({
        type: gc.UPDATE_HEATMAP,
        indicatorPoints: param.indicatorPoints,
        indicatorSettings: param.indicatorSettings
    });
}

function* fetchHeatMapData(param) {
    // 1. getIndicatorPoints
    yield call(fetchEncodedIndicatorMapsRequested, {
        cityKey: param.cityKey,
        distance: param.distance,
        coordinate: param.coordinate
    });

    // 2. Update heatMap data, calculate weighted values for every points
    if (param.stage > 2) {
        yield call(updateHeatMap, param);
    }
}

function* refreshMapLayers() {
    // get data from the application state
    const cities = yield select(state => state.cities);
    const distance = yield select(state => state.heatMaps.settings.distance);
    const locationCoordinate = yield select(state => state.locations.locationCoordinate);
    const indicators = yield select(state => state.indicators);
    const stage = yield select(state => state.wizard.stage);
    const pois = yield select(state => state.pois);
    const properties = yield select(state => state.properties);

    const coordinate = getReferenceCoordinate(cities.cityCenterCoordinate, locationCoordinate);

    // 1. call fetchEncodedIndicatorMapsRequested
    yield call(fetchHeatMapData, {
        cityKey: cities.selectedCityKey,
        distance: distance,
        coordinate: coordinate,
        indicatorPoints: indicators.indicatorPoints,
        indicatorSettings: indicators.indicatorSettings,
        stage: stage
    });

    // 2. call fetchPOI generator function
    yield call(fetchAllPoiRequested, {
        poiTypes: pois.poiTypes
    });

    // 3. call filterProperty
    yield call(filterAllPropertiesRequested, {
        cityKey: cities.selectedCityKey,
        coordinate,
        distance: distance,
        size: gc.PROPERTY_FILTER_TEXTS.REQUEST_SIZE,
        backendFilterObj: getFilterObjectForBackend(properties.filterObject),
        selectedSortType: properties.selectedSortType,
        indicators: indicators,
        filterObject: properties.filterObject
    });
}

export default function* watchActions() {
    yield takeEvery(gc.FETCH_ALL_CITIES_REQUESTED, fetchAllCitiesRequested);
    yield takeEvery(gc.SEARCH_LOCATION_BY_NAME_LIKE, fetchLocationRequested);
    yield takeEvery(gc.SELECT_CITY, fetchCityConfigurationsRequested);
    yield takeEvery(gc.SET_LOCATION_AND_REFRESH_MAP_LAYERS, setLocationAndRefreshMapLayers);
    yield takeEvery(gc.GO_TO_MAIN_PAGE, goToMainPage);
    yield takeEvery(gc.FILTER_ALL_PROPERTY_REQUESTED, filterAllPropertiesRequested);
    yield takeLatest(gc.FETCH_ALL_POIS_REQUESTED, fetchAllPoiRequested);
    yield takeEvery(gc.FETCH_NEAREST_INDICATOR_REQUESTED, fetchNearestIndicatorMap);
    yield takeLatest(gc.REFRESH_MAP_LAYERS, refreshMapLayers);
}
