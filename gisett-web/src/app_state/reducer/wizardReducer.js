import * as gc from '../../utils/GisettConstants';

const initialState = {
    stage: 1
};

const wizardReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.GO_TO_STAGE_2:
            return { ...state, stage: 2 };
        case gc.GO_TO_STAGE_3:
            return { ...state, stage: 3 };
        case gc.GO_TO_STAGE_4:
            return { ...state, stage: 4 };
        case gc.RESET_STAGE:
            return { ...state, stage: 1 };
        case gc.SELECT_CITY:
            return state.stage >= 2 ? { ...state } : { ...state, stage: 2 };
        default:
            return state;
    }
};

export default wizardReducer;
