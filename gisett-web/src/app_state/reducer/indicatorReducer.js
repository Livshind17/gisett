import * as gc from '../../utils/GisettConstants';
import * as geo from '../../utils/coordinateProcesor/geoPointHandler';
import {
    indicatorStageEnum,
    summerizedAndIndicatorRating,
    transformIndicatorCategories
} from '../../utils/indicators/indicatorHelper';
import { decodeBackendResponse } from '../../utils/coordinateProcesor/GeoIndicatorHandler';

const initialState = {
    // holds the indicator categories base on the cityConfig.
    indicatorCategories: [],
    // holds the indicators based on the cityConfig. For example, [asthma, lowbirth, ozone, etc.]
    indicators: [],
    // every Point will have an active indicator list, based on the cityConfig.
    // For example: {x: , y: , [asthma: 9, ozone: 3, etc.]}
    indicatorPoints: [],
    // holds the selected indicator categories. On the Page2, the user can select maximum 2 categories.
    selectedCategories: [],
    // it has helper purpose only
    mustHaveCategoryMap: new Map(),
    indicatorSettings: {
        maxSelectableCategory: 2,
        maxSelectableGroup: 7,
        mustHave: [],
        veryImportant: [],
        important: [],
        niceToHave: [],
        all: []
    },
    // contains the nearest point which has data about the indicators
    // updates when the click event is triggered at map side
    nearestIndicator: null,
    error: null
};

const indicatorReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.FETCH_CITY_CONFIGURATIONS_FULLFILLED:
            let config = action.cityConfigurations.data;
            return {
                ...state,
                indicatorCategories: config.indicatorCategories,
                indicators: config.indicators
            };
        case gc.UPDATE_INDICATOR_GROUP_IMPORTANCE:
            return {
                ...state,
                indicatorSettings: {
                    ...state.indicatorSettings,
                    mustHave: action.mustHave,
                    veryImportant: action.veryImportant,
                    important: action.important,
                    niceToHave: action.niceToHave
                }
            };
        case gc.SELECT_MUST_HAVE_INDICATOR_CATEGORY:
            let selectedCategories = handleCategorySelection(state, action.selectedCategory);
            return {
                ...state,
                indicatorSettings: {
                    ...state.indicatorSettings,
                    mustHave: transformSelectedCategories(selectedCategories),
                    all: transformAllCategories(state.indicatorCategories, selectedCategories)
                },
                selectedCategories: selectedCategories
            };
        case gc.INIT_INDICATOR_GROUPS:
            return {
                ...state,
                indicatorSettings: {
                    ...state.indicatorSettings,
                    all: transformAllCategories(state.indicatorCategories, state.selectedCategories)
                }
            };
        case gc.CLEAR_MUST_HAVE_INDICATORS:
            return {
                ...state,
                indicatorSettings: { ...state.indicatorSettings, mustHave: [] },
                selectedCategories: []
            };
        case gc.FETCH_ALL_ENCODED_INDICATOR_FULLFILLED:
            return { ...state, indicatorPoints: decodeBackendResponse(action.payload.data, state.indicators) };
        case gc.FETCH_ALL_ENCODED_INDICATOR_REJECTED:
            return { ...state, error: action.payload };
        case gc.FETCH_NEAREST_INDICATOR_FULLFILLED:
            return { ...state, nearestIndicator: nearestIndicatorReturn(state, action.payload, action.coordinate) };
        case gc.FETCH_ALL_POIS_REQUESTED:
        case gc.FETCH_NEAREST_INDICATOR_REMOVED:
            return { ...state, nearestIndicator: null };
        case gc.FETCH_NEAREST_INDICATOR_REJECTED:
            return { ...state, error: action.error, nearestIndicator: null };
        default:
            return state;
    }
};
// Transform indicator categories into customized indicator group. IndicatorButtons use these customized indicator group object to build itself, set their state, etc.
// The selected categories must appear in the MUST HAVE group, their state/stage must be BASE.
const transformSelectedCategories = indicatorCategories => {
    return transformIndicatorCategories(indicatorCategories, [], () => indicatorStageEnum.BASE);
};

// Transform indicator categories into customized indicator group. Here tranform all indicator categories.
// The selected categories must have DISABLED stage, others will have BASE stage.
const transformAllCategories = (indicatorCategories, selectedCategories) => {
    return transformIndicatorCategories(indicatorCategories, selectedCategories, (category, selectedCategories) => {
        return selectedCategories.includes(category) ? indicatorStageEnum.DISABLED : indicatorStageEnum.BASE;
    });
};

// Add/Remove category to the collection. Maximum selection must be 2.
const handleCategorySelection = (state, category) => {
    let selectedCategories = [];
    let categoryMap = state.mustHaveCategoryMap;

    if (!categoryMap.has(category.key)) {
        if (categoryMap.size < state.indicatorSettings.maxSelectableCategory) {
            categoryMap.set(category.key, category);
        }
    } else {
        categoryMap.delete(category.key);
    }

    for (const categoryKey of categoryMap.keys()) {
        selectedCategories.push(categoryMap.get(categoryKey));
    }
    return selectedCategories;
};

const nearestIndicatorReturn = (state, nearestPoint, coordinate) => {
    let heatmapPopup = {
            position: coordinate
        },
        lat = coordinate[0],
        lng = coordinate[1];

    let dist = geo.getDistance(
        geo.getGeoPointModel(nearestPoint.point.x, nearestPoint.point.y),
        geo.getGeoPointModel(lng, lat)
    );
    if (dist < 300) {
        let allRatings, generalRating;
        [allRatings, generalRating] = summerizedAndIndicatorRating(nearestPoint, state);
        heatmapPopup.value = generalRating != null && generalRating.length > 0 ? generalRating[0].value : 0;
        heatmapPopup.allRatings = allRatings;
        heatmapPopup.position = coordinate;
    }
    return heatmapPopup;
};

export default indicatorReducer;
