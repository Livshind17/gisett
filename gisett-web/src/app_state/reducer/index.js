import cityReducer from './cityReducer';
import { combineReducers } from 'redux';
import heatMapReducer from './heatMapReducer';
import indicatorReducer from './indicatorReducer';
import locationReducer from './locationReducer';
import poiReducer from './poiReducer';
import propertyReducer from './propertyReducer';
import wizardReducer from './wizardReducer';

const allReducers = combineReducers({
    cities: cityReducer,
    locations: locationReducer,
    wizard: wizardReducer,
    indicators: indicatorReducer,
    heatMaps: heatMapReducer,
    properties: propertyReducer,
    pois: poiReducer
});

export default allReducers;
