import * as gc from '../../utils/GisettConstants';
import { sortByTypes } from '../../utils/properties/sortByFunctions';

const initialState = {
    propertyData: null,
    filterObject: gc.FILTER_PROPERTY_OBJECT,
    showPropertyDetails: false,
    currentProperty: null,
    selectedSortType: gc.PROPERTY_SORT_TYPES.BEST_LOCATION_FOR_ME,
    error: null
};

const propertyReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.FETCH_ALL_PROPERTIES_FULLFILLED:
            return {
                ...state,
                propertyData: sortByTypes(action.payload, action.selectedSortType, action.indicators),
                filterObject: action.returnFilter,
                selectedSortType: action.selectedSortType
            };
        case gc.FETCH_ALL_PROPERTIES_REJECTED:
            return { ...state, error: action.payload };
        case gc.SHOW_PROPERTY_DETAILS:
            return { ...state, showPropertyDetails: true, currentProperty: action.currentProperty };
        case gc.HIDE_PROPERTY_DETAILS:
            return { ...state, showPropertyDetails: false };
        default:
            return state;
    }
};

export default propertyReducer;
