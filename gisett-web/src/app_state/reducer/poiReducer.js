import * as gc from '../../utils/GisettConstants';

const initialState = {
    // result set of API call
    pois: [],
    // this map will contains the poiCategories in swapped order -> (key: poiSubCategory, value: poiMainCategory)
    poiConfigMap: new Map(),
    // this array will sotre the activated poiTypes. It's required for the API call, to get the required POIs in the defined zone.
    poiTypes: [],
    error: null
};

const poiReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.FETCH_CITY_CONFIGURATIONS_FULLFILLED:
            let config = action.cityConfigurations.data;
            return {
                ...state,
                poiConfigMap: processPoiConfig(config.poiCategories)
            };
        case gc.FETCH_ALL_POIS_FULLFILLED:
            return { ...state, pois: action.payload, poiTypes: action.poiTypes, loading: false };
        case gc.FETCH_CITY_CONFIGURATIONS_REJECTED:
        case gc.FETCH_ALL_POIS_REJECTED:
            return { ...state, error: action.error };
        default:
            return state;
    }
};

const processPoiConfig = poiConfigs => {
    let poiConfigMap = new Map();
    poiConfigs.map(poiConfig => {
        let key = poiConfig.key;
        poiConfig.poiTypes.map(type => poiConfigMap.set(type, key));
        // poiTypes.push.apply(poiTypes, poiConfig.poiTypes);
    });

    return poiConfigMap;
};

export default poiReducer;
