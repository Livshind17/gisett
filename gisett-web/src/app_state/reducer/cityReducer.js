import * as gc from '../../utils/GisettConstants';
import { calculateCoordinateAvg } from '../../utils/commons/Helper';

const initialState = {
    selectedCityKey: '',
    cities: [],
    selectedCityConfig: null,
    cityCenterCoordinate: [],
    error: null
};

const cityReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.FETCH_ALL_CITIES_FULLFILLED:
            return { ...state, cities: action.cities.data };
        case gc.FETCH_CITY_CONFIGURATIONS_FULLFILLED:
            const config = action.cityConfigurations.data;
            return {
                ...state,
                selectedCityConfig: config,
                selectedCityKey: config.key,
                cityCenterCoordinate: calculateCoordinateAvg(config.coordinatesNW, config.coordinatesSE)
            };
        case gc.RESET_CITY:
            return { ...state, selectedCityKey: '' };
        case gc.FETCH_ALL_CITIES_REJECTED:
        case gc.FETCH_CITY_CONFIGURATIONS_REJECTED:
            return { ...state, error: action.error };
        default:
            return state;
    }
};

export default cityReducer;
