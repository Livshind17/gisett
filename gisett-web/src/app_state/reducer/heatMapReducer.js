import * as gc from '../../utils/GisettConstants';
import { calculateCoordinateAvg } from '../../utils/commons/Helper';
import { calculateWeightedPoints } from '../../utils/indicators/indicatorHelper';

const initialState = {
    // heatmap base configurations
    settings: {
        radius: 0.004,
        distance: 10000,
        max: 100,
        zoom: 12
    },
    // this holds the information for the heatmap. The heatmap requires a Point(x,y) and a value.
    // Here the value will be weighted, base on the selected indicators.
    heatMapPoints: [],
    error: null
};

const heatMapReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.UPDATE_HEATMAP:
            return {
                ...state,
                heatMapPoints: calculateWeightedPoints(action.indicatorPoints, action.indicatorSettings)
            };
        default:
            return state;
    }
};

export default heatMapReducer;
