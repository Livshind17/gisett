import * as gc from '../../utils/GisettConstants';

const initialState = {
    locations: [],
    // this state will holds the entire selected location object, not only the name
    selectedLocation: null,
    locationCoordinate: [],
    error: null
};

const locationReducer = (state = initialState, action) => {
    switch (action.type) {
        case gc.FETCH_LOCATIONS_FULLFILLED:
            return { ...state, locations: action.payload.data };
        case gc.FETCH_ALL_CITIES_REJECTED:
            return { ...state, error: action.payload };
        case gc.SET_LOCATION:
            const coordinates = action.location.coordinates;
            return {
                ...state,
                selectedLocation: action.location,
                locationCoordinate: [coordinates.longitude, coordinates.latitude]
            };
        case gc.RESET_LOCATION:
        case gc.FETCH_CITY_CONFIGURATIONS_FULLFILLED:
            return initialState;
        default:
            return state;
    }
};

export default locationReducer;
