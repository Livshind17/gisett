import * as gc from '../utils/GisettConstants';

export function searchLocation(cityKey, locationName) {
    return {
        type: gc.SEARCH_LOCATION_BY_NAME_LIKE,
        key: cityKey,
        locName: locationName
    };
}

export function setLocation(location) {
    return {
        type: gc.SET_LOCATION_AND_REFRESH_MAP_LAYERS,
        location
    };
}

export function resetLocation() {
    return {
        type: gc.RESET_LOCATION
    };
}
