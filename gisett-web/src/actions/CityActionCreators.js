import * as gc from '../utils/GisettConstants';

export function fetchAllCities() {
    return {
        type: gc.FETCH_ALL_CITIES_REQUESTED
    };
}

export function selectCity(cityKey) {
    return {
        type: gc.SELECT_CITY,
        cityKey: cityKey
    };
}
