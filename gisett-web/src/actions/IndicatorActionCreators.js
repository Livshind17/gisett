import * as gc from '../utils/GisettConstants';

export function clearMustHaveCategories() {
    return {
        type: gc.CLEAR_MUST_HAVE_INDICATORS
    };
}

export function selectIndicatorCategory(category) {
    return {
        type: gc.SELECT_MUST_HAVE_INDICATOR_CATEGORY,
        selectedCategory: category
    };
}

export function initIndicatorGroups() {
    return {
        type: gc.INIT_INDICATOR_GROUPS
    };
}

export function updateIndicatorGroupImportance(indicatorSettings) {
    return {
        type: gc.UPDATE_INDICATOR_GROUP_IMPORTANCE,
        mustHave: indicatorSettings.mustHave,
        veryImportant: indicatorSettings.veryImportant,
        important: indicatorSettings.important,
        niceToHave: indicatorSettings.niceToHave
    };
}

export function fetchNearestIndicatorPoint(cityKey, coordinate) {
    return {
        type: gc.FETCH_NEAREST_INDICATOR_REQUESTED,
        cityKey: cityKey,
        coordinate: coordinate
    };
}
export function removeNearestIndicatorPoint() {
    return {
        type: gc.FETCH_NEAREST_INDICATOR_REMOVED
    };
}
