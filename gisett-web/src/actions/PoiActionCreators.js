import * as gc from '../utils/GisettConstants';

export function setSelectedPoiTypes(poiTypes) {
    return {
        type: gc.FETCH_ALL_POIS_REQUESTED,
        poiTypes
    };
}
