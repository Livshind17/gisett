import * as gc from '../utils/GisettConstants';

export function updateHeatMap(indicatorPoints, indicatorSettings) {
    return {
        type: gc.UPDATE_HEATMAP,
        indicatorPoints: indicatorPoints,
        indicatorSettings: indicatorSettings
    };
}
