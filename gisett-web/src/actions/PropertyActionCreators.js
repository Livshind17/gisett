import * as gc from '../utils/GisettConstants';
import { getFilterObjectForBackend } from '../utils/properties/filterObjectCustomization';

export function fetchAllProperties() {
    return {
        type: gc.FETCH_ALL_PROPERTIES_REQUESTED
    };
}

export function filterProperties(cityKey, coordinate, distance, filterObject, selectedSortType, indicators) {
    return {
        type: gc.FILTER_ALL_PROPERTY_REQUESTED,
        cityKey,
        coordinate,
        distance,
        size: gc.PROPERTY_FILTER_TEXTS.REQUEST_SIZE,
        backendFilterObj: getFilterObjectForBackend(filterObject),
        selectedSortType,
        indicators,
        filterObject
    };
}

export function showPropertyDetails(currentProperty) {
    return {
        type: gc.SHOW_PROPERTY_DETAILS,
        currentProperty
    };
}

export function hidePropertyDetails() {
    return {
        type: gc.HIDE_PROPERTY_DETAILS
    };
}
