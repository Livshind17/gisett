import * as gc from '../utils/GisettConstants';

export function gotoMainPage() {
    return {
        type: gc.GO_TO_MAIN_PAGE
    };
}
