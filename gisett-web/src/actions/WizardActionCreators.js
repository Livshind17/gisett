import * as gc from '../utils/GisettConstants';

export function gotoStage3() {
    return {
        type: gc.GO_TO_STAGE_3
    };
}

export function gotoStage4() {
    return {
        type: gc.GO_TO_STAGE_4
    };
}
