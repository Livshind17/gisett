export const applyDrag = (arr, dragResult) => {
    const { removedIndex, addedIndex, payload } = dragResult;
    if (removedIndex === null && addedIndex === null) return arr;

    const result = [...arr];
    let itemToAdd;
    // we need to make a copy, otherwise the object is contained by two different list.
    // the base and a panel
    if (payload.stage === 2) {
        itemToAdd = payload;
    } else {
        itemToAdd = { ...payload };
    }
    if (removedIndex !== null) {
        itemToAdd = result.splice(removedIndex, 1)[0];
    }

    if (addedIndex !== null) {
        payload.stage = 0;
        itemToAdd.stage = 2;
        result.splice(addedIndex, 0, itemToAdd);
    }

    return result;
};
