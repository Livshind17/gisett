import GeneralIndicatorRating, { INDICATOR_RATING_TYPE_POPUP } from '../../commons/GeneralIndicatorRating';
import React, { Component } from 'react';

class HeatmapPopup extends Component {
    render() {
        const { classes, popup } = this.props;

        return <GeneralIndicatorRating popup={popup} classes={classes} variant={INDICATOR_RATING_TYPE_POPUP} />;
    }
}

export default HeatmapPopup;
