import { Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import CurrencyFormat from 'react-currency-format';
import PropTypes from 'prop-types';
import StarRating from '../../commons/StarRating';
import Translator from '../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

class PropertyCard extends Component {
    render() {
        const { classes, property } = this.props;
        const { currency, price, bedrooms, bathrooms, builtSurface } = property;
        const { city, state, addressLine1 } = this.props.property.propertyAddress;

        const item = (
            <Grid container>
                <Grid item container justify="space-between" className={classes.popupHeader}>
                    <Typography variant="body2" gutterBottom className={classes.gridContentHeader}>
                        <CurrencyFormat value={price} displayType={'text'} thousandSeparator={true} prefix={currency} />
                    </Typography>
                    <StarRating numberOfGoodStars={4} />
                </Grid>
                <Grid item className={classes.padding}>
                    <Typography variant="caption" gutterBottom className={classes.gridTextContent}>
                        <Translator i18nKeys="PROPERTY.COMPACT_DETAILS">
                            {{ bedNr: bedrooms }} | {{ bathNr: bathrooms }} |{{ surf: builtSurface }} surface <br />
                            <span className={classes.opacity}>
                                {addressLine1} <br /> {city},{state}
                            </span>
                        </Translator>
                    </Typography>
                </Grid>
            </Grid>
        );

        return <div className={classes.propertyElements}>{item}</div>;
    }
}

PropertyCard.propTypes = {
    property: PropTypes.object.isRequired
};

const styles = theme => ({
    padding: {
        padding: 12
    },
    popupHeader: {
        backgroundColor: theme.palette.background.main,
        borderRadius: '12px 12px 0px 0px'
    },
    gridTextContent: {
        color: theme.palette.propertyText.main
    },
    opacity: {
        opacity: '0.5'
    },
    gridContentHeader: {
        paddingLeft: '8px',
        paddingTop: '4px'
    },
    propertyElements: {
        margin: '-13px -19px',
        minWidth: 200
    }
});

export default withStyles(styles)(PropertyCard);
