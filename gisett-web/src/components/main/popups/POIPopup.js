import { Grid, Typography, withStyles } from '@material-ui/core';
import React, { PureComponent } from 'react';

import classNames from 'classnames';
import Translator from '../../translate/Translator';

class POIPopup extends PureComponent {
    render() {
        const { poi, category, classes } = this.props;
        return (
            <Grid className={classes.popupContainer}>
                <Grid container>
                    <Grid
                        item
                        container
                        className={classNames(
                            classes.borderHeader,
                            classes['color' + category.charAt(0).toUpperCase() + category.slice(1)]
                        )}
                    >
                        <Typography gutterBottom variant="body2">
                            <Translator i18nKeys={'POI_TYPE.' + poi.type.toUpperCase()} />
                        </Typography>
                    </Grid>
                    <Grid item container className={classes.padding}>
                        <Typography gutterBottom variant="caption">
                            {poi.name}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    strictSize: {
        width: 160,
        '&:hover': {
            whiteSpace: 'normal'
        }
    },
    padding: { padding: 20 },
    borderHeader: {
        padding: '6px 12px 0px 12px',
        borderRadius: '12px 12px 0px 0px'
    },
    popupContainer: {
        margin: '-13px -19px',
        width: 300
    },

    colorHealth: {
        backgroundColor: theme.palette.health[30]
    },
    colorEnvironment: {
        backgroundColor: theme.palette.environment[30]
    },
    colorPublic_safety: {
        backgroundColor: theme.palette.safety[30]
    },
    colorRisk: {
        backgroundColor: theme.palette.risks[30]
    },
    colorEducation: {
        backgroundColor: theme.palette.education[30]
    },
    colorRecreation: {
        backgroundColor: theme.palette.recreation[30]
    },
    colorTransportation: {
        backgroundColor: theme.palette.transportation[30]
    },
    colorSocio: {
        backgroundColor: theme.palette.socio[30]
    },
    colorDemographics: {
        backgroundColor: theme.palette.demographics[30]
    },
    colorShopping: {
        backgroundColor: theme.palette.shopping[30]
    }
});

export default withStyles(styles)(POIPopup);
