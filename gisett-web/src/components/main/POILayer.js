import { LayerGroup, Marker, Popup } from 'react-leaflet';
import { POIIconCreateFunction, POIMarkerIcon } from './mapIcons/CustomIconCreator';
import React, { PureComponent } from 'react';

import { GisettPoiIcons } from '../../resources/icons/icons';
import gisTheme from '../../resources/theme';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import POIPopup from './popups/POIPopup';

const POIMarker = props => {
    // TODO: at the moment it handles only the poiType='Point'. Need to figure it out how to handle the Polylines and Polygons
    return props.pois.filter(poi => poi.geometry.type === 0).map((poi, i) => {
        // get the poiMainCat from the config, based on the known poiSubCategory.
        let poiMainCat = props.poiConfigMap.get(poi.type);
        // GisettPoiIcons containse the mapping between the poiMainCategory and its icon URL.
        let poiIconUrl = GisettPoiIcons.get(poiMainCat);
        let poiPoint = poi.geometry.coordinates[0][0];
        const color = gisTheme.palette[poiMainCat] ? gisTheme.palette[poiMainCat].main : 'white';
        return poiIconUrl ? (
            <Marker
                key={i}
                position={[poiPoint.latitude, poiPoint.longitude]}
                icon={POIMarkerIcon(i, poiIconUrl, color, props.selectedPoi)}
                onClick={() => {
                    props.handlePoiClick(i);
                }}
            >
                <Popup closeButton={false}>
                    <POIPopup poi={poi} category={poiMainCat} />
                </Popup>
            </Marker>
        ) : null;
    });
};

export default class POILayer extends PureComponent {
    shouldComponentUpdate(nextProps) {
        return this.props.pois !== nextProps.pois;
    }

    render() {
        const { map, layerContainer, pois, handlePoiClick, selectedPoi } = this.props;
        return (
            <LayerGroup map={map} layerContainer={layerContainer}>
                <MarkerClusterGroup
                    spiderfyOnMaxZoom={false}
                    zoomToBoundsOnClick={false}
                    showCoverageOnHover={false}
                    disableClusteringAtZoom={16}
                    iconCreateFunction={POIIconCreateFunction}
                >
                    <POIMarker
                        pois={pois.pois}
                        poiConfigMap={pois.poiConfigMap}
                        handlePoiClick={handlePoiClick}
                        selectedPoi={selectedPoi}
                    />
                </MarkerClusterGroup>
            </LayerGroup>
        );
    }
}
