import React, { PureComponent } from 'react';

import Grid from '@material-ui/core/Grid';
import Translator from '../translate/Translator';
import { Typography } from '@material-ui/core';

export default class AreaTabTitle extends PureComponent {
    render() {
        const { classes } = this.props;
        return (
            <Grid container direction="column" justify="flex-end" className={classes.flexGrowForTitle}>
                <Grid item container direction="column" alignItems="center">
                    <Typography variant="display4" color="primary">
                        <Translator i18nKeys="MAIN.AREA_TITLE" />
                    </Typography>
                </Grid>
                <Grid item container direction="column" alignItems="center">
                    <Typography variant="subheading">
                        <Translator i18nKeys="MAIN.AREA_SUB_TITLE" />
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}
