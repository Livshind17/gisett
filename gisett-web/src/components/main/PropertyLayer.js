import { LayerGroup, Marker, Popup } from 'react-leaflet';
import { PropertyIconCreateFunction, PropertyMarkerIcon } from './mapIcons/CustomIconCreator';
import React, { PureComponent } from 'react';
import { GisettMapPin } from '../../resources/icons/icons';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import PropertyCard from './popups/PropertyCard';

const PROPERTY_PIN = 'propertyPin';
const PropertyMarker = props => {
    let propertyIconUrl;
    return props.properties.propertyData.content.map((property, i) => {
        propertyIconUrl = GisettMapPin.get(PROPERTY_PIN);
        return propertyIconUrl ? (
            <Marker
                key={i}
                position={[property.coordinates.y, property.coordinates.x]}
                icon={PropertyMarkerIcon(i, props.selectedPropertyId)}
                onClick={() => {
                    props.handlePropertyClick(i);
                }}
            >
                <Popup closeButton={false}>
                    <PropertyCard property={property} />
                </Popup>
            </Marker>
        ) : null;
    });
};

class PropertyLayer extends PureComponent {
    render() {
        const { map, layerContainer, properties, selectedPropertyId, handlePropertyClick } = this.props;

        return (
            <LayerGroup map={map} layerContainer={layerContainer}>
                <MarkerClusterGroup
                    spiderfyOnMaxZoom={false}
                    zoomToBoundsOnClick={false}
                    showCoverageOnHover={false}
                    disableClusteringAtZoom={16}
                    iconCreateFunction={PropertyIconCreateFunction}
                >
                    {properties.propertyData ? (
                        <PropertyMarker
                            properties={properties}
                            handlePropertyClick={handlePropertyClick}
                            selectedPropertyId={selectedPropertyId}
                        />
                    ) : null}
                </MarkerClusterGroup>
            </LayerGroup>
        );
    }
}

export default PropertyLayer;
