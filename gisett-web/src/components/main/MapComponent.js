import * as IndicatorActionCreators from '../../actions/IndicatorActionCreators';

import { LayerGroup, LayersControl, Map, Marker, Popup, TileLayer } from 'react-leaflet';
import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getReferenceCoordinate } from '../../utils/commons/Helper';
import HeatmapOverlay from 'leaflet-heatmap-radius';
import HeatmapPopup from './popups/HeatmapPopup';
import { OverallMarkerIcon } from './mapIcons/CustomIconCreator';
import POILayer from './POILayer';
import PropertyLayer from './PropertyLayer';
import { withTheme } from '@material-ui/core';

var heatmapOverlay;
class WrappedHeatmapLayer extends Component {
    componentDidMount() {
        const { points, settings } = this.props;
        // No. 1: This HeatmapOverlay is an extenstion of the Patrick Wied's library (leaflet-heatmap-radius) where we can set the radius size in meter.
        heatmapOverlay = new HeatmapOverlay({
            radius: settings.radius,
            maxOpacity: 0.4,
            blur: 0.5,
            scaleRadius: true,
            useLocalExtrema: true,
            latField: 'x',
            lngField: 'y',
            valueField: 'value',
            // these are the default colors, so if we remove the gradient config will display the heatmap with these colors
            gradient: {
                '0': 'blue',
                '0.2': 'royalblue',
                '0.4': 'cyan',
                '0.6': 'lime',
                '0.9': 'yellow',
                '1': 'red'
            }
        });

        heatmapOverlay.setData({
            // Attention! This should be the maximum of value range [0-15]. If not set correctly it will break the heatmap colors.
            // If the data will receive values from [0-100] have to set max to 100.
            max: settings.max,
            data: points
        });

        // add heatmap to the specified layer
        this.LayerGroup.leafletElement.addLayer(heatmapOverlay);
    }

    componentDidUpdate() {
        const { points, settings } = this.props;
        heatmapOverlay.setData({ max: settings.max, data: points });
    }

    render() {
        const { map, layerContainer } = this.props;
        return <LayerGroup ref={node => (this.LayerGroup = node)} map={map} layerContainer={layerContainer} />;
    }
}

const mapStateToProps = state => {
    return {
        cityKey: state.cities.selectedCityKey,
        cityCenterCoordinate: state.cities.cityCenterCoordinate,
        locationCoordinate: state.locations.locationCoordinate,
        heatMap: state.heatMaps,
        heatMapSettings: state.heatMaps.settings,
        heatMapPoints: state.heatMaps.heatMapPoints,
        pois: state.pois,
        properties: state.properties,
        nearestPoint: state.indicators.nearestIndicator
    };
};
const mapDispatchToProps = dispatch => {
    return {
        indicatorActions: bindActionCreators(IndicatorActionCreators, dispatch)
    };
};
export class MapComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activePoiId: null,
            activePropertyId: null
        };
    }
    componentDidUpdate() {
        this.openOverallPopup();
    }
    render() {
        const {
            pois,
            heatMapPoints,
            heatMapSettings,
            properties,
            cityCenterCoordinate,
            locationCoordinate,
            nearestPoint
        } = this.props;
        // for the map requires a coordinate with [latitude, longitude]
        const center = getReferenceCoordinate(cityCenterCoordinate, locationCoordinate);
        return (
            <Map
                center={center}
                zoom={heatMapSettings.zoom}
                zoomControl={false}
                onClick={e => {
                    this.showHeatmapInformation(e);
                }}
            >
                <LayersControl position={this.props.theme.direction === 'rtl' ? 'topleft' : 'topright'}>
                    <LayersControl.BaseLayer name="Map" checked={true}>
                        <TileLayer
                            attribution={process.env.REACT_APP_MAP_ATTRIBUTION}
                            url={process.env.REACT_APP_MAP_URL}
                            minZoom={5}
                            maxZoom={17}
                        />
                    </LayersControl.BaseLayer>
                    <LayersControl.Overlay name="Heatmap">
                        <WrappedHeatmapLayer points={heatMapPoints} settings={heatMapSettings} />
                    </LayersControl.Overlay>
                    <LayersControl.Overlay name="POI">
                        <POILayer
                            pois={pois}
                            handlePoiClick={this.handlePoiClick}
                            selectedPoi={this.state.activePoiId}
                        />
                    </LayersControl.Overlay>
                    <LayersControl.Overlay name="Property">
                        <PropertyLayer
                            properties={properties}
                            handlePropertyClick={this.handlePropertyClick}
                            selectedPropertyId={this.state.activePropertyId}
                        />
                    </LayersControl.Overlay>
                    {nearestPoint ? (
                        <Marker
                            position={nearestPoint.position}
                            ref={ref => (this.overlayMarker = ref)}
                            icon={OverallMarkerIcon()}
                        >
                            <Popup>
                                <HeatmapPopup popup={nearestPoint} />
                            </Popup>
                        </Marker>
                    ) : null}
                </LayersControl>
            </Map>
        );
    }
    showHeatmapInformation(event) {
        const { indicatorActions, cityKey } = this.props;

        indicatorActions.removeNearestIndicatorPoint();
        indicatorActions.fetchNearestIndicatorPoint(cityKey, [event.latlng.lat, event.latlng.lng]);
        this.setState({ activePoiId: null, activePropertyId: null });
    }

    closeOverallPopup = () => {
        if (this.overlayMarker) {
            this.overlayMarker.leafletElement.closePopup();
        }
    };
    openOverallPopup = () => {
        if (this.overlayMarker && this.state.activePoiId === null && this.state.activePropertyId === null) {
            this.overlayMarker.leafletElement.openPopup();
        }
    };

    handlePoiClick = id => {
        this.setState({ activePoiId: id });
        this.closeOverallPopup();
    };

    handlePropertyClick = id => {
        this.setState({ activePropertyId: id });
        this.closeOverallPopup();
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTheme()(MapComponent));
