import * as PropertyActionCreators from '../../actions/PropertyActionCreators';
import React, { Component } from 'react';
import { Tab, Tabs } from '@material-ui/core';
import AreaTab from './AreaTab.js';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { getReferenceCoordinate } from '../../utils/commons/Helper';
import Grid from '@material-ui/core/Grid';
import PropertyTab from './PropertyTab.js';
import PropTypes from 'prop-types';
import Translator from '../translate/Translator.js';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pProperties: state.properties,
        pSelectedCity: state.cities,
        pSelectedLocation: state.locations,
        pHeatMapsSettings: state.heatMaps.settings,
        pIndicators: state.indicators
    };
};

const mapDispatchToProps = dispatch => {
    return {
        propertyActions: bindActionCreators(PropertyActionCreators, dispatch)
    };
};

export class GisettTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 0
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = selectedTab => {
        this.setState({ selectedTab });
    };

    fetchAllProperties() {
        const {
            propertyActions,
            pSelectedLocation,
            pHeatMapsSettings,
            pIndicators,
            pProperties,
            pSelectedCity
        } = this.props;
        const coordinates = getReferenceCoordinate(
            pSelectedCity.cityCenterCoordinate,
            pSelectedLocation.locationCoordinate
        );
        propertyActions.filterProperties(
            pSelectedCity.selectedCityKey,
            coordinates,
            pHeatMapsSettings.distance,
            pProperties.filterObject,
            pProperties.selectedSortType,
            pIndicators
        );
    }

    render() {
        const { classes } = this.props;
        const { selectedTab } = this.state;
        return (
            <Grid container direction="column" style={{ height: '100%' }}>
                <Tabs fullWidth value={selectedTab} className={classNames(classes.tabBar)}>
                    <Tab
                        label={<Translator i18nKeys="MAIN.LOCATION" />}
                        onClick={() => {
                            this.handleChange(0);
                        }}
                        className={classNames(
                            this.state.selectedTab === 0 ? classes.tabsSelected : classes.tabsBack,
                            classes.locationTab
                        )}
                    />

                    <Tab
                        label={<Translator i18nKeys="MAIN.PROPERTIES" />}
                        onClick={() => {
                            this.handleChange(1);
                            this.fetchAllProperties();
                        }}
                        className={classNames(
                            this.state.selectedTab === 1 ? classes.tabsSelected : classes.tabsBack,
                            classes.propertyTab
                        )}
                    />
                </Tabs>
                {selectedTab === 0 ? <AreaTab {...classes} /> : <PropertyTab {...classes} />}
            </Grid>
        );
    }
}

const styles = theme => ({
    tabBar: {
        color: theme.palette.secondary.main,
        backgroundColor: theme.palette.background.main
    },

    tabsSelected: {
        color: theme.palette.primary.main
    },
    tabsBack: {
        backgroundColor: theme.palette.grid.main,
        color: theme.palette.grid.font
    },
    area: {
        backgroundColor: theme.palette.background.main
    },
    propertyTab: {
        width: '550px'
    }
});
GisettTabs.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(GisettTabs));
