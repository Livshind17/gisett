import * as PoiActionCreators from '../../actions/PoiActionCreators';
import { CheckBox, CheckBoxOutlineBlank, ExpandMore, NavigateNext } from '@material-ui/icons';
import { Checkbox, Collapse, List, ListItem, Typography, withStyles } from '@material-ui/core';
import DrawerButton, { DRAWER_BUTTON_STYLE_LEFT } from '../commons/DrawerButton';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import CheckedEye from '../../resources/images/checked_pois.png';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { GISETT_DRAWER_ALIGN_RIGHT } from '../commons/GisettDrawer';
import { GisettPoiIcons } from '../../resources/icons/icons';
import Grid from '@material-ui/core/Grid';
import Translator from '../translate/Translator';

const mapStateToProps = state => {
    return {
        pCityPoiCategories: state.cities.selectedCityConfig.poiCategories
    };
};

const mapDispatchToProps = dispatch => {
    return {
        poiActions: bindActionCreators(PoiActionCreators, dispatch)
    };
};
const GISETT_MAIN_DRAWER_CONTAINER_RTL = {
    button: DRAWER_BUTTON_STYLE_LEFT,
    drawer: GISETT_DRAWER_ALIGN_RIGHT
};

class POITypesLegend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openedCheckboxList: [],
            checkedPoiCategories: [],
            checkedPoiTypes: [],
            drawerIsOpen: true
        };
    }

    isAlreadyExistInList(listElementIndex) {
        return listElementIndex !== -1;
    }

    getCurrentIndexAndClonedList(value, list) {
        let currentIndex = list.indexOf(value);
        let savedList = [...list];
        return { currentIndex, savedList };
    }

    handleToggle = value => () => {
        let openCheckboxes = this.getCurrentIndexAndClonedList(value, this.state.openedCheckboxList);
        this.isAlreadyExistInList(openCheckboxes.currentIndex)
            ? openCheckboxes.savedList.splice(openCheckboxes.currentIndex, 1)
            : openCheckboxes.savedList.push(value);
        this.setState({
            openedCheckboxList: openCheckboxes.savedList
        });
    };

    handleCheckOfPoiCategories = event => {
        let poiCategoryValue = event.target.value;
        const { checkedPoiCategories, checkedPoiTypes } = this.state;
        const { pCityPoiCategories, poiActions } = this.props;
        const newCheckedPoiTypes = [...checkedPoiTypes];
        let poiCategories = this.getCurrentIndexAndClonedList(poiCategoryValue, checkedPoiCategories);

        // if already checked the current category
        if (this.isAlreadyExistInList(poiCategories.currentIndex)) {
            // then remove his value form the category checked representation list
            poiCategories.savedList.splice(poiCategories.currentIndex, 1);
            // and remove his child elements too from the poiTypes checked representation list
            this.props.pCityPoiCategories.find(item => item.key === poiCategoryValue).poiTypes.forEach(element => {
                if (newCheckedPoiTypes.includes(element))
                    newCheckedPoiTypes.splice(newCheckedPoiTypes.indexOf(element), 1);
            });
            // if isn't checked the current category yet
        } else {
            // then put into the new category value and his child elements too in the representation list
            poiCategories.savedList.push(poiCategoryValue);
            pCityPoiCategories.find(item => item.key === poiCategoryValue).poiTypes.forEach(i => {
                if (!newCheckedPoiTypes.includes(i)) newCheckedPoiTypes.push(i);
            });
        }
        this.setState({
            checkedPoiCategories: poiCategories.savedList,
            checkedPoiTypes: newCheckedPoiTypes
        });
        poiActions.setSelectedPoiTypes(newCheckedPoiTypes);
    };

    handleCheckOfPoiTypes = categoriesValue => event => {
        const { checkedPoiTypes, checkedPoiCategories } = this.state;
        const { pCityPoiCategories, poiActions } = this.props;
        let value = event.target.value;
        let poiTypes = this.getCurrentIndexAndClonedList(value, checkedPoiTypes);
        let poiCategories = this.getCurrentIndexAndClonedList(categoriesValue, checkedPoiCategories);
        let anyPoiTypeChecked = false;

        // if already checked the current poiType
        if (this.isAlreadyExistInList(poiTypes.currentIndex)) {
            // then remove his value form the poyTypes checked representation list
            poiTypes.savedList.splice(poiTypes.currentIndex, 1);
            // and check if any poiType it's checked or not
            pCityPoiCategories.find(item => item.key === categoriesValue).poiTypes.find(i => {
                return (anyPoiTypeChecked = poiTypes.savedList.includes(i));
            });
            // because if all types is unchecked then the category checkeBox must be unchecked too
            if (!anyPoiTypeChecked) poiCategories.savedList.splice(poiCategories.currentIndex, 1);
        } else {
            if (poiCategories.currentIndex === -1) poiCategories.savedList.push(categoriesValue);
            poiTypes.savedList.push(value);
        }
        this.setState({
            checkedPoiTypes: poiTypes.savedList,
            checkedPoiCategories: poiCategories.savedList
        });

        poiActions.setSelectedPoiTypes(poiTypes.savedList);
    };

    handleDrawer = () => {
        this.setState({
            drawerIsOpen: !this.state.drawerIsOpen,
            openedCheckboxList: []
        });
    };

    createListElement(
        element,
        index,
        classes,
        drawerIsOpen,
        openedCheckboxList,
        checkedPoiTypes,
        checkedPoiCategories
    ) {
        return (
            <div key={index} className={classes.listItems}>
                <ListItem
                    key={index}
                    className={classes.listElement}
                    button
                    onClick={drawerIsOpen ? this.handleToggle(element.key) : null}
                    value={element.key}
                >
                    <Checkbox
                        tabIndex={0}
                        checked={checkedPoiCategories.includes(element.key)}
                        onClick={this.handleCheckOfPoiCategories}
                        checkedIcon={<img src={CheckedEye} />}
                        value={element.key}
                    />
                    <Grid item xs={2}>
                        <img className={classes.poiIcons} src={GisettPoiIcons.get(element.key)} alt="Icon" />
                    </Grid>
                    {drawerIsOpen ? (
                        <Grid item xs={6}>
                            <Typography className={classes.parentElementsText}>
                                <Translator i18nKeys={'POI_CATEGORY.' + element.key.toUpperCase()} />
                            </Typography>
                        </Grid>
                    ) : null}
                    {drawerIsOpen ? (
                        <Grid item xs={2}>
                            {openedCheckboxList.includes(element.key) ? (
                                <ExpandMore className={classes.expandIcon} />
                            ) : (
                                <NavigateNext className={classes.expandIcon} />
                            )}
                        </Grid>
                    ) : null}
                </ListItem>
                <Collapse in={openedCheckboxList.includes(element.key)} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {element.poiTypes.map((elem, index) => (
                            <ListItem key={index} button className={classes.listElement} value={elem}>
                                <Checkbox
                                    style={{ fontSize: 20 }}
                                    key={index}
                                    tabIndex={0}
                                    value={elem}
                                    checked={checkedPoiTypes.includes(elem)}
                                    onClick={this.handleCheckOfPoiTypes(element.key)}
                                    icon={<CheckBoxOutlineBlank className={classes.childCheckBox} />}
                                    checkedIcon={<CheckBox className={classes.childCheckBox} />}
                                />

                                <Grid item>
                                    <Typography className={classes.childElementsText}>
                                        <Translator i18nKeys={'POI_TYPE.' + elem.toUpperCase()} />
                                    </Typography>
                                </Grid>
                            </ListItem>
                        ))}
                    </List>
                </Collapse>
            </div>
        );
    }

    render() {
        const { classes, pCityPoiCategories } = this.props;
        const { drawerIsOpen, openedCheckboxList, checkedPoiTypes, checkedPoiCategories } = this.state;
        const listViewComponent = pCityPoiCategories.map((element, index) =>
            this.createListElement(
                element,
                index,
                classes,
                drawerIsOpen,
                openedCheckboxList,
                checkedPoiTypes,
                checkedPoiCategories
            )
        );
        const drawerTypeWithDirection = GISETT_MAIN_DRAWER_CONTAINER_RTL;
        const drawerButton = (
            <DrawerButton
                id={1}
                gisStyle={drawerTypeWithDirection.button}
                onClick={() => {
                    this.handleDrawer();
                }}
                fixedWidth="true"
            >
                {drawerIsOpen}
            </DrawerButton>
        );

        return (
            <div>
                <Grid
                    container
                    className={classNames(classes.list, {
                        [classes.expandLessPanel]: !this.state.drawerIsOpen
                    })}
                >
                    <ListItem
                        key={1}
                        className={classNames(classes.listHeader, {
                            [classes.expandLessHeader]: !this.state.drawerIsOpen
                        })}
                    >
                        <Grid item xs={12} className={classes.headerText}>
                            <Typography variant={'body2'}>
                                {this.state.drawerIsOpen ? (
                                    <Translator i18nKeys={'DATA.POINT_OF_INTEREST'} />
                                ) : (
                                    <Translator i18nKeys={'DATA.POI'} />
                                )}
                            </Typography>
                        </Grid>
                    </ListItem>
                    {listViewComponent}
                </Grid>
                <div
                    className={classNames(classes.drawerButton, {
                        [classes.expandLessButton]: !this.state.drawerIsOpen
                    })}
                >
                    {drawerButton}
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    list: {
        backgroundColor: 'white',
        position: 'fixed',
        top: 200,
        right: 6,
        height: '100%',
        maxWidth: 230,
        maxHeight: 470,
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    drawerButton: {
        top: 234,
        right: 235,
        position: 'fixed'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4
    },
    listElement: {
        width: '100%',
        height: 28,
        padding: 0
    },
    poiIcons: {
        width: 44,
        height: 44,
        margin: '0px 1px -10px -12px'
    },
    expandIcon: {
        color: '#CDD1D4',
        marginLeft: 10
    },
    parentElementsText: {
        fontFamily: 'Nunito Sans Bold',
        fontSize: 14
    },
    childElementsText: {
        fontFamily: 'Nunito Sans Regular',
        fontSize: 12
    },
    listHeader: {
        width: '100%',
        padding: 0,
        height: 32,
        backgroundColor: theme.palette.poiHeader.main
    },
    headerText: {
        textAlign: 'center'
    },
    expandLessPanel: {
        maxWidth: 82
    },
    expandLessHeader: {
        alignContent: 'center'
    },
    expandLessButton: {
        right: 88
    },
    childCheckBox: {
        fontSize: 17
    },
    listItems: {
        width: '100%'
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(POITypesLegend));
