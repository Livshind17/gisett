import * as HeatMapActionCreators from '../../actions/HeatMapActionCreators';
import * as IndicatorActionCreators from '../../actions/IndicatorActionCreators';
import React, { Component } from 'react';

import { applyDrag } from './util';
import AreaTabTitle from './AreaTabTitle';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DragAndDropZone from './DragAndDropZone';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core';

const columnInformation = [
    { name: 'MUST_HAVE', panelKey: 'mustHave' },
    { name: 'VERY_IMPORTANT', panelKey: 'veryImportant' },
    { name: 'IMPORTANT', panelKey: 'important' },
    { name: 'NICE_TO_HAVE', panelKey: 'niceToHave' },
    { name: 'ALL', panelKey: 'all' }
];

export const stageEnum = { DISABLED: 0, BASE: 1, PANEL: 2 };

const mapStateToProps = state => {
    return {
        pIndicatorCategories: state.cities.selectedCityConfig.indicatorCategories,
        indicatorSettings: state.indicators.indicatorSettings,
        indicatorPoints: state.indicators.indicatorPoints
    };
};

const mapDispatchToProps = dispatch => {
    return {
        heatmapActions: bindActionCreators(HeatMapActionCreators, dispatch),
        indicatorActions: bindActionCreators(IndicatorActionCreators, dispatch)
    };
};

export class AreaTab extends Component {
    constructor(props) {
        super(props);
        this.onCardDrop = this.onCardDrop.bind(this);
        this.getCardPayload = this.getCardPayload.bind(this);
        const { indicatorSettings } = this.props;

        // TODO: It should move this local state to application state. Need to check this case. Leave this way until the release.
        this.state = {
            settings: {
                mustHave: indicatorSettings.mustHave,
                veryImportant: indicatorSettings.veryImportant,
                important: indicatorSettings.important,
                niceToHave: indicatorSettings.niceToHave,
                all: indicatorSettings.all
            }
        };
    }

    render() {
        const { classes, heatmapActions, indicatorPoints, indicatorSettings } = this.props;
        const base = columnInformation[columnInformation.length - 1];
        return (
            <Grid container direction="column" justify="flex-start" className={classes.area}>
                <AreaTabTitle classes={classes} />
                <DragAndDropZone
                    columnInformation={columnInformation.slice(0, 4)}
                    base={base}
                    classes={classes}
                    onCardDrop={this.onCardDrop}
                    cardPayload={this.getCardPayload}
                    onUpdateButtonPressed={() => {
                        heatmapActions.updateHeatMap(indicatorPoints, indicatorSettings);
                    }}
                    draggableElements={indicatorSettings} // TODO: here should pass the indicatorSettings
                />
            </Grid>
        );
    }

    getCardPayload(columnId, index) {
        return this.state.settings[columnId][index];
    }

    getPayloadIndex(columnId, payload) {
        return columnId !== 'all' ? this.state.settings[columnId].indexOf(payload) : null;
    }

    // @Param (columnId) index of the respective panel (must_have, all, etc)
    // @Param (dropResult) object which contains
    //      - removedIndex: the index where the element was removed
    //      - addedIndex:   the index where the element was added
    //      - payload:      the actual element which is moved (an object)
    //      - returnsToBase: is set when the onDrop normally is not called, eg: when the drop
    // ends over the base panel or anywhere else. On that case the moved payload is returned
    // back to the base panel
    onCardDrop(columnId, dropResult) {
        const settings = { ...this.state.settings };
        if (dropResult.returnsToBase) {
            dropResult.removedIndex = this.getPayloadIndex(columnId, dropResult.payload);
            this.elementRemove(dropResult.payload);
        }

        if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
            if (dropResult.payload.stage !== stageEnum.DISABLED) {
                let newColumn = settings[columnId];

                newColumn = applyDrag(newColumn, dropResult);
                settings[columnId] = newColumn;

                this.setState({
                    settings
                });
                this.props.indicatorActions.updateIndicatorGroupImportance(settings);
            }
        }
    }

    // @Param (element) the element which will be removed from the upper pannels
    // so in the base panel it is set its stage value to BASE
    // the respective element is again accessible
    elementRemove(element) {
        const base = { ...this.state.settings };

        base['all'].map(elem => {
            if (elem.key === element.key) {
                elem.stage = stageEnum.BASE;
            }
        });

        this.setState({
            settings: base
        });
        this.props.indicatorActions.updateIndicatorGroupImportance(base);
    }
}

const styles = theme => ({
    flexGrowForTitle: {
        // flex: '1 0 125px',
        margin: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },

    flexGrowForBase: {
        marginTop: 20,
        flex: '2 0 auto'
    },
    flexGrowForPanels: {
        marginTop: 20,
        flexGrow: 3,
        minHeight: 250,
        maxHeight: 300,
        // overflow: 'auto',
        justifyContent: 'space-around'
    },
    dndZoneContainer: {
        // flex: 1,
        justifyContent: 'space-between',
        minHeight: 600
    },

    area: {
        backgroundColor: theme.palette.background.main,
        alignItems: 'center',
        flexGrow: 1,
        flexWrap: 'nowrap',
        minHeight: 0,
        height: 'calc(100% - 48px)',
        overflow: 'auto'
    },
    indicatorAlign: {
        margin: 3,
        height: 'auto'
    },
    gridComponent: {
        display: 'flex',
        flex: 1,
        height: '100%',
        width: '100%',
        backgroundColor: theme.palette.grid.main,
        color: theme.palette.grid.font
    },
    indicatorComponent: {
        backgroundColor: theme.palette.background.main
    },
    root: {
        paddingLeft: 0,
        paddingRight: 0,
        display: 'flex',
        flex: 1
    },
    grid: {
        width: 140,
        flexGrow: 4,
        flexWrap: 'nowrap',
        textAlign: 'center'
    },
    styleBase: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    fontSize: {
        fontSize: 16
    },
    showMapButton: {
        alignSelf: 'flex-end'
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(AreaTab));
