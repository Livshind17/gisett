import DrawerContainer, { GISETT_MAIN_DRAWER_CONTAINER } from '../commons/DrawerContainer.js';
import React, { Component } from 'react';

import GisettTabs from './GisettTabs.js';
import Grid from '@material-ui/core/Grid';
import MapComponent from './MapComponent';
import POITypesLegend from './POITypesLegend';
import { withStyles } from '@material-ui/core';

export class MainView extends Component {
    render() {
        const { classes } = this.props;

        return (
            <Grid container className={classes.root}>
                <DrawerContainer floatingElement={true} drawerType={GISETT_MAIN_DRAWER_CONTAINER}>
                    <GisettTabs />
                    <POITypesLegend floatingElement={true} drawerType={GISETT_MAIN_DRAWER_CONTAINER} />
                </DrawerContainer>
                <Grid item xs={12}>
                    <MapComponent />
                </Grid>
            </Grid>
        );
    }
}

const styles = () => ({
    root: {
        position: 'relative',
        overflow: 'hidden'
    }
});

export default withStyles(styles)(MainView);
