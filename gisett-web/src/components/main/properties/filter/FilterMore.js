import * as CONST from '../../../../utils/GisettConstants';
import { Button, Card, Checkbox, Grid, ListItem, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const CheckboxList = param => {
    const { classes } = param;
    return (
        <div>
            <Typography variant="body2" className={classes.title}>
                <Translator i18nKeys={param.checkBoxHeader} />
            </Typography>
            <Grid container className={classes.gridPanels} direction="row">
                {param.listItems.map((element, index) => (
                    <Grid item xs={6} key={index}>
                        <ListItem
                            key={index}
                            button
                            className={classes.listElement}
                            onClick={param.handleToggle(index, param.checkedElements, param.isAmanities)}
                        >
                            <Checkbox checked={param.checkedElements.indexOf(index) !== -1} tabIndex={-1} />
                            <Typography variant="title">
                                <Translator i18nKeys={'PROPERTY.' + element} />
                            </Typography>
                        </ListItem>
                    </Grid>
                ))}
            </Grid>
        </div>
    );
};

class FilterMore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            floorInterval: props.floorInterval,
            builtYearInterval: props.builtYear,
            checkedAmenitites: props.checkedAmenities,
            checkedPets: props.checkedPets
        };
    }

    onChangeFloorStartPoint = val => {
        this.setState({
            floorInterval: [val.target.value, this.state.floorInterval[1]]
        });
        this.props.returnIntervals(true, [val.target.value, this.state.floorInterval[1]]);
    };

    onChangeFloorEndPoint = val => {
        this.setState({
            floorInterval: [this.state.floorInterval[0], val.target.value]
        });
        this.props.returnIntervals(true, [this.state.floorInterval[0], val.target.value]);
    };
    onChangeYearStartPoint = val => {
        this.setState({
            builtYearInterval: [val.target.value, this.state.builtYearInterval[1]]
        });
        this.props.returnIntervals(false, [val.target.value, this.state.builtYearInterval[1]]);
    };

    onChangeYearEndPoint = val => {
        this.setState({
            builtYearInterval: [this.state.builtYearInterval[0], val.target.value]
        });
        this.props.returnIntervals(false, [this.state.builtYearInterval[0], val.target.value]);
    };

    handleToggle = (value, checkedElements, isAmanities) => () => {
        const currentIndex = checkedElements.indexOf(value);
        const newChecked = [...checkedElements];

        currentIndex === -1 ? newChecked.push(value) : newChecked.splice(currentIndex, 1);

        if (isAmanities)
            this.setState({
                checkedAmenitites: newChecked
            });
        else
            this.setState({
                checkedPets: newChecked
            });
        this.props.returnSelectedElements(isAmanities, newChecked);
    };

    resetAllFilters() {
        this.setState({
            floorInterval: [],
            builtYearInterval: [],
            checkedAmenitites: [],
            checkedPets: []
        });

        this.props.returnSelectedElements(true, []);
        this.props.returnSelectedElements(false, []);
        this.props.returnIntervals(true, []);
        this.props.returnIntervals(false, []);
    }

    render() {
        const { classes } = this.props;
        const { floorInterval, builtYearInterval } = this.state;
        return (
            <Card className={classes.root}>
                <Typography variant="body2" className={classes.title}>
                    <Translator i18nKeys="PROPERTY.FLOOR" />
                </Typography>

                <Grid container className={classes.gridPanels}>
                    <Typography variant="title" className={classes.subtitleFloor}>
                        <Translator i18nKeys="PROPERTY.MIN_FLOOR" />
                    </Typography>

                    <Typography variant="title" className={classes.subtitleFloor}>
                        <Translator i18nKeys="PROPERTY.MAX_FLOOR" />
                    </Typography>

                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            onChange={this.onChangeFloorStartPoint}
                            value={floorInterval[0]}
                        />
                    </Card>
                    <Typography variant="title" className={classes.label}>
                        -
                    </Typography>
                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            onChange={this.onChangeFloorEndPoint}
                            value={floorInterval[1]}
                        />
                    </Card>
                </Grid>

                <CheckboxList
                    listItems={this.props.amenitiesListElements}
                    checkBoxHeader="PROPERTY.AMENITIES"
                    handleToggle={this.handleToggle}
                    classes={classes}
                    checkedElements={this.state.checkedAmenitites}
                    isAmanities={true}
                />
                <CheckboxList
                    listItems={this.props.petsListElements}
                    checkBoxHeader="PROPERTY.PETS_ALLOVANCE"
                    handleToggle={this.handleToggle}
                    checkedElements={this.state.checkedPets}
                    classes={classes}
                    isAmanities={false}
                />
                <Typography variant="body2" className={classes.title}>
                    <Translator i18nKeys="PROPERTY.YEAR_BUILT" />
                </Typography>
                <Grid container className={classes.gridPanels}>
                    <Typography variant="title" className={classes.subtitleYear}>
                        <Translator i18nKeys="PROPERTY.MIN_YR" />
                    </Typography>

                    <Typography variant="title" className={classes.subtitleYear}>
                        <Translator i18nKeys="PROPERTY.MAX_YR" />
                    </Typography>

                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            onChange={this.onChangeYearStartPoint}
                            value={builtYearInterval[0]}
                        />
                    </Card>
                    <Typography variant="title" className={classes.label}>
                        -
                    </Typography>
                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            onChange={this.onChangeYearEndPoint}
                            value={builtYearInterval[1]}
                        />
                    </Card>
                </Grid>
                <Grid container direction="column" alignItems="stretch">
                    <Button
                        variant="outlined"
                        color="primary"
                        className={classes.button}
                        onClick={this.resetAllFilters.bind(this)}
                    >
                        <Translator i18nKeys="PROPERTY.RESET" />
                    </Button>
                </Grid>
            </Card>
        );
    }
}

const styles = theme => ({
    root: {
        width: 370,
        height: 755,
        padding: 12,
        overflow: 'auto',
        '@media (max-height: 900px)': {
            height: '530px'
        }
    },
    textField: {
        padding: 10,
        width: 80,
        variant: theme.typography.body3,
        textAlign: 'center',
        border: 0,
        fontFamily: 'Nunito Sans Regular',
        fontSize: 16
    },
    label: {
        margin: 10,
        display: 'inline'
    },
    title: {
        margin: '10px 0px 0px 12px'
    },
    subtitleFloor: {
        margin: '1px 70px 8px 2px',
        opacity: 0.6
    },
    subtitleYear: {
        margin: '1px 90px 8px 2px',
        opacity: 0.6
    },
    gridPanels: {
        borderBottom: '2px solid rgba(0, 0, 0, 0.23)',
        padding: 14
    },
    button: {
        margin: theme.spacing.unit,
        color: theme.palette.propertyButton.main,
        borderColor: theme.palette.propertyButton.main
    },
    listElement: {
        height: 32,
        padding: 0
    }
});
export default withStyles(styles)(FilterMore);
