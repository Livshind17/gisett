import * as CONST from '../../../../utils/GisettConstants';
import { Card, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import MultiSlider from 'multi-slider';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

class FilterSquareMeter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderValues: [
                props.minSquare,
                props.middle,
                CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX - props.maxSquare
            ],
            endSquareMeter: props.maxSquare
        };
    }

    onChange = val => {
        this.setState({
            endSquareMeter: CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX - val[2]
        });
        this.setState({
            sliderValues: val
        });
        this.props.returnSelectedElement(
            val[0],
            CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX - val[0] - val[2],
            CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX - val[2]
        );
    };

    render() {
        const { classes, theme } = this.props;
        var colors = Array(3).fill(theme.palette.secondary.main);
        const TextLabel =
            this.state.sliderValues[0] == CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MIN &&
            this.state.endSquareMeter === CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX ? (
                <Typography variant="title">
                    <Translator i18nKeys="PROPERTY.ALL_SQUARE" />
                </Typography>
            ) : (
                <Typography variant="title">
                    <Translator
                        i18nKeys="PROPERTY.SQUARE_METER_PARAMETE"
                        insertValue={{
                            min: this.state.sliderValues[0],
                            max: this.state.endSquareMeter
                        }}
                    />
                </Typography>
            );

        return (
            <Card className={classes.root}>
                <Grid container style={{ padding: 12 }} direction="row" justify="space-evenly">
                    {TextLabel}
                </Grid>
                <Grid container style={{ height: 30 }}>
                    <MultiSlider
                        handleSize={10}
                        colors={colors}
                        values={this.state.sliderValues}
                        padX={50}
                        onChange={this.onChange}
                        width={500}
                        height={5}
                        trackSize={3}
                        handleStrokeSize={12}
                        handleInnerDotSize={8}
                        bg={theme.palette.sliderBackgorund.main}
                    />
                </Grid>
            </Card>
        );
    }
}

const styles = theme => ({
    root: {
        width: 380,
        height: 80,
        padding: '12px 0px 12px 0px'
    }
});

export default withStyles(styles, { withTheme: true })(FilterSquareMeter);
