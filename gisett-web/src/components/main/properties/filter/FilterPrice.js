import * as CONST from '../../../../utils/GisettConstants';
import { Button, Card, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import MultiSlider from 'multi-slider';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

class FilterPrice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderValue: [
                props.minPrice,
                CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE,
                CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE - props.maxPrice
            ],
            endPrice: props.maxPrice
        };
    }

    onChange = val => {
        this.setState({
            endPrice: CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE - val[2],
            sliderValue: val
        });
    };

    onChangeStartPoint = val => {
        let value = val.target.value;
        if (value >= CONST.PROPERTY_FILTER_TEXTS.MIN_PRICE && value <= CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE)
            this.setState({
                sliderValue: [Number(value), this.state.endPrice - Number(value), this.state.sliderValue[2]]
            });
    };

    onChangeEndPoint = val => {
        let value = val.target.value;
        if (value <= CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE)
            this.setState({
                endPrice: value,
                sliderValue: [
                    this.state.sliderValue[0],
                    Number(value),
                    CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE - Number(value)
                ]
            });
    };

    doneButton() {
        this.props.returnSelectedElement(
            this.state.sliderValue[0],
            CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE - this.state.sliderValue[2]
        );
        this.props.visibilitySetter(false);
    }

    render() {
        const { classes, theme } = this.props;
        var colors = Array(3).fill(theme.palette.secondary.main);

        return this.props.visible ? (
            <Card className={classes.root}>
                <Typography variant="body2" className={classes.title}>
                    <Translator i18nKeys="PROPERTY.PRICE_RANGE" />
                </Typography>
                <Grid container style={{ padding: 15 }} direction="row" justify="space-evenly">
                    <span className={classes.currencyLabel}>
                        <Translator i18nKeys={'PROPERTY.' + CONST.PROPERTY_FILTER_TEXTS.CURRENCY_DOLLAR} />
                    </span>
                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            value={this.state.sliderValue[0]}
                            onChange={this.onChangeStartPoint}
                        />
                    </Card>
                    <Typography variant="title" className={classes.label}>
                        <Translator i18nKeys="PROPERTY.TO" />
                    </Typography>
                    <Card>
                        <input
                            className={classes.textField}
                            type="text"
                            value={this.state.endPrice}
                            onChange={this.onChangeEndPoint}
                        />
                    </Card>
                </Grid>
                <Grid container style={{ height: 50 }}>
                    <MultiSlider
                        handleSize={10}
                        colors={colors}
                        values={this.state.sliderValue}
                        padX={50}
                        onChange={this.onChange}
                        width={500}
                        height={5}
                        trackSize={3}
                        handleStrokeSize={12}
                        handleInnerDotSize={8}
                        bg={theme.palette.sliderBackgorund.main}
                    />
                </Grid>
                <Grid container direction="column" alignItems="flex-end" justify="flex-end">
                    <Button
                        variant="outlined"
                        color="primary"
                        className={classes.button}
                        onClick={this.doneButton.bind(this)}
                    >
                        <Translator i18nKeys="PROPERTY.DONE" />
                    </Button>
                </Grid>
            </Card>
        ) : null;
    }
}

const styles = theme => ({
    root: {
        width: 360,
        height: 200,
        padding: 12,
        position: 'absolute',
        zIndex: 1,
        left: 100
    },
    textField: {
        padding: 10,
        width: 120,
        variant: theme.typography.body3,
        textAlign: 'center',
        border: 0,
        fontFamily: 'Nunito Sans Regular',
        fontSize: 16
    },
    label: {
        margin: 10,
        display: 'inline'
    },
    button: {
        margin: theme.spacing.unit,
        alignSelf: 'flex-end',
        color: theme.palette.secondary.main
    },
    title: {
        margin: 5,
        opacity: 0.5,
        marginBottom: 2,
        fontSize: 15,
        fontFamily: 'Nunito Sans Regular'
    },
    currencyLabel: {
        fontSize: 18,
        fontfamily: ' Nunito Sans Regular',
        margin: '10px 3px 0px -4px'
    }
});

export default withStyles(styles, { withTheme: true })(FilterPrice);
