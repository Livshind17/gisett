import React, { Component } from 'react';
import { Card } from '@material-ui/core';
import InlineList from '../../../commons/InlineList';
import { withStyles } from '@material-ui/core/styles';

class FilterBedsAndBath extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndexBeds: props.checkedBedroomTypes,
            selectedIndexBath: props.checkedBathroomTypes
        };
    }

    render() {
        const {
            classes,
            bedroomListItems,
            bathroomListItems,
            checkedBedroomTypes,
            returnSelectedElements,
            checkedBathroomTypes
        } = this.props;
        return (
            <Card className={classes.root} direction="row">
                <InlineList
                    listItems={bedroomListItems}
                    listHeader="PROPERTY.BEDROOMS"
                    returnSelectedElements={returnSelectedElements}
                    checkedRoomsTypes={checkedBedroomTypes}
                    isBedroom={true}
                />
                <InlineList
                    listItems={bathroomListItems}
                    listHeader="PROPERTY.BATH"
                    returnSelectedElements={returnSelectedElements}
                    checkedRoomsTypes={checkedBathroomTypes}
                    isBedroom={false}
                />
            </Card>
        );
    }
}

const styles = theme => ({
    root: {
        width: 360,
        height: 160,
        backgroundColor: '#FFFFFF',
        padding: 15
    }
});
export default withStyles(styles)(FilterBedsAndBath);
