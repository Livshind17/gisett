import { Card, Checkbox, List, ListItem, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

class FilterTypes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checkedElement.concat(Array(props.listElements.length - 1).fill(false))
        };
    }

    handleToggle = index => () => {
        const { checked } = this.state;
        const newChecked = [...checked];
        newChecked[index] = !newChecked[index];
        index != 0 ? (newChecked[0] = false) : newChecked.fill(false, 1);
        if (newChecked.filter(Boolean).length != 0) {
            this.setState({
                checked: newChecked
            });
            this.props.returnSelectedElements(newChecked);
        }
    };

    render() {
        const { classes, listElements } = this.props;
        return (
            <Card>
                <List className={classes.root} component="nav">
                    {listElements.map((element, index) => (
                        <ListItem key={index} button className={classes.listElement} onClick={this.handleToggle(index)}>
                            <Checkbox tabIndex={0} checked={this.state.checked[index]} disableRipple />
                            <Typography variant="title">
                                <Translator i18nKeys={'PROPERTY.' + element} />
                            </Typography>
                        </ListItem>
                    ))}
                </List>
            </Card>
        );
    }
}

const styles = theme => ({
    root: {
        backgroundColor: '#FFFFFF',
        padding: 0
    },
    listElement: {
        width: 245,
        height: 32
    }
});
export default withStyles(styles)(FilterTypes);
