import { Card, List, ListItem } from '@material-ui/core';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import Translator from '../../../translate/Translator';

class FilterSortBy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: props.checkedElement
        };
    }

    handleListItemClick = (event, index) => {
        this.setState({ selectedIndex: index });
        this.props.returnSelectedElements(index);
    };

    render() {
        const { classes } = this.props;
        return (
            <MuiThemeProvider theme={muiTheme}>
                <Card>
                    <List className={classes.root} component="nav">
                        {this.props.listElements.map((element, index) => (
                            <ListItem
                                key={index}
                                button
                                classes={{ selected: classes.listItem }}
                                className={classes.listElement}
                                selected={this.state.selectedIndex === index}
                                onClick={event => this.handleListItemClick(event, index)}
                            >
                                <Translator i18nKeys={'PROPERTY.' + element} />
                            </ListItem>
                        ))}
                    </List>
                </Card>
            </MuiThemeProvider>
        );
    }
}

const styles = theme => ({
    root: {
        width: 200,
        height: 280,
        padding: 0
    },
    listElement: {
        width: 200,
        height: 32,
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize
    },
    listHeader: {
        background: theme.palette.secondary.main,
        fontSize: theme.typography.fontSize,
        color: '#FFFFFF'
    }
});

const muiTheme = outerTheme => ({
    ...outerTheme,
    overrides: {
        MuiListItem: {
            root: {
                background: '#FFFFFF',
                fontSize: 14,
                '&$selected': {
                    backgroundColor: '#00505B',
                    color: '#FFFFFF'
                },
                '&$selected:hover': {
                    backgroundColor: '#00505B',
                    color: '#FFFFFF'
                }
            }
        }
    }
});
export default withStyles(styles)(FilterSortBy);
