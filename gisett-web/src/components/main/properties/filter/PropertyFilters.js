import * as CONST from '../../../../utils/GisettConstants';
import * as PropertyActionCreators from '../../../../actions/PropertyActionCreators';
import { Button, ClickAwayListener, Grid, Grow, Popper, Typography } from '@material-ui/core';
import GisettButton, { GISSET_BUTTON_STYLE_1, GISSET_BUTTON_STYLE_2 } from '../../../commons/GisettButton';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {ExpandLess,ExpandMore} from '@material-ui/icons';
import FilterBedsAndBath from './FilterBedsAndBath';
import FilterMore from './FilterMore';
import FilterPrice from './FilterPrice';
import FilterSortBy from './FilterSortBy';
import FilterSquareMeter from './FilterSquareMeter';
import FilterTypes from './FilterTypes';
import { getReferenceCoordinate } from '../../../../utils/commons/Helper';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pProperties: state.properties,
        pSelectedCity: state.cities,
        pSelectedLocation: state.locations,
        pHeatMapsSettings: state.heatMaps.settings,
        pIndicators: state.indicators
    };
};

const mapDispatchToProps = dispatch => {
    return {
        propertyActions: bindActionCreators(PropertyActionCreators, dispatch)
    };
};

let filter = CONST.FILTER_PROPERTY_OBJECT;

class PropertyFilters extends Component {
    constructor(props) {
        super(props);
        const { filterObject, selectedSortType } = props.pProperties;
        this.state = {
            saleComponent: false,
            sortByComponentOpen: false,
            squareMeterComponentOpen: false,
            typesComponentOpen: false,
            priceComponentOpen: false,
            bedsAndBathComponentOpen: false,
            moreComponentOpen: false,

            sortBySelectedItem: selectedSortType,
            propertySale: filterObject.isSale,
            checkedTypes: filterObject.houseType,
            checkedBeds: filterObject.bedCount,
            checkedBath: filterObject.bathCount,
            checkedAmenities: filterObject.amenities,
            checkedPets: filter.petAllows,
            squareMeter: filterObject.squareMeter,
            priceInterval: filterObject.priceInterval,
            floorInterval: filterObject.floorInterval,
            builtYear: filterObject.builtYear,
            sortByElement: null,
            squareMeterElement: null,
            typesElement: null,
            priceElement: null,
            bedsAndBathElement: null,
            moreElement: null
        };
    }

    setVisibility = display => {
        this.setState({
            priceComponentOpen: display
        });
        this.filterAndSortProperties();
    };

    sortBySelectedElement = index => {
        this.setState({ sortBySelectedItem: index });
    };

    typesSelectedElement = checked => {
        this.setState({ checkedTypes: checked });
    };

    squareMeterInterval = (begin, middle, end) => {
        this.setState({ squareMeter: [begin, middle, end] });
    };

    priceInterval = (begin, end) => {
        this.setState({ priceInterval: [begin, end] });
        filter.priceInterval = [begin, end];
    };

    bedsAndBathSelectedElement = (beds, checked) => {
        beds ? this.setState({ checkedBeds: checked }) : this.setState({ checkedBath: checked });
    };

    amenitiesSelectedElement = (amenities, checked) => {
        amenities ? this.setState({ checkedAmenities: checked }) : this.setState({ checkedPets: checked });
    };

    setIntervalElements = (floor, interval) => {
        floor ? this.setState({ floorInterval: interval }) : this.setState({ builtYear: interval });
    };

    handleClose = () => {
        this.setState({
            sortByComponentOpen: false,
            squareMeterComponentOpen: false,
            typesComponentOpen: false,
            priceComponentOpen: false,
            bedsAndBathComponentOpen: false,
            moreComponentOpen: false
        });
        this.filterAndSortProperties();
    };

    filterAndSortProperties() {
        const { propertyActions, pSelectedLocation, pSelectedCity, pHeatMapsSettings, pIndicators } = this.props;
        const coordinates = getReferenceCoordinate(
            pSelectedCity.cityCenterCoordinate,
            pSelectedLocation.locationCoordinate
        );
        propertyActions.filterProperties(
            pSelectedCity.selectedCityKey,
            coordinates,
            pHeatMapsSettings.distance,
            filter,
            this.state.sortBySelectedItem,
            pIndicators
        );
    }

    setSale = isSale => {
        this.setState({ propertySale: isSale });
        filter.isSale = isSale;
        this.handleClose();
    };

    render() {
        const { classes } = this.props;
        const {
            sortByComponentOpen,
            squareMeterComponentOpen,
            typesComponentOpen,
            priceComponentOpen,
            bedsAndBathComponentOpen,
            moreComponentOpen,
            squareMeter,
            priceInterval,
            checkedBeds,
            checkedBath,
            propertySale,
            checkedTypes,
            floorInterval,
            builtYear,
            sortBySelectedItem
        } = this.state;
        const sqm =
            squareMeter[0] == CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MIN &&
            squareMeter[1] == CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX ? (
                <Translator i18nKeys="PROPERTY.Square_Meter" />
            ) : (
                <Translator
                    i18nKeys="PROPERTY.SQM"
                    insertValue={{
                        min: squareMeter[0],
                        max: squareMeter[2]
                    }}
                />
            );

        const price =
            priceInterval[0] == CONST.PROPERTY_FILTER_TEXTS.MIN_PRICE &&
            priceInterval[1] == CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE ? (
                <Translator i18nKeys="PROPERTY.ANY_PRICE" />
            ) : (
                <Translator
                    i18nKeys="PROPERTY.CURRENCY_DOLLAR_PARAMETER"
                    insertValue={{
                        min: priceInterval[0],
                        max: priceInterval[1]
                    }}
                />
            );
        const checkedBedsFirstTrueIndex = checkedBeds
            .reduce((out, bool, index) => (bool ? out.concat(index) : out), [])
            .reverse()[0];
        const checkedBathFirstTrueIndex = checkedBath
            .reduce((out, bool, index) => (bool ? out.concat(index) : out), [])
            .reverse()[0];
        const beds = checkedBeds[0] ? (
            <Translator i18nKeys="PROPERTY.ALL_BEDS" />
        ) : (
            <Translator
                i18nKeys="PROPERTY.BEDS"
                insertValue={{
                    max: '$t( ' + CONST.PROPERTY_FILTER_TEXTS.BEDROOM_LIST_ITEMS[checkedBedsFirstTrueIndex] + ')'
                }}
            />
        );
        const bath = checkedBath[0] ? (
            <Translator i18nKeys="PROPERTY.ALL_BATH" />
        ) : (
            <Translator
                i18nKeys="PROPERTY.BATHS"
                insertValue={{ max: CONST.PROPERTY_FILTER_TEXTS.BATHROOM_LIST_ITEMS[checkedBathFirstTrueIndex] }}
            />
        );
        filter = {
            isSale: propertySale,
            priceInterval,
            squareMeter,
            bedCount: checkedBeds,
            bathCount: checkedBath,
            houseType: checkedTypes,
            floorInterval,
            builtYear,
            amenities: this.state.checkedAmenities,
            petAllows: this.state.checkedPets
        };
        return (
            <Grid container className={classes.filterPart}>
                <Grid container className={classes.filterButtons}>
                    <Grid item>
                        <GisettButton
                            index={1}
                            filterButton={true}
                            gisStyle={propertySale ? GISSET_BUTTON_STYLE_2 : GISSET_BUTTON_STYLE_1}
                            onClick={() => this.setSale(true)}
                        >
                            <Translator i18nKeys="PROPERTY.SALE" />
                        </GisettButton>
                        <GisettButton
                            index={2}
                            filterButton={true}
                            gisStyle={propertySale ? GISSET_BUTTON_STYLE_1 : GISSET_BUTTON_STYLE_2}
                            onClick={() => this.setSale(false)}
                        >
                            <Translator i18nKeys="PROPERTY.RENT" />
                        </GisettButton>
                    </Grid>

                    <Button
                        buttonRef={node => {
                            this.sortByElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                sortByComponentOpen: !this.state.sortByComponentOpen,
                                priceComponentOpen: false
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        <Typography variant="title" className={classes.buttonText}>
                            <Translator i18nKeys="PROPERTY.SORT_BY" />
                        </Typography>
                        <Translator
                            i18nKeys={'PROPERTY.' + CONST.PROPERTY_FILTER_TEXTS.SORTBY_LIST_ITEMS[sortBySelectedItem]}
                        />
                        {sortByComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Popper
                        open={sortByComponentOpen}
                        anchorEl={this.sortByElement}
                        transition
                        disablePortal
                        className={classes.popper}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                }}
                            >
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <FilterSortBy
                                        checkedElement={sortBySelectedItem}
                                        listElements={CONST.PROPERTY_FILTER_TEXTS.SORTBY_LIST_ITEMS}
                                        returnSelectedElements={this.sortBySelectedElement.bind(this)}
                                    />
                                </ClickAwayListener>
                            </Grow>
                        )}
                    </Popper>

                    <Button
                        buttonRef={node => {
                            this.bedsAndBathElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                bedsAndBathComponentOpen: !this.state.bedsAndBathComponentOpen,
                                priceComponentOpen: false
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        {beds}, {bath}
                        {bedsAndBathComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Popper
                        open={bedsAndBathComponentOpen}
                        anchorEl={this.bedsAndBathElement}
                        transition
                        disablePortal
                        className={classes.popper}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                }}
                            >
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <FilterBedsAndBath
                                        bedroomListItems={CONST.PROPERTY_FILTER_TEXTS.BEDROOM_LIST_ITEMS}
                                        bathroomListItems={CONST.PROPERTY_FILTER_TEXTS.BATHROOM_LIST_ITEMS}
                                        checkedBedroomTypes={this.state.checkedBeds}
                                        checkedBathroomTypes={this.state.checkedBath}
                                        returnSelectedElements={this.bedsAndBathSelectedElement.bind(this)}
                                    />
                                </ClickAwayListener>
                            </Grow>
                        )}
                    </Popper>
                </Grid>

                <Grid container className={classes.filterButtons}>
                    <Button
                        buttonRef={node => {
                            this.squareMeterElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                squareMeterComponentOpen: !this.state.squareMeterComponentOpen,
                                priceComponentOpen: false
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        {sqm}
                        {squareMeterComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Popper
                        open={squareMeterComponentOpen}
                        anchorEl={this.squareMeterElement}
                        transition
                        disablePortal
                        className={classes.popper}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                }}
                            >
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <FilterSquareMeter
                                        minSquare={squareMeter[0]}
                                        middle={squareMeter[1]}
                                        maxSquare={squareMeter[2]}
                                        returnSelectedElement={this.squareMeterInterval.bind(this)}
                                    />
                                </ClickAwayListener>
                            </Grow>
                        )}
                    </Popper>
                    <Button
                        buttonRef={node => {
                            this.priceElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                priceComponentOpen: !this.state.priceComponentOpen
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        {price}
                        {priceComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Button
                        buttonRef={node => {
                            this.typesElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                typesComponentOpen: !this.state.typesComponentOpen,
                                priceComponentOpen: false
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        <Translator i18nKeys="PROPERTY.TYPES" />
                        {!checkedTypes[0] ? '(' + checkedTypes.filter(Boolean).length + ')' : null}
                        {typesComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Popper
                        open={typesComponentOpen}
                        anchorEl={this.typesElement}
                        transition
                        disablePortal
                        className={classes.popper}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                }}
                            >
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <FilterTypes
                                        checkedElement={this.state.checkedTypes}
                                        listElements={CONST.PROPERTY_FILTER_TEXTS.TYPES_LIST_ITEMS}
                                        returnSelectedElements={this.typesSelectedElement.bind(this)}
                                    />
                                </ClickAwayListener>
                            </Grow>
                        )}
                    </Popper>

                    <Button
                        buttonRef={node => {
                            this.moreElement = node;
                        }}
                        onClick={() =>
                            this.setState({
                                moreComponentOpen: !this.state.moreComponentOpen,
                                priceComponentOpen: false
                            })
                        }
                        variant="outlined"
                        className={classes.button}
                    >
                        <Translator i18nKeys="PROPERTY.MORE" />
                        {moreComponentOpen ? (
                            <ExpandLess className={classes.expandIcon} />
                        ) : (
                            <ExpandMore className={classes.expandIcon} />
                        )}
                    </Button>
                    <Popper
                        open={moreComponentOpen}
                        anchorEl={this.moreElement}
                        transition
                        disablePortal
                        className={classes.popper}
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
                                }}
                            >
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <FilterMore
                                        floorInterval={floorInterval}
                                        builtYear={builtYear}
                                        checkedAmenities={this.state.checkedAmenities}
                                        checkedPets={this.state.checkedPets}
                                        amenitiesListElements={CONST.PROPERTY_FILTER_TEXTS.AMENITIES_LIST_ITEMS}
                                        petsListElements={CONST.PROPERTY_FILTER_TEXTS.PETS_LIST_ITEMS}
                                        returnSelectedElements={this.amenitiesSelectedElement.bind(this)}
                                        returnIntervals={this.setIntervalElements.bind(this)}
                                    />
                                </ClickAwayListener>
                            </Grow>
                        )}
                    </Popper>
                    <Grid container>
                        <FilterPrice
                            visible={priceComponentOpen}
                            minPrice={priceInterval[0]}
                            maxPrice={priceInterval[1]}
                            returnSelectedElement={this.priceInterval.bind(this)}
                            visibilitySetter={this.setVisibility.bind(this)}
                        />
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    filterPart: {
        background: theme.palette.background.main,
        flex: 2,
        boxShadow: '0px 10px 10px -15px #111',
        width: '100%',
        paddingBottom: 16
    },
    filterButtons: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    button: {
        height: 32,
        padding: '6px 16px',
        textTransform: 'capitalize',
        fontSize: 14,
        fontFamily: 'Nunito Sans Regular'
    },
    buttonText: {
        textTransform: 'none',
        fontWeight: 'bold',
        color: theme.palette.secondary.main
    },
    expandIcon: {
        color: '#CDD1D4',
        marginLeft: 10
    },
    popper: {
        zIndex: 1
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(PropertyFilters));
