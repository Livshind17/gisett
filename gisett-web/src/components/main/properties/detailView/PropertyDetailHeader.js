import * as PropertyActionCreators from '../../../../actions/PropertyActionCreators';
import GisettButton, { GISSET_BUTTON_STYLE_1, GISSET_BUTTON_STYLE_4 } from '../../../commons/GisettButton';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const mapDispatchToProps = dispatch => {
    return {
        propertyActions: bindActionCreators(PropertyActionCreators, dispatch)
    };
};

class PropertyDetailHeader extends Component {
    render() {
        const { classes, propertyActions } = this.props;
        return (
            <Grid container direction={'row'} justify={'flex-end'} alignItems={'center'} className={classes.headerGrid}>
                <GisettButton classes={classes.button} gisStyle={GISSET_BUTTON_STYLE_4}>
                    <Translator i18nKeys="PROPERTY.DETAIL_VIEW.HEADER.OWNER" />
                </GisettButton>
                <GisettButton
                    classes={classes.button}
                    gisStyle={GISSET_BUTTON_STYLE_1}
                    onClick={() => propertyActions.hidePropertyDetails(false)}
                >
                    <Translator i18nKeys="PROPERTY.DETAIL_VIEW.HEADER.BACK" />
                </GisettButton>
            </Grid>
        );
    }
}

const styles = theme => ({
    headerGrid: {
        background: theme.palette.background.main,
        flex: '1 0 auto',
        boxShadow: '0px 10px 10px -15px #111',
        width: '100%',
        height: '60px'
    },
    buttonsCard: {
        backgroundColor: theme.palette.background.main
    },
    button: {
        height: 32,
        width: 56.44,
        textTransform: 'capitalize',
        fontSize: 12,
        fontFamily: 'Nunito Sans Regular',
        margin: '0px 16px 0px 0px'
    }
});

export default connect(
    null,
    mapDispatchToProps
)(withStyles(styles)(PropertyDetailHeader));
