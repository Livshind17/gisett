import { Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import Carousel from 'nuka-carousel';
import { connect } from 'react-redux';
import GisettIcons from '../../../../resources/icons/icons';
import { Image } from 'cloudinary-react';
import imgButtonArrow from '../../../../resources/images/pic_arrow_big@2x.png';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pCurrentProperty: state.properties.currentProperty,
        pSelectedCityKey: state.cities.selectedCityKey
    };
};

class PropertyDetailImageArea extends Component {
    render() {
        const { classes, pCurrentProperty } = this.props;
        var actSlide;
        return (
            <Carousel
                className={classes.carouselClass}
                renderCenterLeftControls={({ previousSlide }) => (
                    <button onClick={previousSlide} className={classes.sliderButton}>
                        <img src={imgButtonArrow} className={classes.arrowLeft} alt="" />
                    </button>
                )}
                renderCenterRightControls={({ nextSlide }) => (
                    <button onClick={nextSlide} className={classes.sliderButton}>
                        <img src={imgButtonArrow} className={classes.arrowRight} alt="" />
                    </button>
                )}
                renderBottomCenterControls={({ currentSlide }) => (
                    <div className={classes.invisibleDiv}>{(actSlide = currentSlide + 1)}</div>
                )}
                renderTopLeftControls={({ previousSlide }) => (
                    <button onClick={previousSlide} className={classes.slideNrButton}>
                        <Grid container direction={'row'} justify={'space-evenly'} alignItems={'center'}>
                            <img src={GisettIcons.cameraIcon} alt="" className={classes.cameraIconClass} />
                            <Typography className={classes.currentSlideFont}>
                                {actSlide} /{pCurrentProperty.photos.length}
                            </Typography>
                        </Grid>
                    </button>
                )}
            >
                {pCurrentProperty.photos.map(imageName => (
                    <Image
                        className={classes.media}
                        cloudName="gisett"
                        publicId={'gisett/' + this.props.pSelectedCityKey + '/' + imageName + '.jpg'}
                    />
                ))}
            </Carousel>
        );
    }
}

const styles = () => ({
    arrowLeft: {
        width: '20px',
        height: '40px'
    },
    arrowRight: {
        width: '20px',
        height: '40px',
        transform: 'rotate(180deg)'
    },
    media: {
        height: 332.5,
        width: 500,
        cursor: 'default'
    },
    carouselClass: {
        width: '500px',
        cellAlign: 'center',
        slideIndex: 0
    },
    sliderButton: {
        background: 'none',
        border: 'none',
        cursor: 'pointer'
    },
    slideNrButton: {
        color: 'white',
        opacity: 0.75,
        width: '74px',
        height: '24px'
    },
    cameraIconClass: {
        width: '19px',
        height: '12px'
    },
    invisibleDiv: {
        visibility: 'hidden'
    },
    currentSlideFont: {
        fontSize: '12px'
    }
});

export default connect(mapStateToProps)(withStyles(styles)(PropertyDetailImageArea));
