import { Card, CardContent } from '@material-ui/core';
import React, { Component } from 'react';
import PropertyDetailContent from './PropertyDetailContent';
import PropertyDetailHeader from './PropertyDetailHeader';
import { withStyles } from '@material-ui/core/styles';

class PropertyDetailView extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Card className={classes.propertyTab}>
                <CardContent className={classes.cardContentClass}>
                    <PropertyDetailHeader />
                    <PropertyDetailContent />
                </CardContent>
            </Card>
        );
    }
}

const styles = theme => ({
    propertyTab: {
        backgroundColor: theme.palette.background.main,
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '700px',
        height: '100%',
        overflowY: 'scroll'
    },
    cardContentClass: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        paddingTop: '0px'
    }
});

export default withStyles(styles)(PropertyDetailView);
