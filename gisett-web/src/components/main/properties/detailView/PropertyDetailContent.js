import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import PropertyDetailAmenities from './PropertyDetailAmenities';
import PropertyDetailImageArea from './PropertyDetailImageArea';
import PropertyDetailLocationInfo from './PropertyDetailLocationInfo';
import { withStyles } from '@material-ui/core/styles';

class PropertyDetailContent extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Grid
                container
                direction={'column'}
                justify={'space-evenly'}
                alignItems={'center'}
                className={classes.headerGrid}
            >
                <PropertyDetailImageArea />
                <PropertyDetailLocationInfo />
                <PropertyDetailAmenities />
            </Grid>
        );
    }
}

const styles = () => ({
    headerGrid: {
        flex: '1 0 auto',
        margin: '14px 2px 0px 2px',
        width: '700px'
    }
});

export default withStyles(styles)(PropertyDetailContent);
