import GisettButton, { GISSET_BUTTON_STYLE_5, GISSET_BUTTON_STYLE_6 } from '../../../commons/GisettButton';
import { Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import CurrencyFormat from 'react-currency-format';
import GisettIcons from '../../../../resources/icons/icons';
import PropertyDetailCarousel from './PropertyDetailCarousel';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pCurrentProperty: state.properties.currentProperty
    };
};

class PropertyDetailImageArea extends Component {
    render() {
        const { classes, pCurrentProperty } = this.props;
        return (
            <Grid
                container
                direction={'column'}
                justify={'space-evenly'}
                alignItems={'center'}
                className={classes.headerGrid}
            >
                <PropertyDetailCarousel />
                {pCurrentProperty.propertyAddress.addressLine1 &&
                pCurrentProperty.propertyAddress.city &&
                pCurrentProperty.propertyAddress.state ? (
                    <Grid container direction={'row'} justify={'flex-start'} alignItems={'baseline'}>
                        <Typography variant="body2" gutterBottom className={classes.condoClass}>
                            <Translator i18nKeys={'PROPERTY.DETAIL_VIEW.HOUSE_TYPES.' + pCurrentProperty.houseType} />
                        </Typography>
                        <Typography variant="body1" gutterBottom className={classes.condoAddress}>
                            <Translator
                                i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.ADDRESS"
                                insertValue={{
                                    address: pCurrentProperty.propertyAddress.addressLine1,
                                    city: pCurrentProperty.propertyAddress.city,
                                    state: pCurrentProperty.propertyAddress.state
                                }}
                            />
                        </Typography>
                    </Grid>
                ) : null}
                <Grid container direction={'row'} justify={'flex-start'} alignItems={'flex-end'}>
                    {pCurrentProperty.bedrooms ? (
                        <Typography variant="caption" gutterBottom className={classes.condoDetails}>
                            <img src={GisettIcons.bedIcon} alt="" className={classes.iconClass} />
                            <Translator
                                i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.BEDS"
                                insertValue={{
                                    bedNr: pCurrentProperty.bedrooms
                                }}
                            />
                        </Typography>
                    ) : null}
                    {pCurrentProperty.bathrooms ? (
                        <Typography variant="caption" gutterBottom className={classes.condoDetails}>
                            <img src={GisettIcons.bathIcon} alt="" className={classes.iconClass} />
                            <Translator
                                i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.BATH"
                                insertValue={{
                                    bathNr: pCurrentProperty.bathrooms
                                }}
                            />
                        </Typography>
                    ) : null}
                    {pCurrentProperty.builtSurface ? (
                        <Typography variant="caption" gutterBottom className={classes.condoDetails}>
                            <img src={GisettIcons.sqmIcon} alt="" className={classes.iconClass} />
                            <Translator
                                i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.SQM"
                                insertValue={{
                                    sqmNr: pCurrentProperty.builtSurface
                                }}
                            />
                        </Typography>
                    ) : null}
                </Grid>
                <Grid
                    container
                    direction={'row'}
                    justify={'space-between'}
                    alignItems={'center'}
                    className={classes.condoRow2}
                >
                    <GisettButton className={classes.availableButton} gisStyle={GISSET_BUTTON_STYLE_6}>
                        <Typography variant="subtitle2" className={classes.availableText}>
                            <Translator i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.AVAILABILITY" />
                        </Typography>
                    </GisettButton>
                    <GisettButton gisStyle={GISSET_BUTTON_STYLE_5}>
                        <Typography variant="body2" className={classes.priceButton}>
                            <CurrencyFormat
                                value={pCurrentProperty.price}
                                displayType={'text'}
                                thousandSeparator={true}
                                prefix={pCurrentProperty.currency}
                            />
                        </Typography>
                        <Typography variant="button2" className={classes.saleRent}>
                            {pCurrentProperty.category === 'SELL' ? (
                                <Translator i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.SALE" />
                            ) : (
                                <Translator i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.RENT" />
                            )}
                        </Typography>
                    </GisettButton>
                </Grid>
                {pCurrentProperty.description ? (
                    <Grid
                        container
                        direction={'column'}
                        justify={'space-evenly'}
                        alignItems={'flex-start'}
                        className={classes.descriptionGrid}
                    >
                        <Typography variant="body2" gutterBottom className={classes.descTitle}>
                            <Translator i18nKeys="PROPERTY.DETAIL_VIEW.CONDO.DESC_TITLE" />
                        </Typography>
                        <Typography variant="subtitle1" className={classes.description}>
                            {pCurrentProperty.description}
                        </Typography>
                    </Grid>
                ) : null}
            </Grid>
        );
    }
}

const styles = theme => ({
    headerGrid: {
        flex: '1 0 auto',
        margin: '14px 2px 0px 2px',
        width: '100%',
        paddingLeft: '100px',
        paddingRight: '100px'
    },
    condoClass: {
        paddingRight: '9px',
        fontSize: '18px',
        color: theme.palette.primary.main,
        paddingTop: '24px'
    },
    condoAddress: {
        fontSize: '16px',
        color: theme.palette.primary.main,
        paddingTop: '24px'
    },
    condoRow2: {
        boxShadow: '0px 10px 10px -15px #111',
        paddingTop: '20px',
        paddingBottom: '30px'
    },
    condoDetails: {
        fontSize: '16px',
        paddingRight: '20px',
        color: theme.palette.secondary.main,
        paddingTop: '6px'
    },
    priceButton: {
        color: theme.palette.price.main,
        fontSize: '18px',
        paddingRight: '7px'
    },
    saleRent: {
        fontFamily: 'Nunito Sans Regular',
        color: theme.palette.tertiary.main
    },
    availableButton: {
        width: '137px',
        height: '19px',
        minHeight: '19px'
    },
    availableText: {
        fontSize: '14px',
        color: theme.palette.common.white,
        textTransform: 'none',
        fontFamily: 'Nunito Sans Regular'
    },
    descTitle: {
        fontSize: '16px'
    },
    description: {
        fontFamily: 'Nunito Sans Regular',
        fontSize: '16px'
    },
    iconClass: {
        paddingRight: '5px',
        verticalAlign: 'sub'
    },
    descriptionGrid: {
        margin: '15px 0px 0px',
        paddingBottom: '40px'
    }
});

export default connect(mapStateToProps)(withStyles(styles)(PropertyDetailImageArea));
