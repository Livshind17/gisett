import GeneralIndicatorRating, { INDICATOR_RATING_TYPE_DISPLAY } from '../../../commons/GeneralIndicatorRating';
import { Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { summerizedAndIndicatorRating } from '../../../../utils/indicators/indicatorHelper';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pIndicators: state.indicators,
        pNearestIndicatorPoint: state.properties.currentProperty.nearestIndicatorPoint
    };
};

class PropertyDetailLocationInfo extends Component {
    render() {
        const { classes, pIndicators, pNearestIndicatorPoint } = this.props;

        let allRatings,
            generalRating,
            locationGrades = {};
        [allRatings, generalRating] = summerizedAndIndicatorRating(pNearestIndicatorPoint, pIndicators);
        locationGrades.value = generalRating != null && generalRating.length > 0 ? generalRating[0].value : 0;
        locationGrades.allRatings = allRatings;

        return (
            <Grid container className={classes.mainGrid}>
                <Grid
                    container
                    direction={'column'}
                    justify={'space-evenly'}
                    alignItems={'flex-start'}
                    className={classes.contentGrid}
                >
                    <Typography variant="caption" className={classes.title}>
                        <Translator i18nKeys="PROPERTY.DETAIL_VIEW.LOCATION_GRADES.TITLE" />
                    </Typography>
                    <Typography variant="title" className={classes.subtitle}>
                        <Translator i18nKeys="PROPERTY.DETAIL_VIEW.LOCATION_GRADES.SUBTITLE" />
                    </Typography>

                    {locationGrades != null ? (
                        <GeneralIndicatorRating
                            popup={locationGrades}
                            classes={classes}
                            variant={INDICATOR_RATING_TYPE_DISPLAY}
                        />
                    ) : null}
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    mainGrid: {
        flex: '1 0 auto',
        paddingBottom: '22px',
        paddingTop: '29px',
        backgroundColor: theme.palette.secondary.main
    },
    contentGrid: {
        paddingLeft: '100px',
        paddingRight: '100px',
        width: '600px'
    },
    title: {
        fontSize: '18px',
        color: '#FFFFFF',
        lineHeight: '2'
    },
    subtitle: {
        fontSize: '14px',
        color: '#FFFFFF',
        paddingBottom: '13px'
    }
});

export default connect(mapStateToProps)(withStyles(styles)(PropertyDetailLocationInfo));
