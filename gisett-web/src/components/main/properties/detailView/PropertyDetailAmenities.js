import { Checkbox, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pCurrentProperty: state.properties.currentProperty
    };
};

const CheckboxRow = param => {
    if (Array.isArray(param.checkboxItems)) {
        const checkBoxes = param.checkboxItems.map(checkbox => (
            <FormControlLabel
                control={<Checkbox labelPosition={'right'} checked={param.amenities[checkbox]} />}
                label={
                    <Typography variant="subtitle2" className={param.classes.checkboxClass}>
                        <Translator i18nKeys={'PROPERTY.DETAIL_VIEW.AMENITIES.' + checkbox.toUpperCase()} />
                    </Typography>
                }
            />
        ));
        return (
            <FormControl>
                <FormGroup>{checkBoxes}</FormGroup>
            </FormControl>
        );
    } else {
        return null;
    }
};

class PropertyDetailAmenities extends Component {
    putAmenity = (amenities, amenityKey, amenityValue) => {
        if (amenityValue !== null && amenityValue !== undefined) {
            amenities[amenityKey] = amenityValue;
        }
    };

    render() {
        const { classes, pCurrentProperty } = this.props;
        const attr = pCurrentProperty.amenities;
        const amenities = {};
        this.putAmenity(amenities, 'air_condition', attr.AIR_CONDITION);
        this.putAmenity(amenities, 'balcony', attr.BALCONY);
        this.putAmenity(amenities, 'basement', attr.BASEMENT);
        this.putAmenity(amenities, 'bbq_area', attr.BBQ_AREA);
        this.putAmenity(amenities, 'dishwasher', attr.DISHWASHER);
        this.putAmenity(amenities, 'doorman', attr.DOORMAN);
        this.putAmenity(amenities, 'dryer', attr.DRYER);
        this.putAmenity(amenities, 'elevator', attr.ELEVATOR);
        this.putAmenity(amenities, 'fireplace', attr.FIREPLACE);
        this.putAmenity(amenities, 'furnished', attr.FURNISHED);
        this.putAmenity(amenities, 'garden', attr.GARDEN);
        this.putAmenity(amenities, 'gym', attr.GYM);
        this.putAmenity(amenities, 'heating', attr.HEATING);
        this.putAmenity(amenities, 'laundry', attr.LAUNDRY);
        this.putAmenity(amenities, 'parking', attr.PARKING);
        this.putAmenity(amenities, 'pet_allow_cat', attr.PET_ALLOW_CAT);
        this.putAmenity(amenities, 'pet_allow_dog', attr.PET_ALLOW_DOG);
        this.putAmenity(amenities, 'swimming_pool', attr.SWIMMING_POOL);
        this.putAmenity(amenities, 'washer', attr.WASHER);
        this.putAmenity(amenities, 'wheelchair_access', attr.WHEELCHAIR_ACCESS);

        const checkboxPerRow = 3;
        const rowCount = amenities ? Math.ceil(Object.keys(amenities).length / checkboxPerRow) : 0;
        const checkboxes = Array.apply(null, Array(checkboxPerRow)).map((item, i) =>
            Object.keys(amenities).slice(i * rowCount, i * rowCount + rowCount)
        );
        return (
            <Grid
                container
                direction={'column'}
                justify={'space-evenly'}
                alignItems={'flex-start'}
                className={classes.mainGrid}
            >
                {rowCount !== 0 ? (
                    <Grid item className={classes.gridShadow}>
                        <Typography variant="body2" className={classes.title}>
                            <Translator i18nKeys="PROPERTY.DETAIL_VIEW.AMENITIES.TITLE" />
                        </Typography>
                        <Grid container direction="row" justify="space-between" align-items="center">
                            {checkboxes.map((checkboxItem, index) => (
                                <CheckboxRow checkboxItems={checkboxItem} amenities={amenities} classes={classes} />
                            ))}
                        </Grid>
                    </Grid>
                ) : null}
                <Grid
                    container
                    direction={'column'}
                    alignItems={'center'}
                    justify={'space-evenly'}
                    className={classes.mainInfoGrid}
                >
                    {pCurrentProperty.amenities.PET_ALLOW_DOG && pCurrentProperty.amenities.PET_ALLOW_CAT ? (
                        <Grid item className={classes.gridClass}>
                            <Typography variant="body2" className={classes.title}>
                                {pCurrentProperty.amenities.PET_ALLOW_DOG &&
                                pCurrentProperty.amenities.PET_ALLOW_CAT ? (
                                    <Translator i18nKeys="PROPERTY.DETAIL_VIEW.PET_ALLOWED" />
                                ) : (
                                    <Translator i18nKeys="PROPERTY.DETAIL_VIEW.NO_PET_ALLOWED" />
                                )}
                            </Typography>
                        </Grid>
                    ) : null}
                    <Grid item className={classes.gridClass}>
                        <Typography variant="body2" className={classes.title}>
                            <Translator i18nKeys="PROPERTY.DETAIL_VIEW.YEAR_BUILT" />
                            <span className={classes.content}>
                                {pCurrentProperty.yearBuilt ? (
                                    pCurrentProperty.yearBuilt
                                ) : (
                                    <Translator i18nKeys="PROPERTY.DETAIL_VIEW.UNKNOWN_YEAR" />
                                )}
                            </span>
                        </Typography>
                    </Grid>
                    {pCurrentProperty.floor ? (
                        <Grid item className={classes.gridClass}>
                            <Typography variant="body2" className={classes.title}>
                                <Translator i18nKeys="PROPERTY.DETAIL_VIEW.FLOOR" />
                                <span className={classes.content}>{pCurrentProperty.floor}</span>
                            </Typography>
                        </Grid>
                    ) : null}
                </Grid>
            </Grid>
        );
    }
}

const styles = () => ({
    title: {
        fontSize: '16px'
    },
    mainGrid: {
        flex: '1 0 auto',
        paddingTop: '30px',
        paddingBottom: '36px',
        paddingLeft: '100px',
        paddingRight: '100px'
    },
    mainInfoGrid: {
        flex: '1 0 auto',
        paddingBottom: '36px'
    },
    gridShadow: {
        width: '100%',
        boxShadow: '0px 10px 10px -15px #111',
        paddingBottom: '40px'
    },
    gridClass: {
        boxShadow: '0px 10px 10px -15px #111',
        width: '100%',
        height: '50px',
        verticalAlign: 'middle',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        fontSize: '16px',
        fontFamily: 'Nunito Sans Regular'
    },
    checkboxClass: {
        fontSize: '14px'
    }
});

export default connect(mapStateToProps)(withStyles(styles)(PropertyDetailAmenities));
