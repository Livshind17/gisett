import { Card, CardContent, CardMedia, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import starYellowIcon from '../../../../resources/images/star_yellow.svg';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

class PropertyListHeader extends Component {
    render() {
        const { classes, numberOfElements } = this.props;
        return (
            <Grid
                container
                direction={'row'}
                spacing={16}
                justify={'space-between'}
                alignItems={'flex-end'}
                className={classes.gridContent}
            >
                <Card className={classes.headerCards}>
                    <CardContent className={classes.headerContent}>
                        <CardMedia className={classes.iconMedia} image={starYellowIcon} title="Yellow Star" />
                        <Typography variant="button" className={classes.locationMatch}>
                            <Translator i18nKeys="PROPERTY.LOCATION_MATCH" />
                        </Typography>
                    </CardContent>
                </Card>

                <Card className={classes.headerCards}>
                    <CardContent>
                        <Typography variant="body1" className={classes.propFound}>
                            <Translator
                                i18nKeys="PROPERTY.FOUND"
                                insertValue={{
                                    nrProperty: numberOfElements
                                }}
                            />
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        );
    }
}

const styles = theme => ({
    locationMatch: {
        color: '#B6174B'
    },
    iconMedia: {
        height: '16px',
        width: '16.17px',
        marginRight: '8px',
        marginLeft: '-18px'
    },
    propFound: {
        color: theme.palette.background.propertyText,
        opacity: '0.5',
        backgroundColor: theme.palette.background.main,
        marginRight: '10px'
    },
    headerCards: {
        backgroundColor: theme.palette.background.main,
        boxShadow: 'none'
    },

    gridContent: {
        margin: '14px 2px 0px 2px'
    },
    headerContent: {
        display: 'flex',
        alignItems: 'flex-start'
    }
});

export default withStyles(styles)(PropertyListHeader);
