import * as PropertyActionCreators from '../../../../actions/PropertyActionCreators';
import { Card, CardActionArea, CardContent, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CurrencyFormat from 'react-currency-format';
import InfiniteScroll from 'react-infinite-scroller';
import StarRating from '../../../commons/StarRating';
import Translator from '../../../translate/Translator';
import { withStyles } from '@material-ui/core/styles';

const NUMBER_OF_DISPLAYED_PROPERTY = 60;
const NUMBER_OF_PLUS_PROPERTY = 24;

const mapStateToProps = state => {
    return {
        pProperties: state.properties.propertyData,
        pSelectedCityKey: state.cities.selectedCityKey
    };
};

const mapDispatchToProps = dispatch => {
    return {
        propertyActions: bindActionCreators(PropertyActionCreators, dispatch)
    };
};

class PropertyListMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            propertiesNumber: props.pProperties.numberOfElements,
            hasMore: true,
            numberOfDisplay: NUMBER_OF_DISPLAYED_PROPERTY
        };
    }

    fetchMoreData = page => {
        this.setState({
            numberOfDisplay: this.state.numberOfDisplay + NUMBER_OF_PLUS_PROPERTY
        });
        if (this.state.numberOfDisplay >= this.state.propertiesNumber) {
            this.setState({ hasMore: false });
        }
    };

    render() {
        const { classes, pProperties, propertyActions, pSelectedCityKey } = this.props;
        const loader = <div key={1}>Loading ...</div>;
        var items = [];
        pProperties.content.slice(0, this.state.numberOfDisplay).map((currentProperty, index) =>
            items.push(
                <Grid item key={index} xs={4}>
                    <Card className={classes.cardComponent}>
                        <CardActionArea
                            onClick={() => {
                                propertyActions.showPropertyDetails(currentProperty);
                            }}
                        >
                            <Grid container direction="row" justify="space-between">
                                <Grid>
                                    <Typography variant="body2" gutterBottom className={classes.gridContentHeader}>
                                        <CurrencyFormat
                                            value={currentProperty.price}
                                            displayType={'text'}
                                            thousandSeparator={true}
                                            prefix={currentProperty.currency}
                                        />
                                    </Typography>
                                </Grid>
                                <StarRating
                                    numberOfGoodStars={
                                        currentProperty.weightedValue != null
                                            ? Math.ceil(currentProperty.weightedValue / 20)
                                            : 0
                                    }
                                />
                            </Grid>
                            <img
                                className={classes.media}
                                src={
                                    process.env.REACT_APP_IMAGE_STORE_URL +
                                    '/q_auto/gisett/' +
                                    pSelectedCityKey +
                                    '/' +
                                    currentProperty.photos[0]
                                }
                            />
                            <CardContent style={{ padding: '12px' }}>
                                <Typography variant="caption" gutterBottom className={classes.gridTextContent}>
                                    <Translator
                                        i18nKeys="PROPERTY.DETAILS"
                                        insertValue={{
                                            bedNr: currentProperty.bedrooms,
                                            bathNr: currentProperty.bathrooms,
                                            sqmNr: currentProperty.builtSurface
                                        }}
                                    />
                                </Typography>
                                <Typography variant="caption" gutterBottom className={classes.gridTextContentOpacity}>
                                    <Translator
                                        i18nKeys="PROPERTY.ADDRESSLN1"
                                        insertValue={{
                                            addressline1: currentProperty.propertyAddress.addressLine1
                                        }}
                                    />
                                </Typography>
                                <Typography variant="caption" gutterBottom className={classes.gridTextContentOpacity}>
                                    <Translator
                                        i18nKeys="PROPERTY.LOCATIONLN"
                                        insertValue={{
                                            city: currentProperty.propertyAddress.city,
                                            state: currentProperty.propertyAddress.state
                                        }}
                                    />
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            )
        );

        return (
            <Grid
                container
                direction="row"
                spacing={16}
                justify="space-between"
                alignItems="flex-start"
                className={classes.gridContent}
            >
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.fetchMoreData}
                    hasMore={this.state.hasMore}
                    loader={loader}
                    useWindow={false}
                    initialLoad={true}
                >
                    <div
                        style={
                            pProperties.numberOfElements == 1
                                ? { width: '370%' }
                                : pProperties.numberOfElements == 2
                                    ? { width: '180%' }
                                    : pProperties.numberOfElements == 3
                                        ? { width: '115%' }
                                        : {}
                        }
                        className={classes.propertyElements}
                    >
                        {items}
                    </div>
                </InfiniteScroll>
            </Grid>
        );
    }
}

const styles = theme => ({
    cardComponent: {
        maxWidth: 200,
        widht: 200,
        height: 228,
        maxHeight: 228,
        marginBottom: 30
    },
    media: {
        height: 133,
        width: 200
    },
    gridTextContent: {
        color: theme.palette.propertyText.main
    },
    gridTextContentOpacity: {
        color: theme.palette.propertyText.main,
        opacity: '0.5'
    },
    gridContentHeader: {
        paddingLeft: '8px',
        paddingTop: '4px'
    },
    gridContent: {
        flex: 9,
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    style: {
        height: 30,
        border: '1px solid green',
        margin: 6,
        padding: 8
    },
    propertyElements: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignitems: 'flex-start'
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(PropertyListMain));
