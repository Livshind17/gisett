import GisettButton, { GISSET_BUTTON_STYLE_1 } from '../commons/GisettButton';
import GisettDraggableContainer, {
    CONTAINER_TYPE_BASE,
    CONTAINER_TYPE_SIMPLE,
    draggableElement
} from '../commons/GisettDraggableContainer';
import { Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';

import DraggableIndicatorButton from '../commons/DraggableIndicatorButton';
import { stageEnum } from './AreaTab';
import Translator from '../translate/Translator.js';

export class DragAndDropZone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            elementPosition: draggableElement.RELAXED,
            leavedPanel: '',
            enteredPanel: ''
        };
        this.onDragEnter = this.onDragEnter.bind(this);
        this.onDragLeave = this.onDragLeave.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    render() {
        const { columnInformation, base, draggableElements, classes, onCardDrop, cardPayload } = this.props;

        return (
            <Grid container direction="column" className={classes.dndZoneContainer}>
                <Grid container>
                    {columnInformation.map((column, index) => {
                        return (
                            <Grid item container alignContent="center" justify="space-around" key={index} xs={3}>
                                <Typography variant="caption" color="primary" className={classes.fontSize}>
                                    <Translator i18nKeys={'MAIN.' + column.name} />
                                </Typography>
                            </Grid>
                        );
                    })}
                </Grid>
                <Grid container className={classes.flexGrowForPanels}>
                    {columnInformation.map((column, index) => {
                        return (
                            <GisettDraggableContainer
                                id={column.panelKey}
                                key={index}
                                onCardDrop={onCardDrop}
                                onDragEnd={this.onDragEnd}
                                getCardPayload={cardPayload}
                                onDragEnter={this.onDragEnter}
                                onDragLeave={this.onDragLeave}
                                variant={CONTAINER_TYPE_SIMPLE}
                                behaviour="drag-zone"
                                classes={classes}
                            >
                                {draggableElements[column.panelKey].map(element => {
                                    return (
                                        <DraggableIndicatorButton
                                            id={element.key}
                                            key={element.key}
                                            type={element.type}
                                            ignored={element.ignored}
                                            value={element.value}
                                            disabled={element.stage === stageEnum.DISABLED}
                                            classes={classes}
                                        />
                                    );
                                })}
                            </GisettDraggableContainer>
                        );
                    })}
                </Grid>
                <Grid item className={classes.showMapButton}>
                    <GisettButton
                        size="large"
                        gisStyle={GISSET_BUTTON_STYLE_1}
                        onClick={this.props.onUpdateButtonPressed}
                        fixedWidth="true"
                    >
                        <Translator i18nKeys={'BUTTON.SHOW_ON_MAP'} />
                    </GisettButton>
                </Grid>
                <Grid
                    direction="column"
                    container
                    alignItems="center"
                    justify="center"
                    className={classes.flexGrowForBase}
                >
                    <GisettDraggableContainer
                        id={base.panelKey}
                        onCardDrop={onCardDrop}
                        onDragEnd={this.onDragEnd}
                        getCardPayload={cardPayload}
                        onDragEnter={this.onDragEnter}
                        onDragLeave={this.onDragLeave}
                        variant={CONTAINER_TYPE_BASE}
                        behaviour="copy"
                        classes={classes}
                    >
                        {draggableElements[base.panelKey].map(element => {
                            return (
                                <DraggableIndicatorButton
                                    id={element.key}
                                    key={element.key}
                                    type={element.type}
                                    ignored={element.ignored}
                                    value={element.value}
                                    disabled={element.stage === stageEnum.DISABLED}
                                    classes={classes}
                                />
                            );
                        })}
                    </GisettDraggableContainer>
                </Grid>
            </Grid>
        );
    }

    // Is called by the lastly leaved panel, modifies state:
    // elementPosition: the state of the element
    // leavedPanel: the leaved panel's id
    onDragLeave(id) {
        this.setState({
            elementPosition: draggableElement.LEAVED,
            leavedPanel: id
        });
    }

    // Is called by the panel in which te element moved, modifies state:
    // elementPosition: the state of the element
    // enteredPanel: the entered panel's id
    onDragEnter(id) {
        this.setState({
            elementPosition: draggableElement.ENTERED,
            enteredPanel: id
        });
    }

    //It's called when the drag event ends, before the onDrop event and by every
    // panel.
    // @Param (id) the id of the current panel
    // @Param (event) contains informaton about the drag event:
    //      - isSource: [boolean] the current panel (with id) is the source, where
    // the drag started
    //      - payload: the moved object
    //      - willAcceptDrag: [boolean] defines if the current panel has a "copy"
    // behavior or a "move"/"drag-zone".
    // If the panel is the source and the lastly called event was the onDragLeave
    // it means that the moved element was dropped in a "treshold zone" therefore
    // it needs to be removed from the source.
    onDragEnd(id, event) {
        const { elementPosition, enteredPanel, leavedPanel } = this.state;
        if (event.isSource && elementPosition === draggableElement.LEAVED && enteredPanel === leavedPanel) {
            const result = {
                removedIndex: null,
                addedIndex: null,
                payload: event.payload,
                returnsToBase: true
            };

            this.props.onCardDrop(id, result);

            this.setState({
                elementPosition: draggableElement.RELAXED,
                enteredPanel: '',
                leavedPanel: ''
            });
        }
    }
}

export default DragAndDropZone;
