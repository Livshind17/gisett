import * as PropertyActionCreators from '../../actions/PropertyActionCreators';
import { Card, CardContent, CardHeader, CardMedia, Grid, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropertyDetailView from '../main/properties/detailView/PropertyDetailView';
import PropertyFilters from './properties/filter/PropertyFilters';
import PropertyListHeader from './properties/listView/PropertyListHeader';
import PropertyListMain from './properties/listView/PropertyListMain';
import Translator from '../translate/Translator';
import { withStyles } from '@material-ui/core/styles';
import noResultImg from '../../resources/images/no_results.svg';
import imgRedHouse from '../../resources/images/red_house.png';

const mapStateToProps = state => {
    return {
        pProperties: state.properties,
        pIndicatorSettings: state.indicators.indicatorSettings,
        pShowPropertyDetails: state.properties.showPropertyDetails
    };
};

const mapDispatchToProps = dispatch => {
    return {
        propertyActions: bindActionCreators(PropertyActionCreators, dispatch)
    };
};

export class PropertyTab extends Component {
    render() {
        const { classes } = this.props;
        const { propertyData } = this.props.pProperties;

        return !this.props.pShowPropertyDetails ? (
            propertyData && propertyData.numberOfElements > 0 ? (
                <Card className={classes.propertyTab}>
                    <CardContent className={classes.propertyTab}>
                        <PropertyFilters />
                        <PropertyListHeader numberOfElements={propertyData.numberOfElements} />
                        <PropertyListMain />
                    </CardContent>
                </Card>
            ) : (
                <Card className={classes.propertyTab}>
                    <CardContent className={classes.propertyTab}>
                        <PropertyFilters />
                        <Grid container className={classes.gridContent} justify={'center'} alignItems={'flex-end'}>
                            <Typography variant="body2" style={{ fontSize: 20 }}>
                                <Translator i18nKeys="PROPERTY.NOT_FOUND" />
                            </Typography>
                            <img className={classes.noResult} src={noResultImg} alt="" title="Property not found" />
                        </Grid>
                    </CardContent>
                </Card>
            )
        ) : (
            <PropertyDetailView />
        );
    }
}

const styles = theme => ({
    propertyTab: {
        backgroundColor: theme.palette.background.main,
        flex: 10,
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    headerCards: {
        backgroundColor: theme.palette.background.main,
        boxShadow: 'none'
    },
    cardContent: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flex: '10 1 30%'
    },
    noResult: {
        width: '100%'
    },
    gridContent: {
        flex: 10
    }
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(PropertyTab));
