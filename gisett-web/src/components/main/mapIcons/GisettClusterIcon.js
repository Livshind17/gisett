import { Grid, withStyles } from '@material-ui/core';
import React, { PureComponent } from 'react';

import classNames from 'classnames';

class GisettClusterIcon extends PureComponent {
    render() {
        const { classes, count } = this.props;
        return (
            <Grid container className={classNames(classes.gisettMarkerCluser, this.clusterColor())}>
                <Grid item>{count}</Grid>
            </Grid>
        );
    }
    clusterColor() {
        const { classes, type, variant } = this.props;
        if (type) {
            return classes['gisettMarkerCluser' + type + variant];
        }
    }
}

const styles = () => ({
    gisettMarkerCluser: {
        overflow: 'visible',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '50%',
        height: 20,
        width: 20
    },
    gisettMarkerCluserSmall: {
        background: 'rgba(255,123,172,0.9)',
        boxShadow: boxShadow('rgba(255,123,172,0.7)')
    },
    gisettMarkerCluserMedium: {
        background: 'rgba(138,132,226,0.9)',
        boxShadow: boxShadow('rgba(138,132,226,0.7)')
    },
    gisettMarkerCluserLarge: {
        background: 'rgba(197,66,101,0.9)',
        boxShadow: boxShadow('rgba(197,66,101,0.7)')
    },
    gisettMarkerCluserSmallOrange: {
        background: 'rgba(249,168,37,0.9)',
        boxShadow: boxShadow('rgba(249,168,37,0.7)')
    },
    gisettMarkerCluserMediumOrange: {
        background: 'rgba(255,111,0,0.9)',
        boxShadow: boxShadow('rgba(255,111,0,0.7)')
    },
    gisettMarkerCluserLargeOrange: {
        background: 'rgba(196,62,0,0.9)',
        boxShadow: boxShadow('rgba(196,62,0,0.7)')
    },
    gisettMarkerCluserSmallTeal: {
        background: 'rgba(79,179,191,0.9)',
        boxShadow: boxShadow('rgba(79,179,191,0.7)')
    },
    gisettMarkerCluserMediumTeal: {
        background: 'rgba(0,131,143,0.9)',
        boxShadow: boxShadow('rgba(0,131,143,0.7)')
    },
    gisettMarkerCluserLargeTeal: {
        background: 'rgba(0,86,98,0.9)',
        boxShadow: boxShadow('rgba(0,86,98,0.7)')
    }
});
function boxShadow(color) {
    return '0px 0px 0px 5px ' + color;
}
export default withStyles(styles)(GisettClusterIcon);
