import { POIIcon, PropertyIcon } from './PropertyIcon';
import GisettClusterIcon from './GisettClusterIcon';
import Leaflet from 'leaflet';
import React from 'react';
import ReactDOMServer from 'react-dom/server';

const ICON_SIZE = 30;
const POI_ICON_SIZE = 40;
//overrides the original leaflet-div-icon class, and must come from a css file.
const overrideDivIconClass = 'leafletFile';

export const POIMarkerIcon = (index, poiIconUrl, poiColor, selectedPoi) => {
    return new Leaflet.DivIcon({
        html: ReactDOMServer.renderToString(
            <POIIcon index={index} iconUrl={poiIconUrl} poiColor={poiColor} selectedPoi={selectedPoi} />
        ),
        className: overrideDivIconClass,
        iconSize: new Leaflet.Point(POI_ICON_SIZE, POI_ICON_SIZE)
    });
};

export const PropertyMarkerIcon = (index, selectedPropertyId) => {
    return new Leaflet.DivIcon({
        html: ReactDOMServer.renderToString(<PropertyIcon index={index} selectedPropertyId={selectedPropertyId} />),
        className: overrideDivIconClass,
        iconSize: new Leaflet.Point(ICON_SIZE / 2, ICON_SIZE / 2)
    });
};

// this will return a hidden marker icon
export const OverallMarkerIcon = () => {
    return new Leaflet.DivIcon({
        html: '',
        className: overrideDivIconClass
    });
};

export const PropertyIconCreateFunction = function(cluster) {
    var childCount = cluster.getChildCount();

    return new Leaflet.DivIcon({
        html: ReactDOMServer.renderToString(
            <GisettClusterIcon variant="Teal" type={clusterType(childCount)} count={childCount} />
        ),
        className: overrideDivIconClass,
        iconSize: new Leaflet.Point(ICON_SIZE, ICON_SIZE)
    });
};
export const POIIconCreateFunction = function(cluster) {
    var childCount = cluster.getChildCount();

    return new Leaflet.DivIcon({
        html: ReactDOMServer.renderToString(
            <GisettClusterIcon variant="Orange" type={clusterType(childCount)} count={childCount} />
        ),
        className: overrideDivIconClass,
        iconSize: new Leaflet.Point(ICON_SIZE, ICON_SIZE)
    });
};

function clusterType(count) {
    if (count < 10) {
        return 'Small';
    } else if (count < 100) {
        return 'Medium';
    } else {
        return 'Large';
    }
}
