import { Grid, withStyles } from '@material-ui/core';
import React, { PureComponent } from 'react';
import classNames from 'classnames';

class PropertyIconComponent extends PureComponent {
    render() {
        const { classes, index, selectedPropertyId } = this.props;
        return (
            <Grid tabIndex={index} container>
                <Grid item className={classes.propertyCircle} />
                {index === selectedPropertyId ? <Grid item className={classes.propertyCircleSelected} /> : null}
            </Grid>
        );
    }
}
class POIIconComponent extends PureComponent {
    render() {
        const { classes, index, iconUrl, poiColor, selectedPoi } = this.props;
        return (
            <Grid tabIndex={index} container>
                <img src={iconUrl} alt="" className={classes.poiIcon} />
                {index === selectedPoi ? (
                    <Grid item className={classNames(classes.poiIconSelected)} style={{ backgroundColor: poiColor }} />
                ) : null}
            </Grid>
        );
    }
}

const styles = () => ({
    propertyCircle: {
        backgroundColor: 'white',
        border: '1px solid #00505B',
        height: 10,
        width: 10,
        borderRadius: '50%',
        '&:hover': {
            backgroundColor: '#07FEFE'
        }
    },
    propertyCircleSelected: {
        border: 'none',
        borderRadius: '50%',
        height: 30,
        width: 30,
        top: -10,
        left: -10,
        backgroundColor: '#07FEFE',
        position: 'absolute',
        opacity: 0.6
    },
    poiIcon: {
        zIndex: 1
    },
    poiIconSelected: {
        borderRadius: '50%',
        height: 30,
        width: 30,
        backgroundColor: '#07FEFE',
        position: 'absolute',
        opacity: 0.6,
        top: 5,
        left: 5
    }
});

const PropertyIcon = withStyles(styles)(PropertyIconComponent);
const POIIcon = withStyles(styles)(POIIconComponent);

export { PropertyIcon, POIIcon };
