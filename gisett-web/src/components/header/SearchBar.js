import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import LocationAutocomplete from '../wizard/LocationAutocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import React from 'react';
import Search from '@material-ui/icons/Search';
import Select from '@material-ui/core/Select';
import Translator from '../translate/Translator';

const HeaderSearchGrid = props => {
    const { classes, cities, selectedCityKey, cityAction } = props;

    return (
        <MuiThemeProvider theme={muiThemeHeader}>
            <div className={classes.root}>
                <Grid container alignItems="flex-start" spacing={24}>
                    <Grid item xs={2}>
                        <Select
                            fullWidth={true}
                            value={selectedCityKey}
                            onChange={event => {
                                cityAction.selectCity(event.target.value);
                            }}
                        >
                            {cities ? (
                                cities.map(city => {
                                    return (
                                        <MenuItem value={city} key={city}>
                                            <Translator i18nKeys={'LOCATION.' + city} />
                                        </MenuItem>
                                    );
                                })
                            ) : (
                                <MenuItem value="" />
                            )}
                        </Select>
                    </Grid>
                    <Grid item xs={4}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Search />
                            </Grid>
                            <Grid item xs={9}>
                                <LocationAutocomplete selectedCityKey={selectedCityKey} classes={classes} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </MuiThemeProvider>
    );
};

HeaderSearchGrid.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = () => ({
    root: {
        flexGrow: 1
    },
    paper: {
        background: 'white',
        zIndex: 3
    }
});

const muiThemeHeader = outerTheme => ({
    ...outerTheme,
    overrides: {
        MuiInput: {
            root: {
                color: '#E0E0E0'
            },
            input: {
                color: '#E0E0E0',
                textAlign: 'start'
            },
            underline: {
                '&:before': {
                    borderBottomColor: '#E0E0E0'
                },
                '&:after': {
                    borderBottomColor: '#E0E0E0'
                },
                '&:hover:not($disabled):not($error):not($focused):before': {
                    borderBottomColor: '#E0E0E0'
                }
            }
        },
        MuiSelect: {
            select: {
                '&:-moz-focusring': {
                    textShadow: '0 0 0 #E0E0E0'
                }
            },
            icon: {
                color: '#E0E0E0'
            }
        }
    }
});

export default withStyles(styles)(HeaderSearchGrid);
