import { Link } from 'react-router-dom';
import logo from '../../resources/images/white_logo.png';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const GisettLogo = props => {
    const { classes } = props;
    return (
        <div>
            <Link
                to="/"
                onClick={event => {
                    props.action.goToMainPage();
                }}
            >
                <img src={logo} alt="Gisett" className={classes.logo} />
            </Link>
        </div>
    );
};

const styles = {
    logo: {
        height: 40,
        marginRight: 20
    }
};

export default withStyles(styles)(GisettLogo);
