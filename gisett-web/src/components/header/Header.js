import * as CityActionCreators from '../../actions/CityActionCreators';
import * as MainActionCreators from '../../actions/MainActionCreators';

import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GisettLogo from './GisettLogo';
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import SearchBar from './SearchBar';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pStage: state.wizard.stage,
        pCities: state.cities,
        pSelectedCityKey: state.cities.selectedCityKey
    };
};

const mapDispatchToProps = dispatch => {
    return {
        mainActions: bindActionCreators(MainActionCreators, dispatch),
        cityActions: bindActionCreators(CityActionCreators, dispatch)
    };
};

class Header extends Component {
    render() {
        const { classes, pStage, pCities, pSelectedCityKey } = this.props;

        return (
            <Router>
                <div>
                    <AppBar position="static">
                        <Toolbar className={classes.toolbar}>
                            <GisettLogo action={this.props.mainActions} />
                            {pStage === 4 ? (
                                <SearchBar
                                    cities={pCities.cities}
                                    selectedCityKey={pSelectedCityKey}
                                    cityAction={this.props.cityActions}
                                />
                            ) : null}
                        </Toolbar>
                    </AppBar>
                </div>
            </Router>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = theme => ({
    toolbar: {
        height: 48
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(Header));
