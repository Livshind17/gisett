import React, { Component } from 'react';
import { Trans, translate } from 'react-i18next';
import PropTypes from 'prop-types';

export class Translator extends Component {
    static propTypes = {
        i18nKeys: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]).isRequired,
        count: PropTypes.number,
        defaultMessage: PropTypes.object,
        insertValue: PropTypes.object
    };

    render() {
        return <span>{this.generateTranslasion()}</span>;
    }
    generateTranslasion() {
        const { i18nKeys, defaultMessage, count, insertValue, children } = this.props;
        if (defaultMessage || children) {
            if (typeof i18nKeys === 'string') {
                return (
                    <Trans i18nKey={i18nKeys} count={count}>
                        {defaultMessage ? defaultMessage : children}
                    </Trans>
                );
            } else {
                return (
                    <Trans i18nKey={i18nKeys[0]} count={count}>
                        {defaultMessage ? defaultMessage : children}
                    </Trans>
                );
            }
        } else {
            return this.props.t(i18nKeys, insertValue);
        }
    }
}

export default translate()(Translator);
