import { CardMedia, withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import starGreyIcon from '../../resources/images/star_grey.svg';
import starYellowIcon from '../../resources/images/star_yellow.svg';

const NR_MAX_STARS = 5;
export const STAR_SIZE = 'small';
export const STAR_SIZE_MEDIUM = 'medium';

class StarRating extends Component {
    render() {
        const { numberOfGoodStars } = this.props;
        return (
            <Grid style={{ margin: '8px' }}>
                {[...Array(numberOfGoodStars).keys()].map(i => (
                    <CardMedia key={i} image={starYellowIcon} className={this.getStarSize()} title="Stars" />
                ))}
                {[...Array(NR_MAX_STARS - numberOfGoodStars).keys()].map(i => (
                    <CardMedia key={i} image={starGreyIcon} className={this.getStarSize()} title="Stars" />
                ))}
            </Grid>
        );
    }

    getStarSize() {
        const { classes, variant } = this.props;
        if (variant) {
            return {
                [STAR_SIZE_MEDIUM]: classes.starsMedium,
                [STAR_SIZE]: classes.stars
            }[variant];
        } else {
            return classes.stars;
        }
    }
}
StarRating.propTypes = {
    classes: PropTypes.object.isRequired,
    numberOfGoodStars: PropTypes.number.isRequired
};

const styles = () => ({
    stars: {
        width: '12px',
        height: '12.13px',
        display: 'inline-block',
        margin: '1px'
    },
    starsMedium: {
        width: '15px',
        height: '15.16px',
        display: 'inline-block',
        margin: '1px'
    }
});

export default withStyles(styles)(StarRating);
