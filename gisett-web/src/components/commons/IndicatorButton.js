import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import GisettIcons from '../../resources/icons/icons';
import PropTypes from 'prop-types';
import Translator from '../translate/Translator';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

export const INDICATOR_CATEGORY_EDUCATION = 'education';
export const INDICATOR_CATEGORY_ENVIRONMENT = 'environment';
export const INDICATOR_CATEGORY_HEALTH = 'health';
export const INDICATOR_CATEGORY_SOCIO = 'socio';
export const INDICATOR_CATEGORY_RECREATION = 'recreation';
export const INDICATOR_CATEGORY_RISKS = 'risk';
export const INDICATOR_CATEGORY_TRANSPORTATION = 'transportation';
export const INDICATOR_CATEGORY_SAFETY = 'personal_safety';
export const INDICATOR_CATEGORY_DEMOGRAPHICS = 'demographics';
export const INDICATOR_CATEGORY_SHOPPING = 'shopping';

export const gisettIndicatorButtonType = new Map([
    [
        INDICATOR_CATEGORY_EDUCATION,
        {
            dragger: GisettIcons.educationDragger,
            icon: GisettIcons.educationIcon
        }
    ],
    [
        INDICATOR_CATEGORY_ENVIRONMENT,
        {
            dragger: GisettIcons.environmentDragger,
            icon: GisettIcons.environmentIcon
        }
    ],
    [
        INDICATOR_CATEGORY_HEALTH,
        {
            dragger: GisettIcons.healthDragger,
            icon: GisettIcons.healthIcon
        }
    ],
    [
        INDICATOR_CATEGORY_SOCIO,
        {
            dragger: GisettIcons.socioDragger,
            icon: GisettIcons.socioIcon
        }
    ],
    [
        INDICATOR_CATEGORY_RECREATION,
        {
            dragger: GisettIcons.recreationDragger,
            icon: GisettIcons.recreationIcon
        }
    ],
    [
        INDICATOR_CATEGORY_RISKS,
        {
            dragger: GisettIcons.riskDragger,
            icon: GisettIcons.riskIcon
        }
    ],
    [
        INDICATOR_CATEGORY_TRANSPORTATION,
        {
            dragger: GisettIcons.transportationDragger,
            icon: GisettIcons.transportationIcon
        }
    ],
    [
        INDICATOR_CATEGORY_SAFETY,
        {
            dragger: GisettIcons.safetyDragger,
            icon: GisettIcons.safetyIcon
        }
    ],
    [
        INDICATOR_CATEGORY_DEMOGRAPHICS,
        {
            dragger: GisettIcons.demographicsDragger,
            icon: GisettIcons.demographicsIcon
        }
    ],
    [
        INDICATOR_CATEGORY_SHOPPING,
        {
            dragger: GisettIcons.shoppingDragger,
            icon: GisettIcons.shoppingIcon
        }
    ]
]);

class IndicatorButton extends Component {
    getButtonStyle(type) {
        const { classes } = this.props;
        if (type) {
            let associations = {
                [INDICATOR_CATEGORY_EDUCATION]: classes.buttonEdu,
                [INDICATOR_CATEGORY_ENVIRONMENT]: classes.buttonEnv,
                [INDICATOR_CATEGORY_HEALTH]: classes.buttonHealth,
                [INDICATOR_CATEGORY_SOCIO]: classes.buttonSocio,
                [INDICATOR_CATEGORY_RECREATION]: classes.buttonRecreation,
                [INDICATOR_CATEGORY_RISKS]: classes.buttonRisks,
                [INDICATOR_CATEGORY_TRANSPORTATION]: classes.buttonTransp,
                [INDICATOR_CATEGORY_SAFETY]: classes.buttonSafety,
                [INDICATOR_CATEGORY_DEMOGRAPHICS]: classes.buttonDemographics,
                [INDICATOR_CATEGORY_SHOPPING]: classes.buttonShopping
            };
            return associations[type];
        } else {
            return classes.buttonEnv;
        }
    }

    render() {
        const { children, classes, disabled, id, ignored, onClick, selected, type, value } = this.props;
        return (
            <Button
                variant="outlined"
                id={id}
                classes={{ root: classes.root }}
                onClick={() => onClick(value)}
                className={classNames(
                    this.getButtonStyle(type),
                    {
                        [classes.buttonSelected]: selected
                    },
                    {
                        [classes.buttonIgnored]: ignored
                    },
                    {
                        [classes.buttonGrayScale]: disabled
                    }
                )}
            >
                <div className={classes.buttonContainer}>
                    <div className={classes.draggerContainer}>
                        <img src={gisettIndicatorButtonType.get(type).dragger} className={classes.drag} alt="" />
                    </div>
                    <Typography variant="caption" className={classes.label}>
                        <Translator i18nKeys={children} />
                    </Typography>
                    <img src={gisettIndicatorButtonType.get(type).icon} className={classes.icon} alt="" />
                </div>
            </Button>
        );
    }
}

IndicatorButton.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = theme => ({
    root: {
        borderRadius: 32,
        backgroundColor: theme.palette.common.white,
        padding: 4,
        margin: 4,
        height: 32,
        width: 140,
        '&:hover': {
            border: '1px solid',
            backgroundColor: theme.palette.common.white
        }
    },
    buttonSelected: {
        opacity: 0.5
    },
    buttonIgnored: {
        opacity: 0.5
    },
    buttonGrayScale: {
        filter: 'grayscale(100%)',
        opacity: 0.15
    },
    buttonEdu: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.education.main}`,
            borderColor: theme.palette.education.main
        }
    },
    buttonEnv: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.environment.main}`,
            borderColor: theme.palette.environment.main
        }
    },
    buttonSocio: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.socio.main}`,
            borderColor: theme.palette.socio.main
        }
    },
    buttonRisks: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.risks.main}`,
            borderColor: theme.palette.risks.main
        }
    },
    buttonHealth: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.health.main}`,
            borderColor: theme.palette.health.main
        }
    },
    buttonRecreation: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.recreation.main}`,
            borderColor: theme.palette.recreation.main
        }
    },
    buttonTransp: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.transportation.main}`,
            borderColor: theme.palette.transportation.main
        }
    },
    buttonSafety: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.safety.main}`,
            borderColor: theme.palette.safety.main
        }
    },
    buttonDemographics: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.demographics.main}`,
            borderColor: theme.palette.demographics.main
        }
    },
    buttonShopping: {
        '&:hover': {
            boxShadow: `0 1px 3px ${theme.palette.shopping.main}`,
            borderColor: theme.palette.shopping.main
        }
    },
    buttonContainer: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    draggerContainer: {
        width: 20
    },
    label: {
        color: theme.palette.primary.main
    },
    icon: {
        height: 20
    },
    drag: {
        height: 11
    }
});

export default withStyles(styles)(IndicatorButton);
