import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

export const GISSET_BUTTON_STYLE_1 = 'style1';
export const GISSET_BUTTON_STYLE_2 = 'style2';
export const GISSET_BUTTON_STYLE_3 = 'style3';
export const GISSET_BUTTON_STYLE_4 = 'style4';
export const GISSET_BUTTON_STYLE_5 = 'style5';
export const GISSET_BUTTON_STYLE_6 = 'style6';

class GisettButton extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Button
                variant="outlined"
                id={this.props.id}
                color={this.getButtonColor(this.props.gisStyle)}
                size={this.props.size}
                onClick={this.props.onClick}
                className={classNames(
                    classes.button,
                    this.getButtonStyle(this.props.gisStyle),
                    { [classes.filterButton]: this.props.filterButton },
                    { [classes.fixedWidth]: this.props.fixedWidth },
                    { [classes.buttonSelected]: this.props.selected }
                )}
            >
                {this.props.children}
            </Button>
        );
    }

    getButtonColor() {
        return 'secondary';
    }

    getButtonStyle(gisButtonStyle) {
        const { classes } = this.props;
        if (gisButtonStyle) {
            return {
                [GISSET_BUTTON_STYLE_1]: classes.buttonGis1,
                [GISSET_BUTTON_STYLE_2]: classes.buttonGis2,
                [GISSET_BUTTON_STYLE_3]: classes.buttonGis3,
                [GISSET_BUTTON_STYLE_4]: classes.buttonGis4,
                [GISSET_BUTTON_STYLE_5]: classes.buttonGis5,
                [GISSET_BUTTON_STYLE_6]: classes.buttonGis6
            }[gisButtonStyle];
        } else {
            return classes.buttonGis1;
        }
    }
}

GisettButton.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = theme => ({
    button: {
        margin: theme.spacing.unit
    },
    filterButton: {
        margin: 0
    },
    fixedWidth: {
        width: 180
    },
    buttonGis1: {
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.common.white
        }
    },
    buttonGis2: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.common.white
        }
    },
    buttonGis3: {
        color: theme.palette.tertiary.main,
        borderColor: theme.palette.tertiary.main,
        opacity: 0.75,
        '&:hover': {
            backgroundColor: theme.palette.tertiary.main,
            borderColor: theme.palette.tertiary.main,
            color: theme.palette.common.white,
            opacity: 1
        }
    },
    buttonGis4: {
        backgroundColor: theme.palette.tertiary.main,
        borderColor: theme.palette.tertiary.main,
        color: theme.palette.common.white,
        opacity: 0.75,
        '&:hover': {
            backgroundColor: theme.palette.tertiary.main,
            borderColor: theme.palette.tertiary.main,
            color: theme.palette.common.white,
            opacity: 1
        }
    },
    buttonGis5: {
        color: theme.palette.tertiary.main,
        backgroundColor: theme.palette.background.main,
        border: 'none',
        boxShadow: '0px 0px 5px ' + theme.palette.propertyText.main,
        borderRadius: '2px',
        '&:hover': {
            borderColor: theme.palette.tertiary.main,
            backgroundColor: theme.palette.background.main
        }
    },
    buttonGis6: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
        opacity: 0.5,
        borderRadius: '2px',
        '&:hover': {
            backgroundColor: theme.palette.secondary.main,
            color: theme.palette.common.white,
            opacity: 1
        }
    },
    buttonSelected: {
        color: theme.palette.tertiary.main,
        borderColor: theme.palette.tertiary.main,
        backgroundColor: theme.palette.common.white
    }
});

export default withStyles(styles)(GisettButton);
