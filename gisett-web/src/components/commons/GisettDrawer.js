import React, { Component } from 'react';
import { Card } from '@material-ui/core';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

export const GISETT_DRAWER_ALIGN_RIGHT = 'Right';
export const GISETT_DRAWER_ALIGN_LEFT = 'Left';

export class GisettDrawer extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.content}>
                <Card className={classNames(classes.drawerPaper, classes.drawerSize)}>{this.props.children}</Card>
            </Grid>
        );
    }
}
const styles = theme => ({
    drawerPaper: {
        display: 'inline-block',
        position: 'relative',
        backgroundColor: theme.palette.background
    },
    drawerSize: {
        width: theme.componentSize.drawer.width,
        height: theme.componentSize.drawer.height
    },
    content: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    }
});
GisettDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    children: PropTypes.object.isRequired
};
export default withStyles(styles, { withTheme: true })(GisettDrawer);
