import { Grid, Typography, withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import StarRating, { STAR_SIZE_MEDIUM } from './StarRating';

import IndicatorRating from './IndicatorRating';
import Translator from '../translate/Translator';

export const INDICATOR_RATING_TYPE_POPUP = 'popup';
export const INDICATOR_RATING_TYPE_DISPLAY = 'display';

class GeneralIndicatorRating extends Component {
    render() {
        const { classes, popup, variant } = this.props;
        return (
            <Grid
                container
                className={this.getContainertype()}
                direction="column"
                justify="space-between"
                alignItems="center"
            >
                <Grid
                    item
                    container
                    justify="space-between"
                    direction="row"
                    className={variant === 'display' ? classes.displayTitleGrid : null}
                >
                    <Typography
                        variant="body2"
                        gutterBottom
                        className={variant === 'display' ? classes.displayTitle : classes.popupTitle}
                    >
                        <Translator
                            i18nKeys={
                                variant === 'display'
                                    ? 'PROPERTY.DETAIL_VIEW.PERSONAL_OVERALL'
                                    : 'PROPERTY.DETAIL_VIEW.OVERALL'
                            }
                        />
                    </Typography>
                    <StarRating variant={STAR_SIZE_MEDIUM} numberOfGoodStars={Math.ceil(popup.value / 20)} />
                </Grid>
                {popup.allRatings ? (
                    <Grid
                        item
                        xs={12}
                        container
                        justify="space-between"
                        className={variant === 'display' ? classes.displayIndicatorsGrid : null}
                    >
                        {popup.allRatings.map((indicator, index) => {
                            return (
                                <IndicatorRating
                                    key={index}
                                    stars={Math.ceil(indicator.value / 20)}
                                    indicator={indicator.group}
                                    category={indicator.category}
                                    pclasses={this.props.classes}
                                    variant={variant}
                                />
                            );
                        })}
                    </Grid>
                ) : null}
            </Grid>
        );
    }
    getContainertype() {
        const { classes, variant } = this.props;
        if (variant) {
            return {
                [INDICATOR_RATING_TYPE_POPUP]: classes.popupContainer,
                [INDICATOR_RATING_TYPE_DISPLAY]: classes.displayContainer
            }[variant];
        } else {
            return classes.displayContainer;
        }
    }
}
const styles = theme => ({
    icon: {
        fontSize: 16,
        paddingRight: 2
    },
    popupTitle: {
        fontSize: '14px'
    },
    popupContainer: {
        width: 250
    },
    displayTitle: {
        fontSize: '16px'
    },
    displayTitleGrid: {
        backgroundColor: theme.palette.background.main,
        height: '40px',
        alignItems: 'center',
        paddingLeft: '12px',
        border: '#C6CBCE 1px',
        boxShadow: '#000000 0px 3px 6px',
        borderRadius: '4px 4px 4px 4px',
        marginBottom: '12px'
    },
    displayIndicatorsGrid: {
        backgroundColor: '#FFFFFF',
        paddingLeft: '12px',
        fontSize: '14px',
        border: '#C6CBCE 1px',
        boxShadow: '#000000 0px 3px 6px',
        borderRadius: '4px 4px 4px 4px'
    },
    displayContainer: {
        width: '100%'
    }
});

export default withStyles(styles)(GeneralIndicatorRating);
