import { Grid, MenuItem, MenuList, Typography } from '@material-ui/core';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import Translator from '../translate/Translator';

class InlineList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndexes: props.checkedRoomsTypes
        };
    }

    handleListItemClick = (index, isBedroom) => {
        const { selectedIndexes } = this.state;
        let newChecked = [...selectedIndexes];
        newChecked[index] = !newChecked[index];
        index != 0 ? (newChecked[0] = false) : newChecked.fill(false, 1);
        if (newChecked.filter(Boolean).length != 0) {
            this.setState({ selectedIndexes: newChecked });
            this.props.returnSelectedElements(isBedroom, newChecked);
        }
    };

    render() {
        const { classes, listItems, listHeader, isBedroom } = this.props;
        return (
            <MuiThemeProvider theme={muiTheme}>
                <Grid>
                    <Typography variant="title" className={classes.title}>
                        <Translator i18nKeys={listHeader} />
                    </Typography>
                    <MenuList className={classes.list} component="nav">
                        {listItems.map((element, index) => (
                            <MenuItem
                                key={index}
                                button
                                className={classes.listElement}
                                selected={this.state.selectedIndexes[index]}
                                onClick={() => this.handleListItemClick(index, isBedroom)}
                            >
                                <Typography variant="subtitle1">
                                    <Translator i18nKeys={element} />
                                </Typography>
                            </MenuItem>
                        ))}
                    </MenuList>
                </Grid>
            </MuiThemeProvider>
        );
    }
}

const styles = () => ({
    title: {
        opacity: 0.5,
        padding: 5
    },
    list: {
        display: 'flex',
        flexDirection: 'row',
        fontSize: 14
    },
    listElement: {
        borderRadius: '1px',
        border: '1px solid rgba(0, 0, 0, 0.23)',
        height: 32,
        padding: '2px 20px 2px 10px'
    }
});

const muiTheme = outerTheme => ({
    ...outerTheme,
    overrides: {
        MuiMenuItem: {
            root: {
                background: '#FFFFFF',
                opacity: 1,
                fontSize: 14,
                '&$selected': {
                    backgroundColor: '#00505B',
                    color: '#FFFFFF',
                    opacity: 1
                }
            }
        }
    }
});

export default withStyles(styles)(InlineList);
