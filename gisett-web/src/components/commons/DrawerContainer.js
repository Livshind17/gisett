import DrawerButton, {
    DRAWER_BUTTON_STYLE_LEFT,
    DRAWER_BUTTON_STYLE_LEFT_SMALL,
    DRAWER_BUTTON_STYLE_RIGHT
} from './DrawerButton';
import GisettDrawer, { GISETT_DRAWER_ALIGN_LEFT, GISETT_DRAWER_ALIGN_RIGHT } from './GisettDrawer';
import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

export const GISETT_MAIN_DRAWER_CONTAINER = {
    button: DRAWER_BUTTON_STYLE_RIGHT,
    drawer: GISETT_DRAWER_ALIGN_LEFT,
    floatingElement: true
};
const GISETT_MAIN_DRAWER_CONTAINER_RTL = {
    button: DRAWER_BUTTON_STYLE_LEFT,
    drawer: GISETT_DRAWER_ALIGN_RIGHT,
    floatingElement: true
};
export const GISETT_SECOND_DRAWER_CONTAINER = {
    button: DRAWER_BUTTON_STYLE_LEFT_SMALL,
    drawer: GISETT_DRAWER_ALIGN_RIGHT
};

export class DrawerContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerOpen: true
        };
    }

    handleDrawer = () => {
        this.setState({ drawerOpen: !this.state.drawerOpen });
    };

    render() {
        const { classes, drawerType } = this.props;
        const { drawerOpen } = this.state;
        const drawerTypeWithDirection =
            this.props.theme.direction === 'rtl' ? GISETT_MAIN_DRAWER_CONTAINER_RTL : drawerType;
        const drawer = <GisettDrawer {...classes}>{this.props.children}</GisettDrawer>;
        const button = (
            <DrawerButton
                id={1}
                gisStyle={drawerTypeWithDirection.button}
                onClick={() => {
                    this.handleDrawer();
                }}
                fixedWidth="true"
            >
                {drawerOpen}
            </DrawerButton>
        );
        return (
            <div
                className={classNames(
                    classes.container,
                    classes[`content${drawerTypeWithDirection.drawer}`],
                    {
                        [classes.floating]: this.props.floatingElement
                    },
                    {
                        [classes.contentShift]: drawerOpen,
                        [classes[`contentShift${drawerTypeWithDirection.drawer}`]]: drawerOpen
                    }
                )}
            >
                {drawer}
                {button}
            </div>
        );
    }
}

const styles = theme => ({
    container: {
        position: 'absolute',
        overflow: 'visible',
        width: theme.componentSize.drawer.width,
        display: 'flex',
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    floating: {
        zIndex: 1300,
        height: '100%'
    },
    contentLeft: {
        marginLeft: `calc(-${theme.componentSize.drawer.width})`
    },
    contentRight: {
        marginRight: `calc(-${theme.componentSize.drawer.width})`
    },

    contentShiftLeft: {
        marginLeft: 0
    },
    contentShiftRight: {
        marginRight: 0
    }
});
DrawerContainer.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(DrawerContainer);
