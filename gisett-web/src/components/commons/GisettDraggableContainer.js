import { Card, CardContent, Grid } from '@material-ui/core';

import React, { Component } from 'react';
import { Container } from 'react-smooth-dnd';

export const CONTAINER_TYPE_BASE = 'horizontal';
export const CONTAINER_TYPE_SIMPLE = 'vertical';

export const draggableElement = { RELAXED: 0, LEAVED: 1, ENTERED: 2 };

export class GisettDraggableContainer extends Component {
    constructor(props) {
        super(props);
        this.styleDnDBase = {
            flexWrap: 'wrap',
            display: 'flex',
            justifyContent: 'center'
        };
        this.styleDnDSimple = {
            width: '100%',
            height: '100%'
        };
    }
    render() {
        const { classes, getCardPayload, onCardDrop, id, variant } = this.props;
        const xs = variant === CONTAINER_TYPE_BASE ? 12 : 3;
        return (
            <Grid
                item
                xs={xs}
                container
                justify="space-around"
                direction="column"
                spacing={16}
                className={this.setBaseStyle()}
            >
                <Grid container item xs={12}>
                    <Card className={this.getComponentClass()}>
                        <CardContent classes={{ root: classes.root }}>
                            <Container
                                groupName="col"
                                behaviour={this.props.behaviour}
                                orientation={variant}
                                onDrop={e => onCardDrop(id, e)}
                                getChildPayload={index => getCardPayload(id, index)}
                                onDragEnter={() => this.props.onDragEnter(id)}
                                onDragLeave={() => this.props.onDragLeave(id)}
                                onDragEnd={e => this.props.onDragEnd(id, e)}
                                style={this.getDraggableContainerStyle()}
                            >
                                {this.props.children}
                            </Container>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        );
    }

    getComponentClass() {
        const { classes, variant } = this.props;
        if (variant) {
            return {
                [CONTAINER_TYPE_SIMPLE]: classes.gridComponent,
                [CONTAINER_TYPE_BASE]: classes.indicatorComponent
            }[variant];
        } else {
            return classes.gridComponent;
        }
    }

    setBaseStyle() {
        const { classes, variant } = this.props;
        if (variant === CONTAINER_TYPE_SIMPLE) {
            return classes.grid;
        }
        return classes.styleBase;
    }

    getDraggableContainerStyle() {
        const { variant } = this.props;
        return variant === CONTAINER_TYPE_BASE ? this.styleDnDBase : this.styleDnDSimple;
    }
}

export default GisettDraggableContainer;
