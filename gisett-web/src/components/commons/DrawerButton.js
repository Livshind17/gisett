import React, { Component } from 'react';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import classNames from 'classnames';
import { Icon } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

export const DRAWER_BUTTON_STYLE_RIGHT = 'RDefault';
export const DRAWER_BUTTON_STYLE_LEFT = 'LDefault';
export const DRAWER_BUTTON_STYLE_RIGHT_SMALL = 'RSmall';
export const DRAWER_BUTTON_STYLE_LEFT_SMALL = 'LSmall';

export class DrawerButton extends Component {
    render() {
        const { classes } = this.props;

        const icon = this.getArrowDirection(this.props.gisStyle, this.props.children);
        const smallType = this.props.gisStyle.includes('Small');

        return (
            <div
                id={this.props.id}
                onClick={this.props.onClick}
                className={classNames(
                    classes.button,
                    this.getButtonStyle(this.props.gisStyle),
                    {
                        [classes.fixedWidthHeightSmall]: this.props.fixedWidth && smallType
                    },
                    {
                        [classes.fixedWidthHeight]: this.props.fixedWidth && !smallType
                    },
                    {
                        [classes.buttonSelected]: this.props.selected
                    }
                )}
            >
                <Icon
                    className={classNames(
                        {
                            [classes.icon]: !smallType
                        },
                        {
                            [classes.iconSmall]: smallType
                        }
                    )}
                >
                    {icon}
                </Icon>
            </div>
        );
    }

    getButtonStyle(drawerButtonStyle) {
        const { classes } = this.props;
        if (drawerButtonStyle) {
            return {
                [DRAWER_BUTTON_STYLE_RIGHT]: classes.buttonDrawerRight,
                [DRAWER_BUTTON_STYLE_LEFT]: classes.buttonDrawerLeft,
                [DRAWER_BUTTON_STYLE_RIGHT_SMALL]: classes.buttonDrawerRightSmall,
                [DRAWER_BUTTON_STYLE_LEFT_SMALL]: classes.buttonDrawerLeftSmall
            }[drawerButtonStyle];
        } else {
            return classes.buttonDrawerLeft;
        }
    }

    getArrowDirection(drawerButtonStyle, isOpen) {
        if (drawerButtonStyle) {
            return {
                [DRAWER_BUTTON_STYLE_RIGHT]: isOpen ? <ChevronLeftIcon /> : <ChevronRightIcon />,
                [DRAWER_BUTTON_STYLE_LEFT]: isOpen ? <ChevronRightIcon /> : <ChevronLeftIcon />,
                [DRAWER_BUTTON_STYLE_RIGHT_SMALL]: isOpen ? <ChevronLeftIcon /> : <ChevronRightIcon />,
                [DRAWER_BUTTON_STYLE_LEFT_SMALL]: isOpen ? <ChevronRightIcon /> : <ChevronLeftIcon />
            }[drawerButtonStyle];
        } else {
            return isOpen ? <ChevronLeftIcon /> : <ChevronRightIcon />;
        }
    }
}

const styles = theme => ({
    icon: {
        display: 'inline-block',
        marginTop: theme.spacing.unit * 2
    },
    iconSmall: {
        display: 'inline-block',
        marginTop: theme.spacing.unit
    },

    button: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        marginTop: theme.spacing.unit * 2,
        zIndex: 10,
        '&:hover': {
            backgroundColor: theme.palette.secondary.main,
            color: theme.palette.common.white
        }
    },

    fixedWidthHeight: {
        width: theme.componentSize.drawerButton.large / 2,
        height: theme.componentSize.drawerButton.large
    },

    fixedWidthHeightSmall: {
        width: theme.componentSize.drawerButton.small / 2,
        height: theme.componentSize.drawerButton.small
    },

    buttonDrawerRight: {
        float: 'left',
        borderBottomRightRadius: theme.componentSize.drawerButton.large,
        borderTopRightRadius: theme.componentSize.drawerButton.large
    },

    buttonDrawerLeft: {
        float: 'right',
        borderBottomLeftRadius: theme.componentSize.drawerButton.large,
        borderTopLeftRadius: theme.componentSize.drawerButton.large
    },

    buttonDrawerRightSmall: {
        float: 'left',
        borderBottomRightRadius: theme.componentSize.drawerButton.small,
        borderTopRightRadius: theme.componentSize.drawerButton.small
    },

    buttonDrawerLeftSmall: {
        float: 'right',
        borderBottomLeftRadius: theme.componentSize.drawerButton.small,
        borderTopLeftRadius: theme.componentSize.drawerButton.small
    }
});

DrawerButton.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(DrawerButton);
