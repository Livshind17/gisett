import { Divider, Grid, Typography, withStyles } from '@material-ui/core';
import React, { PureComponent } from 'react';

import classNames from 'classnames';
import FiberManualRecord from '@material-ui/icons/FiberManualRecord';
import StarRating from './StarRating';
import Translator from '../translate/Translator';

class IndicatorRating extends PureComponent {
    render() {
        const { pclasses, classes, indicator, category, stars, variant } = this.props;
        return (
            <Grid item xs={12} direction="column" container justify="space-between">
                <Grid item container justify="space-between" alignItems="center">
                    <Typography
                        noWrap
                        variant="title"
                        gutterBottom
                        className={variant === 'display' ? classes.displaySize : classes.strictSize}
                    >
                        <FiberManualRecord
                            className={classNames(
                                pclasses.icon,
                                classes['icon' + category.charAt(0).toUpperCase() + category.slice(1)]
                            )}
                        />
                        <Translator i18nKeys={'INDICATOR-GROUPS.' + indicator.toUpperCase()} />
                    </Typography>
                    <StarRating numberOfGoodStars={stars} />
                </Grid>

                <Divider />
            </Grid>
        );
    }
}

const styles = theme => ({
    strictSize: {
        width: 160,
        '&:hover': {
            whiteSpace: 'normal'
        },
        fontSize: '11px',
        color: '#212121'
    },
    displaySize: {
        fontSize: '14px',
        color: '#212121'
    },
    iconHealth: {
        color: theme.palette.health.main
    },
    iconEnvironment: {
        color: theme.palette.environment.main
    },
    iconPersonal_safety: {
        color: theme.palette.safety.main
    },
    iconRisk: {
        color: theme.palette.risks.main
    },
    iconEducation: {
        color: theme.palette.education.main
    },
    iconRecreation: {
        color: theme.palette.recreation.main
    },
    iconTransportation: {
        color: theme.palette.transportation.main
    },
    iconSocio: {
        color: theme.palette.socio.main
    },
    iconDemographics: {
        color: theme.palette.demographics.main
    },
    iconShopping: {
        color: theme.palette.shopping.main
    }
});

export default withStyles(styles)(IndicatorRating);
