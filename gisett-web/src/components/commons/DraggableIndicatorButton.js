import React, { Component } from 'react';
import { Draggable } from 'react-smooth-dnd';
import IndicatorButton from './IndicatorButton';

export class DraggableIndicatorButton extends Component {
    // this empty function declaration needed, otherwise will throw an error, that onClick function is not defined
    ignoreClickEvent = () => {};

    render() {
        const { classes, id, type, ignored, value, disabled } = this.props;
        return (
            <Draggable style={{ height: 45 }}>
                <div className={classes.indicatorAlign}>
                    <IndicatorButton
                        id={id}
                        type={type}
                        ignored={ignored}
                        disabled={disabled}
                        onClick={this.ignoreClickEvent}
                    >
                        {value}
                    </IndicatorButton>
                </div>
            </Draggable>
        );
    }
}

export default DraggableIndicatorButton;
