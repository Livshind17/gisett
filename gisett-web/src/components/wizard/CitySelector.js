import * as CityActionCreators from '../../actions/CityActionCreators';
import GisettButton, { GISSET_BUTTON_STYLE_1 } from '../commons/GisettButton';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import imgLocationArrow from '../../resources/images/location_arrow.png';
import PropTypes from 'prop-types';
import React from 'react';
import Translator from '../translate/Translator';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pCities: state.cities,
        pSelectedCityKey: state.cities.selectedCityKey
    };
};

const mapDispatchToProps = dispatch => {
    return {
        cityActions: bindActionCreators(CityActionCreators, dispatch)
    };
};

const CitySelector = props => {
    return (
        <Grid container direction={'column'} alignItems={'center'}>
            <Typography color="secondary" gutterBottom variant="display3">
                <Translator i18nKeys="WIZARD.CHOOSE_LOCATION" />
            </Typography>
            <img src={imgLocationArrow} className={props.classes.arrow} alt="" />
            {props.pCities.cities ? (
                <div>
                    {props.pCities.cities.map(currentCity => (
                        <GisettButton
                            id={currentCity}
                            key={currentCity}
                            size="large"
                            gisStyle={GISSET_BUTTON_STYLE_1}
                            onClick={event => {
                                props.cityActions.selectCity(currentCity);
                            }}
                            selected={currentCity === props.pSelectedCityKey}
                            fixedWidth="true"
                        >
                            <Translator i18nKeys={'LOCATION.' + currentCity} />
                        </GisettButton>
                    ))}
                </div>
            ) : (
                <Translator i18nKeys="ERROR_MESSAGE.NOT_FOUND" insertValue={{ found: '$t(DATA.CITY)' }} />
            )}
        </Grid>
    );
};

CitySelector.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = () => ({
    arrow: {
        height: 16
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(CitySelector));
