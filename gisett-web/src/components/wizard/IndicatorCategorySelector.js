import * as IndicatorActionCreators from '../../actions/IndicatorActionCreators';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import IndicatorButton from '../commons/IndicatorButton';
import React from 'react';
import Translator from '../translate/Translator';

const mapStateToProps = state => {
    return {
        pCategories: state.cities.selectedCityConfig.indicatorCategories,
        pIndicators: state.indicators
    };
};

const mapDispatchToProps = dispatch => {
    return {
        indicatorActions: bindActionCreators(IndicatorActionCreators, dispatch)
    };
};

const IndicatorButtonRow = ({ buttons, onClick, selectedCategories }) => {
    if (Array.isArray(buttons)) {
        const indicatorButtons = buttons.map(button => (
            <IndicatorButton
                id={button.key}
                key={button.key}
                type={button.key}
                style={{ margin: '20px' }}
                onClick={onClick}
                value={button}
                selected={selectedCategories.includes(button)}
            >
                {'INDICATOR-CATEGORY.' + button.key.toUpperCase()}
            </IndicatorButton>
        ));
        return (
            <Grid container justify={'center'}>
                {indicatorButtons}
            </Grid>
        );
    } else {
        return null;
    }
};

class IndicatorCategorySelector extends React.Component {
    render() {
        const { pCategories, onClick, pIndicators } = this.props;
        const buttonsPerRow = 5;
        const rowCount = pCategories ? Math.ceil(pCategories.length / buttonsPerRow) : 0;
        const buttons = Array.apply(null, Array(rowCount)).map((item, i) =>
            pCategories.slice(i * buttonsPerRow, i * buttonsPerRow + buttonsPerRow)
        );
        return (
            <Grid container direction={'column'} alignItems={'center'}>
                {rowCount === 0 ? (
                    <Translator
                        i18nKeys="ERROR_MESSAGE.NOT_FOUND"
                        insertValue={{
                            found: '$t(DATA.INDICATOR_CATEGORY_plural)'
                        }}
                    />
                ) : (
                    buttons.map((buttonItem, index) => (
                        <IndicatorButtonRow
                            key={index}
                            buttons={buttonItem}
                            onClick={onClick}
                            selectedCategories={pIndicators.selectedCategories}
                        />
                    ))
                )}
            </Grid>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IndicatorCategorySelector);
