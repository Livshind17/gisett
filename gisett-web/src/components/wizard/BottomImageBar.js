import Grid from '@material-ui/core/Grid';
import imgRedHouse from '../../resources/images/red_house.png';
import React from 'react';

const BottomImageBar = props => {
    return (
        <Grid container justify={getAlignStyle(props.stage)} alignItems={'flex-end'}>
            <Grid item>
                <img src={imgRedHouse} alt="Gisett" />
            </Grid>
        </Grid>
    );
};

const getAlignStyle = stage => {
    if (stage) {
        return {
            1: 'flex-start',
            2: 'center',
            3: 'flex-end'
        }[stage];
    } else {
        return 'flex-start';
    }
};

export default BottomImageBar;
