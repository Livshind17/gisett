import * as IndicatorActionCreators from '../../actions/IndicatorActionCreators';
import * as LocationActionCreators from '../../actions/LocationActionCreators';
import * as PoiActionCreators from '../../actions/PoiActionCreators';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Downshift from 'downshift';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';

function renderInput(inputProps) {
    const { InputProps, classes, ref, ...other } = inputProps;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputRef: ref,
                classes: {
                    root: classes.inputRoot
                },
                ...InputProps
            }}
            {...other}
        />
    );
}

function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem, classes }) {
    const isHighlighted = highlightedIndex === index;
    const isSelected = selectedItem && selectedItem.name === suggestion.name;
    return (
        <MenuItem
            {...itemProps}
            key={suggestion.id}
            selected={isHighlighted}
            component="div"
            classes={{ root: classes.menuItem }}
            style={{
                fontWeight: isSelected ? 500 : 400
            }}
        >
            {suggestion.name}
        </MenuItem>
    );
}
renderSuggestion.propTypes = {
    highlightedIndex: PropTypes.number,
    index: PropTypes.number,
    itemProps: PropTypes.object,
    selectedItem: PropTypes.string,
    suggestion: PropTypes.shape({ name: PropTypes.string }).isRequired,
    classes: PropTypes.classes
};

const mapStateToProps = state => {
    return {
        locations: state.locations.locations,
        selectedLocation: state.locations.selectedLocation,
        stage: state.wizard.stage,
        indicators: state.indicators,
        distance: state.heatMaps.settings.distance,
        pois: state.pois
    };
};

const mapDispatchToProps = dispatch => {
    return {
        locationActions: bindActionCreators(LocationActionCreators, dispatch),
        indicatorActions: bindActionCreators(IndicatorActionCreators, dispatch),
        poiActions: bindActionCreators(PoiActionCreators, dispatch)
    };
};

const BaseAutocomplete = props => {
    const { classes, locations, selectedLocation, selectedCityKey } = props;
    const translate = props.t;
    return (
        <div className={classes.root}>
            <Downshift
                id="downshift-simple"
                selectedItem={selectedLocation}
                onInputValueChange={inputValue => {
                    if (inputValue === '') {
                        props.locationActions.resetLocation();
                    } else {
                        props.locationActions.searchLocation(selectedCityKey, inputValue);
                    }
                }}
                onSelect={inputValue => {
                    props.locationActions.setLocation(inputValue);
                }}
                itemToString={item => (item ? item.name : '')}
                initialInputValue={selectedLocation ? selectedLocation.name : null}
            >
                {({ getInputProps, getItemProps, isOpen, selectedItem, highlightedIndex }) => (
                    <div className={classes.container}>
                        {renderInput({
                            classes,
                            InputProps: getInputProps({
                                placeholder: translate('WIZARD.AUTO_COMPLETE_PLACEHOLDER')
                            })
                        })}
                        {isOpen ? (
                            <Paper className={classes.paper} square>
                                {locations
                                    ? locations.slice(0, 5).map((suggestion, index) =>
                                          renderSuggestion({
                                              suggestion,
                                              index,
                                              itemProps: getItemProps({
                                                  item: suggestion
                                              }),
                                              highlightedIndex,
                                              selectedItem,
                                              classes
                                          })
                                      )
                                    : null}
                            </Paper>
                        ) : null}
                    </div>
                )}
            </Downshift>
        </div>
    );
};

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    container: {
        flexGrow: 1,
        position: 'relative',
        zIndex: 1400
    },
    paper: {
        background: 'none',
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
        width: '450px'
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
    },
    inputRoot: {
        flexWrap: 'wrap',
        color: theme.palette.secondary.main
    },
    menuItem: {
        color: theme.palette.secondary.main
    }
});

BaseAutocomplete.propTypes = {
    classes: PropTypes.object.isRequired
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(translate()(BaseAutocomplete)));
