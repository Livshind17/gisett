import * as CityActionCreators from '../../actions/CityActionCreators';
import * as HeatMapActionCreators from '../../actions/HeatMapActionCreators';
import * as IndicatorActionCreators from '../../actions/IndicatorActionCreators';
import * as PoiActionCreators from '../../actions/PoiActionCreators';
import * as WizardActionCreators from '../../actions/WizardActionCreators';

import GisettButton, { GISSET_BUTTON_STYLE_1, GISSET_BUTTON_STYLE_4 } from '../commons/GisettButton';
import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import BottomImageBar from './BottomImageBar';
import CitySelector from './CitySelector';
import { connect } from 'react-redux';
import { getReferenceCoordinate } from '../../utils/commons/Helper';
import Grid from '@material-ui/core/Grid';
import IndicatorCategorySelector from './IndicatorCategorySelector';
import LocationAutocomplete from './LocationAutocomplete';
import MainView from '../main/MainView';
import PropTypes from 'prop-types';
import Translator from '../translate/Translator';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const mapStateToProps = state => {
    return {
        pCitites: state.cities,
        pSelectedCityKey: state.cities.selectedCityKey,
        pLocations: state.locations.locations,
        pStage: state.wizard.stage,
        pHeatMapSettings: state.heatMaps.settings,
        indicatorPoints: state.indicators.indicatorPoints,
        indicatorSettings: state.indicators.indicatorSettings,
        indicators: state.indicators,
        pois: state.pois
    };
};

const mapDispatchToProps = dispatch => {
    return {
        cityActions: bindActionCreators(CityActionCreators, dispatch),
        indicatorActions: bindActionCreators(IndicatorActionCreators, dispatch),
        poiActions: bindActionCreators(PoiActionCreators, dispatch),
        wizardActions: bindActionCreators(WizardActionCreators, dispatch),
        heatmapActions: bindActionCreators(HeatMapActionCreators, dispatch)
    };
};

const CitySelectorContainer = props => {
    return (
        <Grid container className={props.classes.container}>
            <Grid container direction={'column'} alignItems={'center'}>
                <Typography color="primary" style={{ textAlign: 'center' }} variant="display4">
                    <Translator
                        i18nKeys="WIZARD.TITLE"
                        defaultMessage={
                            <span>
                                Find the
                                <span className={props.classes.redTextInHeader}>perfect</span>
                                place <br /> for you
                            </span>
                        }
                    />
                </Typography>
            </Grid>
            <Grid container>
                <CitySelector cities={props.pCitites} />
            </Grid>
            {props.pStage === 1 && (
                <Grid container direction={'column'} alignItems={'center'}>
                    <Typography color="secondary" gutterBottom style={{ textAlign: 'center' }}>
                        <Translator i18nKeys="WIZARD.HELP_TEXT_1">
                            First row <br /> second help row
                        </Translator>
                    </Typography>
                    <a href="#" className={props.classes.link}>
                        <Typography color="secondary" gutterBottom variant="body2">
                            <Translator i18nKeys="WIZARD.HELP_TEXT_LINK" />
                        </Typography>
                    </a>
                </Grid>
            )}
        </Grid>
    );
};

class IndicatorSelectorContainer extends React.Component {
    render() {
        const {
            classes,
            indicatorActions,
            wizardActions,
            heatmapActions,
            indicatorPoints,
            indicatorSettings
        } = this.props;
        return (
            <Grid container className={classes.container}>
                <Grid container direction={'column'} alignItems={'center'}>
                    <Typography color="primary" variant="display2">
                        <Translator i18nKeys="WIZARD.FIND_PLACE" count={2}>
                            Choose <b>{{ number: 2 }}</b> cat
                        </Translator>
                    </Typography>
                    <Typography color="primary" variant="display1">
                        <Translator i18nKeys="WIZARD.MOST_IMPORTANT" />
                    </Typography>
                </Grid>
                <Grid container>
                    <IndicatorCategorySelector onClick={indicatorActions.selectIndicatorCategory} />
                </Grid>
                <Grid container justify={'center'}>
                    <GisettButton
                        size="large"
                        gisStyle={GISSET_BUTTON_STYLE_4}
                        onClick={() => {
                            indicatorActions.initIndicatorGroups();
                            heatmapActions.updateHeatMap(indicatorPoints, indicatorSettings);
                            wizardActions.gotoStage4();
                        }}
                    >
                        <Translator i18nKeys="BUTTON.SHOW" />
                    </GisettButton>
                    <GisettButton
                        size="large"
                        gisStyle={GISSET_BUTTON_STYLE_1}
                        onClick={() => {
                            indicatorActions.clearMustHaveCategories();
                            indicatorActions.initIndicatorGroups();
                            heatmapActions.updateHeatMap(indicatorPoints, indicatorSettings);
                            wizardActions.gotoStage4();
                        }}
                    >
                        <Translator i18nKeys="BUTTON.SKIP" />
                    </GisettButton>
                </Grid>
            </Grid>
        );
    }
}

const LocationSelectorContainer = props => {
    const { wizardActions } = props;
    return (
        <Grid container className={props.classes.gridContainerColumn}>
            <Grid container justify={'center'}>
                <Typography color="secondary" gutterBottom variant="display3">
                    <Translator i18nKeys="WIZARD.CHOOSE_CITY" />
                </Typography>
            </Grid>
            <Grid container>
                <LocationAutocomplete selectedCityKey={props.pSelectedCityKey} />
            </Grid>
            <Grid container justify={'flex-end'}>
                <GisettButton
                    size="large"
                    gisStyle={GISSET_BUTTON_STYLE_4}
                    onClick={() => {
                        wizardActions.gotoStage3();
                    }}
                >
                    <Translator i18nKeys="BUTTON.GO" />
                </GisettButton>
            </Grid>
        </Grid>
    );
};

class WizardView extends Component {
    componentDidMount() {
        let { cityActions } = this.props;
        cityActions.fetchAllCities();
    }
    render() {
        const { classes, pStage } = this.props;
        return (
            <Grid container className={classes.root}>
                {pStage === 4 ? (
                    <MainView />
                ) : (
                    <Grid container style={{ flexGrow: 1 }}>
                        <Grid container justify={'center'} className={classes.gridContainerColumn}>
                            {(() => {
                                switch (pStage) {
                                    case 1:
                                        return <CitySelectorContainer {...this.props} />;
                                    case 2:
                                        return (
                                            <Grid container className={classes.cityAndLocationContainer}>
                                                <CitySelectorContainer {...this.props} />
                                                <LocationSelectorContainer {...this.props} />
                                            </Grid>
                                        );
                                    case 3:
                                        return <IndicatorSelectorContainer {...this.props} />;
                                }
                            })()}
                        </Grid>
                        <BottomImageBar stage={this.props.pStage} />
                    </Grid>
                )}
            </Grid>
        );
    }
}

WizardView.propTypes = {
    classes: PropTypes.object.isRequired
};

const styles = theme => ({
    root: {
        flexDirection: 'column',
        alignItems: 'center',
        height: '100%'
    },
    redTextInHeader: {
        color: theme.palette.tertiary.main
    },
    link: {
        color: theme.palette.secondary.main
    },
    gridContainerColumn: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: `${theme.spacing.unit / 2}em`
    },
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexGrow: 1
    },
    cityAndLocationContainer: {
        flexDirection: 'column',
        width: 'auto',
        flexGrow: 1
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(WizardView));
