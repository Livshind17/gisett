import { createMuiTheme } from '@material-ui/core';

const colors = {
    darkGreen1: '#002732',
    darkGreen2: '#00505B',
    red2: '#C54265',
    turquoise: '#31C9B7',
    green: '#44CF6C',
    lightGreen: '#A1CE00',
    darkGreen3: '#3E7C88',
    orange: '#F6AE2D',
    pink: '#FF7BAC',
    purple: '#8A84E2',
    lightBlue: '#03A8F3',
    backgroundPrimary: '#F4F7F9',
    fontGray: '#212121',
    red: '#B6174B',
    grey: '#E0E3E5',
    lightRed: '#D36E7F',
    lightGrey: '#CDD1D4'
};

const createColor = color => ({
    main: color,
    '30': hexToRGBA(color, 0.3)
});
const hexToRGBA = (color, opacity) => {
    return (
        'rgba(' +
        parseInt(color.slice(1, 3), 16) +
        ',' +
        parseInt(color.slice(3, 5), 16) +
        ',' +
        parseInt(color.slice(5, 7), 16) +
        ',' +
        opacity +
        ')'
    );
};
const gisTheme = createMuiTheme({
    type: 'main',
    palette: {
        primary: {
            main: colors.darkGreen1,
            '5': 'rgba(0, 39, 50, 0.05)'
        },
        secondary: createColor(colors.darkGreen2),
        tertiary: createColor(colors.red2),
        background: createColor(colors.backgroundPrimary),
        health: createColor(colors.turquoise),
        environment: createColor(colors.green),
        safety: createColor(colors.lightGreen),
        risks: createColor(colors.darkGreen3),
        education: createColor(colors.orange),
        recreation: createColor(colors.pink),
        transportation: createColor(colors.purple),
        socio: createColor(colors.lightBlue),
        demographics: createColor(colors.purple),
        shopping: createColor(colors.lightBlue),
        propertyText: createColor(colors.fontGray),
        propertyButton: createColor(colors.lightRed),
        sliderBackgorund: createColor(colors.lightGrey),
        poiHeader: createColor(colors.grey),
        grid: {
            main: 'rgba(0, 80, 91, 0.12)',
            font: 'rgba(0, 80, 91, 0.5)'
        },
        price: createColor(colors.red)
    },
    typography: {
        fontFamily: 'Nunito Sans Regular',
        fontSize: 14,
        display4: {
            fontFamily: 'Bebas Neue',
            fontSize: 32,
            textTransform: 'uppercase',
            letterSpacing: 2,
            lineHeight: 1.2
        },
        display3: {
            fontFamily: 'Nunito Sans Regular',
            fontSize: 18,
            textTransform: 'uppercase',
            letterSpacing: 1
        },
        display2: {
            fontFamily: 'Bebas Neue',
            fontSize: 32,
            textTransform: 'uppercase',
            letterSpacing: 4,
            lineHeight: 1
        },
        display1: {
            fontFamily: 'Nunito Sans Regular',
            fontSize: 18,
            textTransform: 'uppercase',
            letterSpacing: 4
        },
        body2: {
            fontFamily: 'Nunito Sans Black',
            fontSize: 14
        },
        body1: {
            fontFamily: 'Nunito Sans Regular',
            fontSize: 16
        },
        title: {
            fontFamily: 'Nunito Sans Regular',
            fontSize: 14
        },
        caption: {
            fontFamily: 'Nunito Sans Bold',
            fontSize: 11,
            lineHeight: 1,
            textTransform: 'none'
        },
        button: {
            fontFamily: 'Nunito Sans Black',
            fontSize: 12,
            textTransform: 'uppercase',
            letterSpacing: 1
        },
        button2: {
            fontSize: '12px',
            fontFamily: 'Nunito Sans Regular',
            textTransform: 'uppercase'
        },
        subheading: {
            fontFamily: 'Nunito Sans Regular',
            fontSize: 18,
            color: createColor(colors.fontGray),
            opacity: 0.5
        }
    },
    componentSize: {
        drawer: {
            width: '700px',
            height: '100%'
        },
        drawerButton: {
            small: '40px',
            large: '56px'
        }
    },
    overrides: {
        MuiInput: {
            underline: {
                '&:before': {
                    borderBottomColor: colors.darkGreen2
                },
                '&:after': {
                    borderBottomColor: colors.darkGreen2
                },
                '&:hover:not($disabled):not($error):not($focused):before': {
                    borderBottomColor: colors.darkGreen2
                }
            }
        },
        MuiTabs: {
            indicator: {
                backgroundColor: colors.backgroundPrimary
            }
        },
        MuiTab: {
            label: {
                fontFamily: 'Bebas Neue',
                fontSize: 20,
                padding: 15,
                textTransform: 'uppercase',
                lineHeight: 1.2,
                letterSpacing: 1.5,
                '@media (min-width:960px)': {
                    fontSize: 18
                }
            },
            wrapper: {
                alignItems: 'start',
                textAlign: 'start'
            }
        }
    }
});

export default gisTheme;
