import * as gc from '../../utils/GisettConstants';
import bathIcon from '../images/i_bath.png';
import bedIcon from '../images/i_bad.png';
import blueDrag from '../images/drag_blue.png';
import cameraIcon from '../images/i_camera.png';
import darkGreenDrag from '../images/drag_darkgreen.png';
import demographicsIcon from '../images/c_i_demographics.png';
import eduIcon from '../images/c_i_edu.png';
import envIcon from '../images/c_i_environment.png';
import greenDrag from '../images/drag_green.png';
import healthIcon from '../images/c_i_health.png';
import lightGreenDrag from '../images/drag_lightgreen.png';
import orangeDrag from '../images/drag_orange.png';
import pinkDrag from '../images/drag_pink.png';
import poiEducation from '../images/i_edu.png';
import poiEnviroment from '../images/i_enviroment.png';
import poiFacilities from '../images/i_facilities.png';
import poiHangouts from '../images/i_hangouts.png';
import poiHealth from '../images/i_health.png';
import poiRecreation from '../images/i_recreation.png';
import poiReligion from '../images/i_religion.png';
import poiRisks from '../images/i_risks.png';
import poiSafty from '../images/i_safty.png';
import poiShopping from '../images/i_shopping.png';
import poiTransportation from '../images/i_trans.png';
import propertyPin from '../images/property_point.png';
import purpleDrag from '../images/drag_purple.png';
import recreationIcon from '../images/c_i_recreation.png';
import riskIcon from '../images/c_i_risk.png';
import safetyIcon from '../images/c_i_safety.png';
import shoppingIcon from '../images/c_i_shopping.png';
import socioIcon from '../images/c_i_socio.png';
import sqmIcon from '../images/i_sqft.png';
import transIcon from '../images/c_i_trans.png';
import turquoiseDrag from '../images/drag_turquoise.png';

const GisettIcons = {
    environmentIcon: envIcon,
    educationIcon: eduIcon,
    healthIcon: healthIcon,
    recreationIcon: recreationIcon,
    riskIcon: riskIcon,
    safetyIcon: safetyIcon,
    socioIcon: socioIcon,
    transportationIcon: transIcon,
    demographicsIcon: demographicsIcon,
    shoppingIcon: shoppingIcon,
    environmentDragger: greenDrag,
    educationDragger: orangeDrag,
    healthDragger: turquoiseDrag,
    recreationDragger: pinkDrag,
    riskDragger: darkGreenDrag,
    safetyDragger: lightGreenDrag,
    socioDragger: blueDrag,
    transportationDragger: purpleDrag,
    demographicsDragger: purpleDrag,
    shoppingDragger: blueDrag,
    bedIcon: bedIcon,
    bathIcon: bathIcon,
    sqmIcon: sqmIcon,
    cameraIcon: cameraIcon
};

export const GisettPoiIcons = new Map([
    [gc.POI.EDUCATION, poiEducation],
    [gc.POI.ENVIRONMENT, poiEnviroment],
    [gc.POI.HEALTH, poiHealth],
    [gc.POI.RECREATION, poiRecreation],
    [gc.POI.PUBLIC_SAFETY, poiSafty],
    [gc.POI.SHOPPING, poiShopping],
    [gc.POI.TRANSPORTATION, poiTransportation],
    [gc.POI.RISKS, poiRisks],
    [gc.POI.HANGOUTS, poiHangouts],
    [gc.POI.FACILITIES, poiFacilities],
    [gc.POI.RELIGION, poiReligion]
]);
export const GisettMapPin = new Map([[gc.MAP.PROPERTY_PIN, propertyPin]]);
export default GisettIcons;
