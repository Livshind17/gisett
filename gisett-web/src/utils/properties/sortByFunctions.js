import { calculateWeightedAvg, getIndicators } from '../commons/Helper';
import { PROPERTY_SORT_TYPES } from '../../utils/GisettConstants';

export function sortByTypes(propertyData, sortType, indicators) {
    calculateWeightedPropertyValues(propertyData.content, indicators.indicatorSettings);
    propertyData.content.sort((a, b) => sortedElementsOrder(a, b, sortType));
    return propertyData;
}

function sortedElementsOrder(a, b, sortType) {
    switch (sortType) {
        case PROPERTY_SORT_TYPES.BEST_LOCATION_FOR_ME:
            return b.weightedValue - a.weightedValue;
        case PROPERTY_SORT_TYPES.NEWEST:
            return b.yearBuilt - a.yearBuilt;
        case PROPERTY_SORT_TYPES.MOST_PHOTOS:
            return b.photos.lenght - a.photos.lenght;
        case PROPERTY_SORT_TYPES.PRICE_ASC:
            return a.price - b.price;
        case PROPERTY_SORT_TYPES.PRICE_DESC:
            return b.price - a.price;
        case PROPERTY_SORT_TYPES.BEDROOMS_NO:
            return b.bedrooms - a.bedrooms;
        case PROPERTY_SORT_TYPES.BATHROOMS_NO:
            return b.bathrooms - a.bathrooms;
        case PROPERTY_SORT_TYPES.SQUARE_METER:
            return b.builtSurface - a.builtSurface;
        default:
            return a.weightedValue - b.weightedValue;
    }
}

export function calculateWeightedPropertyValues(properties, indicatorSetting) {
    let indicators = {};
    getIndicators(indicators, indicatorSetting);

    properties.map(currentProperty => {
        let result = calculateWeightedAvg(
            indicators.mustHave,
            indicators.veryImportant,
            indicators.important,
            indicators.niceToHave,
            currentProperty.nearestIndicatorPoint.value
        );
        result ? (currentProperty.weightedValue = result / indicators.div) : (currentProperty.weightedValue = 0);
    });
}
