import * as CONST from '../GisettConstants';

export function getFilterObjectForBackend(filterObj) {
    const customFilterObj = {};
    let tempList = [];
    customFilterObj.category = filterObj.isSale ? 'SELL' : 'RENT';
    customFilterObj.fromPrice =
        filterObj.priceInterval[0] == CONST.PROPERTY_FILTER_TEXTS.MIN_PRICE ? null : filterObj.priceInterval[0];
    customFilterObj.toPrice =
        filterObj.priceInterval[1] == CONST.PROPERTY_FILTER_TEXTS.MAX_PRICE ? null : filterObj.priceInterval[1];
    customFilterObj.fromSurface =
        filterObj.squareMeter[0] == CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MIN ? null : filterObj.squareMeter[0];
    customFilterObj.toSurface =
        filterObj.squareMeter[1] == CONST.PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX ? null : filterObj.squareMeter[1];

    filterObj.bedCount.map((element, index) => (element ? tempList.push(index - 1) : null));
    customFilterObj.bedCount = filterObj.bedCount[0] ? null : tempList;
    tempList = [];
    filterObj.bathCount.map((element, index) => (element ? tempList.push(index) : null));
    customFilterObj.bathCount = filterObj.bathCount[0] ? null : tempList;
    tempList = [];
    filterObj.houseType.map(
        (element, index) =>
            element ? tempList.push(CONST.PROPERTY_FILTER_TEXTS.TYPES_LIST_ITEMS[index].toUpperCase()) : null
    );
    customFilterObj.houseType = filterObj.houseType[0] ? null : tempList;
    customFilterObj.amenities = convertIndexesToListValue(
        filterObj.amenities,
        CONST.PROPERTY_FILTER_TEXTS.AMENITIES_LIST_ITEMS
    );
    customFilterObj.petAllows = convertIndexesToListValue(
        filterObj.petAllows,
        CONST.PROPERTY_FILTER_TEXTS.PETS_LIST_ITEMS
    );

    customFilterObj.fromFloor = filterObj.floorInterval[0] == '' ? null : filterObj.floorInterval[0];
    customFilterObj.toFloor = filterObj.floorInterval[1] == '' ? null : filterObj.floorInterval[1];
    customFilterObj.fromYear = filterObj.builtYear[0] == '' ? null : filterObj.builtYear[0];
    customFilterObj.toYear = filterObj.builtYear[1] == '' ? null : filterObj.builtYear[1];
    return customFilterObj;
}

function convertIndexesToListValue(listIndexes, constTexts) {
    let tempList = [];
    if (listIndexes != []) listIndexes.map(element => tempList.push(constTexts[element].toUpperCase()));
    return tempList;
}
