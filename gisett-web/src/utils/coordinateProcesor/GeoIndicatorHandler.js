import { getDestination, getGeoPointModel } from './geoPointHandler';
import { stringToCoordinateInformation } from './converter';

const DISTANCE = 300;
const DIR_ENUM = { N: 0, E: 1, S: 2, W: 3 };

export function decodeBackendResponse(data, activeIndicators) {
    const { columnCount, indicatorsEncoded } = data;
    const { longitude, latitude } = data.coordinates;

    let numberOfBytes = Math.ceil(activeIndicators.length / 8);
    let string = atob(indicatorsEncoded);
    let mapping = {};
    let indicatorForPoint = [];
    let point = getGeoPointModel(longitude, latitude);
    let firstPoint;
    let count = 0;

    while (string !== '') {
        [string, mapping] = stringToCoordinateInformation(string, numberOfBytes, activeIndicators);
        indicatorForPoint.push({
            x: point.geometry.coordinates[1],
            y: point.geometry.coordinates[0],
            indicators: mapping
        });

        if (count === columnCount - 1) {
            point = getDestination(firstPoint, DIR_ENUM.S, DISTANCE);
            count = 0;
        } else {
            if (count === 0) {
                firstPoint = point;
            }
            point = getDestination(point, DIR_ENUM.E, DISTANCE);
            count++;
        }
    }
    return indicatorForPoint;
}
