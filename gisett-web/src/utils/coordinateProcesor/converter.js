// This function resolves the decoding of the compressed data about theactive indicators
// which define the value of the heatmap
// - compactString: contains the encodedString
// - activeIndicators: the list of the active indicators which are avalibe for the current
// LOCATION
// - byteCount: defines the length of the encoded substring which contains the avalible
// indicators for the resperctive COORDINATES. After slicing that substring, he converted
// binary version of that defines what indicator has value for the current coord and the
// next substring contains the actual values for each active indicators.

//This method decodes only the first pair(ind and indValues) of the string it returns the
// remained encoded string and the generated object with the indicatorName: indicatorValues.
export function stringToCoordinateInformation(compactString, byteCount, activeIndicators) {
    let asciiIndictorNumber = '';
    let ascii = '';
    let binary = '';

    [asciiIndictorNumber, compactString] = splitIn2String(compactString, byteCount);

    binary = asciiToBinary(asciiIndictorNumber);
    let numberOfOnes = binary.split('1').length - 1;

    [ascii, compactString] = splitIn2String(compactString, numberOfOnes);

    return [compactString, mappifyIndicators(binary, ascii, activeIndicators)];
}

function splitIn2String(str, number) {
    return [str.slice(0, number), str.slice(number)];
}

//the recieved indicatornumber is stored 2 or more bytes. It contains ascii chars
// Each charachters code is a decimal number which is converted into binary.
// If the converted byte does not contain 8 chars (0 or 1) then we fill the byte
// with the leading zeros.
function asciiToBinary(indicatorNumber) {
    let binary = '';
    let i = 0;
    while (i < indicatorNumber.length) {
        let byte = parseInt(indicatorNumber.charCodeAt(i++), 10).toString(2);
        binary += '0'.repeat(8 - byte.length) + byte;
    }
    return binary;
}

function mappifyIndicators(binary, ascii, activeIndicators) {
    let indicatorMap = {};
    let i,
        j = 0;
    for (i = 0; i < binary.length; i++) {
        if (binary[i] === '1') {
            indicatorMap[activeIndicators[i]] = ascii.charCodeAt(j++);
        }
    }
    return indicatorMap;
}
