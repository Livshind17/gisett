import destination from '@turf/destination';
import distance from '@turf/distance';

export function getGeoPointModel(x, y) {
    return {
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: [x, y]
        }
    };
}

export function getDistance(currentPoint, destinationPoint) {
    var options = { units: 'meters' };

    return distance(currentPoint, destinationPoint, options);
}

export function getDestination(point, direction, distance) {
    var bearing = 90 * direction;
    var options = { units: 'meters' };

    return destination(point, distance, bearing, options);
}
export const calculateCoordinateAvg = (coord1, coord2) => {
    return (Number.parseFloat(coord1) + Number.parseFloat(coord2)) / 2;
};
