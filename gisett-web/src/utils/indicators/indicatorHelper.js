import { calculateWeightedAvg, getIndicators } from '../commons/Helper';

export const indicatorStageEnum = { DISABLED: 0, BASE: 1, PANEL: 2 };

export const createIndicatorGroup = (indicatorGroup, type, stage = indicatorStageEnum.BASE) => {
    return {
        key: indicatorGroup.key,
        id: indicatorGroup.key,
        type: type,
        value: 'INDICATOR-GROUPS.' + indicatorGroup.key.toUpperCase(),
        ignored: !indicatorGroup.activeForCity,
        indicators: indicatorGroup.indicators,
        stage: stage
    };
};

export const transformIndicatorCategories = (indicatorCategories, selectedCategories, getStage) => {
    let indicators = [];
    indicatorCategories.map(cat =>
        cat.indicatorGroups.map(indGroup => {
            indicators.push(createIndicatorGroup(indGroup, cat.key, getStage(cat, selectedCategories)));
        })
    );
    return indicators;
};

export function calculateWeightedPoints(heatmap, indicatorSetting) {
    let weightedHeatmap = [];
    let indicators = {};
    getIndicators(indicators, indicatorSetting);

    heatmap.map(point => {
        let result = calculateWeightedAvg(
            indicators.mustHave,
            indicators.veryImportant,
            indicators.important,
            indicators.niceToHave,
            point.indicators
        );
        // if the result has no numeric value, then ignore from the heatmap data source.
        if (result) {
            weightedHeatmap.push({
                x: point.x,
                y: point.y,
                value: result / indicators.div
            });
        }
    });
    return weightedHeatmap;
}

export function getIndicatorsFromIndicatorGroup(indicatorGroup) {
    let indicators = [];
    indicatorGroup.map(group => group.indicators.map(ind => indicators.push(ind)));
    return indicators;
}

export function getWeightedSum(param) {
    param = Array.isArray(param) ? param : arguments;
    let sum = 0;
    let existValidValue = false;
    for (var i = 0; i < param.length; i++) {
        if (!isNaN(param[i])) {
            sum += param[i];
            existValidValue = true;
        }
    }
    return existValidValue ? sum : null;
}

export function groupByIndicatorGroupAndAvg(objectArray) {
    return objectArray.reduce((acc, obj) => {
        var lastIndex = acc.length - 1;
        if (acc[lastIndex] && acc[lastIndex].group === obj.group) {
            acc[lastIndex].value += obj.value;
            acc[lastIndex].count++;
        } else {
            acc.push({ ...obj, count: 1 });
        }
        return acc;
    }, []);
}

function findCategoryAndGroup(indicator, allIndicator) {
    let ind = allIndicator.filter(group => group.indicators !== null && group.indicators.includes(indicator))[0];
    return [ind.type, ind.id];
}

export function summerizedAndIndicatorRating(nearestPoint, state) {
    let group,
        category,
        allRatings = [];

    Object.entries(nearestPoint.value).forEach(([indicator, value]) => {
        [category, group] = findCategoryAndGroup(indicator, state.indicatorSettings.all);
        allRatings.push({
            value: value,
            group,
            category
        });
    });

    allRatings = groupByIndicatorGroupAndAvg(allRatings);
    allRatings.map(indGroup => (indGroup.value /= indGroup.count));

    let obj = {
        indicators: nearestPoint.value
    };
    let generalRating = calculateWeightedPoints([obj], state.indicatorSettings);
    return [allRatings, generalRating];
}
