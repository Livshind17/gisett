export const MAP = {
    PROPERTY_PIN: 'propertyPin'
};
export const POI = {
    EDUCATION: 'education',
    ENVIRONMENT: 'environment',
    FACILITIES: 'facilities',
    HANGOUTS: 'hangouts',
    HEALTH: 'health',
    PUBLIC_SAFETY: 'public_safety',
    RECREATION: 'recreation',
    RELIGION: 'religion',
    RISKS: 'risks',
    SHOPPING: 'shopping',
    TRANSPORTATION: 'transportation'
};

export const POI_TYPES = {
    AIRPORT_NOISE: 'airport_noise',
    ATM: 'atm',
    BAKERIES: 'bakeries',
    BARS: 'bars',
    BANK: 'bank',
    BEAUTY_SHOPS: 'beauty_shops',
    BICYCLE_PARKING: 'bicycle_parking',
    BICYCLE_RENTAL: 'bicycle_rental',
    BIKEWAYS: 'bikeways',
    BUDDHIST_TEMPLES: 'buddhist_temples',
    BUS_TREM: 'bus_trem',
    CAFE: 'cafe',
    CAR_SHARING: 'car_sharing',
    CHURCHES: 'churches',
    CLINICS: 'clinics',
    CINEMA: 'cinema',
    COLLEGES_UNIVERSITIES: 'colleges_universities',
    CONVENIENCE_STORES: 'convenience_stores',
    FAULT_ZONES: 'fault_zones',
    FAST_FOOD: 'fast_food',
    FERRY: 'ferry',
    FIRE_STATIONS: 'fire_stations',
    GAS_STATIONS: 'gas_stations',
    GOLF_COURSES: 'golf_courses',
    GREENGROCERS: 'greengrocers',
    HOSPITALS: 'hospitals',
    LAUNDRY: 'laundry',
    METRO: 'metro',
    MOSQUES: 'mosques',
    NATURAL_AREAS: 'natural_areas',
    NIGHTCLUBS: 'nightclubs',
    PARKING: 'parking',
    PARKS: 'parks',
    PHARMACIES: 'pharmacies',
    PLAYGROUNDS: 'playgrounds',
    POLICE: 'police',
    POST_OFFICE: 'post_office',
    PRIVATE_SCH: 'private_sch',
    PUBLIC_ELEMENTARY_SCH: 'public_elementary_sch',
    PUBLIC_MIDDLE_SCH: 'public_middle_sch',
    PUBLIC_HIGH_SCH: 'public_high_sch',
    PUBLIC_HEALTH: 'public_health',
    RECREATION_CENTERS: 'recreation_centers',
    RESTAURANTS: 'restaurants',
    SCHOOLS: 'schools',
    SECONDARY_SCH: 'secondary_sch',
    SHOPPING_CENTERS: 'shopping_centers',
    SPORT_CENTERS: 'sport_centers',
    SUPERMARKETS: 'supermarkets',
    SYNAGOGUES: 'synagogues',
    SWIMMING_POOLS: 'swimming_pools',
    THEATRES: 'theatres',
    TRAIN_STATIONS: 'train_stations',
    TSUNAMI: 'tsunami',
    VETERINARIES: 'veterinaries',
    VIEWPOINTS: 'viewpoints'
};

export const PROPERTY_FILTER_TEXTS = {
    SORTBY_LIST_ITEMS: [
        'Best',
        'Newest',
        'Most_Photos',
        'Price_Asc',
        'Price_Desc',
        'Bedrooms_No',
        'Bathroom_No',
        'Square_Feet'
    ],
    TYPES_LIST_ITEMS: [
        'Any',
        'House',
        'Apartment',
        'Twinhouse',
        'Condo',
        'Multi_Family',
        'Mobile_Manufactured',
        'Roommates',
        'Assisted_Living',
        'Studio_Loft',
        'Land',
        'Garden_Apartment',
        'Penthouse'
    ],
    AMENITIES_LIST_ITEMS: [
        'Air_Condition',
        'Balcony',
        'Basement',
        'BBQ_area',
        'Dishwasher',
        'Doorman',
        'Dryer',
        'Elevator',
        'Fireplace',
        'Furnished',
        'Garden',
        'Gym',
        'Heating',
        'Laundry',
        'Parking',
        'Swimming_Pool',
        'Washer',
        'Wheelchair_Access'
    ],
    PETS_LIST_ITEMS: ['PET_ALLOW_CAT', 'PET_ALLOW_DOG'],
    BEDROOM_LIST_ITEMS: ['Any', 'Studio', '1', '2', '3', '4', '5'],
    BATHROOM_LIST_ITEMS: ['Any', '1', '2', '3', '4', '5'],
    SQUARE_METER_MIN: 0,
    SQUARE_METER_MAX: 500,
    MIN_PRICE: 0,
    MAX_PRICE: 1000000,
    REQUEST_SIZE: 500,
    CURRENCY_DOLLAR: 'CURRENCY_DOLLAR'
};

// --------- Actions and API request/response types ---------------------------
// C
export const CLEAR_MUST_HAVE_INDICATORS = 'CLEAR_MUST_HAVE_INDICATORS';
// F
export const FETCH_ALL_ENCODED_INDICATORS_REQUESTED = 'FETCH_ALL_ENCODED_INDICATORS_REQUESTED';
export const FETCH_ALL_ENCODED_INDICATOR_FULLFILLED = 'FETCH_ALL_ENCODED_INDICATOR_FULLFILLED';
export const FETCH_ALL_ENCODED_INDICATOR_REJECTED = 'FETCH_ALL_ENCODED_INDICATOR_REJECTED';
export const FETCH_ALL_CITIES_REQUESTED = 'FETCH_ALL_CITIES_REQUESTED';
export const FETCH_ALL_CITIES_FULLFILLED = 'FETCH_ALL_CITIES_FULLFILLED';
export const FETCH_ALL_CITIES_REJECTED = 'FETCH_ALL_CITIES_REJECTED';
export const FETCH_ALL_INDICATORS_NEAR_BY_POINT_FULLFILLED = 'FETCH_ALL_INDICATORS_NEAR_BY_POINT_FULLFILLED';
export const FETCH_ALL_INDICATORS_NEAR_BY_POINT_REJECTED = 'FETCH_ALL_INDICATORS_NEAR_BY_POINT_REJECTED';
export const FETCH_ALL_POIS_REQUESTED = 'FETCH_ALL_POIS_REQUESTED';
export const FETCH_ALL_POIS_FULLFILLED = 'FETCH_ALL_POIS_FULLFILLED';
export const FETCH_ALL_POIS_REJECTED = 'FETCH_ALL_POIS_REJECTED';
export const FETCH_ALL_PROPERTIES_REQUESTED = 'FETCH_ALL_PROPERTIES_REQUESTED';
export const FETCH_ALL_PROPERTIES_FULLFILLED = 'FETCH_ALL_PROPERTIES_FULLFILLED';
export const FETCH_ALL_PROPERTIES_REJECTED = 'FETCH_ALL_PROPERTIES_REJECTED';
export const FETCH_CITY_CONFIGURATIONS_FULLFILLED = 'FETCH_CITY_CONFIGURATIONS_FULLFILLED';
export const FETCH_CITY_CONFIGURATIONS_REJECTED = 'FETCH_CITY_CONFIGURATIONS_REJECTED';
export const FETCH_LOCATIONS_FULLFILLED = 'FETCH_LOCATIONS_FULLFILLED';
export const FETCH_LOCATIONS_REJECTED = 'FETCH_LOCATIONS_REJECTED';
export const FETCH_NEAREST_INDICATOR_FULLFILLED = 'FETCH_NEAREST_INDICATOR_FULLFILLED';
export const FETCH_NEAREST_INDICATOR_REJECTED = 'FETCH_NEAREST_INDICATOR_REJECTED';
export const FETCH_NEAREST_INDICATOR_REMOVED = 'FETCH_NEAREST_INDICATOR_REMOVED';
export const FETCH_NEAREST_INDICATOR_REQUESTED = 'FETCH_NEAREST_INDICATOR_REQUESTED';
export const FILTER_ALL_PROPERTY_REQUESTED = 'FILTER_ALL_PROPERTY_REQUESTED';
export const FILTER_PROPERTY_OBJECT = {
    isSale: false,
    priceInterval: [PROPERTY_FILTER_TEXTS.MIN_PRICE, PROPERTY_FILTER_TEXTS.MAX_PRICE],
    squareMeter: [
        PROPERTY_FILTER_TEXTS.SQUARE_METER_MIN,
        PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX,
        PROPERTY_FILTER_TEXTS.SQUARE_METER_MAX
    ],
    amenities: [],
    petAllows: [],
    floorInterval: [],
    builtYear: [],
    bedCount: [true],
    bathCount: [true],
    houseType: [true]
};
// G
export const GO_TO_MAIN_PAGE = 'GO_TO_MAIN_PAGE';
export const GO_TO_STAGE_2 = 'GO_TO_STAGE_2';
export const GO_TO_STAGE_3 = 'GO_TO_STAGE_3';
export const GO_TO_STAGE_4 = 'GO_TO_STAGE_4';
// H
export const HIDE_PROPERTY_DETAILS = 'HIDE_PROPERTY_DETAILS';
//I
export const INIT_INDICATOR_GROUPS = 'INIT_INDICATOR_GROUPS';
//P
export const PROPERTY_SORT_TYPES = {
    BEST_LOCATION_FOR_ME: 0,
    NEWEST: 1,
    MOST_PHOTOS: 2,
    PRICE_ASC: 3,
    PRICE_DESC: 4,
    BEDROOMS_NO: 5,
    BATHROOMS_NO: 6,
    SQUARE_METER: 7
};
// R
export const REFRESH_MAP_LAYERS = 'REFRESH_MAP_LAYERS';
export const RESET_CITY = 'RESET_CITY';
export const RESET_LOCATION = 'RESET_LOCATION';
export const RESET_STAGE = 'RESET_STAGE';
// S
export const SEARCH_LOCATION_BY_NAME_LIKE = 'SEARCH_LOCATION_BY_NAME_LIKE';
export const SELECT_MUST_HAVE_INDICATOR_CATEGORY = 'SELECT_MUST_HAVE_INDICATOR_CATEGORY';
export const SELECT_CITY = 'SELECT_CITY';
export const SET_LOCATION = 'SET_LOCATION';
export const SET_LOCATION_AND_REFRESH_MAP_LAYERS = 'SET_LOCATION_AND_REFRESH_MAP_LAYERS';
export const SHOW_PROPERTY_DETAILS = 'SHOW_PROPERTY_DETAILS';
// T
export const TRANSFORM_MUST_HAVE_INDICATOR_CATEGORIES = 'TRANSFORM_MUST_HAVE_INDICATOR_CATEGORIES';

// U
export const UPDATE_HEATMAP = 'UPDATE_HEATMAP';
export const UPDATE_INDICATOR_GROUP_IMPORTANCE = 'UPDATE_INDICATOR_GROUP_IMPORTANCE';
