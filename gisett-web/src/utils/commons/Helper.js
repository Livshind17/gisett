import { getIndicatorsFromIndicatorGroup, getWeightedSum } from '../indicators/indicatorHelper';
import MathAvg from 'math-avg';

export const calculateCoordinateAvg = (coordNW, coordSE) => {
    return [MathAvg(coordNW.longitude, coordSE.longitude), MathAvg(coordNW.latitude, coordSE.latitude)];
};

export const stringifyCoordinateForAPI = referenceCoordinate => {
    return 'lng=' + referenceCoordinate[1] + '&lat=' + referenceCoordinate[0];
};

// The reference coordinate will be an array whith the following values and order, [latitude, longitued]
export const getReferenceCoordinate = (cityCenterCoordinate, locationCoordinate) => {
    return locationCoordinate && locationCoordinate.length === 2
        ? [locationCoordinate[1], locationCoordinate[0]]
        : [cityCenterCoordinate[1], cityCenterCoordinate[0]];
};

export function getIndicators(indicators, indicatorSetting) {
    indicators.mustHave = getIndicatorsFromIndicatorGroup(indicatorSetting.mustHave);
    indicators.veryImportant = getIndicatorsFromIndicatorGroup(indicatorSetting.veryImportant);
    indicators.important = getIndicatorsFromIndicatorGroup(indicatorSetting.important);
    indicators.niceToHave = getIndicatorsFromIndicatorGroup(indicatorSetting.niceToHave);
    indicators.div =
        25 *
        ((indicators.mustHave.length > 0 ? 4 : 0) +
            (indicators.veryImportant.length > 0 ? 3 : 0) +
            (indicators.important.length > 0 ? 2 : 0) +
            (indicators.niceToHave.length > 0 ? 1 : 0));
}

export function calculateWeightedAvg(mustHave, veryImportant, important, niceToHave, currentIndicator) {
    let a = getWeightedAvg(mustHave, currentIndicator, 100);
    let b = getWeightedAvg(veryImportant, currentIndicator, 75);
    let c = getWeightedAvg(important, currentIndicator, 50);
    let d = getWeightedAvg(niceToHave, currentIndicator, 25);
    return getWeightedSum(a, b, c, d);
}

function getWeightedAvg(indicators, point, weight) {
    return MathAvg(indicators.filter(i => point[i]).map(i => point[i])) * weight;
}
