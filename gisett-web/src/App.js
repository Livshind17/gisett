import { applyMiddleware, compose, createStore } from 'redux';
import React, { Component } from 'react';

import allReducers from './app_state/reducer';
import backgroundImage from './resources/images/background.png';
import createSagaMiddleware from 'redux-saga';
import gisTheme from './resources/theme';
import Grid from '@material-ui/core/Grid';
import Header from './components/header/Header';
import logger from 'redux-logger';
import { MuiThemeProvider } from '@material-ui/core';
import { Provider } from 'react-redux';
import saga from './app_state/sagas/saga';
import { translate } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import WizardView from './components/wizard/WizardView';

const sagaMiddleware = createSagaMiddleware();
const middleware = applyMiddleware(logger, sagaMiddleware);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(allReducers, composeEnhancers(middleware));

sagaMiddleware.run(saga);
class App extends Component {
    constructor(props) {
        super(props);
        gisTheme.direction = props.i18n.dir();
        this.state = {
            theme: gisTheme
        };
    }
    onLanguageChange = direction => {
        const theme = { ...this.state.theme };
        theme.direction = direction;
        this.setState({
            theme
        });
    };

    render() {
        const { classes } = this.props;
        const { theme } = this.state;
        return (
            <MuiThemeProvider theme={theme}>
                <div dir={theme.direction}>
                    <Provider store={store}>
                        <Grid container className={classes.app}>
                            <Grid item xs={12} className={classes.header}>
                                <Header />
                            </Grid>
                            <Grid item xs={12} className={classes.bodyContent}>
                                <WizardView />
                            </Grid>
                        </Grid>
                    </Provider>
                </div>
            </MuiThemeProvider>
        );
    }
}

const styles = () => ({
    app: {
        height: '100vh',
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: 'cover'
    },
    header: {
        height: '64px'
    },
    bodyContent: {
        height: 'calc(100vh - 64px)'
    }
});

export default withStyles(styles)(translate()(App));
