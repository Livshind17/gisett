module.exports = {
    env: {
        browser: true,
        node: true,
        es6: true,
        mocha: true
    },
    extends: ['react-app', 'plugin:react/recommended', 'prettier', 'prettier/react'],
    settings: {
        react: {
            createClass: 'createReactClass',
            pragma: 'React',
            version: '16.4',
            flowVersion: '0.53'
        },
        propWrapperFunctions: ['forbidExtraProps']
    },
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
            modules: true
        }
    },
    rules: {
        // NOTE: Please take a look this site (https://eslint.org/docs/rules/indent) if you will notice any indentation error
        indent: ['error', 4, { SwitchCase: 1, ignoredNodes: ['ConditionalExpression'] }],
        quotes: ['error', 'single'],
        'jsx-quotes': ['error', 'prefer-double'],
        semi: ['error', 'always'],
        'react/prop-types': [0],
        'react/jsx-key': [0],
        'no-duplicate-imports': ['error'],
        'sort-imports': [
            'error',
            {
                ignoreCase: true,
                ignoreMemberSort: false,
                memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single']
            }
        ],
        'no-console': ['warn'],
        'prettier/prettier': [
            'error',
            {
                trailingComma: 'none',
                singleQuote: true,
                printWidth: 120,
                tabWidth: 4,
                semi: true
            }
        ]
    },
    plugins: ['prettier']
};
